class ImageReader {
  constructor(options) {
    this.img = $(options.img);
    this.input = $(options.input);

  }

  static init(options) {
    return new ImageReader(options)
  }

  commit() {
    var me = this;

    me
      .img
      .click(function () {
        me.input.click(); // trigger the input file to start browsing
      })
      .hover(function () {
        me.img.css({'cursor': 'pointer'})
      })

    me.input
      .change(function () {
        readURL(me, this)
      })

    function readURL(me, input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            me.img.attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
    }
  }
}

$.fn.tinyGallery = function(options)
{
 	// everything is possible
 	var me = $(this);
 	me.body = me.find('.modal-body');
 	// pertama setting judul
 	// set judul
 	me.find('h4.modal-title').html(options.title);
 	// terus kirim ajak ke url
 	var dom = ''
		+'<div class="tabbable">'
			+'<ul class="nav nav-tabs" id="myTab">'
			    +'<li class="active"><a href="#tab-1" data-toggle="tab">Upload</a>'
			    +'</li>'
			    +'<li class=""><a href="#tab-2" data-toggle="tab">From url</a>'
			    +'</li>'
			+'</ul>'
		+'<div class="tab-content" style="margin-top: 20px">'
		    +'<div class="tab-pane fade active in" id="tab-1" style="padding-left: 50px;padding-right: 50px">'
		    	+'<img id="img-from-upload" src="http://nemanjakovacevic.net/wp-content/uploads/2013/07/placeholder.png" style="width: 100%">'
		    	+'<input type="file" style="display: none">'
		    	+'<div class="btn-group" style="margin-top: 20px">'
		    		+'<button class="btn-upload btn btn-primary"><i class="fa fa-upload"></i></button>'
		    		+'<button class="btn-select-from-upload btn btn-primary" disabled>Select</button>'
		    	+'</div>'
		    +'</div>'
		    +'<div class="tab-pane fade in" id="tab-2">'
		    	+'<div>'
    				+'<p class="loading-img" style="display:none">Loading image..</p>'
		    		+'<img id="img-from-url" style="width:100%">'
		    	+'</div>'
	    		+'<div class="row">'
	    			+'<div class="col-md-10">'
	    				+'<input class="form-control" name="url-img" placeholder="paste your img url here">'
	    			+'</div>'
	    			+'<div class="col-md-2">'
	    				+'<button class="btn btn-primary select-from-url">Select</button>'
	    			+'</div>'
	    		+'</div>'
		    +'</div>'
		+'</div>';

	me.body.html(dom);

 	// show me as the modal
 	me.modal();
 }
