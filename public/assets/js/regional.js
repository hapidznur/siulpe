/**
* kelas ini digunakan untuk memanipulasi tag <option></option> pada input <select>
*/
class RegionalDomManagemen {

  constructor(dom, type, selectAll) {
    this.dom = dom;
    this.type = type;
    this.selectAll = selectAll;
  }

  static dom(dom, type, selectAll) {
    return new RegionalDomManagemen(dom, type, selectAll);
  }

  loading() {
    $(this.dom)
      .html(
        '<option value="semua">Loading ' + this.type + '...</option>'
      )
    return this;
  }

  render(list, selectedItem) {
    var key = '';
    var value = '';

    switch (this.type) {
      case 'provinsi':
        key = 'id_provinsi';
        value = 'nama_provinsi';
        break;
      case 'kota':
        key = 'id_kabupaten';
        value = 'nama_kabupaten/kota';
        break;
      case 'kecamatan':
        key = "id_kecamatan";
        value = "nama_kecamatan";
        break;
      default:

    }

    var lists = '';


    if (list) {
      if (this.selectAll) {
        lists += '<option value="semua">Pilih semua</option>'
      }
      $.each(list, function (index, item) {
        var selected = selectedItem == item[value] ? 'selected' : '';
        lists += '<option item-id="' + item[key] + '" ' + selected + '>' + item[value] + '</option>'
      });
    }
    else {
      lists += '<option value="">Tidak ada ' + this.type + '</option>';
    }

    $(this.dom).html(lists);
  }
}

class Regional {
  constructor() {
    // just do nothing.
  }

  static init() {
    return new Regional();
  }

  provinsi(selector, selectedProvinsi, selectAll) {
    this.provinsiDom = $(selector);
    this.selectedProvinsi = selectedProvinsi;
    this.provinsiSelectAll = selectAll;
    var me = this;
    this.provinsiDom.change(function (e) {
      e.preventDefault();
      me.grabKota();
    });
    return this;
  }

  kota(selector, selectedKota, selectAll) {
    this.kotaDom = $(selector);
    this.selectedKota = selectedKota;
    this.kotaSelectAll = selectAll;
    var me = this;
    this.kotaDom.change(function (e) {
      e.preventDefault();
      me.grabKecamatan();
    })
    return this;
  }

  kecamatan(selector, selectedKecamatan) {
    this.kecamatanDom = $(selector);
    this.selectedKecamatan = selectedKecamatan;
    return this;
  }

  grab() {
    var provinsiDom = this.provinsiDom;
    var kotaDom = this.kotaDom;
    var kecamatanDom = this.kecamatanDom;
    var manageProvinsiDom = RegionalDomManagemen.dom(provinsiDom, 'provinsi', this.provinsiSelectAll);
    var manageKotaDom = RegionalDomManagemen.dom(kotaDom, 'kota', this.kotaSelectAll);
    var manageKecamatanDom = RegionalDomManagemen.dom(kecamatanDom, 'kecamatan');
    var me = this;
    /**
    * this is where the logic should perform
    * create an ajax request to grab the API of province
    */
    var opt = {};
    opt.url = 'http://saosrajarasa.com/api/getProvinsi.php';
    opt.beforeSend = function () {
      manageProvinsiDom.loading();
      manageKotaDom.loading();
      manageKecamatanDom.loading();
    }
    opt.success = function (response) {
      manageProvinsiDom.render(response, me.selectedProvinsi);
      me.grabKota(); // this will automatically grab the kota and kecamatan (if exists)
    }
    $.ajax(opt);

    return this;
  }

  grabKota() {
    var provinsiId = this.provinsiDom.find('option:selected').attr('item-id');
    var kotaDom = this.kotaDom;
    var manageKotaDom = RegionalDomManagemen.dom(kotaDom, 'kota', this.kotaSelectAll);
    var manageKecamatanDom = RegionalDomManagemen.dom(this.kecamatanDom, 'kecamatan');
    var me = this;

    /**
    * call ajax to get the provinsi id
    */
    var opt = {}
    opt.url = "http://saosrajarasa.com/api/getKota.php";
    opt.data = {id_provinsi: provinsiId};
    opt.beforeSend = function () {
      manageKotaDom.loading();
      manageKecamatanDom.loading();
    }
    opt.success = function (response) {
      manageKotaDom.render(response, me.selectedKota);
      me.grabKecamatan();
    }
    $.ajax(opt);
  }

  grabKecamatan() {
    var kabupatenId = this.kotaDom.find('option:selected').attr('item-id');
    var kecamatanDom = this.kecamatanDom;
    var manageKecamatanDom = RegionalDomManagemen.dom(kecamatanDom, 'kecamatan');
    var me = this;

    /**
    * call ajax to get the provinsi id if the kecamatan fields exist
    */
    var opt = {}
    opt.url = "http://saosrajarasa.com/api/getKecamatan.php";
    opt.data = {id_kabupaten: kabupatenId};
    opt.beforeSend = function () {
      manageKecamatanDom.loading();
    }
    opt.success = function (response) {
      manageKecamatanDom.render(response, me.selectedKecamatan);
    }

    if (kecamatanDom) {
      $.ajax(opt);
    }
  }

  setProvinsi(provinsi) {
    this.selectedProvinsi = provinsi;
    this.provinsiDom.val(provinsi);
    this.provinsiDom.change();
  }

  setKota(kota) {
    this.selectedKota = kota;
    this.kotaDom.val(kota);
    this.kotaDom.change();
  }

  setKecamatan(kecamatan) {
    this.selectedKecamatan = kecamatan;
    this.kecamatanDom.val(kecamatan);
  }

}
