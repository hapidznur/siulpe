(function () {
  $('[name=jumlah_peserta]').change(tampilkanJumlahPeserta);

  function tampilkanJumlahPeserta() {
    var panjangLama = $('#kontainer-peserta .form-group').length;
    var panjangBaru = $(this).val();

    if (panjangBaru > panjangLama) {
      // tambahkan
      for (var i = 0; i < panjangBaru - panjangLama; i++) {
        $('#kontainer-peserta').append(
          '<div class="form-group">'+
            '<label>Nama Peserta</label>'+
            '<input name="nama_peserta[]" class="form-control" placeholder="Nama Peserta" required>'+
          '</div>'
        );
      }
    }
    else {
      // hapus sebanyak selisihnya
      var formGroups = $('#kontainer-peserta .form-group');

      for (var i = formGroups.length - 1; i > formGroups.length - 1 - (panjangLama - panjangBaru); i--) {
        $(formGroups[i]).remove();
      }
    }
  }
})()
