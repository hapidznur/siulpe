(function () {

  var modalDelete = $('#modal-delete')

  addListeners()

  function addListeners() {
    $('body').on('click', '.btn-delete', onBtnDelete)
  }

  function onBtnDelete() {
    var ads_id = $(this).parent().parent().parent().attr('ads-id')
    $('[name=ads_id]').val(ads_id)
    modalDelete.modal('show')
  }
})()
