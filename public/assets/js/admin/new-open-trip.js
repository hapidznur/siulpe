$(document).ready(function () {
  'use strict';

  var btnPilihMasterTrip = $('.btn-pilih-master-trip');
  var modalPilihMasterTrip = $('.modal.pilih-master-trip');
  var formOpenTrip = $('form#open-trip');
  var selectedMasterTripId = null;
  var editOpenTrip = formOpenTrip.attr('edit-open-trip') ? true : false;
  var btnTambahGambar = $('.btn-tambah-gambar');
  var keyword = '';


  if (editOpenTrip) {
    selectedMasterTripId = formOpenTrip.attr('master-trip-id');
  }

  // add listeners
  addListeners();

  function onBtnPilihMasterTripClick() {
    // ambil data maser trip
    var opt = {}
    opt.url = '/admin/master-trip';
    opt.data = {
      q: keyword
    }
    opt.method = 'get';
    opt.beforeSend = function () {
      modalPilihMasterTrip
        .find('.list-group.master-trip')
        .html(
          "<li class=\"list-group-item\">"+
            "<a href=\"#\">"+
              "<b>Loading...</b>"+
              "<br>"+
              "<small>Loading...</small>"+
            "</a>"+
          "</li>"+
          "<li class=\"list-group-item\">"+
            "<a href=\"#\">"+
              "<b>Loading...</b>"+
              "<br>"+
              "<small>Loading...</small>"+
            "</a>"+
          "</li>"+
          "<li class=\"list-group-item\">"+
            "<a href=\"#\">"+
              "<b>Loading...</b>"+
              "<br>"+
              "<small>Loading...</small>"+
            "</a>"+
          "</li>"
        )

      modalPilihMasterTrip.modal('show')
    }
    opt.success = function (response) {
      modalPilihMasterTrip.find('.list-group.master-trip').html('');
      $.each(response.masterTrips, function (index, item) {
        modalPilihMasterTrip
          .find('.list-group.master-trip')
          .append(
            "<li class=\"list-group-item master-trip\">"+
              "<a href=\"#\" master-trip-id=\"" + item.id + "\">"+
                "<b>" + item.title + "</b>"+
                "<br>"+
                "<small>" + item.destinations_lists + "</small>"+
              "</a>"+
            "</li>"
          )
      })
    }
    $.ajax(opt);
  }

  function onMasterTripSelected() {
    var me = $(this);
    var title = me.find('a b').text();
    var destination = me.find('a small').text();

    btnPilihMasterTrip.html(
      '<p>' +
        '<b>' + title + '</b><br>' +
        '<small>' + destination + '</small>' +
      '</p>'
    );

    selectedMasterTripId = me.find('a').attr('master-trip-id');
    // hidupkan tombol
    $('.buttonNext').removeAttr('disabled');
    modalPilihMasterTrip.modal('hide');
  }

  function onBtnSubmitClick() {
    $('textarea[name=price_details]').val(tinyMCE.activeEditor.getContent());
    if (!isTheFormCompleted()) {
      alert("isian belum terpenuhi");
    }
    else {
      // kirim koneksi ajax
      var data = new FormData(document.getElementById('open-trip'));
      var opt = {}
      if (editOpenTrip) {
        opt.url = '/admin/edit-open-trip-post/' + formOpenTrip.attr('open-trip-id');
        data.append('master_trip_id', selectedMasterTripId);
      }
      else {
        opt.url = '/admin/new-open-trip-post/' + selectedMasterTripId;
      }
      opt.data = data
      opt.processData = false;
      opt.contentType = false;
      opt.method = 'post';
      opt.beforeSend = function () {
        $(".input").attr('disabled', '');
        $(".buttonFinish").text("Loading...");
      }
      opt.success = function (response) {
        if (response.errors) {
          return;
        }

        window.location = '/admin/open-trip';
      }
      opt.complete = function () {
        $(".input").removeAttr('disabled')
      }
      $.ajax(opt)
    }
  }

  function isTheFormCompleted() {
    if (!selectedMasterTripId) {
      console.log("inputan ke 1");
      return false;
    }

    if (!cekInputan('#step-2')) {
      console.log("inputan ke 2");
      return false;
    }
    if (!cekInputan('#step-3')) {
      console.log("inputan ke 3");
      return false;
    }

    function cekInputan(idStep) {
      var elements = $(idStep).find('.input')
      for (var i = 0; i < elements.length; i++) {
        if (!$(elements[i]).val()) {
          return false;
        }
      }
      return true;
    }

    return true;
  }

  function onBtnTambahGambarClick() {
    var me = $(this);
    var currentIndeks = $('.open-trip-cover').length;

    if (currentIndeks == 5) {
      return alert("Maksimal 5");
    }

    me.parent().parent().append(
      '<div class="col-md-4">'+
        '<div style="margin-top: 5px; margin-bottom: 5px">'+
          '<div class="open-trip-cover">'+
            '<button class="btn btn-danger btn-close" type="button"><span class="fa fa-close"></span></button>'+
            '<img class="open-trip-cover-' + currentIndeks + '" src="http://nemanjakovacevic.net/wp-content/uploads/2013/07/placeholder.png" alt="Cover" style="width: 100%; height: auto" />'+
          '</div>'+
          '<input id="open-trip-cover-' + currentIndeks + '" type="file" name="cover[]" form="open-trip" style="display: none">'+
        '</div>'+
      '</div>'
    );

    ImageReader
    .init({
      'img': $('.open-trip-cover-' + currentIndeks),
      'input': $('#open-trip-cover-' + currentIndeks)
    })
    .commit();
  }

  function onBtnDeleteCoverClick() {
    // jika ini halaman edit, dan tombol ini ada gambarnya, maka kirim ajax untuk hapus gambar
    if (editOpenTrip && $(this).parent().attr('cover-id')) {
      var options = {}
      options.url = '/admin/open-trip-delete-cover/' + $(this).parent().attr('cover-id')
      options.method = 'post';
      $.ajax(options);
    }
    $(this).parent().parent().parent().remove();
  }

  function addListeners() {
    btnPilihMasterTrip.click(onBtnPilihMasterTripClick);
    $('.buttonFinish').click(onBtnSubmitClick);

    $('body').on('click', '.list-group-item.master-trip', onMasterTripSelected);

    btnTambahGambar.click(onBtnTambahGambarClick);
    for (var i = 1; i <= $('.open-trip-cover').length; i++) {
      ImageReader
      .init({
        'img': $('.open-trip-cover-' + i),
        'input': $('#open-trip-cover-' + i)
      })
      .commit();
    }

    $('[name=q]').keyup(onSearchMasterTrip);
    $('body').on('click', '.open-trip-cover .btn-close', onBtnDeleteCoverClick);
  }

  function onSearchMasterTrip() {
    keyword = $(this).val();
    onBtnPilihMasterTripClick();
  }
})
