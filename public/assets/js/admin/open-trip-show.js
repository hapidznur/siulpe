(function () {

  var modalDeleteOpenTrip = $('#modal-delete-open-trip');
  var modalDeleteBooking = $('#modal-delete-booking');
  var formDeleteOpenTrip = $('#modal-delete-open-trip form');
  var formDeleteBooking = $('#modal-delete-booking form');
  addListeners();

  function onBtnDeleteClick() {
    modalDeleteOpenTrip.modal('show');
  }

  function onBtnDeleteBookingClick() {
    var id = $(this).parent().parent().parent().attr('booking-id');
    formDeleteBooking.find('input[name=booking_id]').val(id);
    modalDeleteBooking.modal('show')
  }

  function addListeners() {
    $('body').on('click', '#btn-delete-open-trip', onBtnDeleteClick)
    $('body').on('click', '.btn-delete-booking', onBtnDeleteBookingClick)
  }
})()
