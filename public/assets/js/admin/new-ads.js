(function () {

  var inputJenis = $('select[name=jenis]')
  var form = $('form#form-new-ads')

  addListeners();


  function addListeners() {
    inputJenis.change(onJenisChange)
    form.submit(onFormSubmit)

    if (inputJenis.val() == 'script' || inputJenis.val() == 'gambar') {
      inputJenis.change()
    }

    $('body').on('change', '#input-gambar input', loadImage);
    $('body').on('keyup', '#input-gambar input', loadImage);
  }

  function onFormSubmit() {
    var jenis = inputJenis.val();

    if (jenis === 'gambar') {
      $('#input-script').remove()
    }
    else {
      $('#input-gambar').remove();
    }
  }

  function onJenisChange() {
    var jenis = $(this).val();

    $('#input-gambar').hide()
    $('#input-script').hide()

    if (jenis === 'gambar') {
      $('#input-gambar').show()
    }
    else {
      $('#input-script').show()
    }
  }

  function loadImage() {
    $('#img').attr('src', $(this).val());
  }
})()
