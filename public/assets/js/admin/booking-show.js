(function () {

  var modalDelete = $('#modal-delete');

  addListeners()

  function addListeners() {
    $('#btn-delete-open-trip').click(onDeleteClick)
  }

  function onDeleteClick() {
    var bookingId = $('[iki-id-booking-e-rek]').attr('iki-id-booking-e-rek');
    modalDelete.modal('show');
    $('input[name=booking_id]').val(bookingId);
  }
})();
