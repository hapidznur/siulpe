(function () {

  var modalDelete = $('#modal-delete');
  var formDelete = $('#modal-delete form');
  addListeners();

  function onBtnDeleteClick() {
    var id = $(this).parent().parent().parent().attr('open-trip-id');
    formDelete.find('input').val(id);
    modalDelete.modal('show');
  }

  function addListeners() {
    $('body').on('click', '.btn-delete', onBtnDeleteClick)
  }
})()
