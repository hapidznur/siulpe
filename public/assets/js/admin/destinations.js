(function () {
  var modalDelete = $('#modal-delete')

  addListeners();

  function addListeners() {
    $('body').on('click', '.btn-delete', onBtnDeleteClick)
  }

  function onBtnDeleteClick() {
    var id = $(this).parent().parent().parent().attr('destinasi-id');

    modalDelete.find('input').val(id);

    modalDelete.modal('show')
  }
})();
