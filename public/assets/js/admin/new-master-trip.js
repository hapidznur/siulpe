(function () {

  var btnTambahGambar = $('.btn-tambah-gambar');
  var editMasterTrip = $('#ini-adalah-halaman-edit-master-trip') ? true : false;
  var idCover = 0

  addListeners();

  function onBtnTambahGambarClick() {
    var me = $(this);
    var currentIndeks = $('.master-trip-cover').length;

    if (currentIndeks == 5) {
      return alert("Maksimal 5");
    }

    me.parent().parent().find('.row').append(
      '<div class="col-md-4">'+
        '<div style="padding-top: 5px; padding-bottom: 5px">'+
          '<div class="master-trip-cover">'+
            '<div class="btn-group">'+
              '<button index-image="' + currentIndeks + '" class="btn btn-success btn-set-cover" type="button"><span class="fa fa-check"></span></button>'+
              '<button class="btn btn-danger btn-close" type="button"><span class="fa fa-close"></span></button>'+
            '</div>'+
            '<img class="master-trip-cover-' + currentIndeks + '" src="http://nemanjakovacevic.net/wp-content/uploads/2013/07/placeholder.png" alt="Cover" style="width: 100%; height: 140px; object-fit: cover" />'+
          '</div>'+
          '<input id="master-trip-cover-' + currentIndeks + '" type="file" name="cover[]" form="open-trip" style="display: none">'+
        '</div>'+
      '</div>'
    );

    ImageReader
    .init({
      'img': $('.master-trip-cover-' + currentIndeks),
      'input': $('#master-trip-cover-' + currentIndeks)
    })
    .commit();
  }

  function onDestinationPlus() {
    var destinationOptions = $('#destination-options').html()
    destinationOptions = $(destinationOptions)
    $('#destination-container').append(
      destinationOptions
    );
    destinationOptions.find('select').select2()
    $('.stepContainer').css({
      height: ''
    })
  }

  function onDestinationClose() {
    $(this).parent().parent().remove();
  }

  function onActivityPlus() {
    var activityOptions = $('#activity-options').html()
    activityOptions = $(activityOptions)
    $('#activity-container').append(
      activityOptions
    );
    activityOptions.find('select').select2()
    $('.stepContainer').css({
      height: ''
    })
  }

  function onActivityClose() {
    $(this).parent().parent().remove();
  }

  function onFormNewMasterTripSubmit() {
    $('#destination-options').remove();
    $('#activity-options').remove();
  }

  function onBtnDeleteCoverClick() {
    // jika ini halaman edit, dan tombol ini ada gambarnya, maka kirim ajax untuk hapus gambar
    if (editMasterTrip && $(this).parent().attr('cover-id')) {
      var options = {}
      options.url = '/admin/master-trip-delete-cover/' + $(this).parent().attr('cover-id')
      options.method = 'post';
      $.ajax(options);
    }
    $(this).parent().parent().parent().parent().remove();
  }

  function onBtnSetCoverClick() {
    var img = $(this).parent().parent().find('img');
    var coverYangSebelumnya = $('img.img.cover.selected')

    // hapus border dari gambar yang sebelumnya
    coverYangSebelumnya.removeClass('img').removeClass('cover').removeClass('selected')

    // jadikan gambar sekarang dikelilingi border
    img.addClass('img').addClass('cover').addClass('selected')

    idCover = $(this).attr('index-image')
    $('input[name=id_cover]').val(idCover)
  }

  function addListeners() {
    btnTambahGambar.click(onBtnTambahGambarClick);
    for (var i = 1; i <= $('.master-trip-cover').length; i++) {
      ImageReader
      .init({
        'img': $('.master-trip-cover-' + i),
        'input': $('#master-trip-cover-' + i)
      })
      .commit();
    }

    $('body').on('submit', '#form-new-master-trip', onFormNewMasterTripSubmit);
    $('body').on('click', '.master-trip-cover .btn-close', onBtnDeleteCoverClick);
    $('body').on('click', '.destination-plus', onDestinationPlus)
    $('body').on('click', '.destination-close', onDestinationClose)
    $('body').on('click', '.activity-plus', onActivityPlus)
    $('body').on('click', '.activity-close', onActivityClose)
    $('body').on('click', '.btn-set-cover', onBtnSetCoverClick)
    $('select#destinations').select2()
    $('select#categories').select2()
  }
})();
