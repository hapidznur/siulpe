(function () {

  var modalDelete = $('#modal-delete')
  var modalDetail = $('#modal-detail')
  var formDelete = $('#fasilitas-delete-form')
  addListeners();

  function onButtonDeleteClick() {
    var id = $(this).parent().parent().parent().attr('facility-id')
    modalDelete.modal('show')
    formDelete.find('input').val(id)
  }

  function onButtonDetailClick() {
    var id = $(this).parent().parent().parent().attr('facility-id')
    var title = $(this).parent().parent().parent().attr('facility-title')
    var description = $(this).parent().parent().parent().attr('facility-description')

    modalDetail.modal('show')

    $('#title').html(title);
    $('#description').html(description);
  }

  function addListeners() {
    $('body').on('click', '.btn-delete', onButtonDeleteClick);
    $('body').on('click', '.btn-detail', onButtonDetailClick);
  }
})();
