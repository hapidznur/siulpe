(function () {
  var modalDetailPembayaran = $('#modal-detail-pembayaran')
  var modalKonfirmasiPembayaran = $('#modal-konfirmasi-pembayaran');
  var modalDeletePembayaran = $('#modal-delete-pembayaran');

  addListeners();

  function onDetailPembayaranClick() {
    var id = $(this).parent().parent().parent().attr('confirmation-id')
    var options = {}
    options.url = '/admin/pembayaran-detail/' + id
    options.method = 'get'
    options.beforeSend = function () {
      tampilkanLoading()
    }
    options.success = function (response) {
      tampilkanData(response.confirmation)
    }
    $.ajax(options);
    modalDetailPembayaran.modal('show')
  }

  function addListeners() {
    $('body').on('click', '.btn-detail-pembayaran', onDetailPembayaranClick)
    $('body').on('click', '.btn-check-pembayaran', onCheckPembayaranClick)
    $('body').on('click', '.btn-delete-pembayaran', onDeletePembayaranClick)
  }

  function tampilkanLoading() {
    setDom('Loading..', 'Loading..', 'Loading..', 'Loading..', 'Loading..', 'Loading..', 'Loading..');
  }

  function tampilkanData(confirmation) {
    setDom(confirmation.nama, confirmation.email, confirmation.bank, confirmation.rekening_pengirim,
      confirmation.total_transfer, confirmation.status, confirmation.tanggal_pembayaran,
      confirmation.bukti_transfer_src);
  }

  function setDom(nama, email, bank, rekening_pengirim, total_transfer, status, tanggal_pembayaran, bukti_transfer) {
    modalDetailPembayaran.find('#nama').html(nama);
    modalDetailPembayaran.find('#email').html(email);
    modalDetailPembayaran.find('#bank').html(bank);
    modalDetailPembayaran.find('#rekening_pengirim').html(rekening_pengirim);
    modalDetailPembayaran.find('#total_transfer').html(total_transfer);
    modalDetailPembayaran.find('#status').html(status);
    modalDetailPembayaran.find('#tanggal_pembayaran').html(tanggal_pembayaran);
    modalDetailPembayaran.find('img#bukti_transfer').attr('src', bukti_transfer)
  }

  function onCheckPembayaranClick() {
    var confirmationId = $(this).parent().parent().parent().attr('confirmation-id');
    modalKonfirmasiPembayaran.find('input[name=invoice_confirmation_id]').val(confirmationId);
    modalKonfirmasiPembayaran.modal('show');
  }

  function onDeletePembayaranClick() {
    var id = $(this).parent().parent().parent().attr('confirmation-id')
    modalDeletePembayaran.find('input[name=invoice_confirmation_id]').val(id)
    modalDeletePembayaran.modal('show')
  }
})()
