(function () {

  'use strict';

  var modalBookingDetail = $('#booking-detail');
  var modalBookingEdit = $('#booking-edit');
  var modalDelete = $('#modal-delete');
  var btnLihatDetaiPembayaran = $('#btn-lihat-detail-pembayaran');
  var regional = Regional.init();
  $('body').on('click', '.btn-lihat-detail', onLihatDetailClick);
  $('body').on('click', '.btn-edit', onEditClick);
  $('body').on('click', '.btn-delete', onDeleteClick);
  btnLihatDetaiPembayaran.click(onLihatDetaiLPembayaranClick)

  // grab regional
  regional
    .provinsi($('select[name=provinsi]'))
    .kota($('select[name=kota]'))
    .kecamatan($('select[name=kecamatan]'))
    .grab();

  function onLihatDetailClick() {
    var bookingId = $(this).parent().parent().parent().attr('id-booking');
    modalBookingDetail.modal('show');

    var opt = {}
    opt.url = '/admin/booking-detail/' + bookingId
    opt.beforeSend = function () {
      displayData("loading...", "loading...", "loading...", "loading...", "loading...", "loading...", "loading...",
        "loading...", "loading...", "loading...", "loading...", "loading...", "loading...", "loading...",
        "loading...", "loading...", "loading...", "Loading...", "Loading...");
    }
    opt.success = function (response) {
      $('#link-new-tab').attr('href', '/admin/booking-show/' + response.booking.id);
      displayData(
        response.booking.open_trip.master_trip.title, response.booking.open_trip.kode, response.booking.open_trip.master_trip.destination,
        response.booking.open_trip.tanggal_berangkat, response.booking.open_trip.quota, response.booking.open_trip.price,
        response.booking.open_trip.master_trip.list_facilities, response.booking.open_trip.status, response.booking.kode,
        response.booking.nama, response.booking.email, response.booking.no_ktp, response.booking.no_tlp, response.booking.provinsi,
        response.booking.kota, response.booking.kecamatan, response.booking.alamat, response.booking.invoice.nomor_invoice,
        response.booking.status_pembayaran, response.booking.tanggal_booking
      );
      tampilkanKonfirmasiPembayaran(response.booking.invoice.confirmations);
    }
    $.ajax(opt);

    function displayData(namaOpenTrip, kodeOpenTrip, destination, tanggalBerangkat, kuota, harga, fasilitas, status, kodeBooking,
      namaPemesan, emailPemesan, noKtp, noHp, provinsi, kota, kecamatan, alamat, invoice, status_pembayaran, tanggal_booking) {
        $('#nama-open-trip').html(namaOpenTrip);
        $('#kode-open-trip').html(kodeOpenTrip);
        $('#destination').html(destination);
        $('#tanggal-berangkat').html(tanggalBerangkat);
        $('#quota').html(kuota);
        $('#harga').html(harga);
        $('#fasilitas').html(fasilitas);
        $('#status-open-trip').html(status);
        $('#kode-booking').html(kodeBooking);
        $('#nama-pemesan').html(namaPemesan);
        $('#email-pemesan').html(emailPemesan);
        $('#no-ktp').html(noKtp);
        $('#no-hp').html(noHp);
        $('#provinsi').html(provinsi);
        $('#kota').html(kota);
        $('#kecamatan').html(kecamatan);
        $('#alamat').html(alamat);
        $('#invoice').html(invoice);
        $('#status-pembayaran').html(status_pembayaran);
        $('#tanggal-booking').html(tanggal_booking);
    }

    function tampilkanKonfirmasiPembayaran(confirmations) {
      var tr = '';

      $.each(confirmations, function (index, confirmation) {
        tr += '' +
        '<tr>' +
          '<td>' + confirmation.nama + '</td>' +
          '<td>' + confirmation.email + '</td>' +
          '<td>' + confirmation.bank + '</td>' +
          '<td>' + confirmation.rekening_pengirim + '</td>' +
          '<td>Rp. ' + confirmation.total_transfer.toLocaleString() + '</td>' +
          '<td>' + confirmation.status + '</td>' +
          '<td>' + confirmation.tanggal_pembayaran + '</td>' +
        '</tr>'
      })

      $('#table-pembayaran tbody').html(tr);
    }
  }

  function onEditClick() {
    var bookingId = $(this).parent().parent().parent().attr('id-booking');
    var opt = {}
    opt.url = '/admin/booking-detail/' + bookingId;
    opt.success = function (response) {
      displayDataToEditForm(response.booking);
    }
    $.ajax(opt);
    modalBookingEdit.modal('show');

    function displayDataToEditForm(booking) {
      $('input[name=booking_id]').val(booking.id);
      $('input[name=nama]').val(booking.nama);
      $('input[name=no_ktp]').val(booking.no_ktp);
      $('input[name=no_tlp]').val(booking.no_tlp);
      $('input[name=email]').val(booking.email);
      $('input[name=jumlah_peserta]').val(booking.jumlah_peserta);
      regional.setProvinsi(booking.provinsi.toUpperCase());
      regional.setKota(booking.kota.toUpperCase());
      regional.setKecamatan(booking.kecamatan.toUpperCase());
      $('textarea[name=alamat]').val(booking.alamat);
      $('input[name=status_invoice]').val(booking.status_invoice.split('_').join(' '));
    }
  }


  function onDeleteClick() {
    var bookingId = $(this).parent().parent().parent().attr('id-booking');
    modalDelete.modal('show');
    $('input[name=booking_id]').val(bookingId);
  }

  function onLihatDetaiLPembayaranClick() {
    $("#table-pembayaran").toggle();
  }

})();
