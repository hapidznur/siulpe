var gulp = require('gulp'),
	browserSync = require('browser-sync');
	// concat-css = require('gulp-concat-css');

var config = {
	appsPath : './application',
	extPath :'./ext',
	fontPath : './ext/fonts',
	cssPath : './ext/css',
	jsPath : './ext/js',
	sourcePath : './node_modules'
}
//moving css from node modules to external directories
gulp.task('move-css',function() {
	return gulp.src([
		config.sourcePath + '/bootstrap/dist/css/bootstrap.min.css',
		config.sourcePath + '/font-awesome/css/font-awesome.min.css',
		config.sourcePath + '/ionicons/dist/css/ionicons.min.css',
		config.extPath + '/Admin-lte/dist/css/AdminLTE.min.css',
		config.extPath + '/Admin-lte/dist/css/skins/_all-skins.min.css',
		config.sourcePath + '/icheck/skins/flat/blue.css',
		config.sourcePath + '/morris.js/morris.css',
		config.sourcePath + '/jvectormap/jquery-vectormap.css',
		config.extPath + '/datepicker/datepicker3.css',
		config.sourcePath + '/daterangepicker/daterangepicker-bs3.min.css',
		config.sourcePath + '/bootstrap3-wysihtml5-npm/dist/bootstrap3-wysihtml5.min.css',
		])
	 .pipe(gulp.dest(config.cssPath));
})

//moving font from node modules to external directories
gulp.task('move-font',function() {
	return gulp.src([
		config.sourcePath + '/bootstrap/fonts/*.*',
		config.sourcePath + '/font-awesome/fonts/*.*',
		config.sourcePath + '/ionicons/dist/fonts/*.*',		
		])
	 .pipe(gulp.dest(config.fontPath));
})

//moving js from node modules to external directories
gulp.task('move-js',function() {
	return gulp.src([
		config.sourcePath + '/jquery/dist/jquery.min.js',
		config.extPath + '/jquery-ui.min.js',
		config.sourcePath + '/bootstrap/dist/js/bootstrap.min.js',
		config.sourcePath + '/raphael/raphael.min.js',
		config.sourcePath + '/morris.js/morris.min.js',
		config.extPath + '/jquery.sparkline.min.js',
		config.sourcePath + '/jvectormap/jquery-vectormap.min.js',
		config.sourcePath + '/icheck/icheck.min.js',
		config.sourcePath + '/jquery-knob/dist/jquery.knob.min.js',
		config.sourcePath + '/moment/min/moment.min.js',
		config.sourcePath + '/daterangepicker/daterangepicker.min.js',
		config.extPath + '/datepicker/bootstrap-datepicker.js',
		config.sourcePath + '/daterangepicker/daterangepicker-bs3.min.css',
		config.sourcePath + '/bootstrap3-wysihtml5-npm/dist/bootstrap3-wysihtml5.all.min.js',
		config.sourcePath + '/jquery-slimscroll/jquery.slimscroll.min.js',
		config.extPath + '/fastclick/fastclick.min.js',
		config.extPath + '/app.min.js',
		config.extPath + '/dashboard.js',
		config.extPath + '/demo.js',
		])
	 .pipe(gulp.dest(config.jsPath));
})