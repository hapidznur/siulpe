<?php

class Migration_Create_invoices_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'	=> [
				'type'	=>	'integer',
				'auto_increment' => true
			],
			'booking_id'		=> [
				'type'			=> 'integer'
			],
			'nomor_invoice' => [
				'type' => 'varchar',
				'constraint' => 10
			],
			'total_harga' => [
				'type' => 'decimal'
			],
			'status'	=> [
				'type' => 'enum("belum_dibayar", "dp", "angsur","lunas")',
				'default' => 'belum_dibayar'
			],
			'created_at' => [
				'type' => 'date'
			],
			'updated_at' => [
				'type' => 'date'
			]
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_invoices');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_invoices');
	}
}
