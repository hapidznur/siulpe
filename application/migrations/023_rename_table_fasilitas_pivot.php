<?php

class Migration_Rename_table_fasilitas_pivot extends CI_Migration {

	public function up()
	{
		$this->dbforge->rename_table('oyisam_open_trip_facility_pivot', 'oyisam_master_trip_facility_pivot');
	}

	public function down()
	{
		$this->dbforge->rename_table('oyisam_master_trip_facility_pivot', 'oyisam_open_trip_facility_pivot');
	}
}
