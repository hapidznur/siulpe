<?php

class Migration_Modif_facilities_table2 extends CI_Migration {

	public function up()
	{
		$this->dbforge->drop_column('oyisam_facilities', 'facility_transportation');
		$this->dbforge->drop_column('oyisam_facilities', 'facility_consumption');
		$this->dbforge->drop_column('oyisam_facilities', 'facility_acomodation');
		$this->dbforge->drop_column('oyisam_facilities', 'master_trip_id');

		$this->dbforge->add_column('oyisam_facilities', [
			'title' => [
					'type'			=>	'VARCHAR',
					'constraint' => '400',
					'null'    => true
			],
			'description' => [
					'type'			=>	'VARCHAR',
					'constraint' => '400',
					'null'    => true
			]
		]);
	}

	public function down()
	{
		// $this->dbforge->drop_table('oyisam_facilities');
	}
}
