<?php

class Migration_Add_flickr_to_identitas extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('oyisam_identitas_web', [
			'flickr'		=> [
				'type'			=> 'varchar',
				'constraint'=> 30,
				'null'			=> true
			]
		]);
	}

	public function down()
	{
		// $this->dbforge->drop_table('oyisam_subscribe');
	}
}
