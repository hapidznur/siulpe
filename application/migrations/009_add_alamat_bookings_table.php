<?php

class Migration_Add_alamat_bookings_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('oyisam_bookings', [
			'alamat'	=> [
				'type'	=>	'varchar',
				'constraint' => 300
			]
		]);
	}

	public function down()
	{
		$this->dbforge->drop_column('oyisam_bookings', 'avatar');
	}
}
