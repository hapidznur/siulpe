<?php

class Migration_Modif_master_trip1 extends CI_Migration {

	public function up()
	{

		$this->dbforge->add_column('oyisam_master_trips', [
			'provinsi' => [
					'type'			=>	'varchar',
					'constraint' => '30',
					'null'    => true
			],
			'kota' => [
					'type'			=>	'varchar',
					'constraint' => '30',
					'null'    => true
			]
		]);
	}

	public function down()
	{
		// $this->dbforge->drop_table('oyisam_facilities');
	}
}
