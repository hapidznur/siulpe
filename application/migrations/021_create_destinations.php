<?php

class Migration_Create_destinations extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'	=> [
				'type'	=>	'integer',
				'auto_increment' => true
			],
			'nama'		=> [
				'type'			=> 'varchar',
				'constraint' => 200,
				'null' => false
			]
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_destinations');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_destinations');
	}
}
