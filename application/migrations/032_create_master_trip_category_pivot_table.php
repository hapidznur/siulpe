<?php

class Migration_Create_master_trip_category_pivot_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'	=> [
				'type'	=>	'integer',
				'auto_increment' => true
			],
			'category_id'		=> [
				'type'			=> 'integer',
			],
			'master_trip_id'		=> [
				'type'			=> 'integer',
			]
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_category_master_trip_pivot');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_category_master_trip_pivot');
	}
}
