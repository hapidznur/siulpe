<?php

class Migration_Create_categories_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'	=> [
				'type'	=>	'integer',
				'auto_increment' => true
			],
			'nama'		=> [
				'type'			=> 'VARCHAR',
				'constraint'=> 50,
				'null' => false
			],
      'icon'		=> [
				'type'			=>	'VARCHAR',
				'constraint'	=>	'300',
        'null'    => false
			],
      'deskripsi'  => [
        'type'			=>	'VARCHAR',
				'constraint' => '400',
				'null'    => true
      ],
			'created_at' => [
				'type' => 'date'
			],
			'updated_at' => [
				'type' => 'date'
			]
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_categories');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_categories');
	}
}
