<?php

class Migration_Create_ads_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'	=> [
				'type'	=>	'integer',
				'auto_increment' => true
			],
			'nama'		=> [
				'type'			=> 'varchar',
				'constraint' => 30
			],
			'posisi'		=> [
				'type'			=> 'enum(\'left-bar\', \'bottom-body\')'
			],
			'jenis'		=> [
				'type'			=> 'enum(\'gambar\', \'script\')'
			],
			'status'		=> [
				'type'			=> 'integer',
				'constraint' => 1
			],
			'script'		=> [
				'type'			=> 'longtext'
			],
			'created_at'		=> [
				'type'			=> 'datetime'
			],
			'updated_at'		=> [
				'type'			=> 'datetime'
			],
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_ads');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_ads');
	}
}
