<?php

class Migration_Nambah_start_date_opentrip extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('oyisam_open_trips', [
			'start_date_pendaftaran' => [
					'type'			=>	'date',
					'null'		=> true
			]
		]);	}

	public function down()
	{

	}
}
