<?php

class Migration_Create_reset_passwords_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'		=> [
				'type'			=> 'int',
				'auto_increment'=>	true
			],
			'id_user'		=> [
				'type'			=>	'VARCHAR',
				'constraint'	=>	'100',
        	'null'    => false
			],
			'password_token'		=>	[
				'type'			=>	'VARCHAR',
				'constraint'	=>	'100'
			]
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_reset_passwords');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_reset_passwords');
	}
}
