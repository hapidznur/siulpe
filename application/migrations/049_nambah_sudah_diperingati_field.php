<?php

class Migration_Nambah_sudah_diperingati_field extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('oyisam_invoices', [
			'sudah_diperingati' => [
				'type'		=>	'boolean'
			],
		]);
	}

	public function down()
	{

	}
}
