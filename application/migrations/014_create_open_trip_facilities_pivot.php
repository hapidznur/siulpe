<?php

class Migration_Create_open_trip_facilities_pivot extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'	=> [
				'type'	=>	'integer',
				'auto_increment' => true
			],
			'open_trip_id'		=> [
				'type'			=> 'integer'
			],
			'facility_id' => [
				'type' => 'integer'
			]
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_open_trip_facility_pivot');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_open_trip_facility_pivot');
	}
}
