<?php

class Migration_Create_master_trips_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'	=> [
				'type'	=>	'integer',
				'auto_increment' => true
			],
      'title'		=> [
				'type'			=>	'VARCHAR',
				'constraint'	=>	'300',
        'null'    => false
			],
      'destination'		=> [
				'type'			=>	'VARCHAR',
				'constraint'	=>	'300',
        'null'    => false
			],
      'itenary'  => [
        'type'			=>	'LONGTEXT',
				'null'    => false
      ],
			'additional_information'		=>	[
				'type'			=>	'varchar',
				'constraint'	=>	'500',
				'null' => true
			],
      'term_of_service' => [
        'type'			=>	'VARCHAR',
				'constraint'	=>	'400',
				'null' => true
      ],
			'created_at' => [
				'type' => 'date'
			],
			'updated_at' => [
				'type' => 'date'
			]
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_master_trips');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_master_trips');
	}
}
