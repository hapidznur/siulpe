<?php

class Migration_Modif_open_trip3 extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('oyisam_open_trips', [
			'status_tutup_otomatis' => [
				'type' => 'date',
				'null' => true
			]
		]);
	}

	public function down()
	{
		// $this->dbforge->drop_table('oyisam_facilities');
	}
}
