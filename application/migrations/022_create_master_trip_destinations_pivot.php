<?php

class Migration_Create_master_trip_destinations_pivot extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'	=> [
				'type'	=>	'integer',
				'auto_increment' => true
			],
			'master_trip_id'		=> [
				'type'			=> 'integer'
			],
			'destination_id' => [
				'type' => 'integer'
			]
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_master_trip_destination_pivot');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_master_trip_destination_pivot');
	}
}
