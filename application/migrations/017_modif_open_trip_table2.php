<?php

class Migration_Modif_open_trip_table2 extends CI_Migration {

	public function up()
	{

		$this->dbforge->add_column('oyisam_open_trips', [
			'price_details' => [
					'type'			=>	'longtext',
					'null'    => true
			]
		]);
	}

	public function down()
	{
		// $this->dbforge->drop_table('oyisam_facilities');
	}
}
