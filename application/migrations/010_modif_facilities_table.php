<?php

class Migration_Modif_facilities_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->drop_column('oyisam_facilities', 'title');
		$this->dbforge->drop_column('oyisam_facilities', 'price');
		$this->dbforge->drop_column('oyisam_facilities', 'description');
		$this->dbforge->add_column('oyisam_facilities', [
			'facility_transportation' => [
					'type'			=>	'VARCHAR',
					'constraint' => '400',
					'null'    => true
			],
			'facility_consumption' => [
					'type'			=>	'VARCHAR',
					'constraint' => '400',
					'null'    => true
			],
			'facility_acomodation' => [
					'type'			=>	'VARCHAR',
					'constraint' => '400',
					'null'    => true
			]
		]);
	}

	public function down()
	{
		// $this->dbforge->drop_table('oyisam_facilities');
	}
}
