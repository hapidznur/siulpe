<?php

class Migration_Ngubah_detail_detail_jadi_longtext extends CI_Migration {

	public function up()
	{
		$this->dbforge->modify_column('oyisam_master_trips', [
			'additional_information' => [
					'name' => 'additional_information',
					'type'			=>	'longtext'
			],
			'term_of_service' => [
					'name' => 'term_of_service',
					'type'			=>	'longtext'
			],
			'fasilitas' => [
					'name' => 'fasilitas',
					'type'			=>	'longtext'
			]
		]);

		$this->dbforge->modify_column('oyisam_identitas_web', [
			'nomor_telepon' => [
					'name' => 'nomor_telepon',
					'type'			=>	'varchar',
					'constraint'	=> 300
			]
		]);
	}

	public function down()
	{

	}
}
