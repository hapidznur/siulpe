<?php

class Migration_Create_subscribers_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'	=> [
				'type'	=>	'integer',
				'auto_increment' => true
			],
			'email'		=> [
				'type'			=> 'VARCHAR',
				'constraint'=> 50,
				'null' => false
			],
			'created_at' => [
				'type' => 'datetime'
			],
			'updated_at' => [
				'type' => 'datetime'
			]
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_subscribe');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_subscribe');
	}
}
