<?php

class Migration_Modif_table_fasilitas_pivot extends CI_Migration {

	public function up()
	{
		$this->dbforge->modify_column('oyisam_master_trip_facility_pivot', [
			'open_trip_id' => [
					'name' => 'master_trip_id',
					'type'			=>	'integer'
			]
		]);
	}

	public function down()
	{
		
	}
}
