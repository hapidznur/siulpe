<?php

class Migration_Modify_invoice_todatetime extends CI_Migration {

	public function up()
	{
		$this->dbforge->modify_column('oyisam_invoices', [
			'created_at' => [
					'name' => 'created_at',
					'type'			=>	'datetime'
			],
			'updated_at' => [
					'name' => 'updated_at',
					'type'			=>	'datetime'
			]
		]);	}

	public function down()
	{

	}
}
