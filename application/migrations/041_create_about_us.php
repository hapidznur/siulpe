<?php

class Migration_Create_about_us extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'	=> [
				'type'	=>	'integer',
				'auto_increment' => true
			],
			'about_us'		=> [
				'type'			=> 'longtext',
				'null' => true
			],
			'contact_us'		=> [
				'type'			=> 'longtext',
				'null' => true
			],
			'legalitas'		=> [
				'type'			=> 'longtext',
				'null' => true
			],
			'faq'		=> [
				'type'			=> 'longtext',
				'null' => true
			],
			'karir'		=> [
				'type'			=> 'longtext',
				'null' => true
			],
			'afiliasi'		=> [
				'type'			=> 'longtext',
				'null' => true
			],
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_about_us');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_about_us');
	}
}
