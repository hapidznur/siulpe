<?php

class Migration_Create_facilities_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'	=> [
				'type'	=>	'integer',
				'auto_increment' => true
			],
			'master_trip_id'		=> [
				'type'			=> 'integer'
			],
      'title'		=> [
				'type'			=>	'VARCHAR',
				'constraint'	=>	'300',
        'null'    => false
			],
      'price'		=> [
				'type'			=>	'integer',
        'null'    => false
			],
      'description'  => [
        'type'			=>	'VARCHAR',
				'constraint' => '400',
				'null'    => true
      ],
			'created_at' => [
				'type' => 'date'
			],
			'updated_at' => [
				'type' => 'date'
			]
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_facilities');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_facilities');
	}
}
