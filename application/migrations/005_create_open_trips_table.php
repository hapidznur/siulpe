<?php

class Migration_Create_open_trips_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'	=> [
				'type'	=>	'integer',
				'auto_increment' => true
			],
			'master_trip_id'		=> [
				'type'			=> 'integer'
			],
			'quota' => [
				'type' => 'integer'
			],
			'durasi' => [
				'type' => 'integer'
			],
      'harga'		=> [
				'type'			=>	'double',
        'null'    => false
			],
      'dp_minimum'  => [
        'type'			=>	'double',
				'null'    => false
      ],
			'angsuran_maksimum'  => [
        'type'			=>	'integer',
				'null'    => false
      ],
			'tanggal_berangkat' => [
				'type' => 'date'
			],
			'jatuh_tempo_pembayaran' => [
				'type' => 'date'
			],
			'status' => [
				'type' => 'enum("pendaftaran", "ditutup", "pasti_berangkat")',
				'default' => 'pendaftaran'
			],
			'cover' => [
				'type' => 'varchar',
				'constraint' => '500',
				'null' => false
			],
			'created_at' => [
				'type' => 'date'
			],
			'updated_at' => [
				'type' => 'date'
			]
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_open_trips');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_open_trips');
	}
}
