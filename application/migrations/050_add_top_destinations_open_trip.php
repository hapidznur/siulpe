<?php

class Migration_Add_top_destinations_open_trip extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('oyisam_open_trips', [
			'top_destination'		=> [
				'type'			=> 'boolean',
				'null'			=> true
			]
		]);
	}

	public function down()
	{
		// $this->dbforge->drop_table('oyisam_subscribe');
	}
}
