<?php

class Migration_Nambah_instagram_sama_access_token extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('oyisam_identitas_web', [
			'instagram' => [
					'type'			=>	'text',
					'constraint' => 16,
					'null'		=> true
			],
			'instagram_access_token' => [
					'type'			=>	'text',
					'constraint' => 80,
					'null'		=> true
			]
		]);
	}

	public function down()
	{

	}
}
