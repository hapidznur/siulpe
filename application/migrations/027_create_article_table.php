<?php

class Migration_Create_article_table extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field([
            'id'    => [
                'type'  =>  'integer',
                'auto_increment' => true
            ],
            'title'     => [
                'type'          =>  'VARCHAR',
                'constraint'    =>  '300',
                'null'    => false
            ],
            'seo_tag'     => [
                'type'          =>  'VARCHAR',
                'constraint'    =>  '300',
                'null'    => false
            ],
            'article'  => [
                'type'          =>  'LONGTEXT',
                'null'    => false
            ],
            'figure'        =>  [
                'type'          =>  'varchar',
                'constraint'    =>  '255',
                'null' => true
            ],
            'user'  => [
                'type'  => 'varchar',
                'constraint' => '100',
                'null' =>  false
            ],
            'created_at' => [
                'type' => 'date'
            ],
            'updated_at' => [
                'type' => 'date'
            ]
        ]);

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('oyisam_article');
    }

    public function down()
    {
        $this->dbforge->drop_table('oyisam_article');
    }
}
