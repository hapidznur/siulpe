<?php

class Migration_Add_garansi_berangkat extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('oyisam_open_trips', [
			'garansi_berangkat'		=> [
				'type'			=> 'integer',
				'constraint'=> 1
			]
		]);
	}

	public function down()
	{
		// $this->dbforge->drop_table('oyisam_subscribe');
	}
}
