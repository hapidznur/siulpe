<?php

class Migration_Nambah_identitas_web extends CI_Migration {

	public function up()
	{

		$this->dbforge->add_column('oyisam_identitas_web', [
			'logo' => [
					'type'			=>	'varchar',
					'constraint' => 40,
					'null'    => true
			],
			'favicon' => [
					'type'			=>	'varchar',
					'constraint' => 40,
					'null'    => true
			],
			'meta_deskripsi' => [
					'type'			=>	'varchar',
					'constraint' => 540,
					'null'    => true
			],
			'meta_keyword' => [
					'type'			=>	'varchar',
					'constraint' => 540,
					'null'    => true
			]
		]);
	}

	public function down()
	{
		// $this->dbforge->drop_column('oyisam_master_trips', 'fasilitas');
	}
}
