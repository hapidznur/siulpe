<?php

class Migration_Add_cover_to_mstcovers extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('oyisam_master_trip_covers', [
			'cover'		=> [
				'type'			=> 'boolean',
				'null'			=> true,
				'after'			=> 'src'
			]
		]);
	}

	public function down()
	{
		// $this->dbforge->drop_table('oyisam_subscribe');
	}
}
