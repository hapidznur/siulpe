<?php

class Migration_Add_upload_itenary extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('oyisam_master_trips', [
			'itenary_file'		=> [
				'type'			=> 'VARCHAR',
				'constraint'=> 100,
				'null' => true
			]
		]);
	}

	public function down()
	{
		// $this->dbforge->drop_table('oyisam_subscribe');
	}
}
