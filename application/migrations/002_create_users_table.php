<?php

class Migration_Create_users_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id_user'		=> [
				'type'			=> 'VARCHAR',
				'constraint'=> 100,
        'null'    => false
			],
			'nama'		=> [
				'type'			=>	'VARCHAR',
				'constraint'	=>	'100',
        'null'    => false
			],
      'username'		=> [
				'type'			=>	'VARCHAR',
				'constraint'	=>	'100',
        'null'    => false
			],
      'email'		=> [
				'type'			=>	'VARCHAR',
				'constraint'	=>	'100',
        'null'    => false
			],
      'password'  => [
        'type'			=>	'VARCHAR',
				'constraint'	=>	'200'
      ],
			'email_confirmed'		=>	[
				'type'			=>	'int',
				'constraint'	=>	'1',
        'default' =>  0
			],
      'email_token' => [
        'type'			=>	'VARCHAR',
				'constraint'	=>	'400'
      ],
      'role_user' => [
        'type'  => "enum('superuser', 'user')",
        'default' =>  'user'
      ],
      'status'  =>  [
        'type'  => 'integer',
        'constraint'  => 1,
        'default' => 1
      ]
		]);

		$this->dbforge->add_key('id_user', true);
		$this->dbforge->create_table('oyisam_users');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_users');
	}
}
