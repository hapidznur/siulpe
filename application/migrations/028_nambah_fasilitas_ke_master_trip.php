<?php

class Migration_Nambah_fasilitas_ke_master_trip extends CI_Migration {

	public function up()
	{

		$this->dbforge->add_column('oyisam_master_trips', [
			'fasilitas' => [
					'type'			=>	'varchar',
					'constraint' => 300,
					'null'    => true
			]
		]);
	}

	public function down()
	{
		$this->dbforge->drop_column('oyisam_master_trips', 'fasilitas');
	}
}
