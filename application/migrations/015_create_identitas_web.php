<?php

class Migration_Create_identitas_web extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'	=> [
				'type'	=>	'integer',
				'auto_increment' => true
			],
			'nama'		=> [
				'type'			=> 'varchar',
				'constraint' => 200,
				'null' => true
			],
			'email'		=> [
				'type'			=> 'varchar',
				'constraint' => 200,
				'null' => true
			],
			'wa'		=> [
				'type'			=> 'varchar',
				'constraint' => 200,
				'null' => true
			],
			'bbm'		=> [
				'type'			=> 'varchar',
				'constraint' => 200,
				'null' => true
			],
			'line'		=> [
				'type'			=> 'varchar',
				'constraint' => 200,
				'null' => true
			],
			'fb'		=> [
				'type'			=> 'varchar',
				'constraint' => 200,
				'null' => true
			],
			'twitter'		=> [
				'type'			=> 'varchar',
				'constraint' => 200,
				'null' => true
			],
			'syarat_dan_ketentuan'		=> [
				'type'			=> 'longtext'
			]
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_identitas_web');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_identitas_web');
	}
}
