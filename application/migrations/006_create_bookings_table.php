<?php

class Migration_Create_bookings_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'	=> [
				'type'	=>	'integer',
				'auto_increment' => true
			],
			'open_trip_id'		=> [
				'type'			=> 'integer'
			],
			'jumlah_peserta' => [
				'type' => 'integer'
			],
			'nama' => [
				'type' => 'varchar',
				'constraint' => 50
			],
			'email' => [
				'type' => 'varchar',
				'constraint' => 50
			],
			'no_ktp' => [
				'type' => 'varchar',
				'constraint' => 50
			],
			'no_tlp' => [
				'type' => 'varchar',
				'constraint' => 50
			],
			'provinsi'  => [
        'type'			=>	'varchar',
				'constraint'    => 20
      ],
			'kota'  => [
        'type'			=>	'varchar',
				'constraint'    => 30
      ],
			'kecamatan'  => [
        'type'			=>	'varchar',
				'constraint'    => 30
      ],
			'status_invoice'	=> [
				'type' => 'enum("belum_terkirim", "terkirim")',
				'default' => 'belum_terkirim'
			],
			'created_at' => [
				'type' => 'date'
			],
			'updated_at' => [
				'type' => 'date'
			]
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_bookings');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_bookings');
	}
}
