<?php

class Migration_Membuat_table_master_trip_covers extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'	=> [
				'type'	=>	'integer',
				'auto_increment' => true
			],
			'master_trip_id'		=> [
				'type'			=> 'integer'
			],
			'src' => [
				'type' => 'varchar',
				'constraint' => 200
			],
			'created_at' => [
				'type' => 'date'
			],
			'updated_at' => [
				'type' => 'date'
			]
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_master_trip_covers');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_master_trip_covers');
	}
}
