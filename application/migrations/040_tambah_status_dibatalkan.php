<?php

class Migration_Tambah_status_dibatalkan extends CI_Migration {

	public function up()
	{
		$this->dbforge->modify_column('oyisam_open_trips', [
			'status'		=> [
				'type'			=> 'enum("pendaftaran", "ditutup", "pasti_berangkat", "dibatalkan")',
			]
		]);
	}

	public function down()
	{
		// $this->dbforge->drop_table('oyisam_subscribe');
	}
}
