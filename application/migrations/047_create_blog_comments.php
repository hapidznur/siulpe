<?php

class Migration_Create_blog_comments extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'	=> [
				'type'	=>	'integer',
				'auto_increment' => true
			],
			'article_id'	=> [
				'type'	=>	'integer'
			],
			'nama'		=> [
				'type'			=> 'varchar',
				'constraint' => 30
			],
			'email'		=> [
				'type'			=> 'varchar',
				'constraint' => 30
			],
			'body'		=> [
				'type'			=> 'varchar',
				'constraint' => 1500
			],
			'created_at'		=> [
				'type'			=> 'datetime'
			],
			'updated_at'		=> [
				'type'			=> 'datetime'
			],
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_blog_comments');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_blog_comments');
	}
}
