<?php

class Migration_Modif_booking extends CI_Migration {

	public function up()
	{

		// $this->dbforge->drop_column('oyisam_bookings', 'provinsi');
		// $this->dbforge->drop_column('oyisam_bookings', 'kota');
		// $this->dbforge->drop_column('oyisam_bookings', 'kecamatan');
		// $this->dbforge->drop_column('oyisam_bookings', 'no_ktp');
		$this->dbforge->add_column('oyisam_bookings', [
			'nama_peserta' => [
				'type' => 'varchar',
				'constraint' => 1000,
				'null' => true
			]
		]);
	}

	public function down()
	{
		// $this->dbforge->drop_table('oyisam_facilities');
	}
}
