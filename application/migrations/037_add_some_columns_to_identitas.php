<?php

class Migration_Add_some_columns_to_identitas extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('oyisam_identitas_web', [
			'alamat'		=> [
				'type'			=> 'VARCHAR',
				'constraint'=> 350,
				'null' => true
			],
			'no_rekening' =>  [
				'type'	=>	'VARCHAR',
				'constraint'	=> 30,
				'null' => true
			],
			'bank' =>  [
				'type'	=>	'VARCHAR',
				'constraint'	=> 15,
				'null' => true
			],
			'atas_nama' =>  [
				'type'	=>	'VARCHAR',
				'constraint'	=> 30,
				'null' => true
			],
		]);
	}

	public function down()
	{
		// $this->dbforge->drop_table('oyisam_subscribe');
	}
}
