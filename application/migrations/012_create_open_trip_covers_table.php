<?php

class Migration_Create_open_trip_covers_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'	=> [
				'type'	=>	'integer',
				'auto_increment' => true
			],
			'open_trip_id'		=> [
				'type'			=> 'integer'
			],
			'src' => [
				'type' => 'varchar',
				'constraint' => 200
			],
			'created_at' => [
				'type' => 'date'
			],
			'updated_at' => [
				'type' => 'date'
			]
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_open_trip_covers');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_open_trip_covers');
	}
}
