<?php

class Migration_Create_invoice_confirmations_table extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field([
			'id'	=> [
				'type'	=>	'integer',
				'auto_increment' => true
			],
			'invoice_id'		=> [
				'type'			=> 'integer'
			],
			'nama' => [
				'type' => 'varchar',
				'constraint' => 30
			],
			'email' => [
				'type' => 'varchar',
				'constraint' => 30
			],
			'bank' => [
				'type' => 'varchar',
				'constraint' => 5
			],
			'rekening_pengirim' => [
				'type' => 'varchar',
				'constraint' => 30
			],
			'total_transfer' => [
				'type' => 'decimal',
			],
			'bukti_transfer' => [
				'type' => 'varchar',
				'constraint' => 100
			],
			'status'	=> [
				'type' => 'enum("belum dikonfirmasi", "sudah dikonfirmasi")',
				'default' => 'belum dikonfirmasi'
			],
			'tanggal_pembayaran' => [
				'type' => 'date'
			],
			'created_at' => [
				'type' => 'datetime'
			],
			'updated_at' => [
				'type' => 'datetime'
			]
		]);

		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('oyisam_invoice_confirmations');
	}

	public function down()
	{
		$this->dbforge->drop_table('oyisam_invoice_confirmations');
	}
}
