<?php

class Migration_Menambahkan_meeting_point extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('oyisam_open_trips', [
			'meeting_point' => [
					'type'			=>	'VARCHAR',
					'constraint' => '400',
					'null'    => true
			]
		]);
	}

	public function down()
	{
		$this->dbforge->drop_column('oyisam_open_trips', 'meeting_point');
	}
}
