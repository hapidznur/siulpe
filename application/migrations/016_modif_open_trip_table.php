<?php

class Migration_Modif_open_trip_table extends CI_Migration {

	public function up()
	{

		$this->dbforge->add_column('oyisam_open_trips', [
			'blog_link' => [
					'type'			=>	'varchar',
					'constraint' => '400',
					'null'    => true
			]
		]);
	}

	public function down()
	{
		// $this->dbforge->drop_table('oyisam_facilities');
	}
}
