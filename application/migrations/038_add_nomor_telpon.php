<?php

class Migration_Add_nomor_telpon extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('oyisam_identitas_web', [
			'nomor_telepon'		=> [
				'type'			=> 'VARCHAR',
				'constraint'=> 30,
				'null' => true
			]
		]);
	}

	public function down()
	{
		// $this->dbforge->drop_table('oyisam_subscribe');
	}
}
