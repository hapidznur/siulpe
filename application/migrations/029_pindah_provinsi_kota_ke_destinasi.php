<?php

class Migration_Pindah_provinsi_kota_ke_destinasi extends CI_Migration {

	public function up()
	{

		$this->dbforge->add_column('oyisam_destinations', [
			'provinsi' => [
					'type'			=>	'varchar',
					'constraint' => 40,
					'null'    => true
			],
			'kota' => [
					'type'			=>	'varchar',
					'constraint' => 40,
					'null'    => true
			]
		]);
		$this->dbforge->drop_column('oyisam_master_trips', 'provinsi');
		$this->dbforge->drop_column('oyisam_master_trips', 'kota');
	}

	public function down()
	{
		// $this->dbforge->drop_column('oyisam_master_trips', 'fasilitas');
	}
}
