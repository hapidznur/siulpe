<?php

class Migration_Add_jam_pulang_opentrip extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_column('oyisam_open_trips', [
			'jam_pulang'		=> [
				'type'			=> 'time',
				'null'			=> true
			]
		]);
	}

	public function down()
	{
		// $this->dbforge->drop_table('oyisam_subscribe');
	}
}
