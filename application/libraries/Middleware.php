<?php

class Middleware {

	function __construct() {
		# get the framework isntance
		# this is to load another library within a library :(
		# i literally don't like this way of codeigniter
		$this->ci =& get_instance();
	}

	public function auth()
	{
		if ($this->ci->grauth->check()) {
			return; # do nothing if the user is already logged in
		}

    	# if not authenticated, then redirect to login place
		# but save the current state first
	    # so the system know where to go after login
		$this->ci->load->library('session');
		$this->ci->session->set_userdata('catched_location', uri_string());
		redirect(base_url('auth/login'));
	}


	public function auth_ppk(){
		if (!auth()->check_ppk()) {
			redirect("login");
		}		
	} 

	public function guest()
	{
		# if it is a guest or not authenticated, then redirect to the dashboard
		if ($this->ci->grauth->check()) {
			redirect('/');
		}
	}

	public function emailConfirmed()
	{
		if (!auth()->Penyedia()->email_confirmed) {
			# redirect to page to confir their email
			redirect('auth/email-confirmation');
		}
	}

	public function email_unconfirmed()
	{
		if (auth()->Penyedia()->email_confirmed) {
			# redirect to page to confir their email
			show_error(404);
		}
	}

	public function sharePublicVariables()
	{
		# you can use this Middleware to share public variables
	}

	public function tutupOpenTripOtomatis()
	{
		$this->ci->load->model('openTripModel');
		$this->ci->openTripModel->tutupOtomatis();
	}

	public function hapusInvoiceYangTidakKonfirmasiOtomatis($value='')
	{
		$this->ci->load->model('invoiceModel');
		$this->ci->invoiceModel->hapusInvoiceYangBelumDiDPSampaiDenganBatasWaktu();
	}
}
