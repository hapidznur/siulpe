<?php

use Nexendrie\Rss\Generator;
use Nexendrie\Rss\RssChannelItem;
use Illuminate\Database\Capsule\Manager as DB;

/**
 * Kelas ini berfungsi untuk men-generate rss/feed pada 
 * suatu website produk aksamedia.
 * 
 * 
 * @author Nurul Huda
 * @copyright 2017 Aksamedia
 */
class Aksarss {

	/**
	 * these three variables are the variable to describe the website title, link
	 * and the short description of the entire web
	 */
	private $title;
	private $description;
	private $link;

	/**
	 * these variables hold what database and what columns we need to 
	 * read to generate the rss-entries
	 */
	private $article_db;
	private $article_db_id;
	private $article_db_title;
	private $article_db_slug;
	private $article_db_pub_date;
	private $article_db_description;

	/**
	 * this variable holds the pattern link of each article
	 * example -> /artikel/{id}/{slug}
	 */
	private $article_link_pattern;

	function __construct(array $params)
	{
		$this->setTitle($params['title']);
		$this->setDescription($params['description']);
		$this->setLink($params['link']);
	}

	/**
	 * This function used to generate the rss/feed xml with 
	 * @return string
	 */
	public function generate()
	{
		$generator = new Generator;
		$generator->title = $this->title;
		$generator->link = $this->link;
		$generator->description = $this->description;
		$generator->dataSource = function () use ($generator) {
			$articles = DB::table($this->article_db)->orderby($this->article_db_id, 'desc')->take(15)->get();
			$entries = [];

			foreach ($articles as $key => $article) {
				$entries[] = new RssChannelItem(
					$article->{$this->article_db_title}, 
					html_entity_decode(strip_tags($article->{$this->article_db_description})), 
					$this->getLink($article),
					$article->{$this->article_db_pub_date}
				);
			}

			return $entries;
		};

		$result = $generator->generate();

		return $result;
	}

	public function setTitle($title)
	{
		$this->title = $title;
	}

	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function setLink($link)
	{
		$this->link = $link;
	}

	public function setDatabaseArticle($db, $id, $title, $slug, $description, $pub_date)
	{
		$this->article_db = $db;
		$this->article_db_id = $id;
		$this->article_db_title = $title;
		$this->article_db_slug = $slug;
		$this->article_db_description = $description;
		$this->article_db_pub_date = $pub_date;
	}

	public function setArticleLinkPattern($pattern)
	{
		$this->article_link_pattern = $pattern;
	}

	private function getLink($article)
	{
		$link = $this->link . $this->article_link_pattern;

		preg_match_all("/\{([a-zA-Z]+)\}/", $this->article_link_pattern, $output_array);

		foreach ($output_array[1] as $key => $attribute) {
			$attr = "article_db_$attribute";
			$pattern = "/\{". $attribute ."\}/";
			$link = preg_replace($pattern, $article->{$this->{$attr}}, $link);
		}

		return $link;
	}
}