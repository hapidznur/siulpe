<?php

class Aksamailer extends \PHPMailer{

	function __construct() {
		parent::__construct();
		$this->isSMTP();                                      // Set mailer to use SMTP
		$this->setFrom($_ENV['EMAIL_USER'], 'ULP UIN MAULANA MALIK IBRAHIM MALANG');
		$this->Host = $_ENV['EMAIL_HOST'];  					// Specify main and backup SMTP servers
		$this->SMTPAuth = true;                               // Enable SMTP authentication
		$this->Username = $_ENV['EMAIL_USER'];                 // SMTP username
		$this->Password = $_ENV['EMAIL_PASS'];                           // SMTP password
		$this->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$this->isHTML(true);
		$this->Port = $_ENV['EMAIL_PORT'];
	}

	public function send()
	{
		parent::send();

		# store error ke file mail.error
		if ($this->ErrorInfo) {
			exec("echo \"" . \Carbon\Carbon::now() . " - {$this->Subject} \t- {$this->ErrorInfo}\" >> ../mail.errors");
		}
	}
}
