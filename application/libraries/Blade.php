<?php

use Philo\Blade\Blade as CoreBlade;

class Blade extends CoreBlade
{

	function __construct()
	{
		$this->views = __DIR__."/../views";
		$this->cache = __DIR__."/../cache";

		parent::__construct($this->views, $this->cache);
	}

	/**
	* this function is to set the other location of blade,
	* but it needs to create new instance of blade by re-calling the
	* parent constructor
	*/
	public function setDirectory($path)
	{
		$this->views = __DIR__."/../{$path}";

		parent::__construct($this->views, $this->cache);
	}

	function nggambar($template, array $values = [])
	{
		echo parent::view()->make($template, $values)->render();
	}

	function njupukHtml($template, array $values = [])
	{
		return parent::view()->make($template, $values)->render();
	}

	public function sebarno($key, $value)
	{
		$this->view()->share($key, $value);
	}

	public function shared($key, array $values = [])
	{
		$this->view()->share($key, $values);
	}
}
