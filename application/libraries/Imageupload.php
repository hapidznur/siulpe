<?php

class Imageupload {

  private $upload_path;
  private $config;

  public function __construct()
  {
    $this->ci =& get_instance();
  }

  private function checkAndResolvePath($upload_path)
  {
    $this->upload_path = $upload_path;
    if (!file_exists($upload_path) && !is_dir($upload_path)) {
      mkdir($upload_path, 0777, true);
    }
  }

  public function prepare($config)
  {
    $this->config = $config;
    $this->checkAndResolvePath($config['upload_path']);
  }

  public function do_upload($fieldName, $convert = true)
  {
    # save the name of file with prefix of large
    $originalName = $_FILES[$fieldName]['name'];
    if ($convert) {
      $this->config['file_name'] = "large-{$originalName}";
    }
    $this->ci->load->library('upload', $this->config);
    $upload = $this->ci->upload->do_upload($fieldName);

    if ($upload) {
      $imageName = $this->ci->upload->data()['file_name'];
      $image = new \Eventviva\ImageResize("{$this->upload_path}/{$imageName}");

      if ($convert) {
        # convert to minimal width 800px
        $image->resizeToWidth(800);
        $image->save("{$this->upload_path}/{$imageName}");
        # conver to large
        $image->resizeToWidth(400);
        $image->save("{$this->upload_path}/medium-{$imageName}");
        # convert to width 250px
        $image->resizeToWidth(250);
        $image->save("{$this->upload_path}/small-{$imageName}");
      }
    }

    return $upload;
  }

  public function data()
  {
    return $this->ci->upload->data();
  }
}
