<?php 
class Aksa_pagination {
	
	function __construct(){
		$this->ci =&get_instance();
		$this->ci->load->library('pagination');
	}

	function paginate($url, $uri_segment, $per_page, $number_row){

		# iki nggolek onok pirang page
		# misal onok 
		$total_rows = ceil($number_row/$per_page);

		$config = [
			'base_url' => $url,
			'total_rows' => $total_rows,
			'per_page' => 1,
			'uri_segment' => $uri_segment,
			'first_link' => 'First',
			'last_link' => 'Last',
			'next_link' => 'Next',
			'prev_link' => 'Prev',
			// 'next_link' => '<span class="glyphicon glyphicon-chevron-right"></span>',
			// 'prev_link' => '<span class="glyphicon glyphicon-chevron-left"></span>',
			'cur_tag_open' => '<li class="active"><a href="#">',
			'cur_tag_close' => '</a></li>',
			'num_tag_open' => '<li>',
			'num_tag_close' => '</li>',
			'first_tag_open' => '<li>',
			'first_tag_close' => '</li>',
			'last_tag_open' => '<li>',
			'last_tag_close' => '</li>',

			'prev_tag_open' => '<li>',
			'prev_tag_close' => '</li>',

			'next_tag_open' => '<li>',
			'next_tag_close' => '</li>'
		];

		$this->ci->pagination->initialize($config);
		return $this->ci->pagination->create_links();

	}
}