<?php

use \PHPExcel\Classes\PHPExcel\IOFactory;

class Spreadsheet
{
  
  public function read($inputFileName){

    try {
      $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
      $objReader = PHPExcel_IOFactory::createReader($inputFileType);
      $objPHPExcel = $objReader->load($inputFileName);
    } catch (Exception $e) {
      die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . 
          $e->getMessage());
    }

    $sheet = $objPHPExcel->getSheet(0);
    $highestRow = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();

    $data = array();

    $cell = $sheet->getCell('A2');
    
    $referenceRow=array();

    if (!$sheet->getCell('A2')->isInMergeRange() || $sheet->getCell('A2')->isMergeRangeValueCell()) {
      // Cell is not merged cell
      // $data[$row][$col] = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow( $col, $row )->getCalculatedValue();
      echo "sfasstring";
      // $referenceRow[$col]=$data[$row][$col];  
      //This will store the value of cell in $referenceRow so that if the next row is merged then it will use this value for the attribute
    } else {
      echo "string";
      // Cell is part of a merge-range
      // $data[$row][$col]=$referenceRow[$col];  
      //The value stored for this column in $referenceRow in one of the previous iterations is the value of the merged cell
    } 

    foreach ($sheet->getMergeCells() as $cells) {
        if ($cell->isInRange($cells) == 'A2:A3') {
            echo $cell->getRangeMerge();
            // break;
            var_dump($sheet->getCell('A2')->isMergeRangeValueCell());

            // var_dump($cell->getMergeCells());
        }
    }

    for ($row = 2; $row <= $highestRow; $row++) { 
      $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, 
                                      null, true, false);
      array_push($data, $rowData);
    }
    return $data;
  }

}