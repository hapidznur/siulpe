<?php

/**
 * This class provides some implementations to upload
 * a favicon
 */
class Favicon
{

  public function __construct()
  {
    $this->ci =& get_instance();
    $this->ci->load->library('imageupload');
  }

  public function uploadFavicon($fieldName)
  {
    # first I need to clear all the files on assets/favicon
    if (!empty($_FILES['favicon']['name'])) {
			$files = glob('assets/favicon/*'); // get all file names
			foreach($files as $file){ // iterate files
				if(is_file($file)){
					unlink($file); // delete file
				}
			}
		}

    # then, upload it using imgupload library
    $this->ci->imageupload->prepare([
			'upload_path'	=>	'assets/favicon',
			'allowed_types'	=>	'jpg|png',
			'max_size'	=> 3000
		]);

		if ($this->ci->imageupload->do_upload($fieldName, false)) {
			# yes, the process is working!
      # i just wanna write a comments here, not a statement :P
      # hehe
      return true; # okay, this is for checking only
		}
		else {
      # if this is errors, just return the errors array
			$errors = $this->ci->upload->display_errors();
			return compact('errors');
		}
  }
}
