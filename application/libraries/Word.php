<?php
/**
* 
*/
class Word
{
	
	function Create_informasi_harga(array $data = [])
	{

		$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('../public/docs/templates/Permintaan-Informasi-harga.docx');

		$paket_name = '<B>'.$data['name'].'</B>';
		$templateProcessor->setValue('NAME', htmlspecialchars('hasna'));

		$templateProcessor->setValue('PROCUREMENT',$paket_name);
		$templateProcessor->setValue('DATEDUE', $data['date_due']);
		$templateProcessor->setValue('PPK', $data['ppk_name']);
		$templateProcessor->setValue('LETTERDATE', $data['date_letter']);
		$templateProcessor->setValue('NIP', $data['nip']);
		$templateProcessor->setValue('DAY', $data['day']);
		$templateProcessor->setValue('HOURS', $data['hours']);
		$templateProcessor->setValue('LETTERNUMBER', $data['number_letter']);

		// echo date('H:i:s'), ' Saving the result document...', EOL;
		$filename = 'Surat Permintaan Informasi harga '.$data['name'];
		$foldername = str_replace(" ", "-", $data['name']);
		$path = '../public/docs/'.$foldername;

        if (!is_dir($path) && strlen($path)>0)
        {
            mkdir($path,0777);
		}
		$templateProcessor->saveAs('../public/docs/'.$foldername.'/'.$filename.'.docx');

		// echo getEndingNotes(array('Word2007' => 'docx'));

	}
}