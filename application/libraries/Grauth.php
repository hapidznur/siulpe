<?php

class Grauth {

  public function __construct()
  {
    $this->ci =& get_instance();
  }

	public function check()
	{
    # check for the login status
		if ($this->ci->session->currentUser) {
			return true;
		}

		return false;
	}

	public function checkAdmin()
	{
		# check for the login status of admin
		if ($this->ci->session->currentAdmin) {
			return true;
		}

		return false;
	}

	public function attempt($email, $password, $role = "user")
	{
	    # login
		if ($role == 'penyedia') 
		{
			$penyedia = PenyediaORM::orWhere('email', $email)
									->orWhere('username', $email)
									->first();
			if(!$penyedia){
				$message = "email dan username tidak tersedia";
			}
			elseif ($penyedia && password_verify($password, $penyedia->password)) 
			{
				$key = 'penyedia';
				$this->ci->session->set_userdata($key, $penyedia->id_penyedia);
				return true;
			}
		}
		elseif ($role == 'ppk' OR $role == 'ulp') 
		{
			$user = UsersORM::Where('role', $role)
							->Where(function($query) use ($email)
				            {
				                $query->where('email', $email)
				                      ->orwhere('username',$email);
				            })
							->first();
			if(!$user){
				$message = "email dan username salah";
			}
			elseif ($user && password_verify($password, $user->password))
			{
				var_dump(password_verify($password, $user->password));
				$key = $role;
				$this->ci->session->set_userdata($key, $user->id_user);
				return true;
			}
		}
		else{
			$message = "email dan username tidak tersedia";
	        $this->ci->session->set_flashdata('error_message', $message);
    	    return false;
        }
		return false;
	}

	/**
	 * Set session user
	 */
	public function auth_user($user, $role = "penyedia")
	{
		if ($role == 'penyedia') {
			$key = 'penyedia';
		}
		elseif($role == "ppk") {
			$key = 'ppk';
		}
		elseif($role == "ulp"){
			$key = 'ulp';			
		}
		$this->ci->session->set_userdata($key, $user->id_user);

		return true;
	}

	function check_ppk(){

		if ($this->ci->session->ppk) {
			return true;
		}
		return false;
	}

	# get the current user
	public function penyedia()
	{
		if ($this->check()) {
			$user = PenyediaORM::find($this->ci->session->penyedia);

			if (!$user) {
				return $this->logout();
			}

			return $user;
		}
		return null;
	}

	# get the current user
	public function ppk()
	{
		if ($this->check()) {
			$user = UsersORM::find($this->ci->session->penyedia);

			if (!$user) {
				return $this->logout();
			}

			return $user;
		}
		return null;
	}

	# get the current user
	public function ulp()
	{
		if ($this->check()) {
			$user = UsersORM::find($this->ci->session->penyedia);

			if (!$user) {
				return $this->logout();
			}

			return $user;
		}
		return null;
	}

	# get the current admin
	public function admin()
	{
		if ($this->checkAdmin()) {
			$user = UserORM::find($this->ci->session->currentAdmin);

			if (!$user) {
				return $this->logout();
			}

			return $user;
		}
		return null;
	}

	public function logout()
	{
		if ($this->check()) {
			$this->ci->session->unset_userdata('currentUser');
		}
	}

	public function logoutAdmin()
	{
		if ($this->checkAdmin()) {
			$this->ci->session->unset_userdata('currentAdmin');
		}
	}

}
