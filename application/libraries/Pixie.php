<?php

class Pixie {

	public function __construct()
	{
		$config = array(
            'driver'    => 'mysql', // Db driver
            'host'      => $_ENV['DB_HOST'],
            'database'  => $_ENV['DB_NAME'],
            'username'  => $_ENV['DB_USERNAME'],
            'password'  => $_ENV['DB_PASSWORD'],
            'charset'   => 'utf8', // Optional
            'collation' => 'utf8_unicode_ci', // Optional
        );

		new \Pixie\Connection('mysql', $config, 'Query');
	}
}
