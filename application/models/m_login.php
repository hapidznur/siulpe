<?php 
/**
* 
*/
class M_login extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }
  function getUser($username, $keycard, $role)
  {
   $this->db->select('*');
   $this->db->from('user');
   $this->db->where('usename', $username);
   $this->db->where('keycard', $keycard);
   $this->db->where('role', $role);
   $query = $this->db->get();

   return $query;
 }
 public function dataPengguna($username)
 {
   $this->db->select('username');
   $this->db->select('nama');
   $this->db->where('username', $username);
   $query = $this->db->get('user');
   
   return $query->row();
 }
 public function get_id_user()
 {
  $query = $this->db->query("SELECT
    `user`.id_user
    FROM
    `user`
    WHERE
    `user`.role = 'penyedia'
    ");
  return $query->result_array();
}

public function insert_user_role_penyedia($registered_on_user)
{
  $this->db->insert('user',$registered_on_user);
}

public function insert_user_on_penyedia($registered_on_penyedia)
{
  $this->db->insert('penyedia',$registered_on_penyedia);
}

}


?>