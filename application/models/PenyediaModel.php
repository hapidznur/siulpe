<?php

/*
* This is generated file by aksamedia
*/

class PenyediaModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->table = 'this-should-be-your-table-name';
		$this->load->database();
	}

	function create(array $values) {

		$penyedia = new PenyediaORM;
		$penyedia->nama_penyedia = $this->input->post('namapenyedia');
		$penyedia->pemilik = $this->input->post('pemilik');
		$penyedia->alamat = $this->input->post('alamat');
		$penyedia->contact = $this->input->post('contact');
		$penyedia->email = $this->input->post('email');

		// $penyedia->npwp = $this->input->post('npwp');
		// $penyedia->no_npwp = $this->input->post('no_npwp');
		// $penyedia->pkp = $this->input->post('pkp');
		// $penyedia->siup = $this->input->post('siup');
		// $penyedia->akta_notaris = $this->input->post('akta_notaris');

		$token = str_rand(100);
		$penyedia->email_token = $token;
		$penyedia->verifikasi = "0";
		$penyedia->password = password_hash($this->input->post('password'), PASSWORD_BCRYPT);
		$penyedia->save();

		// $this->load->library('aksamailer');
		// $this->aksamailer->addAddress($penyedia->email, $penyedia->nama_penyedia);     // Add a recipient
		// $this->aksamailer->Body = blade()->njupukHtml('emails.confirmation', compact('token'));
		// $this->aksamailer->Subject = "Register Confirmation";
		// $this->aksamailer->send();

		return $penyedia;
	}

	function getAll() {
		return $this->db->get($this->table)->result();
	}

	function update(array $values, array $conditions) {
		$this->db->update($this->table, $values, $conditions);
	}

	function delete($conditions) {
		$this->db->delete($this->table, $conditions);
	}

}

