<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
*/
class M_verifikasi extends CI_model
{

  function __construct()
  {
    $this->load->database();
  }
  public function get_verifikasi(){
    $this->db->select();
    $this->db->from('verifikasi');
    $this->db->group_by('paket_name');
    $query = $this->db->get();
    return $query->result_array();
  }

  function data_penyedia_baru()
  {
    $query = $this->db->query("SELECT
      penyedia.id_penyedia,
      penyedia.id_user,
      penyedia.nama_penyedia,
      penyedia.pemilik,
      penyedia.alamat,
      penyedia.email,
      penyedia.contact,
      penyedia.no_telepon,
      penyedia.no_npwp,
      penyedia.npwp,
      penyedia.pkp,
      penyedia.siup,
      penyedia.akta_notaris,
      penyedia.verifikasi
      FROM
      penyedia
      WHERE
      penyedia.verifikasi = 'Tidak'
      ");
    return $query->result_array();
  }
  function data_detail_penyedia_baru($id_penyedia)
  {
    $query = $this->db->query("SELECT
      penyedia.id_penyedia,
      penyedia.id_user,
      penyedia.nama_penyedia,
      penyedia.pemilik,
      penyedia.alamat,
      penyedia.email,
      penyedia.contact,
      penyedia.no_telepon,
      penyedia.no_npwp,
      penyedia.npwp,
      penyedia.pkp,
      penyedia.siup,
      penyedia.akta_notaris,
      penyedia.verifikasi
      FROM
      penyedia
      WHERE
      penyedia.verifikasi = 'Tidak' AND
      penyedia.id_penyedia =".$id_penyedia);
    return $query->result_array();
  }

  function update_penyedia_verifikasi($id_penyedia)
  {
    $kolom_verifikasi= array('verifikasi' => 'Ya');
    $this->db->where('id_penyedia',$id_penyedia);
    $this->db->update('penyedia', $kolom_verifikasi);
  }

  function update_verifikasi_informasi_harga($id_penawaran)
  {
    $kolom_verifikasi= array('verifikasi' => 'Ya');
    $this->db->where('id_penawaran',$id_penawaran);
    $this->db->update('penawaran', $kolom_verifikasi);
  }
  
}