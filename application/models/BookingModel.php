<?php

/*
* This is generated file by aksamedia
*/

class BookingModel extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->model('openTripModel');
	}

	public function getCount()
	{
		return BookingORM::has('openTrip')->count();
	}

	function insert($openTripId, array $values, $dariAdmin = false)
	{
		$values = (object) $values;

		$booking = new BookingORM;
		$booking->open_trip_id = $openTripId;
		$booking->jumlah_peserta = $values->jumlah_peserta;
		$booking->nama = $values->nama;
		$booking->email = $values->email;
		$booking->no_tlp = $values->no_tlp;
		$booking->alamat = $values->alamat;
		$booking->nama_peserta = json_encode($values->nama_peserta);

		# periksa apakah open trip tersebut statusnya pasti berangkat
		# jika pasti berangkat, maka invoice nya langsung dikirim, jika tidak
		# maka tidak dikirim dulu. Nanti dikirimnya
		# jika tidak, hanya dapet email info booking
		$openTrip = $this->openTripModel->getById($openTripId);
		$createInvoice = false;
		if ($openTrip->status == 'pasti_berangkat') {
			$booking->status_invoice = 'terkirim';
			$createInvoice = true; # this will create invoice when the booking id is already inserted
		}
		else {
			$booking->status_invoice = 'belum_terkirim';
		}

		$booking->save();

		if ($openTrip->status == 'pendaftaran') {
			# kirim email pemberitahuan
			$this->load->library('aksamailer');
			$this->aksamailer->addAddress($booking->email, $booking->nama);     // Add a recipient
			$this->aksamailer->Body = blade()->njupukHtml('emails.invoice-pending', compact('openTrip', 'values', 'booking'));
			$this->aksamailer->Subject = "Pemesanan {$booking->openTrip->masterTrip->title}";
			$this->aksamailer->send();
		}
		
		# jika ternyaa status open trip pasti berangkat, maka kirim invoice
		if ($createInvoice) {
			# panggil invoice model, bikin invoice baru untuk id booking ini
			$this->load->model('invoiceModel');
			$invoice = $this->invoiceModel->create($booking);
		}

		return $booking;
	}

	public function update($id, array $values)
	{
		$values = (object) $values;
		$booking = $this->getById($id);
		$booking->jumlah_peserta = $values->jumlah_peserta;
		$booking->nama = $values->nama;
		$booking->email = $values->email;
		$booking->no_tlp = $values->no_tlp;
		$booking->alamat = $values->alamat;
		$booking->nama_peserta = json_encode($values->nama_peserta);
		$booking->save();
	}

	public function getAll()
	{
		return BookingORM::has('openTrip')->desc()->get();
	}

	public function getById($id)
	{
		$booking = BookingORM::with('invoice')->with('openTrip')->where('id', $id)->first();
		# load master trip
		$booking->openTrip->masterTrip;
		# load pembayaran invoice juga
		if ($booking->invoice) {
			$booking->invoice->confirmations;
		}
		return $booking;
	}

	public function delete($id)
	{
		$booking = BookingORM::find($id);
		# delete invoice dan segala pembayarannya
		if ($booking->invoice) {
			$booking->invoice->confirmations()->delete();
			$booking->invoice->delete();
		}
		return $booking->delete();
	}
}
