<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
*@author hapidznur
*/
class M_dtl_penawaran extends CI_model
{

  function __construct()
  {
  $this->load->database();
  }

  function get_dtl_pnawaran($id_penawaran){
    $this->db->select();
    $this->db->from('detail_penawaran');
    $query=$this->db->get();
    return $query->result_array();
  }

  function insert_detail($data){
    $this->db->insert('detail_penawaran',$data);
    // $this->db->_error_message();
  }

}