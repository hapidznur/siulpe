<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*@author hapidznur
*/
class M_penawaran extends CI_model
{

  function __construct()
  {
    $this->load->database();
  }
  public function insert_penawaran($data){

    // $db_debug = $this->db->db_debug; //save setting
    // $this->db->db_debug = FALSE; //disable debugging for queries
    // $this->db->db_debug = $db_debug;
    $result = $this->db->insert('penawaran',$data);
    // $duplicate = $this->db->_error_number();
    // if ($duplicate = 1062) {
    //   $title = "Informasi Harga";
    //   $heading = " Informasi Harga";
    //   $message = "Maaf anda sudah melakukan submit informasi harga";
    //   $this->load->library('siulpe_exception');
    //   echo $this->siulpe_exception->display_error($title, $heading, $message);
    // }
  }

  public function get_penawaran($id_pengadaan){
    $this->db->select('penyedia.id_penyedia,penyedia.nama_penyedia, pengadaan.id_pengadaan, penawaran.id_penawaran, penawaran.surat_informasi_harga, penawaran.total_harga');
    $this->db->from('penawaran');
    $this->db->join('pengadaan','penawaran.id_pengadaan = pengadaan.id_pengadaan');
    $this->db->join('penyedia', 'penawaran.id_penyedia = penyedia.id_penyedia');
    $this->db->where('penawaran.id_pengadaan', $id_pengadaan);
    $this->db->group_by('nama_penyedia');
    $query = $this->db->get();
    return $query->result();
  }

  public function get_rekanan_status($id_pengadaan){
    $this->db->select('penawaran.rekanan_status');
    $this->db->from('penawaran');
    $this->db->where('penawaran.id_pengadaan', $id_pengadaan);
    $query = $this->db->get();
    return $query->row();
  }

    /**
     * Method Mengambil Pengadaan dengan 'pengadaan.status'= ''
     * @author hapidznur
     */
    function get_pengadaan($status){
      $this->db->select('
        penawaran.id_pengadaan,
        pengadaan.lokasi,
        pengadaan.anggaran,
        pengadaan.tahun,
        pengadaan.paket_name,
        hps.HPS,
        user_ppk.PPK_NAME');
      $this->db->from('penawaran');
      $this->db->join('pengadaan','penawaran.id_pengadaan = pengadaan.id_pengadaan');
      $this->db->join('user_ppk ',' pengadaan.id_ppk = user_ppk.id_ppk');
      $this->db->join('hps' , 'hps.id_pengadaan = pengadaan.id_pengadaan');
      $this->db->where('pengadaan.status_pengadaan',$status);
      $this->db->group_by('pengadaan.paket_name');
      $query = $this->db->get();
      return $query;
    }

    /**
      * @author hapidznur
     */
    function get_penyedia($id_pengadaan){
      $this->db->select('penyedia.nama_penyedia,penawaran.id_penyedia,
        penawaran.id_pengadaan');
      $this->db->from('penawaran');
      $this->db->join('penyedia','penawaran.id_penyedia = penyedia.id_penyedia');
      $this->db->where('penawaran.id_pengadaan',$id_pengadaan);
      $this->db->group_by('nama_penyedia');
      $query = $this->db->get();
      return $query->result();
    }

    function surat_penawaran($id_penyedia, $id_pengadaan){
      $this->db->select('penawaran.surat_penawaran');
      $this->db->from('penawaran');
      $this->db->where('id_pengadaan', $id_pengadaan);
      $this->db->where('id_penyedia',$id_penyedia);
      $query = $this->db->get();
      return $query->result_array();
    }

    function penawaran_penyedia($id_penyedia){
      $this->db->select('penawaran.id_penyedia,penawaran.id_pengadaan,pengadaan.paket_name,pengadaan.status_pengadaan,penawaran.rekanan_status,penawaran.verifikasi');
      $this->db->from('penawaran');
      $this->db->join('pengadaan','penawaran.id_pengadaan = pengadaan.id_pengadaan');
      $this->db->where('penawaran.id_penyedia',$id_penyedia);
      $query = $this->db->get();
      return $query->result_array();
    }

    function detail_tawaran_penyedia($id_penawaran, $id_pengadaan)
    {
      $query = $this->db->query("SELECT DISTINCT
        penyedia.nama_penyedia,
        pengadaan.paket_name,
        pengadaan.lokasi,
        penawaran.verifikasi,
        penawaran.id_penawaran,
        detail_penawaran.spesifikasi_rekanan,
        detail_penawaran.harga_tawar,
        detail_penawaran.jumlah_tawaran,
        barang.spesifikasi,
        penyedia.id_penyedia,
        barang.nama_barang,
        penawaran.surat_penawaran
        FROM
        detail_penawaran
        INNER JOIN penawaran ON detail_penawaran.id_penawaran = penawaran.id_penawaran
        INNER JOIN barang ON detail_penawaran.id_barang = barang.id_barang
        INNER JOIN penyedia ON penawaran.id_penyedia = penyedia.id_penyedia
        INNER JOIN pengadaan ON penawaran.id_pengadaan = pengadaan.id_pengadaan
        WHERE
        detail_penawaran.id_barang = barang.id_barang
        AND detail_penawaran.id_penawaran = ".$id_penawaran."
        AND penawaran.id_penyedia = penyedia.id_penyedia
        AND penawaran.id_pengadaan = ".$id_pengadaan."
        AND pengadaan.id_pengadaan = penawaran.id_pengadaan");
    return $query->result_array();
}

}