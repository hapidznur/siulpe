<?php

use Illuminate\Database\Capsule\Manager as DB;

/*
* This is generated file by aksamedia
*/
class OpenTripORM extends Aksa_Orm {

	protected $table = 'oyisam_open_trips';
	protected $hidden = ['harga', 'status'];
	protected $appends = ['covers', 'cover', 'kode', 'price', 'status_open_trip', 'quota_detail', 'tanggal_pulang', 'quota_masuk', 'sisa_quota', 'sisa_hari', 'is_closed', 'is_belum_berangkat', 'tanggal_berangkat_manusia', 'tanggal_pulang_manusia'];

	public function masterTrip()
	{
		return $this->belongsTo('MasterTripORM', 'master_trip_id');
	}

	public function bookings()
	{
		return $this->hasMany('BookingORM', 'open_trip_id');
	}

	public function getCoversAttribute()
	{
		return $this->masterTrip->covers;
	}

	public function getCoverAttribute()
	{
		if (!$this->masterTrip->cover_src) {
			return 'http://nemanjakovacevic.net/wp-content/uploads/2013/07/placeholder.png';
		}

		// return base_url("assets/img/covers/{$this->covers[0]->src}");
		return $this->masterTrip->cover_src;
	}

	public function getJamBerangkatAttribute($value)
	{
		return $value ? substr($value, 0, -3) : '-';
	}

	public function getJamPulangAttribute($value)
	{
		return $value ? substr($value, 0, -3) : '-';
	}

	public function getKodeAttribute()
	{
		$masterTripId = $this->master_trip_id;
		while (strlen($masterTripId) < 3) {
			$masterTripId = "0{$masterTripId}";
		}

		$id = $this->id; # tapi kudu dikurangi sama first id dari open-trip ini terus ditambah satu, kalau bingung hubungi 0877 0281 4646
		$firstOpenTripOfThisMasterTrip = $this->masterTrip->openTrips()->first();
		$firstIdOfTheFirstOpenTrip = (int) $firstOpenTripOfThisMasterTrip->id;

		# sekarang, baru proses id-nya
		$id = $id - $firstIdOfTheFirstOpenTrip + 1;

		while (strlen($id) < 3) {
			$id = "0{$id}";
		}

		return "OT{$masterTripId}{$id}";
	}

	public function getSisaHariAttribute()
	{
		$now = \Carbon\Carbon::now();
		$tanggal_berangkat = \Carbon\Carbon::createFromFormat("m/d/Y", $this->tanggal_berangkat);
		return $now > $tanggal_berangkat ? -1 * $tanggal_berangkat->diffInDays($now) : $tanggal_berangkat->diffInDays($now);
	}

	public function getTanggalBerangkatManusiaAttribute()
	{
		return \Carbon\Carbon::createFromFormat('m/d/Y', $this->tanggal_berangkat)->toFormattedDateString();
	}

	public function getTanggalPulangManusiaAttribute()
	{
		return \Carbon\Carbon::createFromFormat('m/d/Y', $this->tanggal_pulang)->toFormattedDateString();
	}

	public function getJatuhTempoPembayaranAttribute($value)
	{
		return date_reverse($value, '-', '/', true);
	}

	public function getJatuhTempoPembayaranManusiaAttribute()
	{
		return \Carbon\Carbon::createFromFormat('m/d/Y', $this->jatuh_tempo_pembayaran)->toFormattedDateString();
	}

	public function getTanggalBerangkatAttribute($value)
	{
		return date_reverse($value, '-', '/', true);
	}

	public function getTanggalPulangAttribute()
	{
		$tanggal_pulang = \Carbon\Carbon::createFromFormat('m/d/Y', $this->tanggal_berangkat)->addDay($this->durasi);

		return $tanggal_pulang->month . "/" . $tanggal_pulang->day . "/" . $tanggal_pulang->year;
	}

	public function getStatusTutupOtomatisAttribute($value)
	{
		return date_reverse($value, '-', '/', true);
	}

	public function getStatusTutupOtomatisManusiaAttribute($value)
	{
		return \Carbon\Carbon::createFromFormat('m/d/Y', $this->status_tutup_otomatis)->toFormattedDateString();
	}

	public function getTanggalDeadlinePendaftaranAttribute()
	{
		$tanggal = $this->status_tutup_otomatis;
		if (!$tanggal) {
			$tanggal = $this->tanggal_berangkat;
		}
		return \Carbon\Carbon::createFromFormat('m/d/Y', $tanggal)->toFormattedDateString();
	}

	public function getPriceAttribute()
	{
		return "IDR. " . number_format($this->harga);
	}

	public function getQuotaDetailAttribute()
	{
		$sudahDibooking = $this->bookings()->lunas()->count();
		return "{$sudahDibooking}/{$this->quota}";
	}

	public function getQuotaMasukAttribute()
	{
		$jumlah = $this->bookings()->sum('jumlah_peserta');

		return $jumlah;
	}

	public function getIsClosedAttribute()
	{
		if ($this->status == 'ditutup' || $this->status == 'dibatalkan' || (!$this->is_belum_berangkat)) {
			return true;
		}

		return false;
	}

	public function getIsBelumBerangkatAttribute()
	{
		return $this->sisa_hari > 0;
	}

	public function getSisaQuotaAttribute()
	{
		return $this->quota - $this->quota_masuk;
	}

	public function getPastiBerangkatAttribute()
	{
		return $this->status === 'pasti_berangkat';
	}

	public function getStatusOpenTripAttribute()
	{
		$status = $this->status;

		switch ($status) {
			case 'pasti_berangkat':
			case 'pendaftaran':
			$status = "Open";
				break;
			case 'ditutup':
				$status = "Closed";
				break;
			default:
				# haha, sepurane aku nggak nggawe function ucwords
				break;
		}

		return $status;
	}

	public function scopeTopDestinations($query)
	{
		return $query->where('top_destination', 1);
	}

	public function scopePriceBetween($query, $min, $max)
	{
		$query->where('harga', '>=', $min);
		$query->where('harga', '<=', $max);
	}

	public function scopeStartDate($query, $startDate)
	{
		$query->where('tanggal_berangkat', '>=', date_reverse($startDate));
	}

	public function scopeEndDate($query, $endDate)
	{
		$query->where('tanggal_berangkat', '<=', date_reverse($endDate));
	}

	public function scopeWithClosed($query)
	{
		return $query->addSelect('oyisam_open_trips.*')->addSelect(DB::raw("case when status = 'ditutup' or status = 'dibatalkan' or tanggal_berangkat <= now() then 1 else 0 end as closed"));
	}

	public function scopeReadyDaftar($query)
	{
		return $query->where(function ($query) {
			$query->where('start_date_pendaftaran', '<=', \Carbon\Carbon::now())->orwhere('start_date_pendaftaran', null);
		});
	}

	public function scopeBelumBerangkat($query)
	{
		return $query->where('tanggal_berangkat', '>=', \Carbon\Carbon::now());
	}

	public function scopeBukanTopDestiantions($query)
	{
		return $query->where('top_destination', 0);
	}

	public function scopeDuration($query, $duration, $durationComparison = 'lt')
	{
		switch ($durationComparison) {
			case 'lt':
				$comparison = '<';
				break;
			case 'eq':
				$comparison = '=';
				break;
			case 'gt':
				$comparison = '>';
				break;
			default:
				$comparison = '<';
				break;
		}

		$query->where('durasi', $comparison, $duration);
	}

	public function scopeCategories($query, $categories = [])
	{
		return $query->whereHas('masterTrip', function ($query) use ($categories) {
			$query->categories($categories);
		});
	}

	public function scopeDestination($query, $destination)
	{
		$query->whereHas('masterTrip', function($query) use ($destination) {
			$query->search($destination);
		});
	}

	public function scopeProvinsi($query, $provinsi)
	{
		return $query->whereHas('masterTrip', function($query) use ($provinsi) {
			$query->provinsi($provinsi);
		});
	}

	public function scopeKota($query, $kota)
	{
		return $query->whereHas('masterTrip', function($query) use ($kota){
			$query->kota($kota);
		});
	}

	public function scopeSearch($query, $priceRange = null, $destination = null, $startDate = null, $endDate = null, $duration = null, $durationComparison = 'lt', $provinsi = null, $kota = null, $categories = [])
	{
		if ($priceRange) {
			$priceRange = explode(";", $priceRange);
			$min = $priceRange[0];
			$max = $priceRange[1];
			$query->priceBetween($min, $max);
		}

		if ($destination) {
			$query->destination($destination);
		}

		if ($startDate) {
			$query->startDate($startDate);
		}

		if ($endDate) {
			$query->endDate($endDate);
		}

		if ($startDate || $endDate) {
			$query->where('status', '!=', 'ditutup');
		}

		if ($duration) {
			$query->duration($duration, $durationComparison);
		}

		if ($provinsi && $provinsi != 'semua') {
			$query->provinsi($provinsi);
		}

		if ($kota && $kota != 'semua') {
			$query->kota($kota);
		}

		if ($categories) {
			if (!is_array($categories)) {
				$categories = [];
			}
			$query->categories($categories);
		}

		return $query;
	}
}
