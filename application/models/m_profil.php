<?php 
/**
* bagian ini digunakan untuk mendapatkan model untuk tampilan profile. 
*/
class M_profil extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}

	/*
	@author elfarqy
	digunakan untuk mengisi konten pada laman profil ppk
	 */
	function get_profile($id_user)
	{
		$query = $this->db->query("SELECT
			user_ppk.id_ppk,
			user_ppk.id_user,
			user_ppk.ppk_name,
			user_ppk.satker,
			user_ppk.nip
			FROM
			user_ppk
			WHERE
			user_ppk.id_user = ".$id_user."
			");
		return $query->result_array();
	}
}






?>