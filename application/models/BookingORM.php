<?php

/*
* This is generated file by aksamedia
*/

class BookingORM extends Aksa_Orm {

	protected $table = 'oyisam_bookings';
	protected $appends = ['kode', 'status_pembayaran', 'tanggal_booking'];

	public function openTrip()
	{
		return $this->belongsTo('OpenTripORM', 'open_trip_id');
	}

	public function invoice()
	{
		return $this->hasOne('InvoiceORM', 'booking_id');
	}

	public function scopeLunas($query)
	{
		return $query->whereHas('invoice', function($query) {
			$query->lunas();
		});
	}

	public function setProvinsiAttribute($value)
	{
		$this->attributes['provinsi'] = ucwords(strtolower($value));
	}

	public function setKotaAttribute($value)
	{
		$this->attributes['kota'] = ucwords(strtolower($value));
	}

	public function setKecamatanAttribute($value)
	{
		$this->attributes['kecamatan'] = ucwords(strtolower($value));
	}

	public function getKodeAttribute()
	{
		$id = $this->id;
		# untuk penjelasan, silakan lihat di file OpenTripORM, soalnya ini penjelasannya sama kayak itu

		$firstBookingOfTheOpenTrip = $this->openTrip->bookings()->first();
		$firstIdOfTheFirstBooking = (int) $firstBookingOfTheOpenTrip->id; # why i assign an (int) ? because it will convert null to be 0
		$id = $id - $firstIdOfTheFirstBooking + 1;
		
		# assign 0 
		while (strlen($id) < 3) {
			$id = "0{$id}";
		}

		return $this->openTrip->kode . $id;
	}

	public function getNamaPesertaAttribute($value)
	{
		if (!$value) {
			return [];
		}
		return json_decode($value);
	}

	public function getNamaPesertaSrcAttribute()
	{
		$str = '';
		if (!count($this->nama_peserta)) {
			return "-";
		}
		$value = $this->nama_peserta;

		foreach ($value as $key => $namaPeserta) {
			$str .= $namaPeserta;

			if ($key < count($value) - 1) {
				$str .= ", ";
			}
		}

		return $str;
	}

	public function getStatusPembayaranAttribute()
	{
		$invoice = $this->invoice;

		if ($invoice) {
			return $invoice->status_pembayaran;
		}

		return "Belum pasti berangkat";
	}

	public function getNoInvoiceAttribute()
	{
		if (!$this->invoice) {
			return "belum dikirim";
		}

		return $this->invoice->nomor_invoice;
	}

	public function getTotalHargaAttribute()
	{
		return $this->jumlah_peserta * $this->openTrip->harga;
	}

	public function getTanggalBookingAttribute()
	{
		$value = $this->created_at;

		return \Carbon\Carbon::createFromFormat("Y-m-d h:i:s", $value)->toFormattedDateString();
	}
}
