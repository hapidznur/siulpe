<?php

/*
* This is generated file by aksamedia
*/

class FacilityModel extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getById($id)
	{
		return FacilityORM::findOrFail($id);
	}

	function getAll() {
		return FacilityORM::desc()->get();
	}

	public function create(array $values)
	{
		$values = (object) $values;

		$facility = new FacilityORM;
		$facility->title = $values->title;
		$facility->description = $values->description;
		$facility->save();

		return $facility;
	}

	public function update($id, array $values)
	{
		$values = (object) $values;

		$facility = $this->getById($id);
		$facility->title = $values->title;
		$facility->description = $values->description;
		$facility->save();

		return $facility;
	}

	public function delete($id)
	{
		return FacilityORM::find($id)->delete();
	}

}
