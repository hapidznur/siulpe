<?php

/*
* This is generated file by aksamedia
*/
class OpenTripModel extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getById($id)
	{
		return OpenTripORM::find($id);
	}

	public function getAll()
	{
		return OpenTripORM::with('masterTrip', 'masterTrip.destinations', 'bookings')->desc()->get();
	}

	public function getCount()
	{
		return OpenTripORM::count();
	}

	public function getPagination($page, $rowPerPage = 5)
	{
		if (!$page) {
			$page = 1;
		}

		$priceRange = $this->input->get('price_range');
		$destination = $this->input->get('destination');
		$startDate = $this->input->get('start_date');
		$endDate = $this->input->get('end_date');
		$duration = $this->input->get('duration');
		$durationComparison = $this->input->get('duration_comparison');
		$provinsi = $this->input->get('provinsi');
		$kota = $this->input->get('kota');
		$categories = $this->input->get('categories');
		$month = $this->input->get('month') ? $this->input->get('month') : date('m');

		$openTrips = OpenTripORM::
												search($priceRange, $destination, $startDate, $endDate, $duration, $durationComparison, $provinsi, $kota, $categories)
												->withClosed()
												->with('masterTrip')
												->with('masterTrip.destinations')
												->with('bookings')
												->whereMonth('tanggal_berangkat', '=', $month)
												->take($rowPerPage)
												->skip(($page - 1) * $rowPerPage)
												->orderBy('closed');

		# sorting
		$sortBy = 'upcoming_trips';
		if ($sortBy) {
			$openTrips = $openTrips->join('oyisam_master_trips as mp', 'mp.id', '=', 'oyisam_open_trips.master_trip_id');
		}
		switch ($sortBy) {
			case 'name_asc':
				$openTrips = $openTrips->orderby('mp.title');
				break;
			case 'name_desc':
				$openTrips = $openTrips->orderby('mp.title', 'desc');
				break;
			case 'upcoming_trips':
				$openTrips = $openTrips->orderby('tanggal_berangkat')->belumBerangkat();
				break;
			case 'lowest_price':
				$openTrips = $openTrips->orderby('harga');
				break;
			default:
				$openTrips = $openTrips; # just do nothing
				break;
		}

		return $openTrips->get();
	}

	public function getNumberRowsOfSearch()
	{
		$priceRange = $this->input->get('price_range');
		$destination = $this->input->get('destination');
		$startDate = $this->input->get('start_date');
		$endDate = $this->input->get('end_date');
		$duration = $this->input->get('duration');
		$durationComparison = $this->input->get('duration_comparison');
		$provinsi = $this->input->get('provinsi');
		$kota = $this->input->get('kota');
		$categories = $this->input->get('categories');
		$month = $this->input->get('month') ? $this->input->get('month') : date('m');

		return $openTrips = OpenTripORM::
												search($priceRange, $destination, $startDate, $endDate, $duration, $durationComparison, $provinsi, $kota, $categories)
												->whereMonth('tanggal_berangkat', '=', $month)
												->count();
	}

	public function getTopDestinations()
	{
		$openTrips = OpenTripORM::belumBerangkat()->topDestinations()->desc()->get();

		return $openTrips->where('status_open_trip', 'Open')->take(8);
	}

	public function getYangBelumBerangkat()
	{
		return OpenTripORM::belumBerangkat()->desc()->get();
	}

	public function insert($masterTripId, array $values)
	{
		$values = (object) $values;
		$openTrip = new OpenTripORM;
		$openTrip->master_trip_id = $masterTripId;
		$openTrip->quota = $values->quota;

		# hitung durasinya
		$berangkat = \Carbon\Carbon::createFromFormat('m/d/Y', $values->tanggal_berangkat);
		$selesai = \Carbon\Carbon::createFromFormat('m/d/Y', $values->tanggal_pulang);
		$durasi = $berangkat->diffInDays($selesai);
		$openTrip->durasi = $durasi;
		$openTrip->harga = $values->harga;
		$openTrip->dp_minimum = $values->dp_minimum;
		$openTrip->status = $values->status;
		$openTrip->jam_berangkat = @$values->jam_berangkat;
		$openTrip->jam_pulang = @$values->jam_pulang;
		$openTrip->meeting_point = $values->meeting_point;
		@$openTrip->garansi_berangkat = $values->garansi_berangkat ? 1 : 0;
		@$openTrip->top_destination = $values->top_destination ? 1 : 0;
		$openTrip->angsuran_maksimum = $values->angsuran_maksimum;
		$openTrip->blog_link = $values->blog_link;
		$openTrip->jatuh_tempo_pembayaran = date_reverse($values->jatuh_tempo);
		$openTrip->tanggal_berangkat = date_reverse($values->tanggal_berangkat);
		$openTrip->status_tutup_otomatis = date_reverse($values->status_tutup_otomatis);
		// $openTrip->cover = $cover;
		$openTrip->save();
	}

	public function update($id, $masterTripId, array $values)
	{
		$values = (object) $values;
		$openTrip = $this->getById($id);
		$openTrip->master_trip_id = $masterTripId;
		$openTrip->quota = $values->quota;

		# hitung durasinya
		$berangkat = \Carbon\Carbon::createFromFormat('m/d/Y', $values->tanggal_berangkat);
		$selesai = \Carbon\Carbon::createFromFormat('m/d/Y', $values->tanggal_pulang);
		$durasi = $berangkat->diffInDays($selesai);
		$openTrip->durasi = $durasi;
		$openTrip->harga = $values->harga;
		$openTrip->dp_minimum = $values->dp_minimum;
		$openTrip->meeting_point = $values->meeting_point;

		# periksa apakah statusnya pasti-berangkat, dan status sebelumnya adalah pendaftaran
		$createInvoice = $openTrip->status === 'pendaftaran' && $values->status === 'pasti_berangkat';
		# periksa apakah statusnya dibatalkan
		$batalkan = $openTrip->status !== 'dibatalkan' && $values->status === 'dibatalkan';

		$openTrip->status = $values->status;
		$openTrip->jam_berangkat = @$values->jam_berangkat;
		$openTrip->jam_pulang = @$values->jam_pulang;
		$openTrip->blog_link = $values->blog_link;
		@$openTrip->garansi_berangkat = $values->garansi_berangkat ? 1 : 0;
		@$openTrip->top_destination = $values->top_destination ? 1 : 0;
		$openTrip->angsuran_maksimum = $values->angsuran_maksimum;
		$openTrip->jatuh_tempo_pembayaran = date_reverse($values->jatuh_tempo);
		$openTrip->tanggal_berangkat = date_reverse($values->tanggal_berangkat);
		$openTrip->status_tutup_otomatis = date_reverse($values->status_tutup_otomatis);
		$openTrip->save();

		# jika ternyata status open trip pasti berangkat, maka kirim invoice
		if ($createInvoice) {
			# panggil invoice model, bikin invoice baru untuk id booking ini
			$this->load->model('invoiceModel');
			$invoice = $this->invoiceModel->broadcast($openTrip);
		}
 		elseif ($batalkan) { # atau ternyata dibatalkan, maka kita perlu mengirim pemberitahuan kepada semua yang sudah daftar
 			# kirim email broadcast pemberitahuan pembatalan
			$this->load->library('aksamailer');
			foreach ($openTrip->bookings as $key => $booking) {
				$this->aksamailer->addAddress($booking->email, $booking->nama);     // Add a recipient
				$this->aksamailer->Body = blade()->njupukHtml('emails.pembatalan', compact('openTrip', 'booking'));
				$this->aksamailer->Subject = "Pembatalan Open Trip {$openTrip->masterTrip->title}";
				$this->aksamailer->send();
			}

 		}
	}

	public function getYangBelumBerangkatdanBukanTopDestinations()
	{
		return OpenTripORM::belumBerangkat()->bukanTopDestiantions()->desc()->get();
	}

	public function setTopDestination($openTripId)
	{
		$openTrip = $this->getById($openTripId);
		$openTrip->top_destination = 1;
		$openTrip->save();

		return $openTrip;
	}

	public function unsetTopDestination($openTripId)
	{
		$openTrip = $this->getById($openTripId);
		$openTrip->top_destination = 0;
		$openTrip->save();

		return $openTrip;
	}

	public function tambahKuota($id, $tambahan)
	{
		$openTrip = $this->getById($id);
		$openTrip->quota = $openTrip->quota + $tambahan;
		$openTrip->save();
	}

	public function delete($id)
	{
		$openTrip = OpenTripORM::find($id);
		#$openTrip->covers()->delete();
		$bookings = $openTrip->bookings;
		$this->load->model('bookingModel');
		foreach ($bookings as $key => $booking) {
			$this->bookingModel->delete($booking->id);
		}
		return $openTrip->delete();
	}

	public function tutupOtomatis()
	{
		$openTrips = OpenTripORM::where('status_tutup_otomatis', '<=', \Carbon\Carbon::now())
														->orwhere('tanggal_berangkat', '<=', \Carbon\Carbon::now())
														->get();

		foreach ($openTrips as $key => $openTrip) {
			$openTrip->status = "ditutup";
			$openTrip->save();
		}
	}
}
