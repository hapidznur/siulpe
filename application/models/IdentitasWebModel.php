<?php

/*
* This is generated file by aksamedia
*/

class IdentitasWebModel extends CI_Model {

	public function get()
	{
		return IdentitasWebORM::first();
	}

	public function create(array $values)
	{
		$identitasWeb = IdentitasWebORM::first();

		if (!$identitasWeb) {
			$identitasWeb = new IdentitasWebORM();
		}

		$config = array(
				'allowed_types' => 'jpg|jpeg|png',
				'upload_path' => 'assets/images',
				'max_size' => 900
			);
		$this->load->library('upload', $config);
		$uploadlogo = $this->upload->do_upload('logo');
		$data_logo = $this->upload->data();
		$foto_logo = $data_logo['file_name'];

		$uploadfavicon = $this->upload->do_upload('favicon');
		$data_favicon = $this->upload->data();
		$foto_favicon = $data_favicon['file_name'];

		$values = (object) $values;
		if ($uploadlogo) {
			@$identitasWeb->logo = $foto_logo ? $foto_logo : $identitasWeb->logo;
		}
		if ($uploadfavicon) {
			@$identitasWeb->favicon = $foto_favicon ? $foto_favicon : $identitasWeb->favicon;
		}
		@$identitasWeb->nama = $values->nama ? $values->nama : $identitasWeb->nama;
		@$identitasWeb->meta_deskripsi = $values->meta_deskripsi ? $values->meta_deskripsi : $identitasWeb->meta_deskripsi;
		@$identitasWeb->meta_keyword = $values->meta_keyword ? $values->meta_keyword : $identitasWeb->meta_keyword;
		@$identitasWeb->email = $values->email ? $values->email : $identitasWeb->email;
		@$identitasWeb->wa = $values->wa ? $values->wa : $identitasWeb->wa;
		@$identitasWeb->bbm = $values->bbm ? $values->bbm : $identitasWeb->bbm;
		@$identitasWeb->instagram = $values->instagram ? $values->instagram : $identitasWeb->instagram;
		@$identitasWeb->alamat = $values->alamat ? $values->alamat : $identitasWeb->alamat;
		@$identitasWeb->no_rekening = $values->no_rekening ? $values->no_rekening : $identitasWeb->no_rekening;
		@$identitasWeb->bank = $values->bank ? $values->bank : $identitasWeb->bank;
		@$identitasWeb->flickr = $values->flickr ? $values->flickr : $identitasWeb->flickr;
		@$identitasWeb->atas_nama = $values->atas_nama ? $values->atas_nama : $identitasWeb->atas_nama;
		@$identitasWeb->line = $values->line ? $values->line : $identitasWeb->line;
		@$identitasWeb->fb = $values->fb ? $values->fb : $identitasWeb->fb;
		@$identitasWeb->nomor_telepon = $values->nomor_telepon ? $values->nomor_telepon : $identitasWeb->nomor_telepon;
		@$identitasWeb->twitter = $values->twitter ? $values->twitter : $identitasWeb->twitter;
		@$identitasWeb->syarat_dan_ketentuan = $values->syarat_dan_ketentuan ? $values->syarat_dan_ketentuan : $identitasWeb->syarat_dan_ketentuan;
		$identitasWeb->save();

		return $identitasWeb;
	}

	public function gantiInstagramAccessToken($token)
	{
		if ($identitasWeb = IdentitasWebORM::first()) {
			$identitasWeb->instagram_access_token = $token;
			$identitasWeb->save();
		}
	}

	public function logoutInstagram()
	{
		if ($identitasWeb = IdentitasWebORM::first()) {
			$identitasWeb->instagram_access_token = null;
			$identitasWeb->save();
		}
	}
}
