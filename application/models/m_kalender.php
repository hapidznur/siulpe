<?php
/**
*@author hapidznur
*/
class M_kalender extends CI_model
{

  function __construct()
  {

  }
  function kalender($id_jadwal){
    $this->db->select();
    $this->db->from('kalender');
    $this->db->where('id_jadwal', $id_jadwal);
    $query=$this->db->get();
    return $query->result_array();
  }
function get_kalender($id_jadwal){
    $this->db->select();
    $this->db->from('kalender');
    $this->db->where('id_jadwal', $id_jadwal);
    $query=$this->db->get();
    return $query;
}
function get_kalender_rekanan($id_pengadaan){
    $this->db->select();
    $this->db->from('kalender');
    $this->db->where('id_pengadaan', $id_pengadaan);
    $query=$this->db->get();
    return $query;
}
  function get_proses($status_proses){
    $this->db->select('id_pengadaan, paket_name,'.$status_proses.',status');
    $this->db->from('kalender');
    $query=$this->db->get();
    return $query->result_array();
  }
  function get_status(){
    $this->db->select('status');
    $this->db->from('kalender');
    $query=$this->db->get();
    return $query->result_array();
  }
  function update_date($array, $id){
    $this->db->where('id_jadwal',$id);
    $this->db->update('penjadwalan',$array);
  }
  function kalender_pengadaan_sendiri_ppk($id_pengadaan)
  {
   $query = $this->db->query("SELECT
    pengadaan.paket_name,
    penjadwalan.id_jadwal,
    penjadwalan.id_pengadaan,
    penjadwalan.p_1_mulai,
    penjadwalan.p_1_akhir,
    penjadwalan.p_2_mulai,
    penjadwalan.p_2_akhir,
    penjadwalan.p_3_mulai,
    penjadwalan.p_3_akhir,
    penjadwalan.p_4_mulai,
    penjadwalan.p_4_akhir,
    penjadwalan.p_5_mulai,
    penjadwalan.p_5_akhir,
    penjadwalan.p_6_mulai,
    penjadwalan.p_6_akhir,
    penjadwalan.p_7_mulai,
    penjadwalan.p_7_akhir,
    penjadwalan.p_8_akhir,
    penjadwalan.p_8_mulai,
    penjadwalan.p_9_mulai,
    penjadwalan.p_9_akhir,
    penjadwalan.p_10_mulai,
    penjadwalan.p_10_akhir,
    penjadwalan.p_11_mulai,
    penjadwalan.p_12_mulai,
    penjadwalan.p_13_mulai,
    penjadwalan.p_14_mulai,
    penjadwalan.p_15_mulai,
    penjadwalan.p_16_mulai,
    penjadwalan.p_17_mulai,
    penjadwalan.p_18_mulai,
    penjadwalan.p_19_mulai,
    penjadwalan.p_20_mulai,
    penjadwalan.p_11_akhir,
    penjadwalan.p_12_akhir,
    penjadwalan.p_13_akhir,
    penjadwalan.p_14_akhir,
    penjadwalan.p_15_akhir,
    penjadwalan.p_16_akhir,
    penjadwalan.p_17_akhir,
    penjadwalan.p_18_akhir,
    penjadwalan.p_19_akhir,
    penjadwalan.p_20_akhir,
    pengadaan.status_pengadaan,
    pengadaan.no_pengadaan
    FROM
    penjadwalan
    INNER JOIN pengadaan ON penjadwalan.id_pengadaan = pengadaan.id_pengadaan
    WHERE
    penjadwalan.id_pengadaan = ".$id_pengadaan."
    AND penjadwalan.id_pengadaan = pengadaan.id_pengadaan");
return $query->result_array();
}
}