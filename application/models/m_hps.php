<?php
/**
*
*/
class M_hps extends CI_model
{

	function __construct()
	{
		$this->load->database();
	}
    /*
     ** Rewrite by @author hapidznur
     ** Rename from get_hps_table() to get_hps()
     */
//	function get_hps_table($id_user)
    function get_hps($id_user)
	{
		/*
			bagian ini seharusnya mendapat hasil dari session, cuma belum dirubah. untuk merubah tinggal update
			dibagian method dengan mendapatkan session. yaitu $check_session

		*/
        $this->db->select('hps.id_hps,hps.id_pengadaan,hps.ppn,hps.biaya,hps.hps,hps.submit,pengadaan.paket_name,user_ppk.id_ppk,user_ppk.id_user');
        $this->db->from('hps');
        $this->db->join('pengadaan', 'pengadaan.id_pengadaan=hps.id_pengadaan');
        $this->db->join('user_ppk', 'pengadaan.id_ppk = user_ppk.id_ppk');
        $this->db->where('user_ppk.id_user', $id_user);
//        $this->db->where('pengadaan.status_pengadaan', 'Undangan Pengadaan Langsung');
        $this->db->where('hps.submit', 0);
        $query = $this->db->get();
        return $query->result_array();
    }

		function detail_header_tabel_hps($id_pengadaan)
		{
			$exec = $this->db->query("SELECT
				penyedia.nama_penyedia,
				penawaran.total_harga
				FROM
				penawaran
				INNER JOIN penyedia ON penawaran.id_penyedia = penyedia.id_penyedia
				WHERE
				penawaran.id_penyedia = penawaran.id_penyedia AND
				penawaran.id_pengadaan = '".$id_pengadaan."'
				ORDER BY penawaran.total_harga ASC LIMIT 3");
			return $exec -> result_array();
		}
		function detail_barang_hps($id_pengadaan, $id_user){
			$exec = $this->db->query("SELECT
				barang.id_pengadaan,
				barang.nama_barang,
				detail_penawaran.harga_tawar,
				detail_penawaran.spesifikasi_rekanan,
				detail_penawaran.id_barang,
				detail_penawaran.id_penawaran,
				penawaran.id_penyedia,
				penawaran.id_pengadaan,
				penyedia.nama_penyedia
				FROM
				detail_penawaran
				INNER JOIN barang ON detail_penawaran.id_barang = barang.id_barang
				INNER JOIN penawaran ON detail_penawaran.id_penawaran = penawaran.id_penawaran
				INNER JOIN penyedia ON penawaran.id_penyedia = penyedia.id_penyedia
				INNER JOIN `user` ON penyedia.id_user = `user`.id_user
				WHERE
				detail_penawaran.id_barang = barang.id_barang
				AND barang.id_pengadaan = penawaran.id_pengadaan
				AND `user`.id_user = penyedia.id_user
				ORDER BY
				penyedia.nama_penyedia ASC

				");
			return $exec->result_array();
		}
		public function get_user_detail_barang_hps($id_pengadaan)
		{
			$exec = $this->db->query("SELECT
				user_ppk.satker,
				user_ppk.ppk_name,
				pengadaan.paket_name,
				pengadaan.lokasi,
				pengadaan.tahun
				FROM
				pengadaan
				INNER JOIN user_ppk ON pengadaan.id_ppk = user_ppk.id_ppk
				WHERE
				pengadaan.id_pengadaan = '".$id_pengadaan."'
				AND pengadaan.id_ppk = user_ppk.id_ppk");
			return $exec->result_array();
		}
		public function get_user_detail_hps($id_pengadaan)
		{
			$exec = $this->db->query("SELECT
				pengadaan.paket_name,
				pengadaan.anggaran,
				pengadaan.tahun,
				pengadaan.tanggal_dipa,
				pengadaan.mak,
				pengadaan.no_dipa
				FROM
				pengadaan
				WHERE
				pengadaan.id_pengadaan = '".$id_pengadaan."'");
			return $exec->result_array();
		}
		public function detail_barang_on_detail_hps($id_pengadaan)
		{
			$query = $this->db->query("SELECT DISTINCT
				barang.nama_barang,
				detail_penawaran.spesifikasi_rekanan,
				barang.satuan,
				barang.volume
				FROM
				detail_penawaran
				INNER JOIN barang ON detail_penawaran.id_barang = barang.id_barang
				WHERE
				detail_penawaran.id_barang = barang.id_barang AND
				barang.id_pengadaan = '".$id_pengadaan."'
				GROUP BY barang.nama_barang
				");
			return $query->result_array();
		}
		public function barang_dari_penawar_terendah_satu($penawar_satu)
		{
			$query = $this->db->query("SELECT
				barang.id_pengadaan,
				barang.nama_barang,
				detail_penawaran.harga_tawar,
				detail_penawaran.spesifikasi_rekanan,
				detail_penawaran.id_barang,
				detail_penawaran.id_penawaran,
				penawaran.id_penyedia,
				penawaran.id_pengadaan,
				penyedia.nama_penyedia
				FROM
				detail_penawaran
				INNER JOIN barang ON detail_penawaran.id_barang = barang.id_barang
				INNER JOIN penawaran ON detail_penawaran.id_penawaran = penawaran.id_penawaran
				INNER JOIN penyedia ON penawaran.id_penyedia = penyedia.id_penyedia
				INNER JOIN `user` ON penyedia.id_user = `user`.id_user
				WHERE
				detail_penawaran.id_barang = barang.id_barang
				AND barang.id_pengadaan = penawaran.id_pengadaan
				AND `user`.id_user = penyedia.id_user
				AND nama_penyedia = '".$penawar_satu."'
				ORDER BY
				penyedia.nama_penyedia ASC");
			return $query->result_array();
		}

	}


	?>