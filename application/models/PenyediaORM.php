<?php

/*
* This is generated file by aksamedia
*/

class PenyediaORM extends Aksa_Orm {

	protected $table = 'penyedia';
	protected $primaryKey = "id_penyedia";
	// protected $hidden = ['password', 'email_token'];
	public $timestamps = false;
	public $incrementing = false;

	public function scopeUnconfirmed($query)
	{
		return $query->where('verifikasi', 0);
	}

}

