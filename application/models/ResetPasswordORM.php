<?php

/*
* This is generated file by aksamedia
*/

class ResetPasswordORM extends Aksa_Orm {

	protected $table = 'reset_password';
	public $timestamps = false;

	public function user()
	{
		return $this->belongsTo('PenyediaORM', 'id_penyedia');
	}

	public function scopeFromUser($query, $id_user)
	{
		$query->where('id_penyedia', $id_user);
	}

	public function scopeToken($query, $token)
	{
		$query->where('password_token', $token);
	}

}
