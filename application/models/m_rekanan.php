    <?php
    /**
    * @Author: Al
    * @Date:   2015-10-22 06:19:47
    * @Last Modified by:   Al
    * @Last Modified time: 2015-11-20 10:03:55
    */
    class M_rekanan extends CI_model
    {
    /**;
    * Rekanan sementara
    */
    public function __construct()
    {
        $this->load->database();
    }

    function get_profil($id_penyedia){
        $this->db->from('penyedia');
        $this->db->where('id_penyedia', $id_penyedia);
        return $query= $this->db->get();
    }

    function get_undangan_penyedia($idpenyedia){
        $this->db->select('t_undangan.id_pengadaan,PAKET_NAME,hps');
        $this->db->from('t_undangan');
        $this->db->join('pengadaan', 't_undangan.id_pengadaan=pengadaan.id_pengadaan');
        $this->db->join('hps', 'pengadaan.id_pengadaan=hps.id_pengadaan');
        $this->db->where('t_undangan.id_penyedia',$idpenyedia);
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
    * @Author: Al
    * @Date:   2015-10-22 06:19:47
    * @Used for: digunakan untuk mengambil data login dari table user, dengan parameter username.
    */
    function get_penyedia($username){
        $query = $this->db->query("SELECT `user`.username, `user`.keycard, `penyedia`.id_penyedia ,`penyedia`.nama_penyedia FROM user INNER JOIN `penyedia` ON penyedia.id_user = `user`.id_user WHERE `user`.username = '".$username."' ;");
        return $query -> result_array();
    }

    public function update_kelengkapan($data, $id_penyedia)
    {
        $this->db->where('id_penyedia', $id_penyedia);
        $this->db->update('penyedia', $data);
    }

    public function last_id($id,$table){
        $query = $this->db->query("SELECT MAX(".$id.") AS ID FROM ".$table.";");
        // $query = $this->db->last_row();
        $row = $query -> row();
        return $row -> ID;
    }

    /**
    * [get_notification description]
    * @return [type] [description]
    */
    public function get_notification($username){
        $this->db-where("sent_to",$username);
    }
}