<?php
/**
*@author hapidznur
*/
class M_penjadwalan extends CI_model
{

  function __construct()
  {
     $this->load->database();
  }
  function get_kalender($id_jadwal){
    $this->db->select();
    $this->db->from('penjadwalan');
    $this->db->where('id_jadwal',$id_jadwal);
    $query = $this->db->get();
    return $query->result_array();
  }
  function get_element($proses, $id_jadwal){
    $this->db->select($proses);
    $this->db->from('penjadwalan');
    $this->db->where('id_jadwal',$id_jadwal);
    $query = $this->db->get();
    return $query->result_array();
  }
  function get_edit_penjadwalan($id, $no){
    $this->db->select('p_'.$no.'_mulai,p_'.$no.'_akhir');
    $this->db->from('penjadwalan');
    $this->db->where('id_jadwal',$id);
    $query = $this->db->get();
    return $query->result_array();
  }
  function update_jadwal($where, $data){
    $this->db->update('penjadwalan', $data, $where);
    return $this->db->affected_rows();
  }
  function get_name(){
      $query = $this->db->list_fields('penjadwalan');
   foreach ($query as $field)
  {
   echo $field;
  }
  }
}