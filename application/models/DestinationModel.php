<?php

/*
* This is generated file by aksamedia
*/

class DestinationModel extends CI_Model {

	public function create(array $values)
	{
		$values = (object) $values;

		$destination = new DestinationORM;
		$destination->nama = $values->nama;
		$destination->provinsi = $values->provinsi;
		$destination->kota = $values->kota;
		$destination->save();

		return $destination;
	}

	public function getAll()
	{
		return DestinationORM::desc()->get();
	}

	public function getById($id)
	{
		return DestinationORM::find($id);
	}

	public function update($id, array $values)
	{
		$values = (object) $values;

		$destination = $this->getById($id);
		$destination->nama = $values->nama;
		$destination->provinsi = $values->provinsi;
		$destination->kota = $values->kota;
		$destination->save();

		return $destination;
	}

	public function delete($id)
	{
		return DestinationORM::find($id)->delete();
	}
}
