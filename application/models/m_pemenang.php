<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* @author hapidznur
*/
class M_pemenang extends CI_model
{

    function __construct()
    {
        $this->load->database();
    }

    function get_pemenang($id_pengadaan)
    {
            $this->db->select('harga_negosiasi,tanggal_penawaran');
            $this->db->from('pemenang');
            $this->db->join('penyedia', 'pemenang.id_penyedia = penyedia.id_penyedia');
            $this->db->where('id_pengadaan', $id_pengadaan);
            $query = $this->db->get();
        // if ($query->result_array() != null)
        // {

            return $query->row();
        // }
        // else
        // {
        //     return "kosong";
        // }
    }
}