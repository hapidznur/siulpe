<?php

/*
* This is generated file by aksamedia
*/

class InvoiceConfirmationORM extends Aksa_Orm {

	protected $table = 'oyisam_invoice_confirmations';
	protected	$appends = [
		'total_transfer_str', 'status_pembayaran', 'bukti_transfer_src'
	];

	public function invoice()
	{
		return $this->belongsTo('InvoiceORM', 'invoice_id');
	}

	public function scopeSudahDikonfirmasi($query)
	{
		return $query->where('status', 'sudah dikonfirmasi');
	}

	public function scopeButuhDiKonfirmasi($query)
	{
		return $query->where('status', 'belum dikonfirmasi');
	}

	public function getTotalTransferStrAttribute()
	{
		return "IDR. " . number_format($this->total_transfer);
	}

	public function getTanggalPembayaranAttribute($value)
	{
		return \Carbon\Carbon::createFromFormat("Y-m-d", $value)->toFormattedDateString();
	}

	public function getStatusPembayaranAttribute()
	{
		return ucwords($this->status);
	}

	public function getBuktiTransferSrcAttribute()
	{
		return url("assets/img/bukti-pembayaran/{$this->bukti_transfer}");
	}

}
