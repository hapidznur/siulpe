<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*author hapidznur
*/
class M_barang extends CI_model
{

  function __construct()
  {
    $this->load->database();
  }

  function get_barang($id_pengadaan){
   $this->db->select('id_barang, barang.id_pengadaan, nama_barang, spesifikasi, satuan, volume');
   $this->db->from('barang');
   $this->db->join('pengadaan', 'barang.id_pengadaan=pengadaan.id_pengadaan');
   $this->db->where('pengadaan.id_pengadaan',$id_pengadaan);
   $query = $this->db->get();
   return $query->result();
 }

 function insert_pengadaan_id($data)
 {
  $query = $this->db->select('id_pengadaan')->from('barang')->where('id_pengadaan',$data)->get();
  if ($query->num_rows() > 0) {
   echo $data;
 }else{
  $query2 = ("INSERT INTO penjadwalan(`id_jadwal`,`status_proses`) values ($data,'Informasi harga')");
  $this->db->query($query2);
}
}
function insert_barang($dataarray)
{

  for($i=0;$i<count($dataarray['nama_barang']);$i++){

    $data = array(
      'id_pengadaan'=>$dataarray['id_pengadaan'],
      'id_barang'=>$dataarray['id_barang'].$i,
      'nama_barang'=>$dataarray['nama_barang'][$i],
      'spesifikasi'=>$dataarray['spesifikasi'][$i],
      'satuan'=>$dataarray['satuan'][$i],
      'volume'=>$dataarray['volume'][$i],
      'harga'=>$dataarray['harga'][$i],
      'jumlah_harga'=>$dataarray['jumlah_harga'][$i]
      );
    $this->db->insert('barang', $data);
  }
}
function edit_barang($id_pengadaan)
{
  $exec = $this->db->query("SELECT
    barang.id_barang,
    barang.id_pengadaan,
    barang.nama_barang,
    barang.spesifikasi,
    barang.satuan,
    barang.volume,
    barang.harga,
    barang.jumlah_harga,
    barang.merk
    FROM
    pengadaan
    INNER JOIN barang ON barang.id_pengadaan = pengadaan.id_pengadaan
    WHERE
    barang.id_pengadaan = ".$id_pengadaan."
    ");
  return $exec ->result_array();
}

function get_barang_params($id_pengadaan)
{
  $exec = $this->db->query("SELECT
    barang.id_barang,
    barang.id_pengadaan,
    barang.nama_barang,
    barang.spesifikasi,
    barang.satuan,
    barang.volume,
    barang.harga,
    barang.jumlah_harga,
    barang.merk
    FROM
    barang
    WHERE
    barang.id_pengadaan =".$id_pengadaan);
  return $exec->result_array();
}
  function get_spesifikasi($id_pengadaan, $id_barang){
    $query = $this->db->query("SELECT barang.spesifikasi from barang WHERE id_pengadaan=$id_pengadaan AND id_barang=$id_barang");
    return $query->row();
  }
}