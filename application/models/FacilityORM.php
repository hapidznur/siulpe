<?php

/*
* This is generated file by aksamedia
*/

class FacilityORM extends Aksa_Orm {

	protected $table = 'oyisam_facilities';

	public function openTrip()
	{
		return $this->belongsToMany('OpenTripORM', 'oyisam_open_trip_facility_pivot', 'facility_id');
	}
}
