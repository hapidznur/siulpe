<?php

/*
* This is generated file by aksamedia
*/

class MasterTripFacilityORM extends Aksa_Orm {

	protected $table = 'oyisam_master_trip_facility_pivot';
	public $timestamps = false;

	public function scopeMasterTrip($query, $id)
	{
		return $query->where('master_trip_id', $id);
	}

}
