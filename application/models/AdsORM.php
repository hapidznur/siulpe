<?php

/*
* This is generated file by aksamedia
*/

class AdsORM extends Aksa_Orm {

	protected $table = 'oyisam_ads';

	public function scopePosisi($query, $posisi)
	{
		return $query->where('posisi', $posisi);
	}

	public function scopeGambar($query)
	{
		return $query->where('jenis', 'gambar');
	}

	public function scopeScript($query)
	{
		return $query->where('jenis','script');
	}

	public function scopeAktif($query)
	{
		return $query->where('status', 1);
	}

	public function getImageSrcAttribute()
	{
		return $this->script;
		# salahe gak sido upload foto
		return url("assets/img/ads/{$this->script}");
	}

}
