<?php

/*
* This is generated file by aksamedia
*/

class ArticleModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->helper('aksa_helper');
	}

	function create(array $values,$covers) {

		$values = (object) $values;

		$artikel = new ArticleORM;
		$artikel->title =  $values->title;
		$artikel->figure = $covers;
		$artikel->seo_tag = seo($values->title);
		$artikel->article =  $values->deskripsi;
		$artikel->user =  $values->user;
		$artikel->save();
	}

	function getAll() {
		return ArticleORM::desc()->get();
	}

	function getByid($id){
		return ArticleORM::findOrFail($id);
	}

	function update($id, array $values, $covers) {
		$values = (object) $values;
		$artikel = ArticleORM::findOrFail($id);

		$artikel->title = $values->title;
		$artikel->figure = $covers ? $covers : $artikel->figure;
		$artikel->seo_tag = seo($values->title);
		$artikel->article =  $values->deskripsi;

		$artikel->save();
	}

	function delete($id) {
		ArticleORM::where('id', $id)->delete();
	}

	public function getRecommendedArticles($article)
	{
		return ArticleORM::recommended($article)->take(5)->get();
	}

	public function addComment($id, $values)
	{
		$values = (object) $values;

		$comment = new BlogCommentORM;
		$comment->article_id = $id;
		$comment->nama = $values->nama;
		$comment->email = $values->email;
		$comment->body = $values->comment;
		$comment->save();

		return $comment;
	}

	public function deleteComment($article_id, $comment_id)
	{
		return $this->getByid($article_id)->comments()->find($comment_id)->delete();
	}
}
