<?php

/*
* This is generated file by aksamedia
*/

class InvoiceModel extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->library('aksamailer');
	}

	public function getAll()
	{
		$invoices = InvoiceORM::has('booking')->with('booking')->desc()->get();
		return $invoices;
	}

	public function getCount()
	{
		return InvoiceConfirmationORM::has('invoice.booking')->count();
	}

	public function search($openTrip, $month, $year)
	{
		$openTrip = $openTrip == 'semua' ? null : $openTrip;
		$month = $month == 'semua' ? null : $month;
		$year = $year == 'semua' ? null : $year;

		return InvoiceORM::has('booking')->with('booking.openTrip')->openTrip($openTrip)->month($month)->year($year)->desc()->get();
	}

	public function getTotalMasuk($openTrip, $month, $year)
	{
		$openTrip = $openTrip == 'semua' ? null : $openTrip;
		$month = $month == 'semua' ? null : $month;
		$year = $year == 'semua' ? null : $year;

		return InvoiceConfirmationORM::sudahDikonfirmasi()->whereHas('invoice', function($query) use ($openTrip, $month, $year) {
			$query->has('booking')->with('confirmations')->openTrip($openTrip)->month($month)->year($year);
		})->sum('total_transfer');
	}

	public function create($booking)
	{
		$invoice = new InvoiceORM;
		$nomor_invoice = str_rand(10);
		while (InvoiceORM::invoice($nomor_invoice)->count() > 0) {
			$nomor_invoice = str_rand(10);
		}

		$invoice->nomor_invoice = $nomor_invoice;
		$invoice->booking_id = $booking->id;
		$invoice->total_harga = $booking->jumlah_peserta * $booking->openTrip->harga;
		$invoice->status = 'belum_dibayar';
		$invoice->save();

		$booking = $invoice->booking;

		# kirim invoice ke email
		$this->load->library('aksamailer');
		$this->aksamailer->addAddress($booking->email, $booking->nama);     // Add a recipient
		$this->aksamailer->Body = blade()->njupukHtml('emails.invoice', compact('booking', 'invoice'));
		$this->aksamailer->Subject = "Invoice {$booking->openTrip->masterTrip->title}";
		$this->aksamailer->send();

		return $invoice;
	}

	public function broadcast($openTrip)
	{
		foreach ($openTrip->bookings as $key => $booking) {
			if ($booking->invoice) {
				continue;
			}
			$this->create($booking);
			
			$booking->status_invoice = 'terkirim';
			$booking->save();
		}
	}

	public function addConfirmation($img_konfirmasi, array $values)
	{
		$values = (object) $values;
		# pertama, cari invoice-nya
		$invoice = InvoiceORM::invoice($values->nomor_invoice)->first();
		if (!$invoice) {
			return false;
		}

		# dan id nya masukkan ke table konfirmasi
		$invoiceConfirmation = new InvoiceConfirmationORM;
		$invoiceConfirmation->invoice_id = $invoice->id;
		$invoiceConfirmation->nama = $values->nama;
		$invoiceConfirmation->email = $values->email;
		$invoiceConfirmation->bank = $values->bank;
		$invoiceConfirmation->rekening_pengirim = $values->rekening_pengirim;
		$invoiceConfirmation->total_transfer = $values->total_transfer;
		$invoiceConfirmation->bukti_transfer = $img_konfirmasi;
		$invoiceConfirmation->status = "belum dikonfirmasi";
		$invoiceConfirmation->tanggal_pembayaran = date_reverse($values->tanggal_pembayaran);
		$invoiceConfirmation->save();

		# kirim invoice ke email
		$this->load->library('aksamailer');
		$this->aksamailer->addAddress($values->email, $values->nama);     // Add a recipient
		$this->aksamailer->Body = blade()->njupukHtml('emails.confirmation-payment-received', compact('invoiceConfirmation'));
		$this->aksamailer->Subject = "Konfirmasi Pembayaran Akan diproses";
		$this->aksamailer->send();

		return $invoiceConfirmation;
	}

	/**
	* this function is used to get all the payment confirmations data.
	*/
	public function getAllInvoiceConfirmations()
	{
		$invoiceConfirmations = InvoiceConfirmationORM::has('invoice.booking')->desc()->get();
		return $invoiceConfirmations;
	}

	public function getById($id)
	{
		return InvoiceORM::findOrFail($id);
	}

	public function getConfirmationById($id)
	{
		return InvoiceConfirmationORM::findOrFail($id);
	}

	public function konfirmasi($id)
	{
		$invoiceConfirmation = $this->getConfirmationById($id);
		$invoiceConfirmation->status = 'sudah dikonfirmasi';
		$invoiceConfirmation->save();

		# ubah status invoice-nya jadi diangsur, kalau jumlahnya masih kurang.
		$invoice = $invoiceConfirmation->invoice;
		# ambil total cicilan yang sudha dikonfirmasi admin
		$totalCicilan = $invoice->confirmations()->sudahDikonfirmasi()->sum('total_transfer');
		# bandingkan dengan harga di invoice tersebut
		if ($totalCicilan < $invoice->total_harga) {
			# jika iya, maka berarti status invoice-nya jadi diangsur
			# tapi periksa dulu, kalau masih baru bayar sekali, berarti dp
			if ($invoice->confirmations()->sudahDikonfirmasi()->count() == 1) {
				$invoice->status = 'dp';
			}
			else {
				$invoice->status = 'angsur';
			}
		}
		elseif ($totalCicilan === $invoice->total_harga) {
			# jika ternyata jumlahnya sama, berarti sudah lunas
			$invoice->status = 'lunas';
		}

		# update invoice
		$invoice->save();
		$booking = $invoice->booking;

		# kirim email pemberitahuan
		// $this->aksamailer->addAddress($invoiceConfirmation->email, $invoiceConfirmation->nama);     // Add a recipient
		// $this->aksamailer->Body = blade()->njupukHtml('emails.payment-confirmed', compact('invoiceConfirmation'));
		// $this->aksamailer->Subject = "Pembayaran Telah Kami Konfirmasi";
		// $this->aksamailer->send();

		# kirim email detail pembayaran yang perlu dibayar
		$this->aksamailer->addAddress($invoiceConfirmation->email, $invoiceConfirmation->nama);     // Add a recipient
		$this->aksamailer->Body = blade()->njupukHtml('emails.next_invoice_details', compact('invoice', 'booking'));
		$this->aksamailer->Subject = "Detail Invoice";
		$this->aksamailer->send();

		return $invoiceConfirmation;
	}

	public function delete($id)
	{
		$confirmation = $this->getConfirmationById($id);
		return $confirmation->delete();
	}

	public function hapusInvoiceYangBelumDiDPSampaiDenganBatasWaktu()
	{
		$now = \Carbon\Carbon::now();

		// hapus yang lebih dari 30 hari, lebih dari 3 x 24 jam dulu.
		$next30Hari = $now->copy()->addDay(30);
		$tigaX24jamYangLalu = $now->copy()->addHour(-3 * 24);
		$invoices = InvoiceORM::where('status', 'belum_dibayar')->where('created_at', '<', $tigaX24jamYangLalu)->whereHas('booking', function($query) use ($next30Hari) {
			$query->whereHas('openTrip', function($query) use ($next30Hari){
				$query->where('tanggal_berangkat', '>=', $next30Hari);
			});
		})->get(); # ->delete();

		foreach ($invoices as $key => $invoice) {
			$this->kirimEmailPemberitahuanInvoiceTelahTerlambatDibayarkan($invoice->booking);
			$invoice->booking->delete();
			$invoice->confirmations()->delete();
			$invoice->delete();
		}

		// terus hapus yang kurang dari 30 hari, kurang dari 1 x 24 jam.
		$satuX24jamYangLalu = $now->copy()->addHour(-1 * 24);
		$invoicesKurangDari30Hari = InvoiceORM::where('status', 'belum_dibayar')->where('created_at', '<', $satuX24jamYangLalu)->whereHas('booking', function($query) use ($next30Hari) {
			$query->whereHas('openTrip', function($query) use ($next30Hari){
				$query->where('tanggal_berangkat', '<', $next30Hari);
			});
		})->get(); # ->delete();

		foreach ($invoicesKurangDari30Hari as $key => $invoice) {
			$this->kirimEmailPemberitahuanInvoiceTelahTerlambatDibayarkan($invoice->booking);
			$invoice->booking->delete();
			$invoice->confirmations()->delete();
			$invoice->delete();
		}
	}

	private function kirimEmailPemberitahuanInvoiceTelahTerlambatDibayarkan ($booking) {
		$this->load->library('aksamailer');
		# kirim email
		$this->aksamailer->addAddress($booking->email, $booking->nama);     // Add a recipient
		$this->aksamailer->Body = blade()->njupukHtml('emails.pembatalan-otomatis', compact('booking'));
		$this->aksamailer->Subject = "Keterlamabatan Pembayaran Invoice {$booking->invoice->nomor_invoice}";
		$this->aksamailer->send();
	}

	public function kirimEmailReminderPembayaranHaMinSatuPelunasan()
	{
		$now = \Carbon\Carbon::now(); # sekarang
		$tomorrow = $now->copy()->addDay(1); # besok
		# change the format of tomorrow
		$tomorrow = $tomorrow->year . '-' . (strlen($tomorrow->month) < 2 ? "0" . $tomorrow->month : $tomorrow->mont) . '-' . (strlen($tomorrow->day) < 2 ? "0" . $tomorrow->day : $tomorrow->day);

		# you know, my code is bad. I could do simple thing like calling the carbon and get the string in  a format i want, but, being crazy sometime is good
		# nek bingung, hubungi nomorku: 0877 0281 4646

		# saiki jukuken data invoice, sing open-trip e iku kudu dilunasi mene... tapi nggawe eloquent ways
		$openTripsSingMeneKuduLunas = InvoiceORM::with('booking', 'booking.openTrip')->whereNull('sudah_diperingati')->where('status', '!=', 'belum_dibayar')->where('status', '!=', 'lunas')->where(function ($query) use ($now, $tomorrow) {
			$query->whereHas('booking', function($query) use ($now, $tomorrow) {
				$query->whereHas('openTrip', function($query) use ($now, $tomorrow) {
					$query->where('jatuh_tempo_pembayaran', $tomorrow);
				});
			});
		})->get();
		// return response_json($openTripsSingMeneKuduLunas);

		
		$this->load->library('aksamailer');
		# kirim email siji-siji
		foreach ($openTripsSingMeneKuduLunas as $key => $invoice) {
			# sakjane iki rodok piye ngunu, sampean nek ngerti strukture database e, sampean haruse sadar nek aku melakukan kesalahan
			# wkwk, tapi sing penting kan beres, kan? Ya, Kan??

			# saiki, berhubung pak haris njaluke, sing diberi peringatan adalah termin ke dua, maka yo tak perikso.
			# sing keloro tok, pokoke guduk sing terakhir, ngunu loh.
			# pertama, cek disek, sing wes dibayar iku ping piro sakjane
			$wesDibayarPingPiro = $invoice->confirmations()->count(); /* kudune wes dibayar ping */ $siji = 1;
			# terus, jukuk jumlah termine
			$jumlahTermin = $invoice->booking->openTrip->angsuran_maksimum;
			# wes mari?
			$saikiCekSelisihe = /*, jika ternyata */ $jumlahTermin >= 3 && $wesDibayarPingPiro == $siji; # dadine true

			if ($nekWisTrue = $saikiCekSelisihe) {
				$kirimenBookinge = 1;
			}

			@$ikiWesMulaiNguantukDadiNggaweVariable = $muterMuter;

			if ($nekTernyataGakTrueIkiGaraGaraneKuwalikane = !$nekWisTrue) {
				continue; 
			}

			@$nekTernyata = $opoSingTakBuletBuletnoNangNdukurIkiJekIsokDiliwati;

			@$kirimAeWesEmaileNangWongWongBenNdangMbayar = $benAkuNdangMari = $benNdangTuruDilukAe;

			@$terus = $ojok ? $lali : $man ? $laqia : $Allahu ? $laa ? $yusriku : $bihi : $syaian; # dakholal Jannah
			$booking = $invoice->booking;
			
			# kirim email
			echo "{$invoice->booking->kode} <br>";
			$this->aksamailer->addAddress($booking->email, $booking->nama);     // Add a recipient
			$this->aksamailer->Body = blade()->njupukHtml('emails.invoice-reminder', compact('booking'));
			$this->aksamailer->Subject = "Reminder Invoice {$booking->invoice->nomor_invoice}";
			$this->aksamailer->send();

			$invoice->sudah_diperingati = 1;
			$invoice->save();
		}
	}
}
