<?php

/*
* This is generated file by aksamedia
*/

class OpenTripCoverORM extends Aksa_Orm {

	protected $table = 'oyisam_open_trip_covers';
	protected $appends = ['cover_src'];

	public function openTrip()
	{
		return $this->belongsTo('OpenTripORM', 'open_trip_id');
	}

	public function getCoverSrcAttribute()
	{
		return base_url("assets/img/covers/{$this->src}");
	}

}
