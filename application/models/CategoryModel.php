<?php

/*
* This is generated file by aksamedia
*/

class CategoryModel extends CI_Model {

	function create(array $values, $icon) {
		$values = (object) $values;
		$category = new CategoryORM;
		$category->nama = $values->nama;
		$category->icon = $icon;
		$category->deskripsi = $values->deskripsi;
		$category->save();
	}

	function getAll() {
		return CategoryORM::desc()->get();
	}

	public function getById($id)
	{
		return CategoryORM::find($id);
	}

	function update($id, array $values, $icon) {
		$values = (object) $values;
		$category = CategoryORM::find($id);
		$category->nama = $values->nama;
		$category->icon = $icon ? $icon : $category->icon;
		$category->deskripsi = $values->deskripsi;
		$category->save();
	}

	function delete($id) {
		CategoryORM::where('id', $id)->delete();
	}

}
