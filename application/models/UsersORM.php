<?php

/*
* This is generated file by aksamedia
*/

class UsersORM extends Aksa_Orm {

	protected $table = 'users';
	protected $primaryKey = 'id_user';
	// protected $hidden = ['password', 'email_token'];
	public $timestamps = false;
	public $incrementing = false;

	public function scopeUnconfirmed($query)
	{
		return $query->where('email_confirmed', 0);
	}
}
