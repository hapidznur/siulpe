<?php

/*
* This is generated file by aksamedia
*/

class MasterTripCoverORM extends Aksa_Orm {

	protected $table = 'oyisam_master_trip_covers';
	protected $appends = ['cover_src'];

	public function masterTrip()
	{
		return $this->belongsTo('MasterTripORM', 'master_trip_id');
	}

	public function getCoverSrcAttribute()
	{
		return base_url("assets/img/covers/{$this->src}");
	}

	public function scopeIsCover($query)
	{
		return $query->where('cover', 1);
	}

}
