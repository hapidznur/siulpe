<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
*@author hapidznur
*/

class M_ulp extends CI_Model
{

function __construct()
{
$this->load->database();
}

/**
* Digunakan Selama proses development untuk mengurangi conflict
* @return Mohon sertakan nama file tempat menyimpan nanti dan table yang digunakan serta paramater jika ada.
*/

/**
* From Tabel Pengadaan
*/

function jumlah_pengadaan()
{
$exec = $this->db->query("SELECT
pengadaan.paket_name,
pengadaan.lokasi,
pengadaan.id_pengadaan,
pengadaan.no_pengadaan,
pengadaan.tanggal_permintaan,
pengadaan.anggaran
FROM
pengadaan
ORDER BY
pengadaan.tanggal_permintaan DESC");
return $exec -> result_array();
}

function lihat_pengadaan($sampai){
return $query = $this->db->query("SELECT
pengadaan.paket_name,
pengadaan.lokasi,
pengadaan.status_pengadaan,
pengadaan.id_pengadaan,
pengadaan.no_pengadaan,
pengadaan.tanggal_permintaan,
pengadaan.jam,
pengadaan.anggaran
FROM
pengadaan
ORDER BY
pengadaan.tanggal_permintaan DESC
LIMIT ".$sampai)->result_array();
}

function getpengadaan($limit, $id){
$query = $this->db->get('pengadaan');
return $query->result();
}

function getsinglepengadaan($id_pengadaan){

$query = $this->db->get('pengadaan');
return $query;
}

function gethps($status){
$this->db->select('id_pengadaan, id_hps, paket_name, date, lokasi');
$this->db->from('pengadaan');
$this->db->where('id_hps', $status);
$query = $this->db->get();
return $query;
}

function update_status($id_pengadaan)
{
$data  = array('status' => 'Pengumuman Penyedia');
$this->db->where('id_pengadaan', $id_pengadaan);
$this->db->update('pengadaan', $data);
}


/**
* From Tabel Penawaran
*/


public function get_penawaran($id_pengadaan){
$this->db->select('penyedia.id_penyedia,penyedia.nama_penyedia, pengadaan.id_pengadaan, penawaran.id_penawaran, penawaran.informasi_harga, penawaran.total_harga');
$this->db->from('penawaran');
$this->db->join('pengadaan','penawaran.id_pengadaan = pengadaan.id_pengadaan');
$this->db->join('penyedia', 'penawaran.id_penyedia = penyedia.id_penyedia');
$this->db->where('penawaran.id_pengadaan', $id_pengadaan);
$this->db->group_by('nama_penyedia');
$query = $this->db->get();
return $query->result();
}

/**
* Dipindah ke M_pengadaan
* @author hapidznur
*/
function get_undangan_from_penawaran($status){
$this->db->select('
pengadaan.id_pengadaan,
pengadaan.anggaran,
pengadaan.tahun,
pengadaan.paket_name,
hps.hps,
proses.status');
$this->db->from('pengadaan');
$this->db->join('proses',' proses.id_proses = pengadaan.status_pengadaan');
$this->db->join('hps', 'hps.id_pengadaan = pengadaan.id_pengadaan');
$this->db->where('hps.submit',$status);
$this->db->group_by('pengadaan.paket_name');
$query = $this->db->get();
return $query;
}

function get_detail_penawaran($id_pengadaan){
    $this->db->select('penyedia.id_penyedia,penyedia.nama_penyedia, pengadaan.id_pengadaan, penawaran.id_penawaran, penawaran.surat_informasi_harga, penawaran.total_harga');
    $this->db->from('penawaran');
    $this->db->join('pengadaan','penawaran.id_pengadaan = pengadaan.id_pengadaan');
    $this->db->join('penyedia', 'penawaran.id_penyedia = penyedia.id_penyedia');
    $this->db->where('penawaran.id_pengadaan', $id_pengadaan);
    $this->db->group_by('nama_penyedia');
    $query = $this->db->get();
    return $query->result();
}
/**
* @author hapidznur
*/
function get_penyedia($id_pengadaan){
$this->db->select('penyedia.nama_penyedia,penawaran.id_penyedia,
penawaran.id_pengadaan');
$this->db->from('penawaran');
$this->db->join('penyedia','penawaran.id_penyedia = penyedia.id_penyedia');
$this->db->where('penawaran.id_pengadaan',$id_pengadaan);
$this->db->group_by('nama_penyedia');
$query = $this->db->get();
return $query->result();
}

/**
* From Tabel Pengumuman
*/

public function ins_pengumuman($data)
{
$this->db->insert('pemenang',$data);
}
/**
* From Tabel Undangan
*/
function get_undangan($idpenyedia){
$this->db->select('undangan.id_pengadaan,pengadaan.paket_name,hps.hps,undangan.undangan');
$this->db->from('undangan');
$this->db->join('pengadaan', 'undangan.id_pengadaan=pengadaan.id_pengadaan');
$this->db->join('hps', 'pengadaan.id_pengadaan=hps.id_pengadaan');
$this->db->join('penyedia', 'undangan.id_penyedia = penyedia.id_penyedia');
$this->db->where('undangan.id_penyedia', $idpenyedia);
$this->db->where('pengadaan.status_pengadaan', 'Undangan Pengadaan Langsung');
$this->db->where('undangan.undangan', 'belum');
$query = $this->db->get();
return $query;
}

function count_undangan($id_penyedia){
$this->db->select('undangan.id_penyedia');
$this->db->from('undangan');
$this->db->join('pengadaan', 'undangan.id_pengadaan=pengadaan.id_pengadaan');
$this->db->join('penyedia', 'undangan.id_penyedia = penyedia.id_penyedia');
$this->db->where('undangan.id_penyedia', $id_penyedia);
$this->db->where('pengadaan.status_pengadaan', 'Undangan Pengadaan Langsung');
$query = $this->db->get();
return $query;
}

function get_penyedia_contact($penyedia)
{
$this->db->select('penyedia.id_penyedia, penyedia.penyedia, penyedia.CONTACT');
$this->db->from('penyedia');
$this->db->where('penyedia.id_penyedia', $penyedia);
$query = $this->db->get();
return $query->result_array();
}

/**
* From Tabel Penjadwalan
*/
function getjadwal(){
$this->db->from('penjadwalan');
$this->db->where('status_proses','informasi_harga');
$query = $this->db->get();
return $query;
}

function getbarangulp($id_pengadaan){
$this->db->select('id_barang, nama_barang, spesifikasi, satuan, harga, total_penawaran');
$this->db->from('detailed_paket');
$this->db->join('pengadaan', 'detailed_paket.id_pengadaan=pengadaan.id_pengadaan');
$this->db->where('pengadaan.id_pengadaan',$id_pengadaan);
$query = $this->db->get();
return $query->result_array();
}

/**
* From Kalender
*/
function kalender($id_jadwal){
$this->db->select();
$this->db->from('kalender');
$this->db->where('id_jadwal', $id_jadwal);
$query=$this->db->get();
return $query->result_array();
}
function get_proses($status_proses){
$this->db->select('id_pengadaan, paket_name,'.$status_proses.',status');
$this->db->from('kalender');
$query=$this->db->get();
return $query->result_array();
}
function get_status(){
$this->db->select('status');
$this->db->from('kalender');
$query=$this->db->get();
return $query->result_array();
}
function update_date($array, $id){
$this->db->where('id_jadwal',$id);
$this->db->update('penjadwalan',$array);
}
/**
* From status_proses
*/

function get_ondate(){
$this->db->select();
$this->db->from('status_proses');
$query = $this->db->get();
return $query->result_array();
}
/*
@author elfarqy
method ini digunakan untuk melakukan pengambilan data untuk login pada role ulp
*/

function get_ulp($username)
{
$query = $this->db->query("SELECT
`user`.username,
`user`.keycard,
`user`.role,
`user`.id_user
FROM
`user`
WHERE
`user`.username = '".$username."'
AND
`user`.role = 'ulp'");
return $query -> result_array();
}

function add_ppk($data)
{
    $this->db->insert('user_ppk',$data);
}

}
/* End of  development Proccess*/
