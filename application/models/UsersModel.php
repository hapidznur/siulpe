<?php

/*
* This is generated file by aksamedia
*/

class UsersModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->table = 'this-should-be-your-table-name';
		$this->load->database();
	}

	function create(array $values) {

		$user = new UsersORM;
		$user->nama = $this->input->post('nama');
		$user->satuan_kerja = $this->input->post('satuan_kerja');
		$user->email = $this->input->post('email');
		$user->role = $this->input->post('role');

		$token = str_rand(100);
		$user->email_token = $token;
		$user->password = password_hash($this->input->post('password'), PASSWORD_BCRYPT);
		$user->save();

		return $user;
	}

	function getAll() {
		return $this->db->get($this->table)->result();
	}

	function update(array $values, array $conditions) {
		$this->db->update($this->table, $values, $conditions);
	}

	function delete($conditions) {
		$this->db->delete($this->table, $conditions);
	}

}

