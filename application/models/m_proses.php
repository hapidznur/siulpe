<?php
/**
*
*/
class M_proses extends CI_model
{

	function __construct()
	{
	  $this->load->database();
	}

	function get_proses(){
		$this->db->from('proses');
		$query = $this->db->get();
		return $query;
	}

    function get_proses_limit($limit){
        $this->db->from('proses');
        $this->db->limit($limit);
        $query = $this->db->get();
        return $query;
    }
}