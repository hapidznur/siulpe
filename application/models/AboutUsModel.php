<?php

/*
* This is generated file by aksamedia
*/

class AboutUsModel extends CI_Model {

	public function get()
	{
		return AboutUsORM::first();
	}

	public function update(array $values)
	{
		$values = (object) $values; # just to make it more convinient

		$aboutUs = AboutUsORM::first();
		if (!$aboutUs) {
			$aboutUs = new AboutUsORM;
		}

		$aboutUs->about_us = @$values->about_us ? $values->about_us : $aboutUs->about_us;
		$aboutUs->contact_us = @$values->contact_us ? $values->contact_us : $aboutUs->contact_us;
		$aboutUs->legalitas = @$values->legalitas ? $values->legalitas : $aboutUs->legalitas;
		$aboutUs->faq = @$values->faq ? $values->faq : $aboutUs->faq;
		$aboutUs->karir = @$values->karir ? $values->karir : $aboutUs->karir;
		$aboutUs->afiliasi = @$values->afiliasi ? $values->afiliasi : $aboutUs->afiliasi;
		$aboutUs->save();
	}

}
