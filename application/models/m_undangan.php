  <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  /**
  *@author hapidznur
  */
  class M_undangan extends CI_model
  {

    function __construct()
    {
      $this->load->database();
    }

    function get_undangan($idpenyedia){
      $this->db->select('undangan.id_pengadaan,pengadaan.paket_name,hps.hps,undangan.undangan');
      $this->db->from('undangan');
      $this->db->join('pengadaan', 'undangan.id_pengadaan=pengadaan.id_pengadaan');
      $this->db->join('hps', 'pengadaan.id_pengadaan=hps.id_pengadaan');
      $this->db->join('penyedia', 'undangan.id_penyedia = penyedia.id_penyedia');
      $this->db->where('undangan.id_penyedia', $idpenyedia);
      $this->db->where('pengadaan.status_pengadaan', 'Undangan Pengadaan Langsung');
      $this->db->where('undangan.undangan', 'belum');
      $query = $this->db->get();
      return $query;
    }

    function count_undangan($id_penyedia){
      $this->db->select('undangan.id_penyedia');
      $this->db->from('undangan');
      $this->db->join('pengadaan', 'undangan.id_pengadaan=pengadaan.id_pengadaan');
      $this->db->join('penyedia', 'undangan.id_penyedia = penyedia.id_penyedia');
      $this->db->where('undangan.id_penyedia', $id_penyedia);
      $this->db->where('pengadaan.status_pengadaan', 'Undangan Pengadaan Langsung');
      $query = $this->db->get();
      return $query;
    }

    function get_penyedia_contact($penyedia)
    {
      $this->db->select('penyedia.id_penyedia, penyedia.penyedia, penyedia.CONTACT');
      $this->db->from('penyedia');
      $this->db->where('penyedia.id_penyedia', $penyedia);
      $query = $this->db->get();
      return $query->result_array();
    }
  }
