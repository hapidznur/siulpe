<?php
/**
*
*/
class M_status extends CI_Model
{

    function __construct()
    {
        $this->load->database();
    }

    function get_status(){
        $this->db->from('status_proses');
        $query = $this->db->get();
        return $query;
    }

    function get_status_now($id_pengadaan){
        $this->db->from('status_proses');
        $this->db->where('id_pengadaan', $id_pengadaan);
        $query = $this->db->get();
        return $query;
    }

}