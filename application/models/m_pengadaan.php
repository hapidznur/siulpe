<?php
/**
*
*/
class M_pengadaan extends CI_model
{

  function __construct()
  {
    $this->load->database();
  }
   function get_name($id_pengadaan){
    $this->db->select('paket_name');
    $this->db->from('pengadaan');
    $this->db->where('id_pengadaan', $id_pengadaan);
    $query = $this->db->get();
    return $query->row();
   }

  function get_status($id_pengadaan){
    $this->db->select('status');
    $this->db->from('pengadaan');
    $this->db->where('id_pengadaan', $id_pengadaan);
    $query = $this->db->get();
    return $query->result();
  }

  function update_status($id_pengadaan)
  {
    $data  = array('status' => 'Pengumuman Penyedia');
    $this->db->where('id_pengadaan', $id_pengadaan);
    $this->db->update('pengadaan', $data);
  }

  function get_pengadaan_table()
  {
    $this->db->select();
    $this->db->from('pengadaan');
    $query = $this->db->get();
    return $query->result_array();
    // return $this->db->count_all("pengadaan");
  }
  function get_list_pengadaan()
  {
    $query = $this->db->query("SELECT
      DISTINCT pengadaan.paket_name,
      pengadaan.date,
      hps.HPS
      FROM
      hps,
      pengadaan
      WHERE
      pengadaan.id_pengadaan = hps.id_pengadaan");
    return $query->result_array();
  }
  function insert_ppk_id($data)
  {
    $query = $this->db->select('id_ppk')->from('user_ppk')->where('id_ppk',$data)->get();
    if ($query->num_rows() == 0) {
     $query2 = ("INSERT INTO user_ppk(`id_ppk`) values ($data)");
     $this->db->query($query2);
   } else{
    return $query->num_rows();
  }
  return $query->result_array();
}
function insert_penjadwalan_id($data_jadwal, $data_pengadaan)
{
  $query = $this->db->query("SELECT
    penjadwalan.id_jadwal,
    penjadwalan.id_pengadaan
    FROM
    pengadaan
    INNER JOIN penjadwalan ON penjadwalan.id_pengadaan = pengadaan.id_pengadaan
    WHERE
    penjadwalan.id_pengadaan = '".$data_pengadaan."'
    ");
  if ($query -> num_rows() > 0) {
    echo "query more than zero";
  } else {
    echo "ya!";
    $query2 = ("INSERT INTO `pl_si`.`penjadwalan` (`id_jadwal`, `id_pengadaan`) values (".$data_jadwal.",'".$data_pengadaan."')");
    $this->db->query($query2);

  }

}

function insert_pengadaan($dataarray)
{
 $this->db->insert('pengadaan',$dataarray);
}

function detail_barang_pengadaan($id_pengadaan)
{
  $exec = $this->db->query("SELECT
    barang.nama_barang,
    barang.spesifikasi,
    barang.satuan,
    barang.volume,
    barang.harga,
    barang.jumlah_harga,
    barang.merk,
    barang.id_barang,
    barang.id_pengadaan,
    pengadaan.id_pengadaan
    FROM
    barang
    INNER JOIN pengadaan ON barang.id_pengadaan = pengadaan.id_pengadaan
    WHERE
    barang.id_pengadaan = '".$id_pengadaan."'");
  return $exec -> result_array();
}
function lihat_pengadaan($sampai){
  return $query = $this->db->query("SELECT
    pengadaan.paket_name,
    pengadaan.lokasi,
    pengadaan.status_pengadaan,
    pengadaan.id_pengadaan,
    pengadaan.no_pengadaan,
    pengadaan.tanggal_permintaan,
    pengadaan.jam,
    pengadaan.anggaran
    FROM
    pengadaan
    ORDER BY
    pengadaan.tanggal_permintaan DESC
    LIMIT ".$sampai)->result_array();
}
function pengadaan_rekanan($sampai){
  return $query = $this->db->query("SELECT
    pengadaan.paket_name,
    pengadaan.lokasi,
    pengadaan.status_pengadaan,
    pengadaan.id_pengadaan,
    pengadaan.no_pengadaan,
    pengadaan.tanggal_permintaan,
    pengadaan.jam,
    pengadaan.anggaran
    FROM
    pengadaan
    WHERE
    pengadaan.status_pengadaan = '2'
    ORDER BY
    pengadaan.tanggal_permintaan DESC
    LIMIT ".$sampai)->result_array();
}

function lihat_barang($id_pengadaan,$sampai)
{
  $exec = $this->db->query("SELECT
    barang.nama_barang,
    barang.spesifikasi,
    barang.satuan,
    barang.volume,
    barang.harga,
    barang.jumlah_harga,
    barang.merk,
    barang.id_barang,
    barang.id_pengadaan,
    pengadaan.id_pengadaan
    FROM
    barang
    INNER JOIN pengadaan ON barang.id_pengadaan = pengadaan.id_pengadaan
    WHERE
    barang.id_pengadaan = '".$id_pengadaan."' LIMIT ".$sampai);
  return $exec -> result_array();
}


function jumlah_pengadaan()
{
  $exec = $this->db->query("SELECT
    pengadaan.paket_name,
    pengadaan.lokasi,
    pengadaan.id_pengadaan,
    pengadaan.no_pengadaan,
    pengadaan.tanggal_permintaan,
    pengadaan.anggaran
    FROM
    pengadaan
    ORDER BY
    pengadaan.tanggal_permintaan DESC");
  return $exec -> result_array();
}

function tmp_pengadaan($datapengadaan)
{
  print_r($datapengadaan);
  // return $datapengadaan;
}
function lihat_pengadaan_sendiri($id_user)
{
  $exec = $this->db->query("SELECT
    pengadaan.paket_name,
    pengadaan.lokasi,
    pengadaan.no_pengadaan,
    `user`.id_user,
    user_ppk.id_user,
    user_ppk.id_ppk,
    pengadaan.id_ppk,
    pengadaan.id_pengadaan
    FROM
    pengadaan,
    `user`
    INNER JOIN user_ppk ON user_ppk.id_user = `user`.id_user
    WHERE
    pengadaan.id_ppk = user_ppk.id_ppk
    AND user_ppk.id_user =".$id_user.";");
  return $exec->result_array();
}
function lihat_pengadaan_sendiri_details($id_user, $limit)
{
  $exec = $this->db->query("SELECT
    pengadaan.paket_name,
    pengadaan.lokasi,
    pengadaan.id_pengadaan,
    pengadaan.no_pengadaan,
    pengadaan.tanggal_permintaan,
    pengadaan.anggaran,
    pengadaan.status_pengadaan
    FROM
    pengadaan,
    `user`
    INNER JOIN user_ppk ON user_ppk.id_user = `user`.id_user
    WHERE
    pengadaan.id_ppk = user_ppk.id_ppk
    AND user_ppk.id_user =".$id_user." LIMIT ".$limit);
  return $exec->result_array();
}

function edit_pengadaan_sendiri_details($id_pengadaan)
{
  $exec = $this->db->query("SELECT
    pengadaan.id_pengadaan,
    pengadaan.id_ppk,
    pengadaan.no_pengadaan,
    pengadaan.paket_name,
    pengadaan.lokasi,
    pengadaan.status_pengadaan,
    pengadaan.tahun,
    pengadaan.tanggal_surat,
    pengadaan.tanggal_permintaan,
    pengadaan.tanggal_dipa,
    pengadaan.jam,
    pengadaan.anggaran,
    pengadaan.no_dipa,
    pengadaan.mak
    FROM
    pengadaan
    WHERE
    pengadaan.id_pengadaan = '".$id_pengadaan."'
    ");
  return $exec->result_array();
}
/*
author @elfarqy
digunakan untuk melakukan percobaan query untuk print. Jangan digunakan untuk yang lain, karena nilainya sangat statis
 */
function get_pengadaan_params($id_pengadaan)
{
  $exec = $this->db->query("SELECT
    pengadaan.id_pengadaan,
    pengadaan.id_ppk,
    pengadaan.no_pengadaan,
    pengadaan.paket_name,
    pengadaan.lokasi,
    pengadaan.status_pengadaan,
    pengadaan.tahun,
    pengadaan.tanggal_surat,
    pengadaan.tanggal_permintaan,
    pengadaan.tanggal_dipa,
    pengadaan.jam,
    pengadaan.anggaran,
    pengadaan.no_dipa,
    pengadaan.mak
    FROM
    pengadaan
    WHERE
    pengadaan.id_pengadaan = ".$id_pengadaan."
    ");
  return $exec->result_array();
}
function get_pengadaan($id_pengadaan){
    $exec = $this->db->query("SELECT
    pengadaan.id_pengadaan,
    pengadaan.id_ppk,
    pengadaan.no_pengadaan,
    pengadaan.paket_name,
    pengadaan.lokasi,
    pengadaan.status_pengadaan,
    pengadaan.tahun,
    pengadaan.tanggal_surat,
    pengadaan.tanggal_permintaan,
    pengadaan.tanggal_dipa,
    pengadaan.jam,
    pengadaan.anggaran,
    pengadaan.no_dipa,
    pengadaan.mak
    FROM
    pengadaan
    WHERE
    pengadaan.id_pengadaan = ".$id_pengadaan."
    ");
    return $exec->row();
}
function update_pengadaan($data_pengadaan)
{
  $this->db->update('pengadaan',$data_pengadaan);
}

function get_nama_paket($id_pengadaan)
{
  $query = $this->db->query("SELECT
    pengadaan.paket_name
    FROM
    pengadaan
    WHERE
    pengadaan.id_pengadaan = '".$id_pengadaan."'
    ");
  return $query->result_array();
}

function search_pengadaan($kata_pencarian)
{
  $query = $this->db->query("SELECT
    pengadaan.paket_name,
    pengadaan.lokasi,
    pengadaan.id_pengadaan,
    pengadaan.no_pengadaan,
    pengadaan.tanggal_permintaan,
    pengadaan.anggaran,
    pengadaan.status_pengadaan
    FROM
    pengadaan
    WHERE
    pengadaan.paket_name LIKE '%".$kata_pencarian."%'
    ORDER BY
    pengadaan.tanggal_permintaan DESC");
  return $query->result_array();
}
function data_verifikasi_penawaran_pengadaan_sendiri($id_pengadaan)
{
  $query = $this->db->query("SELECT DISTINCT
penyedia.nama_penyedia,
pengadaan.paket_name,
pengadaan.lokasi,
penawaran.verifikasi,
penawaran.total_harga,
penawaran.id_penawaran,
penawaran.id_pengadaan,
penawaran.id_penyedia
FROM
    detail_penawaran
    INNER JOIN penawaran ON detail_penawaran.id_penawaran = penawaran.id_penawaran
    INNER JOIN barang ON detail_penawaran.id_barang = barang.id_barang
    INNER JOIN penyedia ON penawaran.id_penyedia = penyedia.id_penyedia
    INNER JOIN pengadaan ON penawaran.id_pengadaan = pengadaan.id_pengadaan
WHERE
    detail_penawaran.id_barang = barang.id_barang
    AND detail_penawaran.id_penawaran = penawaran.id_penawaran
    AND penawaran.id_penyedia = penyedia.id_penyedia
    AND pengadaan.id_pengadaan = penawaran.id_pengadaan
    AND penawaran.id_pengadaan = '.$id_pengadaan.'
    ");
  return $query->result_array();
}

}
