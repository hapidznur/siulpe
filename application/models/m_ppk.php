<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class M_ppk extends CI_Model
{
	
	function __construct()
	{
		$this->load->database();
	}
	/*
	@author elfarqy
	digunakan untuk melakukan parse username ketika login
	 */
	function get_ppk($username){
		$query = $this->db->query("SELECT
			`user`.username,
			`user`.keycard,
			`user`.role,
			`user`.id_user
			FROM
			`user`
			WHERE
			`user`.username = '".$username."'
			AND
			`user`.role = 'ppk'");
		return $query -> result_array();
	}

	/*
	@author elfarqy
	digunakan untuk melakukan parsing dari id.ppk menggunakan parameter username
	 */

	function get_id($value)
	{
		$query = $this->db->query("SELECT
			user_ppk.id_ppk,
			user_ppk.id_user,
			user_ppk.ppk_name,
			user_ppk.satker,
			user_ppk.nip
			FROM
			user_ppk
			INNER JOIN `user` ON user_ppk.id_user = `user`.id_user
			WHERE
			`user`.username = '".$value."'"
			);
		return $query->result_array();
	}
	
}
?>