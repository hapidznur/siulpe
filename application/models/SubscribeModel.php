<?php

 /*
 * This is generated file by aksamedia
 */

 class SubscribeModel extends CI_Model {

 	function __construct() {
 		parent::__construct();
 	}

 	function create($email) {

 		// $values = (object) $values;

 		$subscribe = new SubscribeORM;
 		$subscribe->email = $email;
 		$subscribe->save();
 	}


 	function getAll() {
 		return SubscribeORM::desc()->get();
 	}

 }
