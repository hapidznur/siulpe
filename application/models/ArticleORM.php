<?php

/*
* This is generated file by aksamedia
*/

class ArticleORM extends Aksa_Orm {

	protected $table = 'oyisam_article';

	public function comments()
	{
		return $this->hasMany('BlogCommentORM', 'article_id');
	}

	public function getSummaryAttribute()
	{
	    $summary = preg_replace("/<img[^>]+\>/i", "(image) ", $this->article); 
	    return read_more($summary, 900);
	}

	public function scopeRecommended($query, $article)
	{
		return $query->where(function($query) use ($article) {
			$query->where(function ($query) use ($article) {
				$title = $article->title;
				foreach (explode(" ", $title) as $key => $word) {
					$query->orwhere('title', 'like', "%{$word}%");
				}
			})->orwhere(function ($query) use ($article) {
				$body = strip_tags($article->article);
				foreach (explode(" ", $body) as $key => $word) {
					$query->orwhere('article', 'like', "%{$word}%");
				}
			});
		})->where('id', '!=', $article->id);
	}
}

