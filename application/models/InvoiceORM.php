<?php

/*
* This is generated file by aksamedia
*/

class InvoiceORM extends Aksa_Orm {

	protected $table = 'oyisam_invoices';
	protected $appends = ['jam_sejak_dikirim'];

	public function booking()
	{
		return $this->belongsTo('BookingORM', 'booking_id');
	}

	public function confirmations()
	{
		return $this->hasMany('InvoiceConfirmationORM', 'invoice_id');
	}

	public function scopeInvoice($query, $nomor_invoice)
	{
		return $query->where('nomor_invoice', $nomor_invoice);
	}

	public function scopeOpenTrip($query, $openTrip)
	{
		if ($openTrip) {
			return $query->whereHas('booking', function ($query) use ($openTrip) {
				$query->where('open_trip_id', $openTrip);
			});
		}
	}

	public function scopeLunas($query)
	{
		return $query->where('status', 'lunas');
	}

	public function scopeMonth($query, $month)
	{
		if ($month) {
			return $query->whereMonth('created_at', '=', $month);
		}
	}

	public function scopeYear($query, $year)
	{
		if ($year) {
			return $query->whereYear('created_at', '=', $year);
		}
	}

	public function getStatusPembayaranAttribute()
	{
		if ($confirmation = $this->confirmations()->butuhDiKonfirmasi()->first()) {
			return $confirmation->status_pembayaran;
		}

		switch ($this->status) {
			case 'belum_dibayar':
				return "Belum dibayar";
				break;
			case 'dp':
				return "DP";
				break;
			case 'angsur':
				return "Sedang diangsur";
				break;
			case 'lunas':
				return "Lunas";
				break;
		}
	}

	public function getTotalHargaManusiaAttribute()
	{
		return money_format($this->harga);
	}

	public function getTotalYangSudahDibayarAttribute()
	{
		return $this->confirmations()->sudahDikonfirmasi()->sum('total_transfer');
	}

	public function getTotalYangHarusDibayarAttribute()
	{
		return $this->total_harga - $this->total_yang_sudah_dibayar;
	}

	public function getJamSejakDikirimAttribute()
	{
		$now = \Carbon\Carbon::now();
		$tanggalDikirim = $this->created_at;
		return $tanggalDikirim->diffInHours($now);
	}
}
