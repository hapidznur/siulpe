<?php

/*
* This is generated file by aksamedia
*/

class MasterTripModel extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getById($id)
	{
		return MasterTripORM::find($id);
	}

	public function getAll($keyword = null)
	{
		if ($keyword) {
			return MasterTripORM::search($keyword)->desc()->get();
		}
		return MasterTripORM::desc()->get();
	}

	public function getCount()
	{
		return MasterTripORM::desc()->count();
	}

	public function deleteCover($id)
	{
		return MasterTripCoverORM::find($id)->delete();
	}

	public function insert(array $values, array $covers, $itenary_file)
	{
		# i dont care with all the validation
		$values = (object) $values;
		# insert the new master trip
		$masterTrip = new MasterTripORM;
		$masterTrip->title = $values->title;
		$masterTrip->itenary = $values->itenary;
		$masterTrip->itenary_file = $itenary_file ? $itenary_file : $masterTrip->itenary_file;
		$masterTrip->additional_information = @$values->additional_information;
		$masterTrip->term_of_service = @$values->term_of_service;
		$masterTrip->fasilitas = @$values->fasilitas;
		$masterTrip->save();

		# insert the destinations
		foreach ($values->destinations as $key => $value) {
			$destination = new MasterTripDestinationPivotORM;
			$destination->master_trip_id = $masterTrip->id;
			$destination->destination_id = $value;
			$destination->save();
		}

		foreach ($values->categories as $key => $value) {
			$category = new KategoriMasterTripPivot;
			$category->master_trip_id = $masterTrip->id;
			$category->category_id = $value;
			$category->save();
		}

		// # menambahkan fasilitas
		// foreach ($values->facilities as $key => $facility) {
		// 	$facilityPivot = new MasterTripFacilityORM;
		// 	$facilityPivot->master_trip_id = $masterTrip->id;
		// 	$facilityPivot->facility_id = $facility;
		// 	$facilityPivot->save();
		// }

		# insert covers
		foreach ($covers as $key => $src) {
			$cover = new MasterTripCoverORM;
			$cover->master_trip_id = $masterTrip->id;
			$cover->src = $src;
			if ($key === $values->id_cover) {
				$cover->cover = true;
			}
			$cover->save();
		}
	}

	public function update($id, array $values, array $covers, $itenary_file)
	{
		# i dont care with all the validation
		$values = (object) $values;

		# find the master trip by its id
		$masterTrip = MasterTripORM::find($id);
		$masterTrip->title = $values->title;
		$masterTrip->itenary = $values->itenary;
		$masterTrip->itenary_file = $itenary_file ? $itenary_file : $masterTrip->itenary;
		$masterTrip->additional_information = @$values->additional_information;
		$masterTrip->term_of_service = @$values->term_of_service;
		$masterTrip->fasilitas = @$values->fasilitas;
		$masterTrip->save();

		# delete sek sing lawas ben gampang
		MasterTripDestinationPivotORM::where('master_trip_id', $masterTrip->id)->delete();

		# insert the destinations
		foreach ($values->destinations as $key => $value) {
			$destination = new MasterTripDestinationPivotORM;
			$destination->master_trip_id = $masterTrip->id;
			$destination->destination_id = $value;
			$destination->save();
		}

		MasterTripFacilityORM::masterTrip($masterTrip->id)->delete();

		// # menambahkan fasilitas
		// foreach ($values->facilities as $key => $facility) {
		// 	$facilityPivot = new MasterTripFacilityORM;
		// 	$facilityPivot->master_trip_id = $masterTrip->id;
		// 	$facilityPivot->facility_id = $facility;
		// 	$facilityPivot->save();
		// }

		# insert covers
		# insert covers
		foreach ($covers as $key => $src) {
			$cover = new MasterTripCoverORM;
			$cover->master_trip_id = $masterTrip->id;
			$cover->src = $src;
			$cover->save();
		}

		$existingCovers = $masterTrip->covers()->get(); # new query
		# remove the old covers
		$existingCover = $masterTrip->covers()->isCover()->first();
		if ($existingCover) {
			$existingCover->cover = false;
			$existingCover->save();
		}

		# put new cover
		$id_cover = @$values->id_cover;
		$existingCovers[$id_cover]->cover = true;
		$existingCovers[$id_cover]->save();

		KategoriMasterTripPivot::where('master_trip_id', $masterTrip->id)->delete();
		foreach ($values->categories as $key => $value) {
			$category = new KategoriMasterTripPivot;
			$category->master_trip_id = $masterTrip->id;
			$category->category_id = $value;
			$category->save();
		}
	}

	public function delete($id)
	{
		$this->load->model('openTripModel');
		$masterTrip = $this->getById($id);

		foreach ($masterTrip->openTrips as $key => $openTrip) {
			$this->openTripModel->delete($openTrip->id);
		}

		$masterTrip->delete();
	}

}
