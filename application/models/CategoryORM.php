<?php

/*
* This is generated file by aksamedia
*/

class CategoryORM extends Aksa_Orm {

	protected $table = 'oyisam_categories';

	protected $appends = ['icon_src'];

	public function getIconSrcAttribute()
	{
		return url("assets/img/icons/{$this->icon}");
	}

	public function scopeCategories($query, $categories)
	{
		return $query->whereIn("{$this->table}.id", $categories);
	}

	public function scopeNama($query, $nama)
	{
		return $query->where('nama', 'like', "{$nama}%");
	}

}
