<?php

/*
* This is generated file by aksamedia
*/

class MasterTripORM extends Aksa_Orm {

	protected $table = 'oyisam_master_trips';
	protected $appends = ['kode', 'destinations_lists', 'category_list'];

	public function openTrips()
	{
		return $this->hasMany('OpenTripORM', 'master_trip_id');
	}

	public function covers()
	{
		return $this->hasMany('MasterTripCoverORM', 'master_trip_id');
	}

	public function destinations()
	{
		return $this->belongsToMany('DestinationORM', 'oyisam_master_trip_destination_pivot', 'master_trip_id', 'destination_id');
	}

	public function categories()
	{
		return $this->belongsToMany('CategoryORM', 'oyisam_category_master_trip_pivot', 'master_trip_id', 'category_id');
	}

	public function getItenaryFileSrcAttribute()
	{
		return url("assets/itenary/{$this->itenary_file}");
	}

	public function getProvinsiAttribute($value='')
	{
		return ucwords(strtolower($value));
	}

	public function getKotaAttribute($value='')
	{
		return ucwords(strtolower($value));
	}

	public function getKodeAttribute()
	{
		$id = $this->id;
		while (strlen($id) < 3) {
			$id = "0{$id}";
		}
		return "MT{$id}";
	}

	public function getDestinationsListsAttribute()
	{
		if (count($this->destinations)) {
			$str = "";

			foreach ($this->destinations as $key => $destination) {
				$str .= $destination->nama;

				if ($key < count($this->destinations) - 1) {
					$str .= ', ';
				}
			}

			return $str;
		}
		return "Belum dipilih.";
	}

	public function getIdCoverAttribute()
	{
		$covers = $this->covers;
		$idCover = 0;

		foreach ($covers as $key => $item) {
			if ($item->cover) {
				$idCover = $key;
			}
		}

		return $idCover;
	}

	public function getCoverSrcAttribute()
	{
		if (!$this->covers()->isCover()->first()) {
			if ($this->covers()->count()) {
				return $this->covers()->first()->cover_src;
			}
			else {
				return false;
			}
		}

		return $this->covers()->isCover()->first()->cover_src;
	}

	public function getCategoryListAttribute()
	{
		$str = '';
		foreach ($this->categories as $key => $category) {
			$str .= $category->nama;

			if ($key < count($this->categories) - 1) {
				$str .= ', ';
			}
		}

		if ($str !== '') {
			return $str;
		}

		return "Belum dipilih";
	}

	public function scopeProvinsi($query, $provinsi)
	{
		return $query->whereHas('destinations', function ($query) use ($provinsi) {
			$query->provinsi($provinsi);
		});
	}

	public function scopeKota($query, $kota)
	{
		return $query->whereHas('destinations', function ($query) use ($kota) {
			$query->kota($kota);
		});
	}

	public function scopeCategories($query, $categories)
	{
		return $query->whereHas('categories', function ($query) use ($categories) {
			$query->categories($categories);
		});
	}

	public function scopeSearch($query, $title)
	{
		# mbingungi ya? Tapi powerfull kok hehehe.
		return $query->where(function ($query) use ($title) {

			$query->orwhere(function ($query) use ($title) {
				foreach (explode(" ", $title) as $key => $word) {
					$query->where('title', 'like', "%{$word}%");
				}
			});

			$query->orwhere(function ($query) use ($title) {
				$query->whereHas('destinations', function ($query) use ($title) {
					$query->where(function ($query) use ($title) {
						$query->orwhere(function ($query) use ($title) {
							$query->nama($title);
						});
						$query->orwhere(function ($query) use ($title) {
							$query->provinsi($title);
						});
						$query->orwhere(function ($query) use ($title) {
							$query->kota($title);
						});
					});
				});
			});

			$query->orwhere(function ($query) use ($title) {
				$query->whereHas('categories', function ($query) use ($title){
					$query->nama($title);
				});
			});
		});
	}

	// public function getFacilitiesListsAttribute()
	// {
	// 	if (count($this->facilities)) {
	// 		$str = "";
	//
	// 		foreach ($this->facilities as $key => $facility) {
	// 			$str .= $facility->title;
	//
	// 			if ($key < count($this->facilities) - 1) {
	// 				$str .= ', ';
	// 			}
	// 		}
	//
	// 		return $str;
	// 	}
	// 	return "Belum dipilih.";
	// }
}
