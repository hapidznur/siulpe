<?php

/*
* This is generated file by aksamedia
*/

class DestinationORM extends Aksa_Orm {

	protected $table = 'oyisam_destinations';
	public $timestamps = false;

	public function getProvinsiAttribute($value)
	{
		return ucwords(strtolower($value));
	}

	public function getKotaAttribute($value='')
	{
		return ucwords(strtolower($value));
	}

	public function scopeNama($query, $nama)
	{
		return $query->where('nama', 'like', "{$nama}%");
	}

	public function scopeProvinsi($query, $provinsi)
	{
		return $query->where('provinsi', 'like', "{$provinsi}%");
	}

	public function scopeKota($query, $kota)
	{
		return $query->where('kota', 'like', "{$kota}%");
	}
}
