<?php

/*
* This is generated file by aksamedia
*/

class AdsModel extends CI_Model {

	public function get()
	{
		return AdsORM::get();
	}

	public function getById($id)
	{
		return AdsORM::find($id);
	}

	public function create(array $values, $script)
	{
		$values = (object) $values;
		$ads = new AdsORM;
		$ads->nama = $values->nama;
		$ads->posisi = $values->posisi;
		$ads->jenis = $values->jenis;
		$ads->status = $values->status;
		$ads->script = $script;
		$ads->save();

		return $ads;
	}

	public function update($id, array $values, $script)
	{
		$values = (object) $values;
		$ads = AdsORM::find($id);
		$ads->nama = $values->nama;
		$ads->posisi = $values->posisi;
		$ads->jenis = $values->jenis;
		$ads->status = $values->status;
		$ads->script = $script ? $script : $ads->script;
		$ads->save();

		return $ads;
	}

	public function delete($id)
	{
		return AdsORM::find($id)->delete();
	}

	public function getLeftBarAds()
	{
		return AdsORM::posisi('left-bar')->aktif()->get();
	}

	public function getBottomBody()
	{
		return AdsORM::posisi('bottom-body')->aktif()->get();
	}
}
