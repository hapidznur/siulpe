<?php


/**
 * ini kelas aksa-model, tujuannya untuk mempersingkat beberapa task dari CI_Model
 */
class Aksa_Model extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    $this->load->database();

    function qb()
    {
      return new Magic();
    }
  }


}

class Magic {

  public function __call($method, $args)
  {
    $ci =& get_instance();
    if (method_exists($ci->db, $method)) {
      return $ci->db->$method($args);
    }
    else {
      show_error("{$method} is not found");
    }
  }
}
