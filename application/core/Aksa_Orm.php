<?php


class Aksa_Orm extends Illuminate\Database\Eloquent\Model {
	
	public $ci;
	
	function __construct()
	{	
		# this will make this custom model would have access to the ci
		$this->ci =& get_instance();
		$capsule = new Illuminate\Database\Capsule\Manager;
		$capsule->addConnection([
		    'driver'    => 'mysql',
		    'host'      => $_ENV['DB_HOST'],
		    'database'  => $_ENV['DB_NAME'],
		    'username'  => $_ENV['DB_USERNAME'],
		    'password'  => $_ENV['DB_PASSWORD'],
		    'charset'   => 'utf8',
		    'collation' => 'utf8_unicode_ci',
		    'prefix'    => '',
		]);
		
		// Set the event dispatcher used by Eloquent models... (optional)
		$capsule->setEventDispatcher(new Illuminate\Events\Dispatcher(new Illuminate\Container\Container));

		// Make this Capsule instance available globally via static methods... (optional)
		$capsule->setAsGlobal();

		// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
		$capsule->bootEloquent();
	}

	public function scopeDesc($query)
	{
		# this is a common uses scope, so i defined it here.
		# if you want to get something from your table and you want them to be sorted descendengly by the id
		# just call this scope -> UserModel::where('status', 'active')->desc()->get();
		return $query->orderBy('id', 'desc');
	}
}