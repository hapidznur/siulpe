<div class="col-sm-10 col-sm-offset-2 col-md-10 col-lg-offset-2 main">
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <?php echo form_open_multipart('rekanan/updatepenawaran'); ?>
                <table class="table table-hover table-pengadaan">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th >Barang/Jasa</th>
                            <th class="text-center col-md-1">Rincian</th>
                            <th class="text-center col-md-1">Satuan</th>
                            <th class="text-center col-md-1">Banyak</th>
                            <th >Harga Non PPn</th>
                            <th >Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php $i=0; foreach ($barang as $barang ){ ?>
                        <tr>
                            <td class="text-center"><?php echo $i; ?></td>
                            <td class="text-center"><?php echo $barang->nama_barang; ?></td>
                            <td class="text-center"><a class="btn btn-link" onclick="window.open('<?php echo base_url().'rekanan/pengadaan/barang/spesifikasi?pengadaan='.$barang->id_pengadaan.'&barang='.$barang->id_barang; ?>', 'spesifikasi', 'height=900,width=600')">Rincian</a></td>
                            <td class="text-center"><?php echo $barang->satuan ?></td>
                            <td class="text-center"><span id="volume" name="volume"><?php echo $barang->volume ?></span></td>
                            <td class="col-md-2"><input type="text" name="harga_tawar[]" value="<?php echo set_value('options[]'); ?>" class="form-control hargarekanan" data-harga="<?php echo $barang->volume ?>"></td>
                            <td class="col-md-3 text-center"><span id="jumlah"></span><input type="hidden" name="jumlah[]" class="totalhidden"></td>
                        </tr>
                            <input type="hidden" name="pengadaan" value="<?php echo $barang->id_pengadaan;?>">
                            <input type="hidden" name="detailed[]" value="<?php echo $barang->id_barang;?>">
                            <input type="hidden" name="jumlahtotal" class="jumlahtotal">
                            <?php $i++;}?>
                        <tr>
                            <td colspan="6"><strong>Jumlah Total</strong></td>
                            <td style="text-align:center"><span id="jumlahtotal"><strong>0</strong></span></td>
                        </tr>
                    </tbody>
                </table>
                <div class="col-md-12">
                    <div class="bs-callout bs-callout-info">
                        <label>Surat Informasi Permintaan harga barang</label>
                        <label><a class="btn btn-link" href=""> Spesifikasi</a></label>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="bs-callout bs-callout-warning" id="callout-glyphicons-location">
                        <h4 id="changing-the-icon-font-location">Pemberitahuan</h4>
                        <p>Silahkan upload surat Informasi Harga dalam kotak berikut</p>
                        <input type="file" name="userfile" id="userfile" size="24" class="" value="<?php echo set_value('userfile'); ?>"/>
                    </div>
                </div>
                <p style="text-align:center">
                    <button type="button" class="btn btn-primary" id="clear" style="float:50em">Clear</button>
                    <button type="submit" class="btn btn-primary" name="simpan">Kirim</button>
                </p>
                    <?php form_close();?>
            </div><!--end table-responsive -->
        </div><!--end col-xs-10 col-sm-10 col-md-10-->
    </div> <!--end row-->
</div> <!--end container-fluid content-->