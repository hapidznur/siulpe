<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Detail Barang
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Nama Pengadaan</h3>
            </div>
            <div class="box-body" style="margin-bottom: 10px;">
                <div class="col-md-8">
                    <span style="padding-left: 20px;"><label class="col-md-3">Lokasi </label> : <span style="padding-left: 40px;"><?php echo $pengadaan->lokasi;?></span></span>
                </div>
                <div class="col-md-8">
                    <span style="padding-left: 20px;"><label class="col-md-3">No Pengadaan </label> : <span style="padding-left: 40px;"><?php echo $pengadaan->lokasi;?></span></span>
                </div>
                <div class="col-md-8">
                    <span style="padding-left: 20px;"><label class="col-md-3">Tanggal </label> : <span style="padding-left: 40px;"><?php echo $pengadaan->lokasi;?></span></span>
                </div>
            </div>
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" >
                <thead>
                <tr>
                    <th>nama barang</th>
                    <th>spesifikasi</th>
                    <th>volume</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($artikel as $value) : ?>
                <tr>
                    <td><?php echo $value['nama_barang']; ?></td>
                    <td><?php echo $value['spesifikasi']; ?></td>
                    <td><?php echo $value['volume']; ?></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.box    -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->