@extends('template')

@section('title')
Compas Petualang | Syarat dan ketentuan
@stop

@section('body')
  <div class="container">
    <div class="gap"></div>
      <h2 class="text-left">Syarat dan Ketentuan</h2>
      <div class="gap">
          <div class="row row-wrap">
              <div class="col-md-12">
                <article class="article">
                  {!!$identitasWeb->syarat_dan_ketentuan!!}
                </article>
              </div>
          </div>
      </div>


  </div>
@stop
