@extends('unsigntemplate')

@section('style')
<link rel="stylesheet" type="text/css" href="{{base_url('less/style.css')}}">
<style type="text/css">
form label.error {
  color: #000;
  font-size: 0.8em;
}  
.error{
  color : #a94442;
}
form input.error {
  color: rgb(210, 214, 222);  
  border-color:#a94442;
}  
</style>
@endsection

@section('pieces')
  @include('pieces.unsignheader')
@stop

@section('title', 'signup')

@section('body')
<div class="login-box" style="margin-top: 0;">
  <div class="login-logo">
    <h3 class="text-center">Register Rekanan</h3>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <form id="form-register" action="{{base_url("auth/register")}}" method="post">
    <div class="form-group">
      <label class="control-label" for="namapenyedia">Nama Penyedia</label>
      <input id="namapenyedia" name="namapenyedia" type="text" placeholder="Nama Perusahaan dengan jelas" class="form-control input-md" required>
      <span class="help-block"></span>
    </div>
    <div class="form-group">
        <label class="control-label" for="contact">Email</label>
        <input id="email" name="email" type="email" placeholder="Tuliskan alamat email yang bisa dihubungi" class="form-control input-md">
        <span class="help-block email-help"></span>
    </div>
    <!-- Text input-->
    <div class="form-group">
      <label class="control-label" for="pemilik">Password</label>
      <input id="password" name="register_password" type="password" placeholder="Password" class="form-control input-md" data-maxlength="48">
      <span class="help-block"></span>
    </div>
    <div class="form-group">
      <label class="control-label" for="pemilik">  Password (Confirmation): </label>
      <input id="passwordConfirm" name="register_pass_confirm" type="password" placeholder="Password Confirmation" class="form-control input-md" data-maxlength="48">
      <span class="help-block"></span>
    </div>        
    <div class="row">
      <div class="col-xs-6 pull-right">
        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign
      Up Penyedia</button>
      </div>
      <!-- /.col -->
    </div>
    </form>
  </div>
</div>
@stop
@section('script')
  <!-- <script type="text/javascript" src="{{base_url('js/siulpe.js')}}"></script> -->
  <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
jQuery.validator.addMethod( 'passwordMatch', function(value, element, params) {
    
    // The two password inputs
    var password = $("#password").val();
    var confirmPassword = $("#passwordConfirm").val();

    // Check for equality with the password inputs
    if (password != confirmPassword ) {
        return false;
    } else {
        return true;
    }

}, "Your Passwords Must Match");



// ==========================================================================
// Registration Form : jquery validation
// http://jqueryvalidation.org/validate

$('#form-register').validate({
    // rules
    rules: {
        email : {
          required : true,
          remote: 
          {
            url: "auth/register",
            type: "POST"
          }
      },
        register_password: 
        {
          required: true,
          minlength: 8
        },
        register_pass_confirm: {
            required: true,
            minlength: 8,
            passwordMatch: true // set this on the field you're trying to match
        }
    },

    // messages
    messages: {
        register_password: {
            required: "What is your password?",
            minlength: "Your password must contain more than 3 characters"
        },
        register_pass_confirm: {
            required: "You must confirm your password",
            minlength: "Your password must contain more than 3 characters",
            passwordMatch: "Your Passwords Must Match" // custom message for mismatched passwords
        },
        email: 
        {
          required: "Please Enter Email!",
          email : "Masukkan Email anda sesuai format",
          remote : "Anda sudah terdaftar. Silahkan login"
        }
    }
});//end validate
</script>
@stop