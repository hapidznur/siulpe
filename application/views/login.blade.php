@extends('unsigntemplate')

@section('style')
<link rel="stylesheet" type="text/css" href="{{base_url('less/style.css')}}">
@endsection

@section('title', 'signup')

@section('body')
<div class="login-box">
    <div class="login-logo">
        <a href="{{ base_url('/ppk/pengadaan'); }}"><b>Sistem Infomasi</b><br>Unit Layanan Pengadaan</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        {{ form_open('validation'); }}
        <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="username" name="username" id="username"
                   value="{{set_value('username'); }}">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="password" id="password"
                   value="{{set_value('password'); }}">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <select name="role" id="role" class="role form-control">
                <option value="penyedia">Penyedia</option>
                <option value="ulp">ULP</option>
                <option value="ppk">PPK</option>
            </select>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <button type="submit" class="btn btn-success btn-block btn-flat">Sign In</button>
            </div>
            <!-- /.col -->
            <div class="col-xs-6">
                <a href="{{base_url('signup'); }}" class="btn btn-primary btn-block btn-flat">Sign
                    Up Penyedia</a>
            </div>
            <!-- /.col -->
        </div>
        {{  form_close(); }}

        <div class="social-auth-links text-center">
            <a href="{{base_url('auth/reset_password')}}">I forgot my password</a><br>
        </div>
    </div>
    <!-- /.login-box-body -->
</div>
@stop