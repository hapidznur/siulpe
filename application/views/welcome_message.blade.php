@extends('template')

@section('title')
  Compas Petualang
@stop

@section('body')
  @include('pieces.top-area')
  <div class="container">
    <div class="gap"></div>
    <h3 class="text-center"><span class="text-blue"><i class="fa fa-star"></i> Top </span><span class="text-orange">Destinations</span></h3>
    <div class="gap">
      <div class="row row-wrap">
        @foreach($openTrips as $key => $openTrip)
          <div class="col-md-3 div-gap"  >
            <div class="box nopadding" style="overflow-x:hidden">
              <div class="header">
                <b> {{ucwords($openTrip->masterTrip->title)}}</b>
              </div>
              <div class="img-preview">
                @if($openTrip->garansi_berangkat)
                  <div class="head-label">
                    <span  style="font-size:10px;">
                      Garansi Berangkat
                    </span>
                  </div>
                @endif
                <a class="hover-img" href="{{base_url("open-trip/show/{$openTrip->id}")}}">
                  <img src="{{$openTrip->cover}}" alt="Image Alternative text" title="Upper Lake in New York Central Park" />
                  <i class="fa fa-plus box-icon-white box-icon-border hover-icon-top-right round"></i>
                </a>
                @if($openTrip->is_closed)
                  <div class="head-label-price">
                    <b>Closed</b>
                  </div>
                @else
                  <!-- Status Sold E Ga Ero Aku -->
                @endif

              </div>
              <div class="body">
                <table style="width:100%">
                  <tr>
                    <td width="50%">Deadline Booking</td>
                    <td width="50%" align="right">{{$openTrip->tanggal_deadline_pendaftaran}}</td>
                    <!-- Cak Hud ndukur iki aku nggae tanggal berangkat manusia , aku nggolek deadline booking ga ero sing endi -->
                  </tr>
                </table>
                <hr class="gap-sm">
                <h5 style="font-weight:400" class="text-center">{{$openTrip->price}} <span class="text-normal">/Person </span></h5>
                <p class="text-center text-small">Trip Number : {{$openTrip->kode}}</p>
                <hr class="gap-sm">
                <table style="width:100%">
                  <tr>
                    <td class="text-small" width="50%"><b>Departure</b></td>
                    <td class="text-small" width="50%" align="right"><b>Arrival</b></td>
                  </tr>
                  <tr>
                    <td width="50%">
                      {{$openTrip->tanggal_berangkat_manusia}} <br>
                      {{$openTrip->jam_berangkat}}
                    </td>
                    <td width="50%" align="right">
                      {{$openTrip->tanggal_pulang_manusia}}  <br>
                      {{$openTrip->jam_pulang}}
                    </td>
                    <!-- Tanggal Kedatangan Yo Ga Nemu Cak Hud -->
                    <!-- Iyo mas, hehe, sepurane yo...-->
                  </tr>
                  <tr>
                    <td colspan="2"><hr class="gap-sm"></td>
                  </tr>
                  <tr>
                    <td class="text-small" width="50%"><b>Meeting Point</b></td>
                    <td class="text-small" width="50%" align="right"><b>Quota</b></td>
                  </tr>
                  <tr>
                    <td width="50%" title="{{$openTrip->meeting_point}}">{{read_more($openTrip->meeting_point,14)}}</td>
                    <td width="50%" align="right">{{$openTrip->quota_masuk}}/{{$openTrip->quota}}</td>
                    <!-- Tanggal Kedatangan Yo Ga Nemu Cak Hud -->
                  </tr>
                </table>
              </div>
            </div>
            <div class="text-center gap-sm">
              @if($openTrip->status_open_trip == 'Ditutup')
                <a href="{{base_url("open-trip/show/{$openTrip->id}")}}" class="btn btn-danger btn-sm btn-block">
                  View Trip Detail
                </a>
              @else
                <a href="{{base_url("open-trip/show/{$openTrip->id}")}}" class="btn btn-primary btn-sm btn-block">
                  View Trip Detail
                </a>
              @endif
            </div>
          </div>


        @endforeach
      </div>
      <div class="text-center">
        <a class="btn btn-primary btn-lg" href="{{url('open-trip')}}">See More Trips</a>
      </div>
    </div>


  </div>
@stop

@section('script')
  <script src="{{url('assets/js/regional.js')}}" charset="utf-8"></script>
  <script src="{{url('assets/js/jquery.autocomplete.min.js')}}" charset="utf-8"></script>
  {{-- <script src="{{base_url('assets/js/admin/new-master-trip.js')}}" charset="utf-8"></script> --}}
  <script>
    (function () {

      Regional
      .init()
      .provinsi($('[name=provinsi]'), '{{$controller->input->get('provinsi')}}', true)
      .kota($('[name=kota]'), '{{$controller->input->get('kota')}}', true)
      .grab();
    })();

    // // pencarian
    // $('[name=destination]').keyup(function () {
    //   var me = $(this)
    //   var option = {}
    //   option.url = '/open-trip'
    //   option.data = {
    //     destination: me.val()
    //   }
    //   option.success = function (response) {
    //     $('#destination-list').html('')
    //     $.each(response.openTrips, function (index, item) {
    //       $('#destination-list').append(
    //         '<option value="' + item.master_trip.title + ' | ' + item.master_trip.destinations_lists + '"> Tes </option>'
    //       )
    //       console.log(item);
    //     })
    //
    //     console.log(response);
    //   }
    //
    //   // call the ajax
    //   $.ajax(option)
    // });

    $('[name=destination]').autocomplete({
      serviceUrl: '/open-trip',
      paramName: 'destination',
      transformResult: function (response) {
        response = JSON.parse(response)
        console.log(response);
        var suggestions = []

        $.each(response.openTrips, function (index, item) {
          suggestions.push({
            value: item.master_trip.title,
            data: item.master_trip.title
          })
        })

        return {
          suggestions: suggestions
        };
      }
    })
    </script>
  @endsection
