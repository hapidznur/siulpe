<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Pengadaan
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Pengadaan tersedia</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
            <tr>
             <th>Nama Pengadaan</th>
             <th>Lokasi</th>
             <th>Tanggal Proses</th>
             <th colspan="2">Kalender Pengadaan</th>
           </tr>
         </thead>
         <tbody>
          <?php
          foreach ($artikel as $count_data):
            ?>
          <tr>
            <td class="col-md-6"><?php echo $count_data['paket_name']; ?></td>
            <td><?php echo $count_data['lokasi']; ?></td>
            <td class="text-center"><?php echo $count_data['tanggal_permintaan']; ?></td>
            <td><?php echo $count_data['status_pengadaan']; ?></td>
            <td id="action">
              <p>
                <a href="<?php echo base_url().'ppk/detail_pengadaan/'.$count_data['id_pengadaan'];?>" class="btn btn-default" > Detail</a>
                <a href="<?php echo base_url().'ppk/kalender_pengadaan/'.$count_data['id_pengadaan'];?>" class="btn btn-default" > Kalender Pengadaan</a>
              </p></td>
            </tr>
            <?php
            endforeach;
            ?>
          </tbody>          
        </table>
      </div>
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->