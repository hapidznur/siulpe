<!-- start: Content -->
<div id="content">
    <div class="container-fluid">
        <ul class="breadcrumb">
            <li><?php echo set_breadcrumb();?>
        </li>
        </ul>
        <div class="row-fluid sortable">
            <div class="box span12">
                <div class="box-header" data-original-title>
                    <h2><span class="break"></span>Penjadwalan</h2>
                </div>
                <div class="box-content">
                    <table class="table table-striped table-bordered bootstrap-datatable datatable">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th >Tanggal</th>
                            <th >Nama Pengadaan</th>
                            <th >Nama Barang</th>
                            <th >Spesifikasi</th>
                            <th >Volume</th>
                            <th >Harga Satuan</th>
                            <th >Jumlah</th>
                            <th >Jumlah</th>
                            <th >Jumlah</th>
                            <th >Jumlah</th>
                          </tr>
                        </thead>
                        <tbody>
                         <?php $i=0; foreach ($count_data as $count_data ) {?>
                        </tr>
                        <tr>
                          <td><?php echo $count_data['id_pengadaan']; ?></td>
                          <td><?php echo $count_data['id_jadwal']; ?></td>
                          <td><?php echo $count_data['id_ppk']; ?></td>
                          <td class="center"><?php echo $count_data['no_pengadaan']; ?></td>
                          <td class="center"><?php echo $count_data['paket_name']; ?></td>
                          <td class="col-md-2"><?php echo $count_data['date']; ?></td>
                          <td class="col-md-2">
                           <?php echo $count_data['lokasi']; ?>
                          </td>
                          <td><?php echo $count_data['anggaran']; ?></td>
                          <td><?php echo $count_data['status']; ?></td>
                          <td><?php echo $count_data['tahun']; ?></td>
                        </tr>
                         <?php $i++; ?>
                      </tbody>
                    </table>
              </div>
            </div><!--/span-->
      </div><!--/row-->
    </div><!-- </div>/.fluid-container -->
</div>
<!-- end: Content -->