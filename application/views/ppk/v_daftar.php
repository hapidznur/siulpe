<div class="col-sm-10 col-sm-offset-2 col-md-12 col-lg-offset-2 main">
   <div class="row">
        <div class="col-lg-12">
          <form class="form-horizontal" action="<?php echo base_url().'add_user' ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" data-toggle="validator" role="form">
          <fieldset>
            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-2 control-label" for="namappk" style="text-align: left;">Nama PPK</label>
              <div class="col-md-8">
                <input id="namappk" name="namappk" type="text" placeholder="Nama Pejabat Pembuat Komitmen" class="form-control input-md" value="<?php echo set_value('namappk');?>" required>
                <span class="help-block">Tuliskan nama perusahaan/CV/Organisasi milik Anda</span>
              </div>
            </div>
            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-2 control-label" for="NIP" style="text-align: left;">NIP</label>
              <div class="col-md-8">
                <input id="NIP" name="NIP" type="text" placeholder="Nomor Induk Pegawai" class="form-control input-md" data-maxlength="50" required>
                <span class="help-block"></span>
              </div>
            </div>
             <!-- Text input-->
            <div class="form-group">
              <label class="col-md-2 control-label" for="satker" style="text-align: left;">Satuan Kerja</label>
              <div class="col-md-8">
                <input id="satker" name="satker" type="text" placeholder="Satuan Kerja" class="form-control input-md" data-maxlength="50" required>
                <span class="help-block"></span>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 control-label" for="pemilik" style="text-align: left;">Password</label>
              <div class="col-md-3">
                <input id="password" name="password" type="password" placeholder="Tuliskan password bagian ini" class="form-control input-md" data-minlength="12" required>
                <span class="help-block">Minimal 6 karakter untuk password</span>
              </div>
            </div>
            <!-- Button -->
            <div class="form-group">
              <label class="col-md-2 control-label" for="daftar"></label>
              <div class="col-md-4">
                <button id="daftar" name="daftar" class="btn btn-default">Submit</button>
              </div>
            </div>

          </fieldset>
        </form>
        <?php echo form_close(); ?>
</div>
</div>
</div>