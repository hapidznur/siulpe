@extends('template')

@section('title', 'Submit Informasi Harga Barang')

@section('pieces')
  @include('ppk.ppk-menu')
@stop

@section('style')
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" type="text/css" href="{{base_url('plugins/datepicker/datepicker3.css')}}">

@endsection

@section('body')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Informasi Harga
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">Informasi Harga</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-10 col-md-offset-1">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Permintaan Informasi Harga</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form action="{{base_url('ppk/submit_informasi_harga')}}" enctype="multipart/form-data" method="post">
          <div class="box-body">
            <div class="form-group">
              <label class="col-md-2 control-label" for="paket_name">Nama Paket</label>  
              <div class="col-md-10">
                <input id="paket_name" name="paket_name" type="text" placeholder="" class="form-control input-md" required="">
                <span class="help-block">Penulisan nama paket sesuai pada POK</span>  
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 control-label" for="no_pengadaan">No Pengadaan</label>  
              <div class="col-md-10">
                <input id="no_pengadaan" name="no_pengadaan" type="text" placeholder="" class="form-control input-md" required="">
                <span class="help-block">Pada bagian ini tuliskan nomer pengadaan</span>  
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 control-label" for="tanggal_surat">Tanggal surat</label>  
              <div class="col-md-2">
                <input id="tanggal_surat" name="tanggal_surat" type="text" placeholder="" class="form-control input-md" required="">
                <span class="help-block">bagian ini merupakan tanggal surat dikeluarkan</span>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 control-label" for="tanggal_pengadaan">Batas Waktu</label>  
              <div class="col-md-2">
                <input id="tanggal_permintaan" name="tanggal_permintaan" type="text" placeholder="" class="form-control input-md" required="">
                <span class="help-block">bagian ini merupakan tanggal barang diterima</span>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-1 control-label" for="">Jam</label>  
              <div class="col-md-2">
                <input name="time" id="jam" type="text" placeholder="" class="form-control" required="">
                <span class="help-block">bagian ini merupakan jam barang diterima</span> 
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 control-label" for="anggaran detail">Anggaran Detail</label>
              <div class="col-md-4">
                <select id="anggaran_detail" name="anggaran_detail" class="form-control">
                  <option value="rm">RM</option>
                  <option value="bop">BOP</option>
                  <option value="pnbp">PNBP</option>
                </select>
                <span class="help-block"> </span> 
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 control-label" for="tanggal_dipa">Tanggal DIPA</label>  
              <div class="col-md-4">
                <input id="tanggal_dipa" name="tanggal_dipa" type="text" placeholder="" class="form-control input-md" required="">
                <span class="help-block"> </span>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 control-label" for="no_dipa">No DIPA</label>  
              <div class="col-md-4">
                <input id="no_dipa" name="no_dipa" type="text" placeholder="" class="form-control input-md" required="">
                <span class="help-block"> </span>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-6"><span class="help-block"> </span></div>
              <label class="col-md-2 control-label" for="mak">Mata Anggaran Kode</label>  
              <div class="col-md-4">
                <input id="mak" name="mak" type="text" placeholder="" class="form-control input-md" required="" maxlength="6">
                <span class="help-block">harap di isi 6 digit</span>  
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 control-label" for="lokasi">Lokasi Pengadaan</label>  
              <div class="col-md-10">
                <input id="lokasi" name="lokasi" type="text" placeholder="" class="form-control input-md" required="">
                <span class="help-block"> </span>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 control-label" for="ta_anggaran">Tahun Anggaran</label>  
              <div class="col-md-8">
                <input id="ta_anggaran" name="ta_anggaran" type="text" placeholder="" class="form-control input-md" required="">
                <span class="help-block"> </span>
              </div>
              <div class="col-md-6"></div>
            </div>
            <div class="form-group">
              <div class="col-md-8">
                <input id="filexls" name="filexls" class="input-file" type="file" accept=".xls">
                <span class="help-block"> </span>
                </div>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer" style="margin-left: 15px;">
            <button id="submit" name="submit" class="btn btn-primary" type="submit" value="upload">Submit</button>
          </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (left) -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
@stop

@section('script')
  <script src="{{ base_url('plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
  <!-- datepicker -->
  <script src="{{base_url('plugins/datepicker/bootstrap-datepicker.js')}}"></script>

<script>
var dipa = $('#tanggal_dipa').datepicker({ dateFormat: 'dd-mm-yy' }).val();
var  permintaan= $('#tanggal_permintaan').datepicker({ dateFormat: 'dd-mm-yy' }).val();
var tanggal_surat = $('#tanggal_surat').datepicker({ dateFormat: 'dd-mm-yy' }).val();

$('#jam').datetimepicker({
  format: 'LT'
});
</script>
@stop