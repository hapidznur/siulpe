<footer class="main-footer" style="height: 10px;">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.6
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
</footer>

<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>

</div>
<!-- Bootstrap Core JavaScript -->
<!-- Core Scripts - Include with every page -->
<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url('ext/js/jquery.min.js'); ?>"></script>
<!--<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>-->
<!-- jQuery UI 1.11.4 -->
<!--<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>-->
<script src="<?php echo base_url('ext/js/jquery-ui.min.js'); ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('ext/js/bootstrap.min.js'); ?>"></script>
<!--<script src="bootstrap/js/bootstrap.min.js"></script>-->
<!-- Morris.js charts -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>-->
<script src="<?php echo base_url('ext/js/raphael.min.js'); ?>"></script>
<!--<script src="plugins/morris/morris.min.js"></script>-->
<script src="<?php echo base_url('ext/js/morris.min.js'); ?>"></script>
<!-- Sparkline -->
<!--<script src="plugins/sparkline/jquery.sparkline.min.js"></script>-->
<script src="<?php echo base_url('ext/js/jquery.sparkline.min.js'); ?>"></script>
<!-- jvectormap -->
<!--<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>-->
<script src="<?php echo base_url('ext/js/jquery-jvectormap.min.js'); ?>"></script>

<!--<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>-->
<!-- jQuery Knob Chart -->
<!--<script src="plugins/knob/jquery.knob.js"></script>-->
<script src="<?php echo base_url('ext/js/jquery.knob.min.js'); ?>"></script>
<!-- daterangepicker -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>-->
<script src="<?php echo base_url('ext/js/moment.min.js'); ?>"></script>
<!--<script src="plugins/daterangepicker/daterangepicker.js"></script>-->
<script src="<?php echo base_url('ext/js/daterangepicker.min.js'); ?>"></script>
<!-- datepicker -->
<!--<script src="plugins/datepicker/bootstrap-datepicker.js"></script>-->
<script src="<?php echo base_url('ext/js/bootstrap-datepicker.js'); ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<!--<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>-->
<script src="<?php echo base_url('ext/js/bootstrap3-wysihtml5.all.min.js'); ?>"></script>
<!-- Slimscroll -->
<!--<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>-->
<script src="<?php echo base_url('ext/js/jquery.slimscroll.min.js'); ?>"></script>
<!-- FastClick -->
<!--<script src="plugins/fastclick/fastclick.js"></script>-->
<script src="<?php echo base_url('ext/js/fastclick.min.js'); ?>"></script>
<!-- AdminLTE App -->
<!--<script src="dist/js/app.min.js"></script>-->
<script src="<?php echo base_url('ext/js/app.min.js'); ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="dist/js/pages/dashboard.js"></script>-->
<script src="<?php echo base_url('ext/js/dashboard.js'); ?>"></script>
<!-- AdminLTE for demo purposes -->
<!--<script src="dist/js/demo.js"></script>-->
<script src="<?php echo base_url('ext/js/demo.js'); ?>"></script>

<!-- Datatable JQuery Plugin-->
<script src="<?php echo base_url('ext/js/jquery.dataTables.js')?>"></script>

<!-- Datatable JQuery UI Bootstrap Plugin-->
<script src="<?php echo base_url('ext/js/dataTables.bootstrap.js')?>"></script>

<script type="text/javascript">
function script_onload()
{
  if (window.location.pathname == '/siulpe/ppk/verifikasi_penyedia_yang_menawar') {
      var jumlah_penawar = parseInt(document.getElementById("jumlahPenawar").innerHTML);
      if (jumlah_penawar <3) {
        var StatusPenawar = 'Verifikasi tidak bisa dilakukan karena penawar belum mencukupi';
         document.getElementById("statusPengadaan").innerHTML=StatusPenawar;
         var statsVerifikasi = document.getElementById("statsVerifikasi").innerHTML;
         console.log(statsVerifikasi);
         if (statsVerifikasi == 'Ya') {
          document.getElementById("btn-verifikasi").disabled=true;
         } else{
          document.getElementById("btn-verifikasi").disabled=false;
         }

        // console.log('penawar sedikit');
      } else{
        var StatusPenawar = 'Verifikasi bisa dilakukan karena penawar telah mencukupi';
        document.getElementById("statusPengadaan").innerHTML=StatusPenawar;
        // console.log('penawar Banyak');
      }
      // console.log(jumlah_penawar);
  } else{
    console.log('false');
  }

}
</script>
<script type="text/javascript">
window.onload = script_onload;
</script>

<!-- Menu Toggle Script -->
<script type="text/javascript">
    <!-- Perhitungan HPS -->
    $('select#pajak').change(function(){
        var val = parseFloat($(this).val());
        val = (val ? val * $(this).data('biaya') / 100 : '');
        $(this).closest('td').next().find('span#hps').text(val);
        console.log(val)
    });
$('#tanggal_surat').datepicker({format: 'yyyy-mm-dd'});
$('#tanggal_permintaan').datepicker({format: 'yyyy-mm-dd'});
$('#jam').datetimepicker({format: 'hh:ii'});
$('#tanggal_dipa').datepicker({format: 'yyyy-mm-dd'});



</script>

<script type="text/javascript">
$('#xlsfile').change(function(){
     $('#subfile').val($(this).val());
});
</script>

<script type="text/javascript">
function download_files() {
        window.location.href = "<?php echo base_url().'dependencies/default_template_upload_barang.xls'?>";
    }

</script>
<script type="text/javascript">
$('.time').clockpicker();
</script>


<script type="text/javascript">
function showPassword() {

    var key_attr = $('#key').attr('type');

    if(key_attr != 'text') {

        $('.checkbox').addClass('show');
        $('#key').attr('type', 'text');

    } else {

        $('.checkbox').removeClass('show');
        $('#key').attr('type', 'password');

    }

}
</script>
</body>
</html>
