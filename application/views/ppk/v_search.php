    <div class="col-sm-10 col-sm-offset-2 col-md-10 col-lg-offset-2 main">
      <ol class="breadcrumb"><?php //echo ; ?></ol>
      <div class="row">
        <div class="col-sm-8 col-md-8 col-lg-8">
          <h3>Hasil Pencarian : <small><?php echo $kata_cari; ?></small></h3>
        </div>
      </div><!-- end of row-->

      <div class="row">
        <div class="col-lg-12">
          <table class="table table-hover table-bordered" data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
            <thead>
              <tr>
                <th>Nama Pengadaan</th>
                <th>Lokasi</th>
                <th>Tanggal Proses</th>
                <th>Status Pengadaan</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach ($data_cari as $count_data):
                echo form_open('ppk/detail_pengadaan');?>

              <tbody>
                <tr>
                  <td class="text-left"><?php echo $count_data['paket_name']; ?>
                    <input type="hidden" name="paket_name" value=<?php echo $count_data['paket_name']; ?>>
                    <input type="hidden" name="id_pengadaan" value=<?php echo $count_data['id_pengadaan']; ?>>
                    <input type="hidden" name="lokasi" value=<?php echo $count_data['lokasi']; ?>>
                    <input type="hidden" name="no_pengadaan" value=<?php echo $count_data['no_pengadaan']; ?>>
                    <input type="hidden" name="date" value=<?php echo $count_data['tanggal_permintaan']; ?>>
                    <input type="hidden" name="anggaran" value=<?php echo $count_data['anggaran']; ?>><br>
                    <input type="submit" value="details" class="btn btn-succes">
                  </td>
                  <td><?php echo $count_data['lokasi']; ?></td>
                  <td><?php echo $count_data['tanggal_permintaan']; ?></td>
                  <td><?php echo $count_data['status_pengadaan']; ?></td>

                  <!-- <td><?php //echo $count_data['date']; ?></td> -->

                </tr>
              </tbody>
              <?php 
              echo form_close();
              endforeach; ?> 
            </tbody>
          </table>
          <nav>
            <ul class="pagination"><li><a><?php //echo $halaman; ?></a></li></ul>
          </nav>

        </div><!-- end of table-responsive-->
      </div><!-- end of row-->
    </div><!-- end of col-sm-10 col-sm-offset-2 col-md-10 col-lg-offset-2 main -->                  



