<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Pengadaan
    </h1>
<!--        Sementara Breadcumb off ->
  <!--    <ol class="breadcrumb">-->
  <!--      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>-->
  <!--      <li><a href="#">Examples</a></li>-->
  <!--      <li class="active">Blank page</li>-->
  <!--    </ol>-->
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Pengadaan tersedia</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
      <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th >Pengadaan</th>
            <th >Biaya</th>
            <th >Pajak</th>
            <th >HPS</th>
            <th >Details HPS</th>
            <th >Details Barang</th>
            <th >Submit</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($count_data as$key => $count_data_hps): ?>
          <tr>
            <td><?php echo $count_data_hps['paket_name']; ?></td>
            <td><span id="biaya" name="jumlah_harga"><?php echo $count_data_hps['biaya']; ?></span></td>
            <td>
                <select id="pajak" name="biaya_pajak" data-biaya="<?php echo $count_data_hps['biaya']; ?>">
                    <option>--</option>
                    <option value="10">10%</option>
                    <option value="15">15%</option>
                    <option value="20">20%</option>
                    <option value="25">25%</option>
                </select>
          </div></td>
          <td><span name="total_hps" id="hps"></span></td>
          <td id="action">
            <p>
              <a href="<?php echo base_url().'ppk/detail_hps/'.$count_data_hps['id_pengadaan'];?>" class="btn btn-default" > Detail HPS</a>
            </p></td>
            <td id="action">
              <p>
                <a href="<?php echo base_url().'ppk/detail_barang_hps/'.$count_data_hps['id_pengadaan'];?>" class="btn btn-default" > Detail Barang HPS</a>
              </p></td>
              <td id="action">
                <p>
                  <a href="<?php echo base_url().'ppk/insert_hps/'.$count_data_hps['id_pengadaan'];?>" class="btn btn-default" > Insert HPS</a>
                </p></td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
<!-- /.content-wrapper -->