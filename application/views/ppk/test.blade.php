<!DOCTYPE html>
<html>
<head>
  <title>tes</title>

<!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->
<link rel="stylesheet" href="{{ base_url('css/bootstrap.min.css')}}" />

<link rel="stylesheet" type="text/css" href="{{ base_url('plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}">

<script src="{{ base_url('js/moment.min.js')}}"></script>

  <!--  jQuery -->
<script src="{{ base_url('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>


<script src="{{ base_url('js/bootstrap.min.js')}}" type="text/javascript" charset="utf-8" async defer></script>
<!-- Bootstrap Date-Picker Plugin -->
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
 -->


<script src="{{ base_url('plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
 </head>
<body>
   <div class="col-md-6 col-sm-6 col-xs-12">

    <!-- Form code begins -->
    <form method="post">
      <div class="form-group"> <!-- Date input -->
        <label class="control-label" for="date">Date</label>
        <input class="form-control" id="date" name="date" placeholder="MM/DD/YYY" type="text"/>
      </div>
      <div class="form-group"> <!-- Submit button -->
        <button class="btn btn-primary " name="submit" type="submit">Submit</button>
      </div>
     </form>
     <!-- Form code ends --> 

    </div>
<script>
$(function () {
    $('#date').datetimepicker({
      format: 'LT'
    });
});

    // $(document).ready(function(){
    //   var date_input=$('input[name="date"]'); //our date input has the name "date"
    //   var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
    //   var options={
    //     format: 'mm/dd/yyyy',
    //     container: container,
    //     todayHighlight: true,
    //     autoclose: true,
    //   };
    //   date_input.datepicker(options);
    // })
</script>
</body>
</html>