<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<w:wordDocument xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" xmlns:wsp="http://schemas.microsoft.com/office/word/2003/wordml/sp2" xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core" w:macrosPresent="no" w:embeddedObjPresent="no" w:ocxPresent="no" xml:space="preserve">
	<w:ignoreSubtree w:val="http://schemas.microsoft.com/office/word/2003/wordml/sp2"/>
	<o:DocumentProperties>
		<o:Author>ulp admin</o:Author>
		<o:LastAuthor>Al</o:LastAuthor>
		<o:Revision>2</o:Revision>
		<o:TotalTime>1</o:TotalTime>
		<o:LastPrinted>2015-09-03T04:15:00Z</o:LastPrinted>
		<o:Created>2015-12-01T03:01:00Z</o:Created>
		<o:LastSaved>2015-12-01T03:01:00Z</o:LastSaved>
		<o:Pages>1</o:Pages>
		<o:Words>268</o:Words>
		<o:Characters>1532</o:Characters>
		<o:Lines>12</o:Lines>
		<o:Paragraphs>3</o:Paragraphs>
		<o:CharactersWithSpaces>1797</o:CharactersWithSpaces>
		<o:Version>15</o:Version>
	</o:DocumentProperties>
	<w:fonts>
		<w:defaultFonts w:ascii="Calibri" w:fareast="Calibri" w:h-ansi="Calibri" w:cs="Arial"/>
		<w:font w:name="Times New Roman">
			<w:panose-1 w:val="02020603050405020304"/>
			<w:charset w:val="00"/>
			<w:family w:val="Roman"/>
			<w:pitch w:val="variable"/>
			<w:sig w:usb-0="E0002EFF" w:usb-1="C0007843" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="000001FF" w:csb-1="00000000"/>
		</w:font>
		<w:font w:name="Arial">
			<w:panose-1 w:val="020B0604020202020204"/>
			<w:charset w:val="00"/>
			<w:family w:val="Swiss"/>
			<w:pitch w:val="variable"/>
			<w:sig w:usb-0="E0002AFF" w:usb-1="C0007843" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="000001FF" w:csb-1="00000000"/>
		</w:font>
		<w:font w:name="Courier New">
			<w:panose-1 w:val="02070309020205020404"/>
			<w:charset w:val="00"/>
			<w:family w:val="Modern"/>
			<w:pitch w:val="fixed"/>
			<w:sig w:usb-0="E0002AFF" w:usb-1="C0007843" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="000001FF" w:csb-1="00000000"/>
		</w:font>
		<w:font w:name="Symbol">
			<w:panose-1 w:val="05050102010706020507"/>
			<w:charset w:val="02"/>
			<w:family w:val="Roman"/>
			<w:pitch w:val="variable"/>
			<w:sig w:usb-0="00000000" w:usb-1="10000000" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="80000000" w:csb-1="00000000"/>
		</w:font>
		<w:font w:name="Wingdings">
			<w:panose-1 w:val="05000000000000000000"/>
			<w:charset w:val="02"/>
			<w:family w:val="auto"/>
			<w:pitch w:val="variable"/>
			<w:sig w:usb-0="00000000" w:usb-1="10000000" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="80000000" w:csb-1="00000000"/>
		</w:font>
		<w:font w:name="Cambria Math">
			<w:panose-1 w:val="02040503050406030204"/>
			<w:charset w:val="00"/>
			<w:family w:val="Roman"/>
			<w:pitch w:val="variable"/>
			<w:sig w:usb-0="E00002FF" w:usb-1="420024FF" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="0000019F" w:csb-1="00000000"/>
		</w:font>
		<w:font w:name="Calibri">
			<w:panose-1 w:val="020F0502020204030204"/>
			<w:charset w:val="00"/>
			<w:family w:val="Swiss"/>
			<w:pitch w:val="variable"/>
			<w:sig w:usb-0="E00002FF" w:usb-1="4000ACFF" w:usb-2="00000001" w:usb-3="00000000" w:csb-0="0000019F" w:csb-1="00000000"/>
		</w:font>
		<w:font w:name="Segoe UI">
			<w:panose-1 w:val="020B0502040204020203"/>
			<w:charset w:val="00"/>
			<w:family w:val="Swiss"/>
			<w:pitch w:val="variable"/>
			<w:sig w:usb-0="E4002EFF" w:usb-1="C000E47F" w:usb-2="00000009" w:usb-3="00000000" w:csb-0="000001FF" w:csb-1="00000000"/>
		</w:font>
		<w:font w:name="Cambria">
			<w:panose-1 w:val="02040503050406030204"/>
			<w:charset w:val="00"/>
			<w:family w:val="Roman"/>
			<w:pitch w:val="variable"/>
			<w:sig w:usb-0="E00002FF" w:usb-1="400004FF" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="0000019F" w:csb-1="00000000"/>
		</w:font>
	</w:fonts>
	<w:lists>
		<w:listDef w:listDefId="0">
			<w:lsid w:val="01B24694"/>
			<w:plt w:val="SingleLevel"/>
			<w:tmpl w:val="5B5257A9"/>
			<w:lvl w:ilvl="0">
				<w:start w:val="1"/>
				<w:lvlText w:val="%1)"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:tabs>
						<w:tab w:val="list" w:pos="360"/>
					</w:tabs>
					<w:ind w:left="576"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
					<w:snapToGrid/>
					<w:spacing w:val="8"/>
					<w:sz w:val="21"/>
					<w:sz-cs w:val="21"/>
				</w:rPr>
			</w:lvl>
		</w:listDef>
		<w:listDef w:listDefId="1">
			<w:lsid w:val="039BDA06"/>
			<w:plt w:val="SingleLevel"/>
			<w:tmpl w:val="2DA496E3"/>
			<w:lvl w:ilvl="0">
				<w:start w:val="1"/>
				<w:lvlText w:val="%1)"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:tabs>
						<w:tab w:val="list" w:pos="288"/>
					</w:tabs>
					<w:ind w:left="936" w:hanging="288"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
					<w:snapToGrid/>
					<w:spacing w:val="2"/>
					<w:sz w:val="21"/>
					<w:sz-cs w:val="21"/>
				</w:rPr>
			</w:lvl>
		</w:listDef>
		<w:listDef w:listDefId="2">
			<w:lsid w:val="054DFCC3"/>
			<w:plt w:val="SingleLevel"/>
			<w:tmpl w:val="1BD83A01"/>
			<w:lvl w:ilvl="0">
				<w:start w:val="1"/>
				<w:lvlText w:val="%1)"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:tabs>
						<w:tab w:val="list" w:pos="360"/>
					</w:tabs>
					<w:ind w:left="1008" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
					<w:snapToGrid/>
					<w:spacing w:val="8"/>
					<w:sz w:val="21"/>
					<w:sz-cs w:val="21"/>
				</w:rPr>
			</w:lvl>
		</w:listDef>
		<w:listDef w:listDefId="3">
			<w:lsid w:val="0731E7A9"/>
			<w:plt w:val="SingleLevel"/>
			<w:tmpl w:val="04090011"/>
			<w:lvl w:ilvl="0">
				<w:start w:val="1"/>
				<w:lvlText w:val="%1)"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="1224" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:snapToGrid/>
					<w:spacing w:val="11"/>
					<w:sz w:val="21"/>
					<w:sz-cs w:val="21"/>
				</w:rPr>
			</w:lvl>
		</w:listDef>
		<w:listDef w:listDefId="4">
			<w:lsid w:val="0E631CE4"/>
			<w:plt w:val="HybridMultilevel"/>
			<w:tmpl w:val="B060C1C6"/>
			<w:lvl w:ilvl="0" w:tplc="04090019">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%1."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2016" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%2."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2736" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%3."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="3456" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
				<w:start w:val="1"/>
				<w:lvlText w:val="%4."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="4176" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%5."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="4896" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%6."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="5616" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
				<w:start w:val="1"/>
				<w:lvlText w:val="%7."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="6336" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%8."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="7056" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%9."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="7776" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
		</w:listDef>
		<w:listDef w:listDefId="5">
			<w:lsid w:val="105C4E8F"/>
			<w:plt w:val="HybridMultilevel"/>
			<w:tmpl w:val="5156B52A"/>
			<w:lvl w:ilvl="0" w:tplc="98AA3F86">
				<w:start w:val="1"/>
				<w:lvlText w:val="%1."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="1008" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:i w:val="off"/>
					<w:i-cs w:val="off"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="1" w:tplc="A87620D6">
				<w:start w:val="1"/>
				<w:lvlText w:val="%2)"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="1728" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%3."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="2448" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
				<w:start w:val="1"/>
				<w:lvlText w:val="%4."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="3168" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%5."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="3888" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%6."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="4608" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
				<w:start w:val="1"/>
				<w:lvlText w:val="%7."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="5328" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%8."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="6048" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%9."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="6768" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
		</w:listDef>
		<w:listDef w:listDefId="6">
			<w:lsid w:val="12713C32"/>
			<w:plt w:val="HybridMultilevel"/>
			<w:tmpl w:val="6EE825F2"/>
			<w:lvl w:ilvl="0" w:tplc="04210009">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="v"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="720" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="1" w:tplc="04210009">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="v"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="1440" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="2" w:tplc="04210005" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="§"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2160" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="3" w:tplc="04210001" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2880" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="4" w:tplc="04210003" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="o"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="3600" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New" w:cs="Courier New" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="5" w:tplc="04210005" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="§"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="4320" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="6" w:tplc="04210001" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="5040" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="7" w:tplc="04210003" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="o"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="5760" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New" w:cs="Courier New" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="8" w:tplc="04210005" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="§"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="6480" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
				</w:rPr>
			</w:lvl>
		</w:listDef>
		<w:listDef w:listDefId="7">
			<w:lsid w:val="257D541D"/>
			<w:plt w:val="HybridMultilevel"/>
			<w:tmpl w:val="B060C1C6"/>
			<w:lvl w:ilvl="0" w:tplc="04090019">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%1."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2016" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%2."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2736" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%3."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="3456" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
				<w:start w:val="1"/>
				<w:lvlText w:val="%4."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="4176" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%5."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="4896" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%6."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="5616" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
				<w:start w:val="1"/>
				<w:lvlText w:val="%7."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="6336" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%8."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="7056" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%9."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="7776" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
		</w:listDef>
		<w:listDef w:listDefId="8">
			<w:lsid w:val="359B2B12"/>
			<w:plt w:val="HybridMultilevel"/>
			<w:tmpl w:val="C72C619A"/>
			<w:lvl w:ilvl="0" w:tplc="0409000F">
				<w:start w:val="1"/>
				<w:lvlText w:val="%1."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="720" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%2."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="1440" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%3."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="2160" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
				<w:start w:val="1"/>
				<w:lvlText w:val="%4."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2880" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%5."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="3600" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%6."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="4320" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
				<w:start w:val="1"/>
				<w:lvlText w:val="%7."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="5040" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%8."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="5760" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%9."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="6480" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
		</w:listDef>
		<w:listDef w:listDefId="9">
			<w:lsid w:val="3EF04493"/>
			<w:plt w:val="HybridMultilevel"/>
			<w:tmpl w:val="FBE40F98"/>
			<w:lvl w:ilvl="0" w:tplc="0409000D">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="ü"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="720" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="1" w:tplc="04090003" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="o"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="1440" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New" w:cs="Courier New" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="2" w:tplc="04090005" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="§"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2160" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="3" w:tplc="04090001" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2880" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="4" w:tplc="04090003" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="o"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="3600" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New" w:cs="Courier New" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="5" w:tplc="04090005" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="§"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="4320" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="6" w:tplc="04090001" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="5040" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="7" w:tplc="04090003" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="o"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="5760" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New" w:cs="Courier New" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="8" w:tplc="04090005" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="§"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="6480" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
				</w:rPr>
			</w:lvl>
		</w:listDef>
		<w:listDef w:listDefId="10">
			<w:lsid w:val="40CA4A5C"/>
			<w:plt w:val="HybridMultilevel"/>
			<w:tmpl w:val="C72C619A"/>
			<w:lvl w:ilvl="0" w:tplc="0409000F">
				<w:start w:val="1"/>
				<w:lvlText w:val="%1."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="720" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%2."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="1440" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%3."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="2160" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
				<w:start w:val="1"/>
				<w:lvlText w:val="%4."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2880" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%5."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="3600" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%6."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="4320" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
				<w:start w:val="1"/>
				<w:lvlText w:val="%7."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="5040" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%8."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="5760" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%9."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="6480" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
		</w:listDef>
		<w:listDef w:listDefId="11">
			<w:lsid w:val="422827E5"/>
			<w:plt w:val="HybridMultilevel"/>
			<w:tmpl w:val="9C2A6096"/>
			<w:lvl w:ilvl="0" w:tplc="F2206386">
				<w:nfc w:val="23"/>
				<w:lvlText w:val="-"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="720" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Cambria" w:fareast="Times New Roman" w:h-ansi="Cambria" w:cs="Arial" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="1" w:tplc="04090003" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="o"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="1440" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New" w:cs="Courier New" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="2" w:tplc="04090005" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="§"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2160" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="3" w:tplc="04090001" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2880" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="4" w:tplc="04090003" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="o"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="3600" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New" w:cs="Courier New" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="5" w:tplc="04090005" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="§"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="4320" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="6" w:tplc="04090001" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="5040" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="7" w:tplc="04090003" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="o"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="5760" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New" w:cs="Courier New" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="8" w:tplc="04090005" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="§"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="6480" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
				</w:rPr>
			</w:lvl>
		</w:listDef>
		<w:listDef w:listDefId="12">
			<w:lsid w:val="47781CDC"/>
			<w:plt w:val="HybridMultilevel"/>
			<w:tmpl w:val="EF66D990"/>
			<w:lvl w:ilvl="0" w:tplc="500C621A">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%1."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="648" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:hint="default"/>
					<w:i w:val="off"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%2."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="1368" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%3."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="2088" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
				<w:start w:val="1"/>
				<w:lvlText w:val="%4."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2808" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%5."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="3528" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%6."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="4248" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
				<w:start w:val="1"/>
				<w:lvlText w:val="%7."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="4968" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%8."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="5688" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%9."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="6408" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
		</w:listDef>
		<w:listDef w:listDefId="13">
			<w:lsid w:val="50520D73"/>
			<w:plt w:val="HybridMultilevel"/>
			<w:tmpl w:val="39FCC1D4"/>
			<w:lvl w:ilvl="0" w:tplc="AB4E6670">
				<w:nfc w:val="255"/>
				<w:lvlText w:val=""/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="720" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="1" w:tplc="04090003" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="o"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="1440" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New" w:cs="Courier New" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="2" w:tplc="04090005" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="§"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2160" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="3" w:tplc="04090001" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2880" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="4" w:tplc="04090003" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="o"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="3600" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New" w:cs="Courier New" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="5" w:tplc="04090005" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="§"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="4320" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="6" w:tplc="04090001" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="5040" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="7" w:tplc="04090003" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="o"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="5760" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New" w:cs="Courier New" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="8" w:tplc="04090005" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="§"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="6480" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
				</w:rPr>
			</w:lvl>
		</w:listDef>
		<w:listDef w:listDefId="14">
			<w:lsid w:val="5AC343FE"/>
			<w:plt w:val="HybridMultilevel"/>
			<w:tmpl w:val="E5A47858"/>
			<w:lvl w:ilvl="0" w:tplc="5A7A6C8E">
				<w:nfc w:val="23"/>
				<w:lvlText w:val="-"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="720" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Calibri" w:fareast="Times New Roman" w:h-ansi="Calibri" w:cs="Calibri" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="1" w:tplc="04210003" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="o"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="1440" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New" w:cs="Courier New" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="2" w:tplc="04210005" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="§"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2160" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="3" w:tplc="04210001" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2880" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="4" w:tplc="04210003" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="o"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="3600" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New" w:cs="Courier New" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="5" w:tplc="04210005" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="§"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="4320" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="6" w:tplc="04210001" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="5040" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="7" w:tplc="04210003" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="o"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="5760" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New" w:cs="Courier New" w:hint="default"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="8" w:tplc="04210005" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="§"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="6480" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
				</w:rPr>
			</w:lvl>
		</w:listDef>
		<w:listDef w:listDefId="15">
			<w:lsid w:val="639355C9"/>
			<w:plt w:val="HybridMultilevel"/>
			<w:tmpl w:val="B060C1C6"/>
			<w:lvl w:ilvl="0" w:tplc="04090019">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%1."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2016" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%2."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2736" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%3."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="3456" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
				<w:start w:val="1"/>
				<w:lvlText w:val="%4."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="4176" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%5."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="4896" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%6."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="5616" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
				<w:start w:val="1"/>
				<w:lvlText w:val="%7."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="6336" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%8."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="7056" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%9."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="7776" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
		</w:listDef>
		<w:listDef w:listDefId="16">
			<w:lsid w:val="6FE6133B"/>
			<w:plt w:val="HybridMultilevel"/>
			<w:tmpl w:val="FCD41CCA"/>
			<w:lvl w:ilvl="0" w:tplc="04090011">
				<w:start w:val="1"/>
				<w:lvlText w:val="%1)"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="1296" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="1" w:tplc="04090019">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%2."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="2016" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%3."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="2736" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
				<w:start w:val="1"/>
				<w:lvlText w:val="%4."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="3456" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%5."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="4176" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%6."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="4896" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
				<w:start w:val="1"/>
				<w:lvlText w:val="%7."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="5616" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="4"/>
				<w:lvlText w:val="%8."/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:ind w:left="6336" w:hanging="360"/>
				</w:pPr>
			</w:lvl>
			<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="2"/>
				<w:lvlText w:val="%9."/>
				<w:lvlJc w:val="right"/>
				<w:pPr>
					<w:ind w:left="7056" w:hanging="180"/>
				</w:pPr>
			</w:lvl>
		</w:listDef>
		<w:listDef w:listDefId="17">
			<w:lsid w:val="78D9607B"/>
			<w:plt w:val="Multilevel"/>
			<w:tmpl w:val="5226EAFC"/>
			<w:lvl w:ilvl="0">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:tabs>
						<w:tab w:val="list" w:pos="501"/>
					</w:tabs>
					<w:ind w:left="501" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
					<w:sz w:val="20"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="1" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:tabs>
						<w:tab w:val="list" w:pos="1440"/>
					</w:tabs>
					<w:ind w:left="1440" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
					<w:sz w:val="20"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="2" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:tabs>
						<w:tab w:val="list" w:pos="2160"/>
					</w:tabs>
					<w:ind w:left="2160" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
					<w:sz w:val="20"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="3" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:tabs>
						<w:tab w:val="list" w:pos="2880"/>
					</w:tabs>
					<w:ind w:left="2880" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
					<w:sz w:val="20"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="4" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:tabs>
						<w:tab w:val="list" w:pos="3600"/>
					</w:tabs>
					<w:ind w:left="3600" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
					<w:sz w:val="20"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="5" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:tabs>
						<w:tab w:val="list" w:pos="4320"/>
					</w:tabs>
					<w:ind w:left="4320" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
					<w:sz w:val="20"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="6" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:tabs>
						<w:tab w:val="list" w:pos="5040"/>
					</w:tabs>
					<w:ind w:left="5040" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
					<w:sz w:val="20"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="7" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:tabs>
						<w:tab w:val="list" w:pos="5760"/>
					</w:tabs>
					<w:ind w:left="5760" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
					<w:sz w:val="20"/>
				</w:rPr>
			</w:lvl>
			<w:lvl w:ilvl="8" w:tentative="on">
				<w:start w:val="1"/>
				<w:nfc w:val="23"/>
				<w:lvlText w:val="·"/>
				<w:lvlJc w:val="left"/>
				<w:pPr>
					<w:tabs>
						<w:tab w:val="list" w:pos="6480"/>
					</w:tabs>
					<w:ind w:left="6480" w:hanging="360"/>
				</w:pPr>
				<w:rPr>
					<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
					<w:sz w:val="20"/>
				</w:rPr>
			</w:lvl>
		</w:listDef>
		<w:list w:ilfo="1">
			<w:ilst w:val="6"/>
		</w:list>
		<w:list w:ilfo="2">
			<w:ilst w:val="11"/>
		</w:list>
		<w:list w:ilfo="3">
			<w:ilst w:val="13"/>
		</w:list>
		<w:list w:ilfo="4">
			<w:ilst w:val="10"/>
		</w:list>
		<w:list w:ilfo="5">
			<w:ilst w:val="8"/>
		</w:list>
		<w:list w:ilfo="6">
			<w:ilst w:val="3"/>
		</w:list>
		<w:list w:ilfo="7">
			<w:ilst w:val="0"/>
		</w:list>
		<w:list w:ilfo="8">
			<w:ilst w:val="2"/>
		</w:list>
		<w:list w:ilfo="9">
			<w:ilst w:val="1"/>
		</w:list>
		<w:list w:ilfo="10">
			<w:ilst w:val="1"/>
			<w:lvlOverride w:ilvl="0">
				<w:lvl w:ilvl="0">
					<w:lvlText w:val="%1)"/>
					<w:lvlJc w:val="left"/>
					<w:pPr>
						<w:tabs>
							<w:tab w:val="list" w:pos="360"/>
						</w:tabs>
						<w:ind w:left="1008" w:hanging="360"/>
					</w:pPr>
					<w:rPr>
						<w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
						<w:snapToGrid/>
						<w:spacing w:val="2"/>
						<w:sz w:val="21"/>
						<w:sz-cs w:val="21"/>
					</w:rPr>
				</w:lvl>
			</w:lvlOverride>
		</w:list>
		<w:list w:ilfo="11">
			<w:ilst w:val="5"/>
		</w:list>
		<w:list w:ilfo="12">
			<w:ilst w:val="12"/>
		</w:list>
		<w:list w:ilfo="13">
			<w:ilst w:val="16"/>
		</w:list>
		<w:list w:ilfo="14">
			<w:ilst w:val="15"/>
		</w:list>
		<w:list w:ilfo="15">
			<w:ilst w:val="7"/>
		</w:list>
		<w:list w:ilfo="16">
			<w:ilst w:val="4"/>
		</w:list>
		<w:list w:ilfo="17">
			<w:ilst w:val="17"/>
		</w:list>
		<w:list w:ilfo="18">
			<w:ilst w:val="14"/>
		</w:list>
		<w:list w:ilfo="19">
			<w:ilst w:val="9"/>
		</w:list>
	</w:lists>
	<w:styles>
		<w:versionOfBuiltInStylenames w:val="7"/>
		<w:latentStyles w:defLockedState="off" w:latentStyleCount="371">
			<w:lsdException w:name="Normal"/>
			<w:lsdException w:name="heading 1"/>
			<w:lsdException w:name="heading 2"/>
			<w:lsdException w:name="heading 3"/>
			<w:lsdException w:name="heading 4"/>
			<w:lsdException w:name="heading 5"/>
			<w:lsdException w:name="heading 6"/>
			<w:lsdException w:name="heading 7"/>
			<w:lsdException w:name="heading 8"/>
			<w:lsdException w:name="heading 9"/>
			<w:lsdException w:name="index 1"/>
			<w:lsdException w:name="index 2"/>
			<w:lsdException w:name="index 3"/>
			<w:lsdException w:name="index 4"/>
			<w:lsdException w:name="index 5"/>
			<w:lsdException w:name="index 6"/>
			<w:lsdException w:name="index 7"/>
			<w:lsdException w:name="index 8"/>
			<w:lsdException w:name="index 9"/>
			<w:lsdException w:name="toc 1"/>
			<w:lsdException w:name="toc 2"/>
			<w:lsdException w:name="toc 3"/>
			<w:lsdException w:name="toc 4"/>
			<w:lsdException w:name="toc 5"/>
			<w:lsdException w:name="toc 6"/>
			<w:lsdException w:name="toc 7"/>
			<w:lsdException w:name="toc 8"/>
			<w:lsdException w:name="toc 9"/>
			<w:lsdException w:name="Normal Indent"/>
			<w:lsdException w:name="footnote text"/>
			<w:lsdException w:name="annotation text"/>
			<w:lsdException w:name="header"/>
			<w:lsdException w:name="footer"/>
			<w:lsdException w:name="index heading"/>
			<w:lsdException w:name="caption"/>
			<w:lsdException w:name="table of figures"/>
			<w:lsdException w:name="envelope address"/>
			<w:lsdException w:name="envelope return"/>
			<w:lsdException w:name="footnote reference"/>
			<w:lsdException w:name="annotation reference"/>
			<w:lsdException w:name="line number"/>
			<w:lsdException w:name="page number"/>
			<w:lsdException w:name="endnote reference"/>
			<w:lsdException w:name="endnote text"/>
			<w:lsdException w:name="table of authorities"/>
			<w:lsdException w:name="macro"/>
			<w:lsdException w:name="toa heading"/>
			<w:lsdException w:name="List"/>
			<w:lsdException w:name="List Bullet"/>
			<w:lsdException w:name="List Number"/>
			<w:lsdException w:name="List 2"/>
			<w:lsdException w:name="List 3"/>
			<w:lsdException w:name="List 4"/>
			<w:lsdException w:name="List 5"/>
			<w:lsdException w:name="List Bullet 2"/>
			<w:lsdException w:name="List Bullet 3"/>
			<w:lsdException w:name="List Bullet 4"/>
			<w:lsdException w:name="List Bullet 5"/>
			<w:lsdException w:name="List Number 2"/>
			<w:lsdException w:name="List Number 3"/>
			<w:lsdException w:name="List Number 4"/>
			<w:lsdException w:name="List Number 5"/>
			<w:lsdException w:name="Title"/>
			<w:lsdException w:name="Closing"/>
			<w:lsdException w:name="Signature"/>
			<w:lsdException w:name="Default Paragraph Font"/>
			<w:lsdException w:name="Body Text"/>
			<w:lsdException w:name="Body Text Indent"/>
			<w:lsdException w:name="List Continue"/>
			<w:lsdException w:name="List Continue 2"/>
			<w:lsdException w:name="List Continue 3"/>
			<w:lsdException w:name="List Continue 4"/>
			<w:lsdException w:name="List Continue 5"/>
			<w:lsdException w:name="Message Header"/>
			<w:lsdException w:name="Subtitle"/>
			<w:lsdException w:name="Salutation"/>
			<w:lsdException w:name="Date"/>
			<w:lsdException w:name="Body Text First Indent"/>
			<w:lsdException w:name="Body Text First Indent 2"/>
			<w:lsdException w:name="Note Heading"/>
			<w:lsdException w:name="Body Text 2"/>
			<w:lsdException w:name="Body Text 3"/>
			<w:lsdException w:name="Body Text Indent 2"/>
			<w:lsdException w:name="Body Text Indent 3"/>
			<w:lsdException w:name="Block Text"/>
			<w:lsdException w:name="Hyperlink"/>
			<w:lsdException w:name="FollowedHyperlink"/>
			<w:lsdException w:name="Strong"/>
			<w:lsdException w:name="Emphasis"/>
			<w:lsdException w:name="Document Map"/>
			<w:lsdException w:name="Plain Text"/>
			<w:lsdException w:name="E-mail Signature"/>
			<w:lsdException w:name="HTML Top of Form"/>
			<w:lsdException w:name="HTML Bottom of Form"/>
			<w:lsdException w:name="Normal (Web)"/>
			<w:lsdException w:name="HTML Acronym"/>
			<w:lsdException w:name="HTML Address"/>
			<w:lsdException w:name="HTML Cite"/>
			<w:lsdException w:name="HTML Code"/>
			<w:lsdException w:name="HTML Definition"/>
			<w:lsdException w:name="HTML Keyboard"/>
			<w:lsdException w:name="HTML Preformatted"/>
			<w:lsdException w:name="HTML Sample"/>
			<w:lsdException w:name="HTML Typewriter"/>
			<w:lsdException w:name="HTML Variable"/>
			<w:lsdException w:name="Normal Table"/>
			<w:lsdException w:name="annotation subject"/>
			<w:lsdException w:name="No List"/>
			<w:lsdException w:name="Outline List 1"/>
			<w:lsdException w:name="Outline List 2"/>
			<w:lsdException w:name="Outline List 3"/>
			<w:lsdException w:name="Table Simple 1"/>
			<w:lsdException w:name="Table Simple 2"/>
			<w:lsdException w:name="Table Simple 3"/>
			<w:lsdException w:name="Table Classic 1"/>
			<w:lsdException w:name="Table Classic 2"/>
			<w:lsdException w:name="Table Classic 3"/>
			<w:lsdException w:name="Table Classic 4"/>
			<w:lsdException w:name="Table Colorful 1"/>
			<w:lsdException w:name="Table Colorful 2"/>
			<w:lsdException w:name="Table Colorful 3"/>
			<w:lsdException w:name="Table Columns 1"/>
			<w:lsdException w:name="Table Columns 2"/>
			<w:lsdException w:name="Table Columns 3"/>
			<w:lsdException w:name="Table Columns 4"/>
			<w:lsdException w:name="Table Columns 5"/>
			<w:lsdException w:name="Table Grid 1"/>
			<w:lsdException w:name="Table Grid 2"/>
			<w:lsdException w:name="Table Grid 3"/>
			<w:lsdException w:name="Table Grid 4"/>
			<w:lsdException w:name="Table Grid 5"/>
			<w:lsdException w:name="Table Grid 6"/>
			<w:lsdException w:name="Table Grid 7"/>
			<w:lsdException w:name="Table Grid 8"/>
			<w:lsdException w:name="Table List 1"/>
			<w:lsdException w:name="Table List 2"/>
			<w:lsdException w:name="Table List 3"/>
			<w:lsdException w:name="Table List 4"/>
			<w:lsdException w:name="Table List 5"/>
			<w:lsdException w:name="Table List 6"/>
			<w:lsdException w:name="Table List 7"/>
			<w:lsdException w:name="Table List 8"/>
			<w:lsdException w:name="Table 3D effects 1"/>
			<w:lsdException w:name="Table 3D effects 2"/>
			<w:lsdException w:name="Table 3D effects 3"/>
			<w:lsdException w:name="Table Contemporary"/>
			<w:lsdException w:name="Table Elegant"/>
			<w:lsdException w:name="Table Professional"/>
			<w:lsdException w:name="Table Subtle 1"/>
			<w:lsdException w:name="Table Subtle 2"/>
			<w:lsdException w:name="Table Web 1"/>
			<w:lsdException w:name="Table Web 2"/>
			<w:lsdException w:name="Table Web 3"/>
			<w:lsdException w:name="Balloon Text"/>
			<w:lsdException w:name="Table Grid"/>
			<w:lsdException w:name="Table Theme"/>
			<w:lsdException w:name="Placeholder Text"/>
			<w:lsdException w:name="No Spacing"/>
			<w:lsdException w:name="Light Shading"/>
			<w:lsdException w:name="Light List"/>
			<w:lsdException w:name="Light Grid"/>
			<w:lsdException w:name="Medium Shading 1"/>
			<w:lsdException w:name="Medium Shading 2"/>
			<w:lsdException w:name="Medium List 1"/>
			<w:lsdException w:name="Medium List 2"/>
			<w:lsdException w:name="Medium Grid 1"/>
			<w:lsdException w:name="Medium Grid 2"/>
			<w:lsdException w:name="Medium Grid 3"/>
			<w:lsdException w:name="Dark List"/>
			<w:lsdException w:name="Colorful Shading"/>
			<w:lsdException w:name="Colorful List"/>
			<w:lsdException w:name="Colorful Grid"/>
			<w:lsdException w:name="Light Shading Accent 1"/>
			<w:lsdException w:name="Light List Accent 1"/>
			<w:lsdException w:name="Light Grid Accent 1"/>
			<w:lsdException w:name="Medium Shading 1 Accent 1"/>
			<w:lsdException w:name="Medium Shading 2 Accent 1"/>
			<w:lsdException w:name="Medium List 1 Accent 1"/>
			<w:lsdException w:name="Revision"/>
			<w:lsdException w:name="List Paragraph"/>
			<w:lsdException w:name="Quote"/>
			<w:lsdException w:name="Intense Quote"/>
			<w:lsdException w:name="Medium List 2 Accent 1"/>
			<w:lsdException w:name="Medium Grid 1 Accent 1"/>
			<w:lsdException w:name="Medium Grid 2 Accent 1"/>
			<w:lsdException w:name="Medium Grid 3 Accent 1"/>
			<w:lsdException w:name="Dark List Accent 1"/>
			<w:lsdException w:name="Colorful Shading Accent 1"/>
			<w:lsdException w:name="Colorful List Accent 1"/>
			<w:lsdException w:name="Colorful Grid Accent 1"/>
			<w:lsdException w:name="Light Shading Accent 2"/>
			<w:lsdException w:name="Light List Accent 2"/>
			<w:lsdException w:name="Light Grid Accent 2"/>
			<w:lsdException w:name="Medium Shading 1 Accent 2"/>
			<w:lsdException w:name="Medium Shading 2 Accent 2"/>
			<w:lsdException w:name="Medium List 1 Accent 2"/>
			<w:lsdException w:name="Medium List 2 Accent 2"/>
			<w:lsdException w:name="Medium Grid 1 Accent 2"/>
			<w:lsdException w:name="Medium Grid 2 Accent 2"/>
			<w:lsdException w:name="Medium Grid 3 Accent 2"/>
			<w:lsdException w:name="Dark List Accent 2"/>
			<w:lsdException w:name="Colorful Shading Accent 2"/>
			<w:lsdException w:name="Colorful List Accent 2"/>
			<w:lsdException w:name="Colorful Grid Accent 2"/>
			<w:lsdException w:name="Light Shading Accent 3"/>
			<w:lsdException w:name="Light List Accent 3"/>
			<w:lsdException w:name="Light Grid Accent 3"/>
			<w:lsdException w:name="Medium Shading 1 Accent 3"/>
			<w:lsdException w:name="Medium Shading 2 Accent 3"/>
			<w:lsdException w:name="Medium List 1 Accent 3"/>
			<w:lsdException w:name="Medium List 2 Accent 3"/>
			<w:lsdException w:name="Medium Grid 1 Accent 3"/>
			<w:lsdException w:name="Medium Grid 2 Accent 3"/>
			<w:lsdException w:name="Medium Grid 3 Accent 3"/>
			<w:lsdException w:name="Dark List Accent 3"/>
			<w:lsdException w:name="Colorful Shading Accent 3"/>
			<w:lsdException w:name="Colorful List Accent 3"/>
			<w:lsdException w:name="Colorful Grid Accent 3"/>
			<w:lsdException w:name="Light Shading Accent 4"/>
			<w:lsdException w:name="Light List Accent 4"/>
			<w:lsdException w:name="Light Grid Accent 4"/>
			<w:lsdException w:name="Medium Shading 1 Accent 4"/>
			<w:lsdException w:name="Medium Shading 2 Accent 4"/>
			<w:lsdException w:name="Medium List 1 Accent 4"/>
			<w:lsdException w:name="Medium List 2 Accent 4"/>
			<w:lsdException w:name="Medium Grid 1 Accent 4"/>
			<w:lsdException w:name="Medium Grid 2 Accent 4"/>
			<w:lsdException w:name="Medium Grid 3 Accent 4"/>
			<w:lsdException w:name="Dark List Accent 4"/>
			<w:lsdException w:name="Colorful Shading Accent 4"/>
			<w:lsdException w:name="Colorful List Accent 4"/>
			<w:lsdException w:name="Colorful Grid Accent 4"/>
			<w:lsdException w:name="Light Shading Accent 5"/>
			<w:lsdException w:name="Light List Accent 5"/>
			<w:lsdException w:name="Light Grid Accent 5"/>
			<w:lsdException w:name="Medium Shading 1 Accent 5"/>
			<w:lsdException w:name="Medium Shading 2 Accent 5"/>
			<w:lsdException w:name="Medium List 1 Accent 5"/>
			<w:lsdException w:name="Medium List 2 Accent 5"/>
			<w:lsdException w:name="Medium Grid 1 Accent 5"/>
			<w:lsdException w:name="Medium Grid 2 Accent 5"/>
			<w:lsdException w:name="Medium Grid 3 Accent 5"/>
			<w:lsdException w:name="Dark List Accent 5"/>
			<w:lsdException w:name="Colorful Shading Accent 5"/>
			<w:lsdException w:name="Colorful List Accent 5"/>
			<w:lsdException w:name="Colorful Grid Accent 5"/>
			<w:lsdException w:name="Light Shading Accent 6"/>
			<w:lsdException w:name="Light List Accent 6"/>
			<w:lsdException w:name="Light Grid Accent 6"/>
			<w:lsdException w:name="Medium Shading 1 Accent 6"/>
			<w:lsdException w:name="Medium Shading 2 Accent 6"/>
			<w:lsdException w:name="Medium List 1 Accent 6"/>
			<w:lsdException w:name="Medium List 2 Accent 6"/>
			<w:lsdException w:name="Medium Grid 1 Accent 6"/>
			<w:lsdException w:name="Medium Grid 2 Accent 6"/>
			<w:lsdException w:name="Medium Grid 3 Accent 6"/>
			<w:lsdException w:name="Dark List Accent 6"/>
			<w:lsdException w:name="Colorful Shading Accent 6"/>
			<w:lsdException w:name="Colorful List Accent 6"/>
			<w:lsdException w:name="Colorful Grid Accent 6"/>
			<w:lsdException w:name="Subtle Emphasis"/>
			<w:lsdException w:name="Intense Emphasis"/>
			<w:lsdException w:name="Subtle Reference"/>
			<w:lsdException w:name="Intense Reference"/>
			<w:lsdException w:name="Book Title"/>
			<w:lsdException w:name="Bibliography"/>
			<w:lsdException w:name="TOC Heading"/>
			<w:lsdException w:name="Plain Table 1"/>
			<w:lsdException w:name="Plain Table 2"/>
			<w:lsdException w:name="Plain Table 3"/>
			<w:lsdException w:name="Plain Table 4"/>
			<w:lsdException w:name="Plain Table 5"/>
			<w:lsdException w:name="Grid Table Light"/>
			<w:lsdException w:name="Grid Table 1 Light"/>
			<w:lsdException w:name="Grid Table 2"/>
			<w:lsdException w:name="Grid Table 3"/>
			<w:lsdException w:name="Grid Table 4"/>
			<w:lsdException w:name="Grid Table 5 Dark"/>
			<w:lsdException w:name="Grid Table 6 Colorful"/>
			<w:lsdException w:name="Grid Table 7 Colorful"/>
			<w:lsdException w:name="Grid Table 1 Light Accent 1"/>
			<w:lsdException w:name="Grid Table 2 Accent 1"/>
			<w:lsdException w:name="Grid Table 3 Accent 1"/>
			<w:lsdException w:name="Grid Table 4 Accent 1"/>
			<w:lsdException w:name="Grid Table 5 Dark Accent 1"/>
			<w:lsdException w:name="Grid Table 6 Colorful Accent 1"/>
			<w:lsdException w:name="Grid Table 7 Colorful Accent 1"/>
			<w:lsdException w:name="Grid Table 1 Light Accent 2"/>
			<w:lsdException w:name="Grid Table 2 Accent 2"/>
			<w:lsdException w:name="Grid Table 3 Accent 2"/>
			<w:lsdException w:name="Grid Table 4 Accent 2"/>
			<w:lsdException w:name="Grid Table 5 Dark Accent 2"/>
			<w:lsdException w:name="Grid Table 6 Colorful Accent 2"/>
			<w:lsdException w:name="Grid Table 7 Colorful Accent 2"/>
			<w:lsdException w:name="Grid Table 1 Light Accent 3"/>
			<w:lsdException w:name="Grid Table 2 Accent 3"/>
			<w:lsdException w:name="Grid Table 3 Accent 3"/>
			<w:lsdException w:name="Grid Table 4 Accent 3"/>
			<w:lsdException w:name="Grid Table 5 Dark Accent 3"/>
			<w:lsdException w:name="Grid Table 6 Colorful Accent 3"/>
			<w:lsdException w:name="Grid Table 7 Colorful Accent 3"/>
			<w:lsdException w:name="Grid Table 1 Light Accent 4"/>
			<w:lsdException w:name="Grid Table 2 Accent 4"/>
			<w:lsdException w:name="Grid Table 3 Accent 4"/>
			<w:lsdException w:name="Grid Table 4 Accent 4"/>
			<w:lsdException w:name="Grid Table 5 Dark Accent 4"/>
			<w:lsdException w:name="Grid Table 6 Colorful Accent 4"/>
			<w:lsdException w:name="Grid Table 7 Colorful Accent 4"/>
			<w:lsdException w:name="Grid Table 1 Light Accent 5"/>
			<w:lsdException w:name="Grid Table 2 Accent 5"/>
			<w:lsdException w:name="Grid Table 3 Accent 5"/>
			<w:lsdException w:name="Grid Table 4 Accent 5"/>
			<w:lsdException w:name="Grid Table 5 Dark Accent 5"/>
			<w:lsdException w:name="Grid Table 6 Colorful Accent 5"/>
			<w:lsdException w:name="Grid Table 7 Colorful Accent 5"/>
			<w:lsdException w:name="Grid Table 1 Light Accent 6"/>
			<w:lsdException w:name="Grid Table 2 Accent 6"/>
			<w:lsdException w:name="Grid Table 3 Accent 6"/>
			<w:lsdException w:name="Grid Table 4 Accent 6"/>
			<w:lsdException w:name="Grid Table 5 Dark Accent 6"/>
			<w:lsdException w:name="Grid Table 6 Colorful Accent 6"/>
			<w:lsdException w:name="Grid Table 7 Colorful Accent 6"/>
			<w:lsdException w:name="List Table 1 Light"/>
			<w:lsdException w:name="List Table 2"/>
			<w:lsdException w:name="List Table 3"/>
			<w:lsdException w:name="List Table 4"/>
			<w:lsdException w:name="List Table 5 Dark"/>
			<w:lsdException w:name="List Table 6 Colorful"/>
			<w:lsdException w:name="List Table 7 Colorful"/>
			<w:lsdException w:name="List Table 1 Light Accent 1"/>
			<w:lsdException w:name="List Table 2 Accent 1"/>
			<w:lsdException w:name="List Table 3 Accent 1"/>
			<w:lsdException w:name="List Table 4 Accent 1"/>
			<w:lsdException w:name="List Table 5 Dark Accent 1"/>
			<w:lsdException w:name="List Table 6 Colorful Accent 1"/>
			<w:lsdException w:name="List Table 7 Colorful Accent 1"/>
			<w:lsdException w:name="List Table 1 Light Accent 2"/>
			<w:lsdException w:name="List Table 2 Accent 2"/>
			<w:lsdException w:name="List Table 3 Accent 2"/>
			<w:lsdException w:name="List Table 4 Accent 2"/>
			<w:lsdException w:name="List Table 5 Dark Accent 2"/>
			<w:lsdException w:name="List Table 6 Colorful Accent 2"/>
			<w:lsdException w:name="List Table 7 Colorful Accent 2"/>
			<w:lsdException w:name="List Table 1 Light Accent 3"/>
			<w:lsdException w:name="List Table 2 Accent 3"/>
			<w:lsdException w:name="List Table 3 Accent 3"/>
			<w:lsdException w:name="List Table 4 Accent 3"/>
			<w:lsdException w:name="List Table 5 Dark Accent 3"/>
			<w:lsdException w:name="List Table 6 Colorful Accent 3"/>
			<w:lsdException w:name="List Table 7 Colorful Accent 3"/>
			<w:lsdException w:name="List Table 1 Light Accent 4"/>
			<w:lsdException w:name="List Table 2 Accent 4"/>
			<w:lsdException w:name="List Table 3 Accent 4"/>
			<w:lsdException w:name="List Table 4 Accent 4"/>
			<w:lsdException w:name="List Table 5 Dark Accent 4"/>
			<w:lsdException w:name="List Table 6 Colorful Accent 4"/>
			<w:lsdException w:name="List Table 7 Colorful Accent 4"/>
			<w:lsdException w:name="List Table 1 Light Accent 5"/>
			<w:lsdException w:name="List Table 2 Accent 5"/>
			<w:lsdException w:name="List Table 3 Accent 5"/>
			<w:lsdException w:name="List Table 4 Accent 5"/>
			<w:lsdException w:name="List Table 5 Dark Accent 5"/>
			<w:lsdException w:name="List Table 6 Colorful Accent 5"/>
			<w:lsdException w:name="List Table 7 Colorful Accent 5"/>
			<w:lsdException w:name="List Table 1 Light Accent 6"/>
			<w:lsdException w:name="List Table 2 Accent 6"/>
			<w:lsdException w:name="List Table 3 Accent 6"/>
			<w:lsdException w:name="List Table 4 Accent 6"/>
			<w:lsdException w:name="List Table 5 Dark Accent 6"/>
			<w:lsdException w:name="List Table 6 Colorful Accent 6"/>
			<w:lsdException w:name="List Table 7 Colorful Accent 6"/>
		</w:latentStyles>
		<w:style w:type="paragraph" w:default="on" w:styleId="Normal">
			<w:name w:val="Normal"/>
			<w:rsid w:val="00F91D2A"/>
			<w:rPr>
				<w:rFonts w:ascii="Times New Roman" w:fareast="Times New Roman" w:h-ansi="Times New Roman" w:cs="Times New Roman"/>
				<wx:font wx:val="Times New Roman"/>
				<w:sz w:val="24"/>
				<w:sz-cs w:val="24"/>
				<w:lang w:val="EN-GB" w:fareast="EN-US" w:bidi="AR-SA"/>
			</w:rPr>
		</w:style>
		<w:style w:type="character" w:default="on" w:styleId="DefaultParagraphFont">
			<w:name w:val="Default Paragraph Font"/>
		</w:style>
		<w:style w:type="table" w:default="on" w:styleId="TableNormal">
			<w:name w:val="Normal Table"/>
			<wx:uiName wx:val="Table Normal"/>
			<w:rPr>
				<wx:font wx:val="Calibri"/>
				<w:lang w:val="EN-US" w:fareast="EN-US" w:bidi="AR-SA"/>
			</w:rPr>
			<w:tblPr>
				<w:tblInd w:w="0" w:type="dxa"/>
				<w:tblCellMar>
					<w:top w:w="0" w:type="dxa"/>
					<w:left w:w="108" w:type="dxa"/>
					<w:bottom w:w="0" w:type="dxa"/>
					<w:right w:w="108" w:type="dxa"/>
				</w:tblCellMar>
			</w:tblPr>
		</w:style>
		<w:style w:type="list" w:default="on" w:styleId="NoList">
			<w:name w:val="No List"/>
		</w:style>
		<w:style w:type="paragraph" w:styleId="ListParagraph">
			<w:name w:val="List Paragraph"/>
			<w:basedOn w:val="Normal"/>
			<w:rsid w:val="00F91D2A"/>
			<w:pPr>
				<w:ind w:left="720"/>
			</w:pPr>
			<w:rPr>
				<wx:font wx:val="Times New Roman"/>
			</w:rPr>
		</w:style>
		<w:style w:type="character" w:styleId="Hyperlink">
			<w:name w:val="Hyperlink"/>
			<w:rsid w:val="00F91D2A"/>
			<w:rPr>
				<w:rFonts w:cs="Times New Roman"/>
				<w:color w:val="0000FF"/>
				<w:u w:val="single"/>
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="Style1">
			<w:name w:val="Style 1"/>
			<w:basedOn w:val="Normal"/>
			<w:rsid w:val="003C4CF9"/>
			<w:pPr>
				<w:widowControl w:val="off"/>
				<w:autoSpaceDE w:val="off"/>
				<w:autoSpaceDN w:val="off"/>
				<w:adjustRightInd w:val="off"/>
			</w:pPr>
			<w:rPr>
				<wx:font wx:val="Times New Roman"/>
				<w:sz w:val="20"/>
				<w:sz-cs w:val="20"/>
				<w:lang w:val="EN-US"/>
			</w:rPr>
		</w:style>
		<w:style w:type="character" w:styleId="CharacterStyle1">
			<w:name w:val="Character Style 1"/>
			<w:rsid w:val="003C4CF9"/>
			<w:rPr>
				<w:sz w:val="20"/>
				<w:sz-cs w:val="20"/>
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="Style2">
			<w:name w:val="Style 2"/>
			<w:basedOn w:val="Normal"/>
			<w:rsid w:val="00E2451C"/>
			<w:pPr>
				<w:widowControl w:val="off"/>
				<w:autoSpaceDE w:val="off"/>
				<w:autoSpaceDN w:val="off"/>
				<w:spacing w:before="180" w:line="208" w:line-rule="auto"/>
				<w:ind w:left="864"/>
			</w:pPr>
			<w:rPr>
				<w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
				<wx:font wx:val="Arial"/>
				<w:sz w:val="21"/>
				<w:sz-cs w:val="21"/>
				<w:lang w:val="EN-US"/>
			</w:rPr>
		</w:style>
		<w:style w:type="character" w:styleId="CharacterStyle2">
			<w:name w:val="Character Style 2"/>
			<w:rsid w:val="00E2451C"/>
			<w:rPr>
				<w:sz w:val="20"/>
				<w:sz-cs w:val="20"/>
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="BalloonText">
			<w:name w:val="Balloon Text"/>
			<w:basedOn w:val="Normal"/>
			<w:link w:val="BalloonTextChar"/>
			<w:rsid w:val="00D87330"/>
			<w:rPr>
				<w:rFonts w:ascii="Segoe UI" w:h-ansi="Segoe UI" w:cs="Segoe UI"/>
				<wx:font wx:val="Segoe UI"/>
				<w:sz w:val="18"/>
				<w:sz-cs w:val="18"/>
			</w:rPr>
		</w:style>
		<w:style w:type="character" w:styleId="BalloonTextChar">
			<w:name w:val="Balloon Text Char"/>
			<w:link w:val="BalloonText"/>
			<w:rsid w:val="00D87330"/>
			<w:rPr>
				<w:rFonts w:ascii="Segoe UI" w:fareast="Times New Roman" w:h-ansi="Segoe UI" w:cs="Segoe UI"/>
				<w:sz w:val="18"/>
				<w:sz-cs w:val="18"/>
				<w:lang w:val="EN-GB"/>
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="Header">
			<w:name w:val="header"/>
			<wx:uiName wx:val="Header"/>
			<w:basedOn w:val="Normal"/>
			<w:link w:val="HeaderChar"/>
			<w:rsid w:val="00E42F09"/>
			<w:pPr>
				<w:tabs>
					<w:tab w:val="center" w:pos="4680"/>
					<w:tab w:val="right" w:pos="9360"/>
				</w:tabs>
			</w:pPr>
			<w:rPr>
				<wx:font wx:val="Times New Roman"/>
			</w:rPr>
		</w:style>
		<w:style w:type="character" w:styleId="HeaderChar">
			<w:name w:val="Header Char"/>
			<w:link w:val="Header"/>
			<w:rsid w:val="00E42F09"/>
			<w:rPr>
				<w:rFonts w:ascii="Times New Roman" w:fareast="Times New Roman" w:h-ansi="Times New Roman" w:cs="Times New Roman"/>
				<w:sz w:val="24"/>
				<w:sz-cs w:val="24"/>
				<w:lang w:val="EN-GB"/>
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="Footer">
			<w:name w:val="footer"/>
			<wx:uiName wx:val="Footer"/>
			<w:basedOn w:val="Normal"/>
			<w:link w:val="FooterChar"/>
			<w:rsid w:val="00E42F09"/>
			<w:pPr>
				<w:tabs>
					<w:tab w:val="center" w:pos="4680"/>
					<w:tab w:val="right" w:pos="9360"/>
				</w:tabs>
			</w:pPr>
			<w:rPr>
				<wx:font wx:val="Times New Roman"/>
			</w:rPr>
		</w:style>
		<w:style w:type="character" w:styleId="FooterChar">
			<w:name w:val="Footer Char"/>
			<w:link w:val="Footer"/>
			<w:rsid w:val="00E42F09"/>
			<w:rPr>
				<w:rFonts w:ascii="Times New Roman" w:fareast="Times New Roman" w:h-ansi="Times New Roman" w:cs="Times New Roman"/>
				<w:sz w:val="24"/>
				<w:sz-cs w:val="24"/>
				<w:lang w:val="EN-GB"/>
			</w:rPr>
		</w:style>
	</w:styles>
	<w:divs>
		<w:div w:id="55711623">
			<w:bodyDiv w:val="on"/>
			<w:marLeft w:val="0"/>
			<w:marRight w:val="0"/>
			<w:marTop w:val="0"/>
			<w:marBottom w:val="0"/>
			<w:divBdr>
				<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
				<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
				<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
				<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
			</w:divBdr>
		</w:div>
		<w:div w:id="484709787">
			<w:bodyDiv w:val="on"/>
			<w:marLeft w:val="0"/>
			<w:marRight w:val="0"/>
			<w:marTop w:val="0"/>
			<w:marBottom w:val="0"/>
			<w:divBdr>
				<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
				<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
				<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
				<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
			</w:divBdr>
		</w:div>
		<w:div w:id="1010257158">
			<w:bodyDiv w:val="on"/>
			<w:marLeft w:val="0"/>
			<w:marRight w:val="0"/>
			<w:marTop w:val="0"/>
			<w:marBottom w:val="0"/>
			<w:divBdr>
				<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
				<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
				<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
				<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
			</w:divBdr>
		</w:div>
		<w:div w:id="1085882544">
			<w:bodyDiv w:val="on"/>
			<w:marLeft w:val="0"/>
			<w:marRight w:val="0"/>
			<w:marTop w:val="0"/>
			<w:marBottom w:val="0"/>
			<w:divBdr>
				<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
				<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
				<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
				<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
			</w:divBdr>
		</w:div>
		<w:div w:id="1635745445">
			<w:bodyDiv w:val="on"/>
			<w:marLeft w:val="0"/>
			<w:marRight w:val="0"/>
			<w:marTop w:val="0"/>
			<w:marBottom w:val="0"/>
			<w:divBdr>
				<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
				<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
				<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
				<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
			</w:divBdr>
		</w:div>
	</w:divs>
	<w:shapeDefaults>
		<o:shapedefaults v:ext="edit" spidmax="1028"/>
		<o:shapelayout v:ext="edit">
			<o:idmap v:ext="edit" data="1"/>
		</o:shapelayout>
	</w:shapeDefaults>
	<w:docPr>
		<w:view w:val="web"/>
		<w:zoom w:percent="100"/>
		<w:doNotEmbedSystemFonts/>
		<w:defaultTabStop w:val="720"/>
		<w:punctuationKerning/>
		<w:characterSpacingControl w:val="DontCompress"/>
		<w:optimizeForBrowser/>
		<w:relyOnVML/>
		<w:allowPNG/>
		<w:validateAgainstSchema/>
		<w:saveInvalidXML w:val="off"/>
		<w:ignoreMixedContent w:val="off"/>
		<w:alwaysShowPlaceholderText w:val="off"/>
		<w:footnotePr>
			<w:footnote w:type="separator">
				<w:p wsp:rsidR="00F17A10" wsp:rsidRDefault="00F17A10" wsp:rsidP="00E42F09">
					<w:r>
						<w:separator/>
					</w:r>
				</w:p>
			</w:footnote>
			<w:footnote w:type="continuation-separator">
				<w:p wsp:rsidR="00F17A10" wsp:rsidRDefault="00F17A10" wsp:rsidP="00E42F09">
					<w:r>
						<w:continuationSeparator/>
					</w:r>
				</w:p>
			</w:footnote>
		</w:footnotePr>
		<w:endnotePr>
			<w:endnote w:type="separator">
				<w:p wsp:rsidR="00F17A10" wsp:rsidRDefault="00F17A10" wsp:rsidP="00E42F09">
					<w:r>
						<w:separator/>
					</w:r>
				</w:p>
			</w:endnote>
			<w:endnote w:type="continuation-separator">
				<w:p wsp:rsidR="00F17A10" wsp:rsidRDefault="00F17A10" wsp:rsidP="00E42F09">
					<w:r>
						<w:continuationSeparator/>
					</w:r>
				</w:p>
			</w:endnote>
		</w:endnotePr>
		<w:compat>
			<w:breakWrappedTables/>
			<w:snapToGridInCell/>
			<w:wrapTextWithPunct/>
			<w:useAsianBreakRules/>
			<w:dontGrowAutofit/>
		</w:compat>
		<wsp:rsids>
			<wsp:rsidRoot wsp:val="00F91D2A"/>
			<wsp:rsid wsp:val="00000ACA"/>
			<wsp:rsid wsp:val="00007714"/>
			<wsp:rsid wsp:val="00024264"/>
			<wsp:rsid wsp:val="00070DCA"/>
			<wsp:rsid wsp:val="00086A99"/>
			<wsp:rsid wsp:val="000A0482"/>
			<wsp:rsid wsp:val="000C7157"/>
			<wsp:rsid wsp:val="000E0F52"/>
			<wsp:rsid wsp:val="00116C46"/>
			<wsp:rsid wsp:val="00117592"/>
			<wsp:rsid wsp:val="00132759"/>
			<wsp:rsid wsp:val="001358D5"/>
			<wsp:rsid wsp:val="00166C6D"/>
			<wsp:rsid wsp:val="00196D57"/>
			<wsp:rsid wsp:val="001C2357"/>
			<wsp:rsid wsp:val="001C5FE6"/>
			<wsp:rsid wsp:val="001D698D"/>
			<wsp:rsid wsp:val="001D73C5"/>
			<wsp:rsid wsp:val="00202700"/>
			<wsp:rsid wsp:val="0020691D"/>
			<wsp:rsid wsp:val="00233905"/>
			<wsp:rsid wsp:val="00234D53"/>
			<wsp:rsid wsp:val="002454F8"/>
			<wsp:rsid wsp:val="002669F2"/>
			<wsp:rsid wsp:val="00270425"/>
			<wsp:rsid wsp:val="00283FB9"/>
			<wsp:rsid wsp:val="00294A05"/>
			<wsp:rsid wsp:val="00294E51"/>
			<wsp:rsid wsp:val="002B0D47"/>
			<wsp:rsid wsp:val="002B5E59"/>
			<wsp:rsid wsp:val="002D09B7"/>
			<wsp:rsid wsp:val="002E3A8C"/>
			<wsp:rsid wsp:val="00330F79"/>
			<wsp:rsid wsp:val="00350FB3"/>
			<wsp:rsid wsp:val="0035704E"/>
			<wsp:rsid wsp:val="00361445"/>
			<wsp:rsid wsp:val="00370984"/>
			<wsp:rsid wsp:val="003717F9"/>
			<wsp:rsid wsp:val="003A5990"/>
			<wsp:rsid wsp:val="003A5A9F"/>
			<wsp:rsid wsp:val="003C4CF9"/>
			<wsp:rsid wsp:val="003E3E02"/>
			<wsp:rsid wsp:val="00402049"/>
			<wsp:rsid wsp:val="00417895"/>
			<wsp:rsid wsp:val="004203F1"/>
			<wsp:rsid wsp:val="0043456B"/>
			<wsp:rsid wsp:val="004434CA"/>
			<wsp:rsid wsp:val="00463F46"/>
			<wsp:rsid wsp:val="004650C7"/>
			<wsp:rsid wsp:val="00472125"/>
			<wsp:rsid wsp:val="00474846"/>
			<wsp:rsid wsp:val="00474CE0"/>
			<wsp:rsid wsp:val="00495653"/>
			<wsp:rsid wsp:val="004A6376"/>
			<wsp:rsid wsp:val="004E3CB5"/>
			<wsp:rsid wsp:val="004F3BE7"/>
			<wsp:rsid wsp:val="00515BD4"/>
			<wsp:rsid wsp:val="0052253D"/>
			<wsp:rsid wsp:val="00523B71"/>
			<wsp:rsid wsp:val="005369AC"/>
			<wsp:rsid wsp:val="00560FB1"/>
			<wsp:rsid wsp:val="00575468"/>
			<wsp:rsid wsp:val="00577667"/>
			<wsp:rsid wsp:val="005B4985"/>
			<wsp:rsid wsp:val="005C061A"/>
			<wsp:rsid wsp:val="005D009F"/>
			<wsp:rsid wsp:val="005F67BE"/>
			<wsp:rsid wsp:val="0061372D"/>
			<wsp:rsid wsp:val="00616320"/>
			<wsp:rsid wsp:val="006444BE"/>
			<wsp:rsid wsp:val="006632D6"/>
			<wsp:rsid wsp:val="00667619"/>
			<wsp:rsid wsp:val="006710F1"/>
			<wsp:rsid wsp:val="006901E4"/>
			<wsp:rsid wsp:val="00697D34"/>
			<wsp:rsid wsp:val="006A3CB0"/>
			<wsp:rsid wsp:val="006C3ADE"/>
			<wsp:rsid wsp:val="006D5A96"/>
			<wsp:rsid wsp:val="00711C84"/>
			<wsp:rsid wsp:val="00735BC7"/>
			<wsp:rsid wsp:val="00744075"/>
			<wsp:rsid wsp:val="0075600B"/>
			<wsp:rsid wsp:val="007759EE"/>
			<wsp:rsid wsp:val="00777E2E"/>
			<wsp:rsid wsp:val="00791790"/>
			<wsp:rsid wsp:val="007B344B"/>
			<wsp:rsid wsp:val="007C4A0B"/>
			<wsp:rsid wsp:val="007C704D"/>
			<wsp:rsid wsp:val="007D06E2"/>
			<wsp:rsid wsp:val="007D5E59"/>
			<wsp:rsid wsp:val="007E1A1C"/>
			<wsp:rsid wsp:val="007F2A58"/>
			<wsp:rsid wsp:val="008050AA"/>
			<wsp:rsid wsp:val="0081153E"/>
			<wsp:rsid wsp:val="00852129"/>
			<wsp:rsid wsp:val="00856FEC"/>
			<wsp:rsid wsp:val="00873A0A"/>
			<wsp:rsid wsp:val="00875476"/>
			<wsp:rsid wsp:val="00876ACA"/>
			<wsp:rsid wsp:val="0088496F"/>
			<wsp:rsid wsp:val="008851C2"/>
			<wsp:rsid wsp:val="008B19D4"/>
			<wsp:rsid wsp:val="008D2091"/>
			<wsp:rsid wsp:val="008E18C9"/>
			<wsp:rsid wsp:val="008F1501"/>
			<wsp:rsid wsp:val="00924ECB"/>
			<wsp:rsid wsp:val="0092680B"/>
			<wsp:rsid wsp:val="00926A0D"/>
			<wsp:rsid wsp:val="009551A9"/>
			<wsp:rsid wsp:val="00964034"/>
			<wsp:rsid wsp:val="009831CC"/>
			<wsp:rsid wsp:val="0099321D"/>
			<wsp:rsid wsp:val="009A6FFB"/>
			<wsp:rsid wsp:val="009C026D"/>
			<wsp:rsid wsp:val="009C0716"/>
			<wsp:rsid wsp:val="00A0037A"/>
			<wsp:rsid wsp:val="00A07361"/>
			<wsp:rsid wsp:val="00A1062A"/>
			<wsp:rsid wsp:val="00A359BD"/>
			<wsp:rsid wsp:val="00A50579"/>
			<wsp:rsid wsp:val="00A74295"/>
			<wsp:rsid wsp:val="00A93319"/>
			<wsp:rsid wsp:val="00A9781E"/>
			<wsp:rsid wsp:val="00AA1357"/>
			<wsp:rsid wsp:val="00AA197E"/>
			<wsp:rsid wsp:val="00AA36E1"/>
			<wsp:rsid wsp:val="00AB1935"/>
			<wsp:rsid wsp:val="00AB5566"/>
			<wsp:rsid wsp:val="00AC2ADB"/>
			<wsp:rsid wsp:val="00AC33A5"/>
			<wsp:rsid wsp:val="00AD1DF3"/>
			<wsp:rsid wsp:val="00AF076F"/>
			<wsp:rsid wsp:val="00AF2400"/>
			<wsp:rsid wsp:val="00B23B42"/>
			<wsp:rsid wsp:val="00B53BC9"/>
			<wsp:rsid wsp:val="00B8040E"/>
			<wsp:rsid wsp:val="00B80B11"/>
			<wsp:rsid wsp:val="00B83A82"/>
			<wsp:rsid wsp:val="00B85DA7"/>
			<wsp:rsid wsp:val="00B90779"/>
			<wsp:rsid wsp:val="00BB0A6D"/>
			<wsp:rsid wsp:val="00BC4EBB"/>
			<wsp:rsid wsp:val="00BD1C66"/>
			<wsp:rsid wsp:val="00BE44FF"/>
			<wsp:rsid wsp:val="00C0561D"/>
			<wsp:rsid wsp:val="00C109DB"/>
			<wsp:rsid wsp:val="00C1297C"/>
			<wsp:rsid wsp:val="00C3261C"/>
			<wsp:rsid wsp:val="00C603B2"/>
			<wsp:rsid wsp:val="00C63267"/>
			<wsp:rsid wsp:val="00C736DD"/>
			<wsp:rsid wsp:val="00C75ED0"/>
			<wsp:rsid wsp:val="00C90EBE"/>
			<wsp:rsid wsp:val="00D02771"/>
			<wsp:rsid wsp:val="00D801C0"/>
			<wsp:rsid wsp:val="00D87330"/>
			<wsp:rsid wsp:val="00D87AB8"/>
			<wsp:rsid wsp:val="00D91978"/>
			<wsp:rsid wsp:val="00D95590"/>
			<wsp:rsid wsp:val="00DB77BE"/>
			<wsp:rsid wsp:val="00DE2028"/>
			<wsp:rsid wsp:val="00DF7645"/>
			<wsp:rsid wsp:val="00E14AE6"/>
			<wsp:rsid wsp:val="00E2157A"/>
			<wsp:rsid wsp:val="00E2451C"/>
			<wsp:rsid wsp:val="00E314D7"/>
			<wsp:rsid wsp:val="00E42F09"/>
			<wsp:rsid wsp:val="00E837F2"/>
			<wsp:rsid wsp:val="00E907EF"/>
			<wsp:rsid wsp:val="00EA6625"/>
			<wsp:rsid wsp:val="00ED50A8"/>
			<wsp:rsid wsp:val="00ED734A"/>
			<wsp:rsid wsp:val="00ED7865"/>
			<wsp:rsid wsp:val="00EE243C"/>
			<wsp:rsid wsp:val="00F05CC6"/>
			<wsp:rsid wsp:val="00F17A10"/>
			<wsp:rsid wsp:val="00F4300D"/>
			<wsp:rsid wsp:val="00F46A33"/>
			<wsp:rsid wsp:val="00F55E99"/>
			<wsp:rsid wsp:val="00F57E0E"/>
			<wsp:rsid wsp:val="00F61184"/>
			<wsp:rsid wsp:val="00F66BC7"/>
			<wsp:rsid wsp:val="00F700FD"/>
			<wsp:rsid wsp:val="00F70D5D"/>
			<wsp:rsid wsp:val="00F8677F"/>
			<wsp:rsid wsp:val="00F91D2A"/>
			<wsp:rsid wsp:val="00F94E43"/>
			<wsp:rsid wsp:val="00F96D2F"/>
			<wsp:rsid wsp:val="00FA2859"/>
			<wsp:rsid wsp:val="00FA3295"/>
			<wsp:rsid wsp:val="00FA568D"/>
			<wsp:rsid wsp:val="00FD42AB"/>
		</wsp:rsids>
	</w:docPr>
	<w:body>
		<wx:sect>
			<w:tbl>
				<w:tblPr>
					<w:tblW w:w="0" w:type="auto"/>
					<w:jc w:val="center"/>
					<w:tblLayout w:type="Fixed"/>
					<w:tblLook w:val="01E0"/>
				</w:tblPr>
				<w:tblGrid>
					<w:gridCol w:w="9348"/>
				</w:tblGrid>
				<w:tr wsp:rsidR="003717F9" wsp:rsidRPr="006D5A96" wsp:rsidTr="00B83A82">
					<w:trPr>
						<w:trHeight w:val="1449"/>
						<w:jc w:val="center"/>
					</w:trPr>
					<w:tc>
						<w:tcPr>
							<w:tcW w:w="9348" w:type="dxa"/>
							<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
						</w:tcPr>
						<w:p wsp:rsidR="003717F9" wsp:rsidRPr="009551A9" wsp:rsidRDefault="009551A9" wsp:rsidP="004650C7">
							<w:pPr>
								<w:jc w:val="center"/>
								<w:rPr>
									<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
									<wx:font wx:val="Cambria"/>
									<w:lang w:val="IN"/>
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:noProof/>
								</w:rPr>
								<w:pict>
									<v:shapetype id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
										<v:stroke joinstyle="miter"/>
										<v:formulas>
											<v:f eqn="if lineDrawn pixelLineWidth 0"/>
											<v:f eqn="sum @0 1 0"/>
											<v:f eqn="sum 0 0 @1"/>
											<v:f eqn="prod @2 1 2"/>
											<v:f eqn="prod @3 21600 pixelWidth"/>
											<v:f eqn="prod @3 21600 pixelHeight"/>
											<v:f eqn="sum @0 0 1"/>
											<v:f eqn="prod @6 1 2"/>
											<v:f eqn="prod @7 21600 pixelWidth"/>
											<v:f eqn="sum @8 21600 0"/>
											<v:f eqn="prod @7 21600 pixelHeight"/>
											<v:f eqn="sum @10 21600 0"/>
										</v:formulas>
										<v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
										<o:lock v:ext="edit" aspectratio="t"/>
									</v:shapetype>
									<w:binData w:name="wordml://02000001.jpg" xml:space="preserve">/9j/4AAQSkZJRgABAQEA3ADcAAD/2wBDAAIBAQEBAQIBAQECAgICAgQDAgICAgUEBAMEBgUGBgYF
BgYGBwkIBgcJBwYGCAsICQoKCgoKBggLDAsKDAkKCgr/2wBDAQICAgICAgUDAwUKBwYHCgoKCgoK
CgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgr/wAARCACtAK8DASIA
AhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQA
AAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3
ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWm
p6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEA
AwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSEx
BhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElK
U1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3
uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9+KKK
KACiiigAoooJA5NABSO6xjLnFfK//BRX/gsb+w//AMEy/C0+o/tAfEtLjxB9nMmm+CdB2XOq3rY+
UCLcBEp/vyMq471+Df7W3/BwJ/wVb/4K4eP7n4AfsJ+CfEHgrw7e7k/sbwU7vqc0B433d8oHkpjO
dpRRk5JoA/cz9vv/AILn/wDBPD/gnbbXem/F74zW2reJ7YEDwZ4WK3uos/8AcdVO2H6yMuK/GD9r
T/g6+/4KG/tmeLW+D/8AwT2+CkvgeDUHMNh/Zlq2sa9dAnG4FU2RHGOEQ4/vGvmP4V/8E2fgZ4E+
IEHhv9p34heIPjX8V7+XenwR+BDf2ndJMTkjVNXYGG35zvEPnMPX0/VP9kX/AIIq/wDBQn4leEY9
Gvb3wj+x38Nb1AJ/B/wlh+0+LtShwP8Aj/1dh5isRxgSNjn5F6U7WFc/JX4o/wDBPP8Aah8f+JG+
KP8AwU//AG3/AAl8Odf1JP8AR4fij4rn1PW5XblUa0thNLbR5PJfYFHIB6V1vwi0P/gtV/wS68Pj
4ufsc/GS98W/De1k899U+Guur4h8PumSxM1qu42+ed2+ONuua/oq/ZV/4IYf8E0v2T9Pkk8N/s36
N4p1y7gaLU/FHj63XWr+73DDkvchgm7nOwLnPOa8u+PX/Buf+yhq/iy4+L/7Efj7xX+zv45bLw6p
8NdUkgsZJOuJbLd5bRk9UG1SM8UXQanw9+wd/wAHnejXUtr4D/4KE/AV9PkysTeNfBOXjB6Fp7KT
5lHcmN2/3K/Zn9lj9tr9lv8AbT8Dx/EL9mj42aF4t09kVpl0y7BntsjO2WI4eI+zAV+G/wC39/wS
v/aV+Hkdxq3/AAUK/Yi0X40+Hl3b/jn+zpZx6V4otl/57X+nbFiu2A5OVOcH96OlfCnhP9iH9oz4
Qa/N+1H/AMEfv2srzxzBoTNPPYeGLmbSfGOhop+aK/0liGlCjhzEZYzg5AFFguf2DhlblTS1/Pn/
AME1f+DvzxX4K1K1+Cn/AAVE+H15M0MwtZfH+g2Pl3Nsw+Um9szjOD95o/mH9w1+6/wG/aK+CH7T
nw7svit8BPidpHirQL+MNBqWj3iypyM7WxyjDurAEelIZ2tFFFABRRRQAU5Ov4U2nJ1/CgBtFFFA
BRRXzt/wUb/4Kd/ssf8ABMn4L3PxZ/aF8axRXUkZXQPDNm4fUNYn7Rwxdcf3nbCqOSegIB7X8TPi
h8Pfg34J1D4j/FHxjpug6FpUBm1DVNVvEgggQdSzuQBX4I/8FYf+Dsvxf8QNYvP2b/8AglJp96v2
uc2bfEeawZrq7Y5Xbp9swJXJPEjjceoUcGvjj9qv9uD/AIKUf8HDvxtvPD+iSR+FvhV4eY3M2my6
gLLw94cswcm91K7fCySbecsSeMRpyc+/f8Ez/wBgLxD471uXwF/wSV8Ji8uYSbPxx+2L480UrbWa
9Jbfw5Zzj5XPOJ9plwM7owRTSFc+UfDX/BPybT/F9n8Y/wDgqr8U/FWo+NfFtwtzovwX8MzNqvjn
xPLIcp9pyXGnRuSPmmJkwciPoD+q37Hf/BEn9rX9o/wPbeF/jLo1l+yt8DbjY4+D3wynA8Q+IY8c
PrWpjMkjED7jM2Nx+RcAV9hfs1fsLf8ABPL/AII9ad4e8W+M72fW/iT8Q/FEGhzfFbxlHNfalrOt
XW7yoWuCrJZJI42KMouSoLMxycq4/bK/4KTeJ/8AgoR8Pv2Xfij4F8IfA/wr4u07UdY0O4mlXxBq
HiJNPmiE+nGRGWC0laGQS8eYdrcEMCKfoB9Pfsj/ALAv7Iv7DPgeLwF+zD8D9D8MW6oBdXttah72
9cDmSe5fMszH1Zj14wOK3PHv7XH7L/wq+IVj8I/iP8ffCGh+J9SKCw0HVNfggupt5wm2N2DfMenH
Nfll4/8A+Cmv7QP7Nn/BYD4zfEex1bx/44+F3hfxTp/hzx34G0nwvfX2neHtGTSoZpdbjuUjMNvL
Bcu4ki3AyRlsjIBHEfty+Kv2fdN+Nnxv8YfD34/+EJ4fiDBYeKI/A3xa+EM2sJ40EljG1v8A2Fql
kRc+Q5iSMIrAwyh/lGSSrBc/an4rfFf4efA/4dav8Wvit4pttF8O6FZtdatqt3ny7aEdXbaCccjo
K8/+Mv8AwUA/Yw/Z61XSNC+Nv7SPhPwxe67ZR3ml2ur6qkUk1s/3Ztp5WM/3mwPevE/2/rn4r/Hn
/giZ411HU/hFqOl+MvE3wkgnvPBVrC9xc2d7LFE8loqqCzMjErjGflr4y/bx1jTvhn+0F4j8dXWq
+Ifhn45b4RaNo2kWniP4aTeKvDfxV09bDf8AYNkVu72NwlxJJbMEkjc5DdOaEkFz9hLXxl4J1aPT
BbeJ9MnGuW7TaOqXkbfb4goZniGf3ihWUkrnAYHvXyV+3B/wQ/8A2Nv2wddb4u+G9Hu/hb8VLdvO
0v4m/DmU6bqMc46PN5RVbj3LDdjowr55/Zo+JPw9k/4KO/AL4dfFnT9G+FEfwf8A2WzeWPgXWtd8
s2Gs6zPDE9pEbpy8hjtrUZBYsoYA966H/gs5/wAFldV/Z1l1P9m79j/4teFNI+ImjaBL4g8TeJdf
lgkttMtoEEy6dBHLlLm/ugAixYJSNmcjO2izA/Pv/gpZ/wAE3fi78KY5G/4KkfAVviB4YjHlab+1
Z8GtHjj12wj6Idc09AouwvUyEbsdJCflr42+H03/AAUS/wCCMHiOz/a3/Yk+Ptv42+E+qzqsfi7w
hcveaDqseci21Kzb5rSfA+5KqupzsY81/SN+1D/wVF+An7Kfwx+Clr8cdIufEOq/HC/0vR9K0HSr
WOWSd7yOISTtE5AMKvKgb/fAGa+cf2zv+CIGm+EPHOt/Gv8A4JR+PdD+Hnj3VLF5/Fvwe1Qx3Phf
xjasTvjuLCXckO88b1UKCcjafmp7gb3/AAR//wCDkD9lj/go9Yaf8KvijeW3w++LLKsb+HNRuQLP
Vn6b7KZsBif+eTYcZ43Dmv0mDKwypyO1fyRftD/8E4/D/wAYPiLq2nfs0fDfUfgn+0R4aka78Rfs
6a5fPF9ukj+c3Xhy7lb/AEhTjetvvL8/uywwK+wP+CMf/B0F8Rvgd4ktf2Ov+Cq99ftZ2M66dpvj
/VbNkvtJdTs8nUU2hnReB5pG9cfNu6hWA/oeorM8HeMvC3xA8MWPjLwX4gtNV0vUrVLmw1GwnWWG
4icbldHUkMpBBBFadIYU5Ov4U2nJ1/CgBtHA5NHSvgv/AILi/wDBbP4Wf8EpPgu2n6IbPXfit4is
ZF8IeF5JsrDnKi9ugp3LCh5A4LkbQRyQAaP/AAWd/wCC3XwH/wCCT/wqFvcz2fiP4ma3bSHwt4Ki
uRvOBj7TdbTmKAEjk4LkEL0JH868PhT4/wD/AAVO+IOu/wDBRn/gpt8cL7wz8L7S9Md14imgxJqM
mSY9F0GzP+ul6LhAVjGXkJwcw/Cb4L+I/wBrDV9W/wCCnv8AwVF8a6/f+FdW10xaFo1ux/tj4iar
n5dM02PBKW6kqryqNsasFX5jx+0X7B//AATCgs9G0z/gpf8A8FZ7DQvC+l+BdCN78NPgyqLB4e+H
OlIm+MyxNxLdhdpYuSd+ScsRhiPGP2Mf+CWKftLfBux+KH7Ynh9f2ef2Q/CI/tLw78Ip9R+w3viW
NAG/tXxDduVd2kAB2sRxgKqA5b7G+KP7TXw8/ao/YP8AEnwd/wCCN+tC01HwRqGlQ674X0W0PhzU
YvDzyZmNgbyONYDPAjiK5I2kByGDciX9oe607/grH+zb4e8Pae8Pwz+II16bxh8E/B/i68WVfE9h
pzobe61HTsAmzn3YMbglAysDngeW/sG/sq/td/tI6x4u/bH079trVvBfxl1rVI/Dfxu+HXif4X2d
zolpHZM4TSreFmWUW6xTExzLM/mLJuPWmI4rwd4c8UeJvDUf/BOH4gfs8XPhnwp+0J9vufAl7d/H
R/F+u+F9Vs7M3VvqrI3mC3hFxCj7klZfMYDOG2j7V0D9lH45/tbfs4/Avxd+1Ldt4F+Mnwo8UWms
T6pp4iu/Ont91vcgbXx5V5BkkZ+XeDjKgVS8K6P/AMEmf+CYv7Qmj/CfRdK8CfDb4l/EvT5ZNHnu
bRrZNQ2uFMSTyEpAjSsAsIdck4AJxn4o/aK/4K6ftceNfgb8RtE+O/wH1b4feNfgl8RvN0rxz8PJ
bm80SW8sn86C01WBS09tb3du21ZG3RPvzuUrgG+w9j9MP2d/h1+yF4J+JHxb+HPwk8V6Pq/ibxF4
rl174p6A+uR3t1b3t7EvyzwZLQRtEF2xsoG31zXmfwO/4KGfsfa7+1r8T/2DbL4ZReEde+BGkJdw
JfWFvFb3OnrCJZJbILyiRqyEjjhwelfmt+xJqf7Yf7e37TPxS/4Kpf8ABOPwz4X025s/HMV/cTa9
rVxHceJ7U6PaLP4cltVj8sp5iHbO7jZIuV9/ev2ov+CUP7YX7Yfxf+KP7Wfwk0eD4V+LviL4H8Py
aI+uXyPPY3rafc6XrOmXSxbgY3tJImDqSC8SEcrwWQXZ0ehf8HFOpeN/+CePhr9rn4f/ALMjap41
8VfEa80DT/htaaq7zCxsonu7y+L+WG2x2cfmH5cbpEGcHNfWXwY/b/8ADH7QX7Wek/AH4feFrXUf
D+qfBfT/AIg2/ihLzdhLy5eGGEJtxyI2bduyMYxXyt8I/wDg3H+HNn+1d4v+Ivx78bza/wDDD+zV
i+G3gfRtWvLCXR7ma2tre9uJZYGjJMkdqqbVYqyu24dKrfsmf8ErP+CkP/BNL48eKvHn7MHjD4X/
ABE8HXmkxeHvCGg+OtS1G0v9H0GG+uLuC0E8cbqzIbl13NuGFUdqWganr/xh/ar/AOCcv7Q/7TGp
fsu/tBfsvab4q1e0+I8Pgex1PVfD1reJdXv9mnUJ3Dt88cNvEYxIxPymZOOa6qT9l/8A4Joftafs
+/Gb9j/9mnU/AcreIm1G38Z33hk2+pT6Tqt5GyG5ZizYmQ/dXcNmzaNuMD4I8b/8EKP21f2kfH3x
M8fftDahceGLm30jxZ4o8GSeDfE+4aj4l1W5CJBKyqHaFbCytImU7dxfGcZWvpH4k/tjaD+yz/wR
E8H+Lvgl8KLP4Z+PviDolh4U8N+D10g6c+l+IbgfZ72R4mVWC2uy6uC7DlYVJPzA0wPOf20v+CWP
7aFl8XPhz8avAOh6R8U9Z8E+IvCeg+AbS8n+x2Ph3QdLt2mlvbss2fMmvwkknlbm8uJEB9O//wCC
IX7Mvjjxz8dvit/wU++O/wAQta8X674zuZfDHhDW9Yia3im0q1nH2i5tbbgW9tNcR/uk5IihUklm
Yn5o/Y//AOCyX7R3wt1HQre5+Kln4u8NTeJdI8K+Ffg/4v0C9i8Ua/oZWGBfFGn3zDF550pnmdGD
IqRj5lPI/Smw/aV/Z8/4KT/B74hfAv8AYr/a/sdJ1vTppNI1fXPDcUct3pg8zZLLBG5UMrgSIk65
Td8ykkYoYlY+d/2pbD/gnH/wWg/ai8cfsF2lrqJ+Ivwi0KLVbH4w+FZEik8P6j5uwQ29ypzK0UmP
MQ5TcGXIZSR+Wv8AwUf/AGB/E114+tP2ff8AgpZ/Z/hX4k3KG2+En7UVha+ToPjpFAEdjrpA2RXX
3ALjhhu+fcvzV9m+Pf2T/iF8FP8AgoJ8R/2b/gbpk/wf+A2kfBbw9pfxD+Nt7KbZ7fRI3ur3UEtr
mQBZL66lnkR58kx/O2CcCvSNO/bo/Y+/bg+G1t+zn8Wf2V2uv2SNaul8D+DfixrGqCZo9ZtwIozc
I2ZbFJMqsFzIQzSKdwAdaBn5g/8ABK3/AILIftff8EOf2gG/Yp/bh8L6vc/DW31EQaho2oBnuNCV
j8t7p8mSs1swIfapKOp3IQev9NfwR+N/wr/aM+GOkfGL4LeOdP8AEfhzXLNbnTdW0y4EkUqEeo6M
OhU8ggggGv58/wDgpN/wTeuv2bRpn7IH7dHiS81n4PX0ptPgD+0rLamS/wDBM5yYNI1l1GJrJhiP
Jxt++mMFR4H/AME1/wDgpd+1n/wb0/taXv7L/wC0po13qnwy1K+juNa0WGYzQ+RLjy9Y0yTOyRHQ
Bvl+WQAjhhwmgP6s6cnX8K5P4KfGv4YftD/C/RfjL8HvGNjrvhzxBZJd6VqdhOHjmiYZH0YdCp5B
BBwRXWJ1/CkM+S/+CwH/AAVW+Ef/AASn/Zgu/i34vlg1HxVqoe08EeFvOCy6lebSdxHVYU4Z3xgD
A6sK/mh+FvhHxN+3t8TPGH/BVn/gpl411O68BWGuAXNtAxF54x1QAG28PaYp4RcbEdlBEUfPUg1q
fEf4w/Fv/gvV+3l4m/aK/aL8Yf8ACHfC3wTpjap4hupZ99v4U8ORSAJbxZwJLmZiEUAAySvnoOP1
z/4I1f8ABO2H9rvxp4R/b6+NfwuHhf4Q+Abf7J+zF8HruIGOwtEb/kOXaEYkup5AZt7DLM2/gBKY
tT0b/gkr/wAEtPG3jjx1pH/BRr9v3wRY6f4oi02O3+DvwjgtgumfDrRxzEiRng3LKVJYgEEEnLHj
6A/4Kj+M/Dtnp2j/AAe/ax+Fdlq/7OfxEjXQvG/im3uZY7rwvqrzo2n3U23hLZphGglHMcpjY/Lm
vpT45a78RfCvwm1rWfhB8P4fFXiS3sHOkeHJtaXTl1CXH+q+0srCEkZw2DggV+TPg7wz+xv8TvEd
x+yX8YPjT+0z+zF448ZtLpsvw4+I3jCTU9F1iScsrw2U14s9ndo7OcY2Ocg7F4oC1j0LwH/wTq/4
KFfB/wCNHin4FeC/Hn/CS6D8TobWJv2p9Q1jf4j8OeFbeIJ/wj8VuQVWfqYriMhD5zyMpdQK98/b
P/4KffDX/gmt4n8E/AKPwLqetPBZ2r6r/aM863NxokcbRS3NjNIjLqV5CyxPLBvEpjLMNxrd8V/E
39nb/gkv+xt4H/Zu/aA/a38Q2k13ps3hvwn461iwN5qUs4hdkk2xRMpaJMFdy4xGAc1+fHjb/grf
+z7+018NNW/4Jufte6NYftDeLtWgdPhB46+Fdqpm1vUVBW1eaBikukahGxDM4/dkByGAOC9wPd/+
Cx2gfCP9srX/ANlzxx8HfinoN3ZfFrX5/BbX8Gjw6vLc6Bq0I866gtnOCYJIkbzSMQN85+7ivd/2
A/8AgjR4Q/YB+Pvi74seD/2lfHHi3QPFnhCw0C48H+Mnhu41S1LeXLJcEb59qs6IpA2K7LlhtCz/
APBJb/glP8Lv2FvhToPjvxb4Shn+LWpeG7ZPFeqS3jXEVjcsivcxWSt8tukkuZJPLCiSQsx64r7N
Ck0rhY534cfCb4YfB7Qf+EW+E/w+0bw1pnnPL/Z2haZFaQeYxyz7IlA3EnJOK6EAnoK/Nj46f8HV
3/BK74GfHy9/Z+uNQ8ceJL/S9Zk0vVNY8N+G1msbe5SQxugeSVHlCuCMxowOOM16NZf8FY9Uuv8A
gtPof/BObU/CC6X4V8Q/BuHxL4e1a+AW41S+nYzKQOsaLDHJFsPzGRZCeAtIZ9sX2taPpd9Z6ZqW
q21vc6jM0WnwTTqr3MixtIyRqTl2CI7EDJCqT0Bq0VI6ivyf/wCC1HiL46eAP+C0n7EfjX4PHU9T
c3mrRSeHYL7bFPCCgvCkZIXzGtpWBPVgoFdV/wAFRf25vj7+yz/wWm/ZF8CaX4q1A/DHxml/a+J9
BsYW2s8zrbG8n2/ejh82KUkjCLG7epAB+mteaftPfshfs+fth+AJfh1+0B8O7TXbE211DazSEpcW
f2iBoJmglXDRM0TspKkZBIrf+Nvx5+EH7OngtfiH8avHdh4e0d9RtrCO91CbYr3NxKsUMQ7lmdgM
fj0FdYpWRBLEwZWAKkdxQB+VH7TP/BAz9qn4nfEL4UeCPBn/AAUC8QyfCnwZJe6f9nv9MtoNc0HR
Z7bypbO3voVDXXmIBCpdU8tTuPmGvl39nT4y+G/gP+1L4L8PfEr4+/DzRdC/Zl+Ims+DfCr+DNDn
l+IPjrS7dGij0aewslMc1uzMHaUqMvCWADE1+/JAPBFfB/8AwV0/4J6/DrVf2Kvih4y/Y/8A2OdE
vfjNrN9Dq+i6x4UtYtO1kav5qr9vju4wsnmIjyOVDAP8wP3jTvcVia91P4P/APBwD+wPrreFPDni
3wNDpnjieHw1d+KNPRSur6VOrRXElsHaO6tfOGx4X4O2RTtZcjhdZvvCn7JUemeP/jZ+ybe6z8bP
jjZnQtT+APgi6trrRfFmoaY6sddEUq+VbKsXlFpnwUSRFYMyrXgX7LPjH4uf8E1fi58NPCP/AAVQ
+J9z8JPh94S+HZ1H4UeD/A2sPLpOoagvmLfWurSJGJNQ1N2m8/yxmN3mJTLLx9r/ALUPwx+Jn7WO
jfCv/goX+xnHb2njDwbouqrY+F/iCs+kJrGjapFCt3ZzvtMtjP8A6PDJHIR8jD5gAchoCx4G+Ovh
L9vRfFP/AAT5/bw/Y7uPAOv6p4UN+/grW9Wt9Us9X0ousZubW6hVVZopCgI2q0bFCOxr8i/+CkP/
AATbg/ZlutO/YF/bB8Uy3fwk1e7kX9m/9oLUYS83gi8kJYaDqj9WsmbgEnCbt6/xKPsT9nX9rf8A
ZO/YZuPCfib9rb9pO5+Nf7QXgXwPqmh2vhr4StdeJJNI0WS6W5eCWQcTSQIkcT3UrJuWMZ55P6H/
ABJ+GH7Nn/BSv9kYeGPHnh638SeAfiH4dhu7dZlG/wAqaMPFNGwz5cqZBDA5VhRsB/OV/wAEcv8A
gqZ+0D/wQw/a/wBU/Yi/bT0+/h+Hd5qv2XXNOnlLjQbl2Hl6nankSQOuCwX5XRg4OVwf6hfBPjLw
z4+8Lad408H63bajpeq2Ud1p9/Zyh4riF1DI6MOCCCDmv5q/2+f+Cbnj1fFEP/BMr9pjVUl+JHh3
Tpbj9lz4w3w2R+MtIjyf+EavpD0uEBAhYk7WXZwrqD7X/wAGk/8AwUw+Nun/ABP1X/glJ8cdNvr6
x0DTr/UPCs93J++0RreQfabFw3Jj3OSo/hO4dDwmgR+Y3/BNyWHVf2Pv2xPCEsZdm+Dum6ui7cjN
nr1kc/gJa/rv/ZB1Ow1v9lX4ca1pqx+RdeB9Lki8pAq7TaxkYA6V/JT/AMEuPBN3oHxw/aX/AGaN
U3Pdal8APGeh+URgvc2U1vc9PUfY34r+oX/gjf8AEBfif/wSz+AnjJbhZWufhjpccrr/AM9IoRC4
/Bo2H4U/sh1PnX/gvB+2P+zX8Jh4U+CPxp/Z9+K3iLWbiUap4X8W+DvEs/hrTNDuW3wiSfWUmRYH
A3cFHAU5OMiqv/BJD4F/tt+PPEKfFb9p79s74efFj4W22Lzwh4JGoQ+LtR0O6BV7aZ9ZeGNvOi55
CsSejjHPvP7SPw6/4Kc+Lfibrsv7P/x5+B174PmZFsfBnjjwRc3E8GEAdZbiK4w+5sn/AFYwD3qh
/wAEo77wD4++HfxQjj/Zu8EfDXx1ovxA1Hwn8R5/hpAkWn6tf2eF+2QOqKSCsoPzjcrZB5FLoHU+
Vv8Aguzr3/BQnQvFmo6B4r+C3gHxb+z7q/2S58MeL1vbrTtX8B6vCi7Lt7yNH+yv5wLx3BUwgHbI
yDr7T/wQU+AnjLRf2UYvFv7V37Mmm6T8VF8V6hqOpeO717DUbjxHLctv/tKC7g3AIyP5YCMEATC8
cV4f8Yf+Davxto3i7WfGvwU/aXuviFpusTSy3vgT4067q5tpQ5JaJLvTbqFkDZI+aNxg9K/Sv9kn
4AeDv2Yv2dfCXwQ8DeBdP8M2Wh6NFG+h6VqNxd2tnOw3zJFNcEyyRiRn2s5yRjgdKOgHoGo39jo+
m3Gr6nOsNtaQPNcSucBEUFmY+wANeS/sa/t4/ssf8FBPhnc/Ez9lz4qWviDTrS8ks9RhjPl3VjKr
MuJYj80e7aWUnhhyCa83+GP7eGlftEf8FOPib+wh4bsI7vwz8O/hzbXHiG7mtsxX2p3VwY5YEYjD
xxRjy3xkeYZEPKEV+OHxl/Y91X/ghN/wW70Lxzofxs8VfCT4GfFnUZP+EY8XeGo0uLCxkZ1L6dqF
vMCkkEbsMq3KxyK6kbDhDP2Vl/4I0f8ABPRdL0Twppv7N/hWy0HR9ah1i40+30C2Nxq9/FL5sc17
eujXNxiQBypkw5GG3LlT8q/tGfBvwB4y/wCDqf4SXnivTJWntv2bF1PQrm3naJrS8tNU1IBlK4yD
G7IynIKseK+kP+Cker/txTL8ID+xvqniLWLLUNakPimHwHe6baXt+v2dZbeZptQilt4rHKSeb8ys
fNjCliQp+dv2pvFWqeA/+Dg79lb4o/FpLHQbm/8AgB4it/ECC9D29iYXknmHnYAdUDnLYAwCcAUA
dd/wWOt/+Ef/AOCl37AXxEUOoT4u6to8kij/AJ+rW3wpPvsb9apf8FZoIvCn/BYb9g34lXcafZ7n
xnrnh+YyAFW+12LIFI75LDrxVn/guJrcd/c/sU/HOCzltlg/an0DyFlUrKkN3YXrqGU8qSIlyD0P
FR/8HAGnWen/ABb/AGLPiHqFvG9tpn7VGgWl75p+XybqeKFwfUFd2RQB8X/8F7Phx+3H+098J/jr
8ZvH3xTli+FPgTxXo2m/D7R7eWJtMudQOpIgvrV0jEqmG3kaOZ2JzLKypkR8fqR8DPE/7azfsx/s
73/j+xXSvF+k6/p+i/GHSZEWddXt1s7mzluoJhx5bS+RfKwxlF2nByK+Vf8AgrV+0zZftH/8Euv2
xfgj/wAKnuPCWqfA3WrS0XT7qHYl5YF4Lmzv4gAAEkAkAx/zzJ7179+23/wUU1j4IfA3wF8Cv2X9
LtPFHx5+KXhq1/4QLQDKv2fS4GtlafWr5z8sFpbxlnLMRuKhRxuIYH2PoviPQPEou28P6tBdixvZ
LS7NvIGEU6Y3xnHRlJAI7Hirgwc57V8DwftQ/Az/AIIsf8ErtF+I3j74xx/FKW08TxW/ifxPpV8t
1/aut6lfedfTF4ywQKZZnCnoqIvGRX3pZXtnqljBqmnXUc9vcxLLBNEwKyIwBVgR1BBBzSA/Dv8A
4Lc/sr/toeCv27tS/am0/WGvfAkmmQXGgeNfEfiqC3/4RNyqxyWGn+er/ZJWcEo1nby3T+Z8rqwr
6w/4IKfCT9tLSP2f/GFj+1n8JtD8L+AvFEqz+D9BuL+/vNavvOEovb3UJb12lP2hWi2q+1/lJKJn
Fe+f8FSP2F9P/bi+DOleHdN+EXgzxT4n0LWo7rw7N441nU7O00tmG2S6U6dIkkkqDaVQkAkdV614
j+wN/wAEM/EH7Kfx1sv2lviX+2V421zXLMsYfCPh/Vry28PJlWUo0N1PcSzrhv436jNVfQVtTzr4
B/sQ/DL9jweDbH4zfEf4ffCy4+CfxS8TW2l6rqt7Ywv448Daok6rDcqsiOZds0Q3ygkPbZw3Br69
/wCCYGu/skaf+z+/wR/ZC/aWsPiVoPgTUprV7ux1KK5/sxLiWS4hsy0fASNH2J/sIB2r50/4LOfs
k+Cv2odff4BfAr9h/wAOeLPjX8SPD+yT4r+JNEX7D4S0y2kB+0y3jKSJiR5cUUfzlnyflBrtv+CU
H7On7UPw3+NnxQ+N/wAef2Z9E+ENn4o8N+GdFtvCWj69Z3y6heaXBcRT6qPsZMcKTLLGqocSfu/m
AwMpgeb/APBwjDZav8Y/2LfC0lpC093+0pZTCVkG9Y4raTcAeoUl1J91HpX5b/8ABuFrCeIv+Dg/
x5rsJ3Ld6f4tmBHo10pr9K/+C4niSHU/+ClX7HHgszADQLvxV4su0Y5CR2mnrIrke3kvX5w/8Ghf
hyTx7/wVe+InxMMIljsPBepTNMexubyMA/jg0wvqeX/D3wfH8Av+DmDxt8H7+IR2viX4heJtDKP0
aLWLW5VB24/0pfyr9uv+DZTxpLrv/BJTwj4AvpGN74C8VeIvDd5G4w0Zg1S4kjUj2imjFfjr/wAH
D2hXn7IH/BwPoH7QmnobaHVLvw94mSbGAWikSKYj/v1X6nf8EAPEVr8Nv2jP2uf2RI5AkGifF4eK
9BtycZ0/V7dLiN1H93YYTkcfNR0DqfTHxl/4I1/sBfHP4gax8VvGXw08Q2viHXr03mr6n4f+Iut6
a1zMcAsVtbxEHAHCqBXs37Nn7MnwV/ZI+F1t8HPgH4Lj0PQbaeW4+zi5lnlnnlYtLPNNMzyTSuxy
0jszE9TXfYPpRwRkGpGGDjIH1pJrm2s7Z7q8uEiiRSZJJGCqo9ST0pa+V/24v2lLDwr+2H+zZ+xx
qetJp1h8UvFWq3+s3EkgT7VBpVi08FiCeP393Jb5HVliKfx0Aec/DXxB4S8Jf8F4PjRfokNlpWgf
sxeHbi+a3h+SMHVNRuZZNqDkkPuOASSe5rF/4KC/Ej9kL/grD+zj4J/Zm8O/C7xJ8RvDXxfi+12H
jfw/pvl/8IIfNa3s9VuUnCSx7rkPGIwu5linJAVSah+E2reNpv8Agv5+00vgXw9Z6hf2XwQ8JWlr
FqF4YIAzB5V3sFY7cyc4BOK+tf2Qv2abn9nvwVeS+LtQsb/xVr919q1+802Ax2sZ3yOltbq3zLBG
ZZNueS0jseWNAH5w/wDBCP8Ab/8AjN+zF8c9V/4IY/8ABRjUWs/HvgSSS2+FfiHUZMR+INLjBaK3
jlb/AFv7keZCT8xjBQ4aPbTP+DhX4LeLfjh/wUh/ZU+FngPxXDoer+PPCXjvwtYarOpKQzXNhFs3
45KEkqQOzHFd9/wcofsX/AT40fAjT/2p9D+PHhX4a/HL4WzJqHw/1/VfEVtp0uqGNw/9nbpHVnkY
jdFtyRJgYw5r42+G3/BVrRv+Cl/7RP7Bfi/4iLFpXxa+Hfxev/D/AMRNAkjMUpll0/Ed8iN8wjlM
ZyP4ZFZT2yAfWH/BX3Sf2ofCP/BO79n/AMRftl+JfDOpeP8ASf2o/CN5q8/g2we306yVjdwpFAJC
XYBXALMcksegwK9b/wCC/vgzUfH3w8/Zv0TQLQy39x+1h4Jjtdo+ZQ14SzewCgsT2ArqP+C4vwm1
X48/BH4PfBzRLGSe6139o/wmqrGpOyOF7m6lc46KsUDsT2ANdR/wVc/aG/Z8/ZR+G3g/4/fGiGfW
9a8KeJjdfDLwDpqiS+8TeI5LeS2s7eCMZZiDMxyAdu4N1ABBHz9/wcMfFf8AZl/ZX/Yf+NWn+K79
7r4g/tAeH4NI0Xw9p8Pm3d4LGHAmMa8rBArSO8p4HmBc5IFZv7D3/BMZPGn/AASZ1r4v+Lfihqni
v4yftBfBKye98dfY1lnsNPuNOjks9Is4SVWO2jjKRMild5LEkcYs+C/+CcXxZ8Wfsr/HL9u7/goQ
0WtfH34kfCrWYLPSs7rPwHpZspnh0mxU52PyPNk6swx67rX7DH7TH7QXww/4Jd/sX/EH4fax4Wk8
ISeC7bw/400XU7OeTU9Tkh0+W3s47FkYKrie2UEEEyEqijc4FNAfLH/BTH9hf9p39nn/AIIJ/EXR
/jn4c+E3hHTrS60XVLb4efB/wr9is7S/+0xpc31zM7OZZ3URIRGVjAU8Nwa/Yr9jfwa3w9/ZR+Hf
gdfGNz4gg0zwdYW+n6xeqBPc2qwKIGkxwziLywzADcQTgZxXxL/wUd/ZW+NvhD/gid+0xovxy+PG
u+PNW8SaO/iiGHXWib+wWVLSWewtiiLi2SWGYxryVVsZJJJ7r/gm3/wUA+FnxHl+CP7M2ufF3R9O
18/s4+G9WsPDVzerHd63fT2Y85o1Y5kEENszbV6+e7EHYCEM+56KD1NFAHiP7Vn7Bvww/a/1rS9Z
+IfxM+JGif2TbPDDa+BvHt5o0U4Zg26ZbZl8xgRgE9BVX9lz/gnT+z/+yH4xv/H3wt1zx9fapqNi
bS6l8W/EXVNYj8ournbFdzvGjblHzKobqM44r3ikbO049KLgfih/wWf+Kqv/AMFW/G3isXH7j4J/
sZ+I9S3ZGIrvUHFpGPqwvBj6V5L/AMGRfwxkl8UfHD40z25KxWmmaRFKRxudpJmH/joryr/grt8b
xrl1+3h+0Vb3wZPEnxA8LfCDw7Pv+9HaF76+RTnkf6IAa++f+DOb4If8ID/wS+1D4s3do0c/jzx9
f3MMhGPMtrUJapjjp5iT1TEj5x/4PcP2eyNL+C37U+nWTYW7vvDGpzqvClkF1bgn32XH5Vuf8EZv
2hox/wAFF/gH8dZb8fZP2kf2Z4tD1iQniXxD4ddrKUHn7xjtVPrjFfc3/Byj+y9N+1N/wSQ+I+k6
Xp32jVPCKW/inSVVNziWzYmTb7mB5l/4FX4Mf8E4/wBoTVPCH7FHhb40aIzSa3+yn+0FpXi3ykb5
38Paq8dvfReoTzIwD2zcE0kDP62Qc80i7cYWqHhPxLo/jLwtp3i7w9epc2GqWMV3ZTociSKRA6MP
YqwNaFIYA5r48/4LJf8ABLK4/wCCmvwT8OQ/Db4oy+BPih8Odf8A7c+HPjGHdi0utoDwyFPmEcm2
M7l+ZWiQ4IBU/YdOQ8YoA/nj/ZH0X/g4t8Af8FSfjh4J8A+IfhZ4s+Lum+EfDtl8Qtf8UuhtZNP+
zRtYSw7PL3P5TLv+XJIOQTX2fof7I/8AwVU+OvxR0b4S/wDBRj/gtDpXgLU/EljcXmlfC/4J2Vrp
+palawlROVuJVDBU3qCyxyHk4IwSPMf+CnX7VnjH/gi9/wAFzdL/AOCgHjPwJqes/B340eAbHw54
0uNLizJZ3dmdgkXPys6IsLhCQXUyAcqK9T+JH/BYP/g3F+I3xg8Ift9+OfjNoWpfEPwTpM9t4cvB
ot4dStopfm2NAI/mdSW2FslDI+DzQB7N8Kv+CGv7Cnh/4oeH/iPf/Cu519vCeorfw67451OfVtV1
3UUyEuLm4uWZvJjJLpEoVGfDEYVRX5//APBx5+yx8M/+Ca/7bXwa/wCC1HwV8MW5v3+IFsnjXwcg
8qDVLqFGlF3Gy/6uSSJXR+CCwR8Z3bvVvif/AMF3/wBoj48fEn4O/Hb4cfBPVvhd+yhd/GrRNE1/
4k+MXFrfeJ1meR0MUXSCxBhy77iWHBIBK17p/wAFndU+DH7QHxi/Zc+EjeOfDupWtn8WG8XeKYWv
YbiG28Pafp8893dXCgkC3K4Qs3ynzMUAe9eI/wDgpf8AsMaj+wro3/BTTXPGlnc+BrCy/tPQ7iTa
11HqEkMlv9jjjzn7YfNlg2Dn527HNeG/8E/f2Tfjd+2P8ck/4K7/APBQPwo1l4lnt3/4UX8Kb3mD
wRpJ/wBVcSq3W/mGHZyAU3duFT8oP2K/hn4K+CH/AAUh+FXxP+LnwT8ZwfsZfE34s6pdfATSPEWp
FtOtdabEdlqM1sRgqQrGNXAPluGy2xg33v4f/wCDiP4q+Ev+Crlp8I/jd8MLTRf2ZviFr9x4X+Fn
j1oQBLf2lybKW8e4B2mJ7uORCh2mNDGx4DEgHtnwd+CP/BcLxJ+0/rOsftaftD+Drn4Sa54Z1tdT
8D+HtCjRLHzomjsbSOdk3yyAOWkkBwPLxk7wK+df+Ca37fX/AATg/YG/4Jm/sy+Pv219bNl4pvLr
X9N8IXMttNfNpyR6veRSXIjBKwRgEIZQueSB3r9Pv2oP22v2Xf2TfBNx4n+OPxi0XSmaIrYaP9uS
S/1GU8JBbWykyTSMxChVU8kV+C37dH/BMv8AaT+Dt38HPDfjT/gnh4k+OXgab4ESaRcWvhm8lgu/
C+vX97PqE00EkayBJoZZwv7xCkiqw+XIIYH7kftvXvg39oX/AIJofFrVPAPiKz1nRfE/wb1yfSNT
sJxJDcxyabM0ciMMgg8Gvg3/AIIff8E3/B/xz+FX7MX/AAU01r4u6p5vhj4crZxeCF0yDyJdRtlu
9PivDc581VWCRx5IG1m2tnjB4v8A4NwfEf7UPw2/YS/aA/YE/a9+GviTw1efD7RLjVvCmneK7R4p
00jUbS6Vol3DDRLNbSEFcjMrjjbgfcH/AAQO+Fmu/Bz/AIJBfAzwj4ltXgvZfBy6jJE6kFVu5pLm
Pg/9M5UP40gPr08k0UUUAFct8b/it4f+Bnwd8U/GPxVOsem+FvD15qt67HAEVvC8rfouK6mvz5/4
OSPjB4k8P/sCRfsyfDaYt4u+Ovi/TvBOhQRt8zC5mUzHA5wEGD7MaFuJ7H89/wDwUI8c65ZfsDfA
34f69I7eIvih4n8R/FbxTHnLSzX90ba1yOuQkcxHtLX9Q/8AwR//AGfo/wBl3/gmh8GPgk9l5F1p
Xga0m1FCuD9ruQbqcn3Msz1/N/4++GWi/t4/8F+fBn7Inwwj+1eD/A/iLSvBWmGMZT+y9DjVLmXA
4wxgmY+uc96/rK0qxtdNsYNPsoRHFBCscSKOFVQAB+QpsEZ/jrwnpHj7wTq3gXxBarNYaxp01lex
MMh4pUKMPyY1/JR+y18JU/ZH/wCCs/xW/wCCYvxkmFroXxLTWvhneyzLiOOW9UtpF4AfSc2kintv
Br+uw9K/nB/4PCf2P9b+CX7XPgP/AIKH/Di2ezHimKCx1e+tcq0OrWG1raYkdGaFUAP/AExFCGfq
r/wbyftIa98cP+CdOh/Dj4hTsPGXwj1S68D+LLaVsyQ3Fi+yPd3wYthH0r7pr8Pf+CP37ZOi+Af+
Ch3hr4p297Hb+A/21fAkOoukZ2wWXjnSgYb6HHRXkLsxHfz4/Sv3CoYkFKDg5pKRmCkJ1YgkL7DH
+IpDPmPXv+CfvhX4pfte/Fvxt8cdGg8ZfDL4l+CtDtNQ8F+KgL2xXU7QzR+fbxOCIMQeXkrg73LA
5JNeSeB/+CIX/BJf4e3viv4pfs6f8E//AAd4j8T+GZbiLRrDxBqM89lPqkSFxBtuHkiRRIVQsytg
54GMn75U8YPevmj9gL4Eftr/AAI8YfEnw9+0Z498Hat4IuPFt9qHw6GgRTnUZory7luZHv2kUKro
HSJEj3AgMS3QUAeS+LvgPrn/AAVG/wCCYvhv9lb/AIKZfAuD4ZeMvHV6qWXhrwpqCNLpctk7Sx30
IKlYNkKEtGdwxIEyDIAPhzRv+CRH7E37En7XHhr/AIJk+CfjFfXl34+8ON4r+OHjvxlq8FrMnhG2
uCItFtyuxYhd3ETeaR8xiiIJwwr9CvEX7QmlWH/BdPQfgD481VbJZvgLc3PgeG4kCpe3b6gjXix5
6yCKGLgclUPYGsH/AIKL/wDBv1+xj/wUw/aZ0X9qD44+I/F2navp+hw6RqVn4e1VYIdTtIpJJI1c
lWKEea6krjIx6ZoA87/af+O/7LH/AAVQ+K3gH/gmf+x9oqeL9G8DeONK8ReOfHXhq3C6H4NsdLLM
ltBcoNjXU3FuiR8KkjknAxXxJ+29/wAGln/Cv/hf8Wfi74G/4KA6zqGieEND1jxjpHgHUtD3FbhL
ea4CvKLnYrSeUU80RAkDnOK/XqDX/wDgmh/wR4/Z/g8ILr3gP4Q+DtLi3LZtcxxT3T45kYZM91K2
PvHczGvy60P/AIKoeCf+CgX/AAVU1j4O+JPjPf8Aw1/Zp+J8Gk3+nX3jPTJNLk8eW+lIYZNNgmkI
WKxuZ2lMm4hpI1KHbvZaAPpT/gnF/wAENP2Bv+CfGv8Aws+JmpaBqfjf4u+MLSKXTdR8a3AuU0mZ
LP7Tdy28AUJGYxlQ5BYFkGQTmv008ReIdC8IeH73xT4m1a3sNN021kub69upQkUEKKWd2Y8BQAST
7V+f3/BV39of9hy31n4ZfHjWP+Cq+l/CW8+G1/fvPH4N1K31G713TbyOFbqxSCLzHDuIIwsiLuTL
EYOCPALX47ftWf8AByZ43X4TfBbwx4k+Fn7HWl6io8aeL9TJt9X8exxOGWxhCk7InZVLgMQF++Sc
JQB9gfsD/ErTP+Ckeu/Gj9re60O5h+G3jCKP4ffDwTKYpNW0GwFz9q1EEchLm6vrlEPUJbKfc/YW
l6Vpmg6Va6DothDa2dlbpBaWtvGEjhjRQqoqjgKAAAB0ArL+Gfw08CfBj4e6N8Kfhh4ZtNG8P6Bp
8VjpGl2UQSK2gjUKqqB7Dr3PJrbIIGduaACigkhtpGDRQAhIUZPSvxI/4LCftfafq/8AwU18Q/FK
K5S48K/safCa51eMOQYp/GuqgxWUeOhaMPAcdR5cvpX65/ta/tE+Cv2TP2bPGv7RvxCv1t9J8H+H
rjUrpmON5RDsjHqzuVQDuWAr+U7/AIKS/GPxj4L/AGS/Dvw18a3cx+JX7Rniu4+LnxSUsTLBYzO8
Oiae/fiNZp9h5BlX1FNdxM+wP+DMr9knU/iN+0x8Sf28PG8T3UXhzSW0LR7m5BZpdSvXWW5m3H+J
Yk2k9/tDV/RvHwePSvjb/gg7+w0v7BX/AATX8A/DDW9IW08TazYDXvFilMOL26USGN/eNCiEdipr
7KTr+FIY2vlH/gtR+wlZ/wDBQv8A4J3/ABA+Bdpp8c3iODSZNW8GOwGU1W1UyQoCegkwYifSTPav
q6kdFdSjDII5FAH8gP8AwTX8eeP/AB98CvHP7F+n3E9j8Tfhl4gX4m/BDzgVnttZ04Eappqg8/v7
dA3l9C1ueCTX9S3/AATz/bC8Gft4fseeBv2nvBdwm3xHokT6rZA/PYagi7Lq2cdmjmV19wARwRX8
8P8AwcNfsnePv+CS3/BVvw5+3d+ztYvp+g+NNZPiPRZooz5Fvq0Lqb2zfHG2QOH291lYD7pr7e/4
IrfthfD/APZx/bGtPhX4U1NLT4HftY2D+NPhIJZR5Ph/xKVLanoBbojLKJEVe+yMDO4ZrdC2P2nk
BIADY5HNTYBOcfjUQIdcjoaejdsVIwkKqu52wB3pFfjIORTiqsMMAfrVS41nQrLU4NDutXtYb27R
3tbSS4VZZlXAZkQnLAblyQONw9aAPkD/AIK8f8EkPDP/AAUy8E+H/E/gr4m33w7+LfgK6N14A+IW
lO6y2TEgvDJ5bKxjYgEFSGVhkdSD8Nyf8Ehv+DnnxXAvgPxb/wAFnIbTQABEdQ0/Vbpbsx9Mlo4I
5WOPWXJ9a/a0oMcUbGzQB+Wn7IH/AAawfsrfDjxvb/Gn9un4teJf2gvGEMgmRPF15KNMjmyDvaEu
zz8jpI5Q90NfXH7dn/BJL9hf/goh8JdL+EP7QXwggS18PRFPC2p+HXFheaKCMbbd4xtCcDMbKyHA
O3IBr6T2v6/rSbWPWgD8vv2ff+DR3/gk/wDBbxtF428baT4v+IhtpRJa6T4u1wfYQQcjzIrdIzN/
uuxU91Nfpd4O8HeEfh34XsfBHgHwxYaLo+mWy2+n6XplqkEFtEowqIiAKoAHQCtTYfWmzXFragNc
3EcYZsKXYDJ9OaAHqowOKWgfWkdiq5UZPpQAkqsU+UcjpTSQOSaUux68e1cD+09+0P8ADn9lH4Ce
Kv2hfivrEdjoXhTR5r+9ldwpfYpKxrnq7thQO5YUAfm3/wAF/Pj9ov7QXxs8Af8ABMG38SCz8KRK
3xC/aA1BJMJp/hjSwblYZWHTzZIwFXu5iHevyq/4Ji/BjWf+C3X/AAXEf4weMPC5fwF4e1dNf1LT
2T9xZ6RZFY9OsMHjbhIY9vcK3vWV/wAFHf2p/iHo/wCzz4k+I3xFung+MP7YWqp4k8SWYb99oPga
KbfpWmKPvRrOUjk28Fo4kz981+0X/BsJ/wAE2X/YU/YDtPiH8QNBa28e/FWWPXNc8+PbLZ2WwCzt
PUBUJkYf3pm9BVCP0niiSFPLjXAHQCpE6/hTacnX8KkY2iiigD5V/wCCyP8AwTp8Mf8ABTP9h7xL
8Ar21iXxFbRnU/BeosButNUiVjHyeivkxt7PX81X/BO3xR4q8Rx+KP8AglZ8V9Vn8J+OtN8VPrnw
T1q+cwy+HPHGnuc2LFsbEuxE0BHH73yz1xX9f4OK/no/4Ox/+CU2vfCj4h2v/BVz9nLTJrWGe/tl
+If9mxlWsb8Oq22pfL03tsR2/v7SfvU0wP1t/wCCQf8AwUDsv+Cgf7Jtj408SW39m/EDwvdv4f8A
iX4cmGyfTNYtwFlDIeVWTh1PQhiOqmvqqv5s/wDgnb/wUI+M2r/FHTP2/f2M4fB+s/E7X9JHh79o
H4G6/wCMLfRT4pvLdAbXXbBpTh5JUOHVVYrIj8EPX6PaX/wcnfDL4WzRaV+3j+w78ZvglcnCyajr
Phtr/Sy3+xdwABx77RxQ0JH6XqxzkngCvwd/4KBftVeP3/4LH/Dz9sXxD4I8T39p4Z8a6z4L+CHg
i3hmt31x9Lsgb6d1YDENxqF/ap5pAVY7RycgA1+oPwJ/4LSf8Euf2iEhHw5/bX8CefOBsstY1lNP
mye2252ZP0zXsXjv4T/s5/tPaLHqXiXQvD/iVRpd7Y6drto8UtxaW95CYrgW1zHl4fMjOCUYZwPQ
Uh7nwL/wTl/4KQ/tO+Bv+CdvxM/4KFf8FE/ipZ+JNLvvHV9b/CzRNF0mO3OogXLW0NpYBR5lwk10
TFDuLMI49x4BI9//AGUv+Cq+n/GD/gnJ4s/b1+O/weu/A9z8PrnW7bxx4Mt74Xlxp8+myMHhDsse
52Ty22kDBfGe9SfGz/gjd+y78eV+Fvgnxj4g8T2vw6+D/h4WHgv4daJq7WVnbXaIkcGpGeHbcNcx
IpCvv6sW+8ST4hqv/BIP9oP4Qf8ABOr9rH9i74FeOLLX0+L/AIvutb+G7a/r08l1bRXcNlFcQXtz
OuWf/RWYPlt2/k5zQB+hfwv8f6V8Vvhn4e+KWiWlxb2PiTQrTVbOC8QLLHFcQpMiuOzBXAI7HNeI
fsf/APBUH9mP9uLxd8SfAfwH1LULrVvhnqk1nqVrfWwh/tFEkli+02pJPmwGWGSMSDjcuDiuV8Gf
EL/goPd/steNfhpq37D9l4J8R+H/AIdGx8AvY/Eu11WLVL4W5gijDLDEYNuFfcwx2r5k/Y1/4Ix/
ttfsQfE74DftB+Dv2k9K8Wah4Z0oeGviP4OuvD1vpUI0G9mku7zZcws73k8N3NJMjS/fJYZUOaAN
fQv+Djj4cfFn4U2Hjv4YeCLfw74h8O/FbTtA+KngPxxuGoafotxfNYy6jZtFIizLHOYw8nzrHhg6
glSeF/asvPA3x1/bp/aP+Hv7Y3jWU6z4N0PR7/8AZ78F6/8AFi48H6Je6TJaxtc38d1E6h51uPO3
yYcosagAZzXq/wAUP+Deb4G/tGeDfin8N/jj4lW0s/EHxQvvFXww8S+EwYdX8Lw3yRPd2bO67JYn
uEkk8s7lJkJ4NfZvjD9j/wDZx+LPhPwx4a+P/wAJ/DnxBn8K2EFvp+q+L9Ctru4LRxqplJZMKzld
zBQFJPSgDyr/AII8ftd/EH9tL9i2w+KfxJ+H9poN7pniHUNBtpNKnuZ9P1W0s5RHBfWc9yxluYZI
9o85j87pIRxivqJmyeK5TxZ8Svgf8B/CsS+MfHXhbwfo9jAI7ddS1K3sLeCNRgKodlVVAHAFfKXx
2/4OF/8Agkt8BpJLDUv2sdH8SairbU0zwXDJqk0jf3VMClCf+BUAfazHapb0FfjP/wAFwv2wfAn7
V37R037IGq+IWh+BvwCtE8ZftGatZzALqd2hB07QEf7peWQEFOSSx/55GvQfjH/wXv8A2qfjV8ON
Xk/YT/4JueNrLT7qwmhs/ip8YL+Dw3o2nsyMEugtx/rlUkNgsoOOtfiX+2/8TdQ8QWHhf/gmP+y3
4wX4ga1r3ihNc+K/jDRbj7QfG/jG7ICxpKuRLbWocxxnO0s0j+lNaCvdnrv/AASK/Za8f/8ABdf/
AIK9al+0j8ddGceAfDerLrviW2gjItbe2iO3TdGhzwsYCRR7RyIonPU5r+qqys7XT7SKxsoEihhj
CRRxrhVUDAAHYAV8of8ABGP/AIJqeEf+CYf7E3hz4JQQQXHiy/gTU/HerxoM3WpyoDIinqY4v9Wm
eoTPVjX1pQxhTk6/hTacnX8KQDaKd5fvR5fvQA2ub+MHwp8D/HP4Ya98IPiToMGp6F4k0qbT9VsL
lAyTQSqVZSD7Hg9jium8v3o8v3oA/ny/al/4Mn/FMd3feIv2Pv2vdPlheV5LXw7450d4jGpJIjF3
bl92BgZMQ9zXzNef8E3P+DmL/gnHC8Pwrl8fX2h2w2/ZvBvigaxp7IP+nR2YbeOhjFf1TeX70eWP
X9KdwP47viR/wUM/aY8Jag2j/t4/8E3/AIZeJLkNturjxj8KG0O+l9SbizFu7N781V8B/t8/8E5r
G6/tD/hiz4n/AAx1AkE6p8G/jhdWoU+qwXMLdOCAZK/sG8TeAvBfjSyfTvF/hPTNUt5FIkg1Gwjn
Rh6EOCDXz58V/wDgjT/wSz+NUr3XxA/YR+Gk1xJnzLrT/DENjM2e5e2EbE/U0XFY/n1+Hn/BWb4L
aMkS/D3/AIK5ftk+AwvK23izQtO8Rwxe25bpWb64H0r1fwv/AMFnPirawrB4T/4OJpmwconjL9nS
dGx6O8buM/QGv008ff8ABqz/AMEavG0rzWHwC1bQGkfcRonim6RV9gJGfArzDU/+DN7/AIJZ3l49
xY+K/iRaxsciFPEEbBfoTFmncLHyLZ/8Fuv2urdPKt/+C9fwPuAPuve/BvUkY/XFuaZqH/Bbj9rW
9h2X3/BfH4LWaD7z6X8GNSkfHsGtxk19bf8AEGp/wS//AOh8+Jf/AIPYf/jVH/EGp/wS/wD+h8+J
f/g9h/8AjVFwPhXxZ/wWU8cXSg+Lf+DibxVKFHzQ+B/2dXRm9g80yD88V458R/8Agqd+zP4gR4fi
X/wUW/bX+J28nzbbTL/T/DVpMD1BCTyOFPpjiv1d8O/8GdX/AASo0i+Fzq+r/EXU4wP9RP4kRF/8
ciBr1v4df8GwH/BGf4etHLL+y82vSJjD+IPEF3ODjuVWRVP5UXA/nV8Rft/f8E/dL1R9X8D/APBN
GbxVqGcjWPjD8V9Q1qSRv7zRQLAvXtuNd98If2y/+Ct3xfcaN+wJ+xxpvgaG6HlwXfwl+D0VvKFP
b7e8TyfiZK/p5+EX/BMD/gnf8B/Kk+En7Fnw00WeEgx3tv4PtGuAR0PnOjSE/wDAq9t07RdL0iBb
XS7CC3iUYWOCFUUD6AUrhY/l28H/APBuP/wXe/b98Q2/iz9rLx3LosNxIHfUPid4zmvZ4VPUpbRm
VlOP4cKPcV+of/BIH/g2H+DP/BND41W37S3xK+Mb/EjxnY2DRaQG0RbOx0uZ+HmiQvI7ybflVmYY
BJxnp+p3lgdD+lHl+9FxjQABtHQUU7y/ejy/ekA2nJ1/Cjy/elVdpzmgD//Z
</w:binData>
									<v:shape id="Picture 3" o:spid="_x0000_s1027" type="#_x0000_t75" alt="UIN MMI white" style="position:absolute;left:0;text-align:left;margin-left:-3.9pt;margin-top:9.15pt;width:57.65pt;height:57pt;z-index:-2;visibility:visible">
										<v:imagedata src="wordml://02000001.jpg" o:title="UIN MMI white"/>
										<w10:wrap type="square"/>
									</v:shape>
								</w:pict>
							</w:r>
							<w:r wsp:rsidR="003717F9" wsp:rsidRPr="009551A9">
								<w:rPr>
									<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
									<wx:font wx:val="Cambria"/>
									<w:sz w:val="22"/>
									<w:sz-cs w:val="22"/>
								</w:rPr>
								<w:t>KEMENTERIAN AGAMA</w:t>
							</w:r>
						</w:p>
						<w:p wsp:rsidR="003717F9" wsp:rsidRPr="009551A9" wsp:rsidRDefault="003717F9" wsp:rsidP="004650C7">
							<w:pPr>
								<w:jc w:val="center"/>
								<w:rPr>
									<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
									<wx:font wx:val="Cambria"/>
									<w:b/>
								</w:rPr>
							</w:pPr>
							<w:r wsp:rsidRPr="009551A9">
								<w:rPr>
									<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
									<wx:font wx:val="Cambria"/>
									<w:b/>
									<w:sz w:val="22"/>
									<w:sz-cs w:val="22"/>
								</w:rPr>
								<w:t>UNIVERSITAS ISLAM NEGERI MAULANA MALIK IBRAHIM MALANG</w:t>
							</w:r>
						</w:p>
						<w:p wsp:rsidR="00D87330" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00D87330" wsp:rsidP="004650C7">
							<w:pPr>
								<w:jc w:val="center"/>
								<w:rPr>
									<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
									<wx:font wx:val="Cambria"/>
									<w:b/>
								</w:rPr>
							</w:pPr>
							<w:r wsp:rsidRPr="009551A9">
								<w:rPr>
									<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
									<wx:font wx:val="Cambria"/>
									<w:b/>
									<w:sz w:val="22"/>
									<w:sz-cs w:val="22"/>
								</w:rPr>
								<w:t>FAKULTAS ILMU TARBIYAH DAN KEGURUAN</w:t>
							</w:r>
						</w:p>
						<w:p wsp:rsidR="003717F9" wsp:rsidRPr="009551A9" wsp:rsidRDefault="003717F9" wsp:rsidP="004650C7">
							<w:pPr>
								<w:jc w:val="center"/>
								<w:rPr>
									<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
									<wx:font wx:val="Cambria"/>
								</w:rPr>
							</w:pPr>
							<w:r wsp:rsidRPr="009551A9">
								<w:rPr>
									<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
									<wx:font wx:val="Cambria"/>
									<w:sz w:val="22"/>
									<w:sz-cs w:val="22"/>
								</w:rPr>
								<w:t>Jl. Gajayana No. 50 Malang 65144,  Telp. (0341) 55</w:t>
							</w:r>
							<w:r wsp:rsidR="00D87330" wsp:rsidRPr="009551A9">
								<w:rPr>
									<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
									<wx:font wx:val="Cambria"/>
									<w:sz w:val="22"/>
									<w:sz-cs w:val="22"/>
								</w:rPr>
								<w:t>2398</w:t>
							</w:r>
							<w:r wsp:rsidRPr="009551A9">
								<w:rPr>
									<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
									<wx:font wx:val="Cambria"/>
									<w:sz w:val="22"/>
									<w:sz-cs w:val="22"/>
								</w:rPr>
								<w:t>, Fax. (0341) </w:t>
							</w:r>
							<w:r wsp:rsidR="00D87330" wsp:rsidRPr="009551A9">
								<w:rPr>
									<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
									<wx:font wx:val="Cambria"/>
									<w:sz w:val="22"/>
									<w:sz-cs w:val="22"/>
								</w:rPr>
								<w:t>552398</w:t>
							</w:r>
						</w:p>
						<w:p wsp:rsidR="003717F9" wsp:rsidRPr="009551A9" wsp:rsidRDefault="009551A9" wsp:rsidP="00D87330">
							<w:pPr>
								<w:jc w:val="center"/>
								<w:rPr>
									<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
									<wx:font wx:val="Cambria"/>
									<w:color w:val="000000"/>
								</w:rPr>
							</w:pPr>
							<w:r>
								<w:rPr>
									<w:noProof/>
								</w:rPr>
								<w:pict>
									<v:line id="Line 2" o:spid="_x0000_s1026" style="position:absolute;left:0;text-align:left;z-index:2;visibility:visible;mso-wrap-distance-top:-6e-5mm;mso-wrap-distance-bottom:-6e-5mm" from="-68.15pt,26.95pt" to="400.7pt,26.95pt" o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF&#xA;90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA&#xA;0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD&#xA;OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893&#xA;SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y&#xA;JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl&#xA;bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR&#xA;JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY&#xA;22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i&#xA;OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA&#xA;IQCLjC7VGQIAADQEAAAOAAAAZHJzL2Uyb0RvYy54bWysU8GO2jAQvVfqP1i+QxIIFCLCqkqgl22L&#xA;tNsPMLZDrDq2ZRsCqvrvHRuC2PZSVc3BGXtmnt/MPK+ezp1EJ26d0KrE2TjFiCuqmVCHEn973Y4W&#xA;GDlPFCNSK17iC3f4af3+3ao3BZ/oVkvGLQIQ5YrelLj13hRJ4mjLO+LG2nAFzkbbjnjY2kPCLOkB&#xA;vZPJJE3nSa8tM1ZT7hyc1lcnXkf8puHUf20axz2SJQZuPq42rvuwJusVKQ6WmFbQGw3yDyw6IhRc&#xA;eoeqiSfoaMUfUJ2gVjvd+DHVXaKbRlAea4BqsvS3al5aYnisBZrjzL1N7v/B0i+nnUWCwewwUqSD&#xA;ET0LxdEkdKY3roCASu1sqI2e1Yt51vS7Q0pXLVEHHhm+XgykZSEjeZMSNs4A/r7/rBnEkKPXsU3n&#xA;xnYBEhqAznEal/s0+NkjCoez5SyfLmcY0cGXkGJINNb5T1x3KBgllsA5ApPTs/OBCCmGkHCP0lsh&#xA;ZRy2VKgv8XSRpaAH2hkone1lTHZaChYCQ4qzh30lLTqRIJ34xQrB8xhm9VGxCNxywjY32xMhrzYQ&#xA;kSrgQVlA7WZdtfFjmS43i80iH+WT+WaUp3U9+rit8tF8m32Y1dO6qursZ6CW5UUrGOMqsBt0muV/&#xA;p4Pbi7kq7K7Ue0uSt+ixd0B2+EfSca5hlFdR7DW77Owwb5BmDL49o6D9xz3Yj499/QsAAP//AwBQ&#xA;SwMEFAAGAAgAAAAhAFBeFUvfAAAACgEAAA8AAABkcnMvZG93bnJldi54bWxMj8FuwjAMhu+T9g6R&#xA;kXaDpOuGoDRFDAlNaLuM7QFMY9qKxqmaAO3bL9MO29H2p9/fn68H24or9b5xrCGZKRDEpTMNVxq+&#xA;PnfTBQgfkA22jknDSB7Wxf1djplxN/6g6yFUIoawz1BDHUKXSenLmiz6meuI4+3keoshjn0lTY+3&#xA;GG5b+ajUXFpsOH6osaNtTeX5cLEawlm9vr3gbtzY0z5Uy7G0++271g+TYbMCEWgIfzD86Ed1KKLT&#xA;0V3YeNFqmCbpPI2shud0CSISC5U8gTj+LmSRy/8Vim8AAAD//wMAUEsBAi0AFAAGAAgAAAAhALaD&#xA;OJL+AAAA4QEAABMAAAAAAAAAAAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQSwECLQAUAAYA&#xA;CAAAACEAOP0h/9YAAACUAQAACwAAAAAAAAAAAAAAAAAvAQAAX3JlbHMvLnJlbHNQSwECLQAUAAYA&#xA;CAAAACEAi4wu1RkCAAA0BAAADgAAAAAAAAAAAAAAAAAuAgAAZHJzL2Uyb0RvYy54bWxQSwECLQAU&#xA;AAYACAAAACEAUF4VS98AAAAKAQAADwAAAAAAAAAAAAAAAABzBAAAZHJzL2Rvd25yZXYueG1sUEsF&#xA;BgAAAAAEAAQA8wAAAH8FAAAAAA==&#xA;" strokeweight="3pt">
										<v:stroke linestyle="thinThin"/>
									</v:line>
								</w:pict>
							</w:r>
							<w:r wsp:rsidR="003717F9" wsp:rsidRPr="009551A9">
								<w:rPr>
									<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
									<wx:font wx:val="Cambria"/>
									<w:sz w:val="22"/>
									<w:sz-cs w:val="22"/>
								</w:rPr>
								<w:t>Website : </w:t>
							</w:r>
							<w:hlink w:dest="http://www.fitk.uin-malang.ac.id">
								<w:r wsp:rsidR="00D87330" wsp:rsidRPr="009551A9">
									<w:rPr>
										<w:rStyle w:val="Hyperlink"/>
										<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
										<wx:font wx:val="Cambria"/>
										<w:sz w:val="22"/>
										<w:sz-cs w:val="22"/>
									</w:rPr>
									<w:t>www.fitk.uin-malang.ac.id</w:t>
								</w:r>
							</w:hlink>
							<w:r wsp:rsidR="003717F9" wsp:rsidRPr="009551A9">
								<w:rPr>
									<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
									<wx:font wx:val="Cambria"/>
									<w:sz w:val="22"/>
									<w:sz-cs w:val="22"/>
								</w:rPr>
								<w:t>
								</w:t>
							</w:r>
						</w:p>
					</w:tc>
				</w:tr>
			</w:tbl>
			<w:p wsp:rsidR="003717F9" wsp:rsidRPr="009551A9" wsp:rsidRDefault="003717F9" wsp:rsidP="006632D6">
				<w:pPr>
					<w:tabs>
						<w:tab w:val="left" w:pos="900"/>
						<w:tab w:val="left" w:pos="1260"/>
					</w:tabs>
					<w:spacing w:before="240"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="EN-US"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Nomor </w:t>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
					<w:t>:</w:t>
				</w:r>
				<w:r wsp:rsidR="00B53BC9" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>
					</w:t>
				</w:r>
				<w:r wsp:rsidR="00086A99" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="EN-US"/>
					</w:rPr>
					<w:t>
					</w:t>
				</w:r>
				<w:r wsp:rsidR="006632D6" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="EN-US"/>
					</w:rPr>
					<w:t><?php foreach ($detail_pengadaan as $key => $value) {
						echo $value['no_pengadaan'];
					}?></w:t>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="EN-US"/>
					</w:rPr>
					<w:tab/>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
				</w:r>
				<w:r wsp:rsidR="00283FB9" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
				</w:r>
				<w:r wsp:rsidR="00ED734A" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="EN-US"/>
					</w:rPr>
					<w:t>
					</w:t>
				</w:r>
				<w:r wsp:rsidR="006632D6" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="EN-US"/>
					</w:rPr>
					<w:t><?php  foreach ($detail_pengadaan as $key => $value) {
						echo $value['tanggal_surat'];
					}?></w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="003717F9" wsp:rsidRPr="009551A9" wsp:rsidRDefault="003717F9" wsp:rsidP="003717F9">
				<w:pPr>
					<w:tabs>
						<w:tab w:val="left" w:pos="900"/>
						<w:tab w:val="left" w:pos="1260"/>
					</w:tabs>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Lampiran </w:t>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
					<w:t>:  1 (satu) bendel</w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="003717F9" wsp:rsidRPr="009551A9" wsp:rsidRDefault="003717F9" wsp:rsidP="003717F9">
				<w:pPr>
					<w:tabs>
						<w:tab w:val="left" w:pos="900"/>
						<w:tab w:val="left" w:pos="1260"/>
					</w:tabs>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Perihal </w:t>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
					<w:t>:  </w:t>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:b/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Permintaan Informasi </w:t>
				</w:r>
				<w:r wsp:rsidR="00B80B11" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:b/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Harga </w:t>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="003717F9" wsp:rsidRPr="009551A9" wsp:rsidRDefault="003717F9" wsp:rsidP="003717F9">
				<w:pPr>
					<w:tabs>
						<w:tab w:val="left" w:pos="900"/>
						<w:tab w:val="left" w:pos="1260"/>
					</w:tabs>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
			</w:p>
			<w:p wsp:rsidR="003717F9" wsp:rsidRPr="009551A9" wsp:rsidRDefault="003717F9" wsp:rsidP="003717F9">
				<w:pPr>
					<w:tabs>
						<w:tab w:val="left" w:pos="900"/>
						<w:tab w:val="left" w:pos="1260"/>
					</w:tabs>
					<w:spacing w:before="240"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
					<w:t>Kepada Yth. </w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="003717F9" wsp:rsidRPr="009551A9" wsp:rsidRDefault="003717F9" wsp:rsidP="003717F9">
				<w:pPr>
					<w:ind w:left="1276"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>............................................................................................</w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="003717F9" wsp:rsidRPr="009551A9" wsp:rsidRDefault="003717F9" wsp:rsidP="003717F9">
				<w:pPr>
					<w:ind w:left="1276"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
					</w:rPr>
					<w:t>d</w:t>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>i</w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="003717F9" wsp:rsidRPr="009551A9" wsp:rsidRDefault="003717F9" wsp:rsidP="003717F9">
				<w:pPr>
					<w:ind w:left="1996" w:first-line="164"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>‘</w:t>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>- T e m p a t – </w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="00F91D2A" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00F91D2A" wsp:rsidP="00F91D2A">
				<w:pPr>
					<w:ind w:left="1996" w:first-line="164"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
			</w:p>
			<w:p wsp:rsidR="00F91D2A" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00F91D2A" wsp:rsidP="00F91D2A">
				<w:pPr>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
			</w:p>
			<w:p wsp:rsidR="00F91D2A" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00F91D2A" wsp:rsidP="00F91D2A">
				<w:pPr>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Dengan hormat,</w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="00F91D2A" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00F91D2A" wsp:rsidP="00086A99">
				<w:pPr>
					<w:spacing w:before="120"/>
					<w:jc w:val="both"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Sehubungan rencana realisasi pelaksanaan pekerjaan</w:t>
				</w:r>
				<w:r wsp:rsidR="00270425" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:b/>
						<w:i/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>
					</w:t>
				</w:r>
				<w:r wsp:rsidR="00086A99" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria" w:cs="Calibri"/>
						<wx:font wx:val="Cambria"/>
						<w:b-cs/>
						<w:sz w:val="22"/>
						<w:sz-cs w:val="22"/>
						<w:lang w:val="EN-US"/>
					</w:rPr>
					<w:t> <?php foreach ($detail_pengadaan as $key => $value) {
						echo $value['paket_name'];
					}?></w:t>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:b/>
						<w:i/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>, </w:t>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>bersama ini kami bermaksud agar perusahaan saudara memberikan informasi tentang </w:t>
				</w:r>
				<w:r wsp:rsidR="00B80B11" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Harga Jasa</w:t>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t> sesuai dengan Rencana Anggaran Biaya (RAB) yang kami lampirkan dalam surat ini.</w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="00B85DA7" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00B85DA7" wsp:rsidP="00B85DA7">
				<w:pPr>
					<w:spacing w:before="120"/>
					<w:jc w:val="both"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Apabila informasi harga yang saudara berikan sesuai dan kami nilai wajar, maka kami akan memberi kesempatan perusahaan saudara untuk membuat penawaran terhadap pekerjaaan tersebut.</w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="00F91D2A" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00F91D2A" wsp:rsidP="00F91D2A">
				<w:pPr>
					<w:spacing w:before="120"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Kami harap data barang dapat kami terima paling lambat pada :</w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="00F91D2A" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00F91D2A" wsp:rsidP="00086A99">
				<w:pPr>
					<w:spacing w:before="120"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="EN-US"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Hari</w:t>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
					<w:t>: </w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="00F91D2A" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00F91D2A" wsp:rsidP="00086A99">
				<w:pPr>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="EN-US"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Tanggal</w:t>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
					<w:t>: </w:t>
				</w:r>
				<w:r wsp:rsidR="00086A99" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="EN-US"/>
					</w:rPr>
					<w:t><?php foreach ($detail_pengadaan as $key => $value) {
						echo $value['tanggal_permintaan'];
					}?></w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="00667619" wsp:rsidRPr="009551A9" wsp:rsidRDefault="000E0F52" wsp:rsidP="00086A99">
				<w:pPr>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="EN-US"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Pukul</w:t>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
					<w:t>: </w:t>
				</w:r>
				<w:r wsp:rsidR="00086A99" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="EN-US"/>
					</w:rPr>
					<w:t><?php foreach ($detail_pengadaan as $key => $value) {
						echo $value['jam'];
					}?></w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="00F55E99" wsp:rsidRPr="009551A9" wsp:rsidRDefault="0061372D" wsp:rsidP="00F55E99">
				<w:pPr>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Tempat </w:t>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
				</w:r>
				<w:r wsp:rsidR="00F55E99" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:tab/>
				</w:r>
				<w:r wsp:rsidR="00F91D2A" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>: </w:t>
				</w:r>
				<w:r wsp:rsidR="00F55E99" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Kantor Unit Layanan Pengadaan</w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="00F55E99" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00F55E99" wsp:rsidP="00F55E99">
				<w:pPr>
					<w:tabs>
						<w:tab w:val="left" w:pos="1985"/>
						<w:tab w:val="left" w:pos="2268"/>
					</w:tabs>
					<w:ind w:left="2268"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Lantai II Gedung Rektorat UIN Maulana Malik Ibrahim Malang </w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="00417895" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00F55E99" wsp:rsidP="00417895">
				<w:pPr>
					<w:tabs>
						<w:tab w:val="left" w:pos="1985"/>
						<w:tab w:val="left" w:pos="2268"/>
					</w:tabs>
					<w:ind w:left="2268"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Jl. Gajayana No. 50 Malang (0341) 570886</w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="00417895" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00417895" wsp:rsidP="00417895">
				<w:pPr>
					<w:tabs>
						<w:tab w:val="left" w:pos="1985"/>
						<w:tab w:val="left" w:pos="2268"/>
					</w:tabs>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
			</w:p>
			<w:p wsp:rsidR="001C5FE6" wsp:rsidRDefault="001C5FE6" wsp:rsidP="001C5FE6">
				<w:pPr>
					<w:tabs>
						<w:tab w:val="left" w:pos="1985"/>
						<w:tab w:val="left" w:pos="2268"/>
					</w:tabs>
					<w:ind w:left="2268" w:hanging="2268"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
					</w:rPr>
					<w:t>Surat tentang informasi harga barang tersebut, ditujukan:</w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="000E0F52" wsp:rsidRDefault="000E0F52" wsp:rsidP="001C5FE6">
				<w:pPr>
					<w:tabs>
						<w:tab w:val="left" w:pos="1985"/>
						<w:tab w:val="left" w:pos="2268"/>
					</w:tabs>
					<w:ind w:left="2268" w:hanging="2268"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:i/>
					</w:rPr>
				</w:pPr>
			</w:p>
			<w:p wsp:rsidR="001C5FE6" wsp:rsidRDefault="001C5FE6" wsp:rsidP="001C5FE6">
				<w:pPr>
					<w:tabs>
						<w:tab w:val="left" w:pos="1985"/>
						<w:tab w:val="left" w:pos="2268"/>
					</w:tabs>
					<w:ind w:left="2268" w:hanging="2268"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:i/>
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:i/>
					</w:rPr>
					<w:t>Kepada Yth:</w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="00DF7645" wsp:rsidRPr="00086A99" wsp:rsidRDefault="001C5FE6" wsp:rsidP="00086A99">
				<w:pPr>
					<w:tabs>
						<w:tab w:val="left" w:pos="1985"/>
						<w:tab w:val="left" w:pos="2268"/>
					</w:tabs>
					<w:ind w:left="2268" w:hanging="2268"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:i-cs/>
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:i/>
					</w:rPr>
					<w:t>Pejabat Pembuat Komitmen </w:t>
				</w:r>
				<w:r wsp:rsidR="00086A99">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:i-cs/>
					</w:rPr>
					<w:t><?php foreach ($ppk_profil as $key => $value) {
						echo $value['satker'];
					}?></w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="001C5FE6" wsp:rsidRDefault="001C5FE6" wsp:rsidP="001C5FE6">
				<w:pPr>
					<w:tabs>
						<w:tab w:val="left" w:pos="1985"/>
						<w:tab w:val="left" w:pos="2268"/>
					</w:tabs>
					<w:ind w:left="2268" w:hanging="2268"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:i/>
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:i/>
					</w:rPr>
					<w:t>Universitas Islam Negeri Maulana Malik Ibrahim Malang</w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="001C5FE6" wsp:rsidRDefault="001C5FE6" wsp:rsidP="001C5FE6">
				<w:pPr>
					<w:tabs>
						<w:tab w:val="left" w:pos="1985"/>
						<w:tab w:val="left" w:pos="2268"/>
					</w:tabs>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:i/>
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:i/>
					</w:rPr>
					<w:t>Jalan Gajayana No. 50 Malang</w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="001C5FE6" wsp:rsidRPr="009551A9" wsp:rsidRDefault="001C5FE6" wsp:rsidP="001C5FE6">
				<w:pPr>
					<w:tabs>
						<w:tab w:val="left" w:pos="1985"/>
						<w:tab w:val="left" w:pos="2268"/>
					</w:tabs>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
			</w:p>
			<w:p wsp:rsidR="00417895" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00417895" wsp:rsidP="00000ACA">
				<w:pPr>
					<w:tabs>
						<w:tab w:val="left" w:pos="1985"/>
						<w:tab w:val="left" w:pos="2268"/>
					</w:tabs>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Adapun informasi harga tersebut bisa dikirim via e-mail ke : </w:t>
				</w:r>
				<w:r wsp:rsidR="00F700FD" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:b/>
						<w:color w:val="000000"/>
						<w:lang w:val="EN-US"/>
					</w:rPr>
					<w:t>ulpuinmaliki@gmail.com</w:t>
				</w:r>
				<w:hlink w:dest="mailto:ulp@uin-malang.ac.id"/>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t> atau </w:t>
				</w:r>
				<w:hlink w:dest="mailto:ulp_uinmalang@kemenag.go.id">
					<w:r wsp:rsidRPr="009551A9">
						<w:rPr>
							<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
							<wx:font wx:val="Cambria"/>
							<w:b/>
							<w:b-cs/>
							<w:color w:val="000000"/>
							<w:lang w:val="IN"/>
						</w:rPr>
						<w:t>ulp_uinmalang@kemenag.go.id</w:t>
					</w:r>
				</w:hlink>
				<w:r wsp:rsidR="00000ACA" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t> atau bisa dikirim langsung ke kantor ULP atau di Fax ke (</w:t>
				</w:r>
				<w:r wsp:rsidR="00000ACA" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="EN-US"/>
					</w:rPr>
					<w:t>0341) 570886</w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="00F91D2A" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00417895" wsp:rsidP="00417895">
				<w:pPr>
					<w:spacing w:before="120"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Demikian atas perhatian dan kerjasamanya yang baik, kami sampaikan terima kasih.</w:t>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:br/>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:br/>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="00F91D2A" wsp:rsidRPr="009551A9" wsp:rsidRDefault="005D009F" wsp:rsidP="00F91D2A">
				<w:pPr>
					<w:spacing w:before="120"/>
					<w:ind w:left="5040" w:first-line="720"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>Pejabat Pembuat Komitmen</w:t>
				</w:r>
				<w:r wsp:rsidR="00F91D2A" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>,</w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="00F91D2A" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00F91D2A" wsp:rsidP="00F91D2A">
				<w:pPr>
					<w:spacing w:before="120"/>
					<w:ind w:left="5040" w:first-line="720"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
			</w:p>
			<w:p wsp:rsidR="00F91D2A" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00F91D2A" wsp:rsidP="00F91D2A">
				<w:pPr>
					<w:spacing w:before="120"/>
					<w:ind w:left="5040" w:first-line="720"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
			</w:p>
			<w:p wsp:rsidR="00283FB9" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00283FB9" wsp:rsidP="00F91D2A">
				<w:pPr>
					<w:spacing w:before="120"/>
					<w:ind w:left="5040" w:first-line="720"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
				</w:pPr>
			</w:p>
			<w:p wsp:rsidR="004203F1" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00086A99" wsp:rsidP="00283FB9">
				<w:pPr>
					<w:ind w:left="5760"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria" w:cs="Calibri"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:lang w:val="PT-BR"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria" w:cs="Calibri"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:lang w:val="PT-BR"/>
					</w:rPr>
					<w:t><?php foreach ($ppk_profil as $key => $value) {
						echo $value['ppk_name'];
					}?></w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="00F91D2A" wsp:rsidRPr="009551A9" wsp:rsidRDefault="005D009F" wsp:rsidP="00086A99">
				<w:pPr>
					<w:ind w:left="5760"/>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="EN-US"/>
					</w:rPr>
				</w:pPr>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>NIP</w:t>
				</w:r>
				<w:r wsp:rsidR="004203F1" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="EN-US"/>
					</w:rPr>
					<w:t>.</w:t>
				</w:r>
				<w:r wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
						<w:lang w:val="IN"/>
					</w:rPr>
					<w:t>
					</w:t>
				</w:r>
				<w:r wsp:rsidR="00086A99" wsp:rsidRPr="009551A9">
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria" w:cs="Calibri"/>
						<wx:font wx:val="Cambria"/>
						<w:color w:val="000000"/>
						<w:lang w:val="SV"/>
					</w:rPr>
					<w:t><?php foreach ($ppk_profil as $key => $value) {
						echo $value['nip'];
					}?></w:t>
				</w:r>
			</w:p>
			<w:p wsp:rsidR="00024264" wsp:rsidRPr="009551A9" wsp:rsidRDefault="00024264" wsp:rsidP="00FA3295">
				<w:pPr>
					<w:rPr>
						<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
						<wx:font wx:val="Cambria"/>
					</w:rPr>
				</w:pPr>
			</w:p>
			<w:sectPr wsp:rsidR="00024264" wsp:rsidRPr="009551A9" wsp:rsidSect="00FA3295">
				<w:ftr w:type="odd">
					<w:p wsp:rsidR="00E42F09" wsp:rsidRPr="00E42F09" wsp:rsidRDefault="00E42F09">
						<w:pPr>
							<w:pStyle w:val="Footer"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:sz w:val="16"/>
								<w:sz-cs w:val="16"/>
							</w:rPr>
						</w:pPr>
						<w:r wsp:rsidRPr="00E42F09">
							<w:rPr>
								<w:sz w:val="16"/>
								<w:sz-cs w:val="16"/>
							</w:rPr>
							<w:t>Page </w:t>
						</w:r>
						<w:r wsp:rsidRPr="00E42F09">
							<w:rPr>
								<w:b/>
								<w:b-cs/>
								<w:sz w:val="16"/>
								<w:sz-cs w:val="16"/>
							</w:rPr>
							<w:fldChar w:fldCharType="begin"/>
						</w:r>
						<w:r wsp:rsidRPr="00E42F09">
							<w:rPr>
								<w:b/>
								<w:b-cs/>
								<w:sz w:val="16"/>
								<w:sz-cs w:val="16"/>
							</w:rPr>
							<w:instrText> PAGE </w:instrText>
						</w:r>
						<w:r wsp:rsidRPr="00E42F09">
							<w:rPr>
								<w:b/>
								<w:b-cs/>
								<w:sz w:val="16"/>
								<w:sz-cs w:val="16"/>
							</w:rPr>
							<w:fldChar w:fldCharType="separate"/>
						</w:r>
						<w:r wsp:rsidR="009551A9">
							<w:rPr>
								<w:b/>
								<w:b-cs/>
								<w:noProof/>
								<w:sz w:val="16"/>
								<w:sz-cs w:val="16"/>
							</w:rPr>
							<w:t>1</w:t>
						</w:r>
						<w:r wsp:rsidRPr="00E42F09">
							<w:rPr>
								<w:b/>
								<w:b-cs/>
								<w:sz w:val="16"/>
								<w:sz-cs w:val="16"/>
							</w:rPr>
							<w:fldChar w:fldCharType="end"/>
						</w:r>
						<w:r wsp:rsidRPr="00E42F09">
							<w:rPr>
								<w:sz w:val="16"/>
								<w:sz-cs w:val="16"/>
							</w:rPr>
							<w:t> of </w:t>
						</w:r>
						<w:r wsp:rsidRPr="00E42F09">
							<w:rPr>
								<w:b/>
								<w:b-cs/>
								<w:sz w:val="16"/>
								<w:sz-cs w:val="16"/>
							</w:rPr>
							<w:fldChar w:fldCharType="begin"/>
						</w:r>
						<w:r wsp:rsidRPr="00E42F09">
							<w:rPr>
								<w:b/>
								<w:b-cs/>
								<w:sz w:val="16"/>
								<w:sz-cs w:val="16"/>
							</w:rPr>
							<w:instrText> NUMPAGES  </w:instrText>
						</w:r>
						<w:r wsp:rsidRPr="00E42F09">
							<w:rPr>
								<w:b/>
								<w:b-cs/>
								<w:sz w:val="16"/>
								<w:sz-cs w:val="16"/>
							</w:rPr>
							<w:fldChar w:fldCharType="separate"/>
						</w:r>
						<w:r wsp:rsidR="009551A9">
							<w:rPr>
								<w:b/>
								<w:b-cs/>
								<w:noProof/>
								<w:sz w:val="16"/>
								<w:sz-cs w:val="16"/>
							</w:rPr>
							<w:t>1</w:t>
						</w:r>
						<w:r wsp:rsidRPr="00E42F09">
							<w:rPr>
								<w:b/>
								<w:b-cs/>
								<w:sz w:val="16"/>
								<w:sz-cs w:val="16"/>
							</w:rPr>
							<w:fldChar w:fldCharType="end"/>
						</w:r>
					</w:p>
					<w:p wsp:rsidR="00E42F09" wsp:rsidRDefault="00E42F09">
						<w:pPr>
							<w:pStyle w:val="Footer"/>
						</w:pPr>
					</w:p>
				</w:ftr>
				<w:pgSz w:w="12242" w:h="18722" w:code="258"/>
				<w:pgMar w:top="1440" w:right="1440" w:bottom="1440" w:left="1327" w:header="720" w:footer="720" w:gutter="0"/>
				<w:cols w:space="720"/>
				<w:docGrid w:line-pitch="360"/>
			</w:sectPr>
		</wx:sect>
	</w:body>
</w:wordDocument>