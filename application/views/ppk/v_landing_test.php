<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/_site/bootstrap/css/bootstrap-theme.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/_site/bootstrap/css/normalize.css')?>">
	<!-- <link rel="stylesheet" type="text/css" href="<?php //echo base_url('ext/_site/bootstrap/css/style.css');?>"> -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/_site/bootstrap/css/customize.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/_site/bootstrap/css/bootstrap.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/_site/bootstrap/css/simple-sidebar.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/_site/bootstrap/css/datepicker.css');?>">
	<title>Pengadaan</title>
	<style type="text/css">
	th{
		text-align: center;
	}
	</style>
</head>
<body>
	<!-- Fixed navbar -->
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container ">
			<a class="navbar-brand text-center" href="#">My Website's Brand</a>
			<div class="navbar-header text-center">
				<!-- <a class="navbar-brand text-center" href="#">My Website's Brand</a> -->
			</div>
		</div>
	</div>

	<!-- <div class=""> -->
	<div class="container-fluid content">
		<div class="row">	
			<div class=" col-xs-2 col-sm-2 col-md-2">
				<ul id="sidebar" class="nav nav-stacked fixed">
					<li>
						<a href="<?php echo base_url('/ppk/pengadaan');?>">List Pengadaan</a>
					</li>
					<li>
						<a href="<?php //echo base_url('/ppk/hasilsurvey');?>">Harga Perkiraan dan Spesifikasi</a>
					</li>
					<li>
						<a href="<?php echo base_url('/ppk/submitharga');?>">Form Submit Informasi Harga</a>
					</li>
					<li>
						<a href="<?php echo base_url('/ppk/submithps');?>">Form Submit HPS </a>
					</li>
					<li>
						<a href="<?php echo base_url('/ppk/profil');?>">Profile</a>
					</li>
					<li>
						<a href="<?php echo base_url('/ulp/');?>">Keluar</a>
					</li>
				</ul>
			</div>
			<div class="col-xs-10 col-sm-10 col-md-10">
				<div class="panel panel-default">					
					<table class="table">
						<tr>
							<td>Identitas Pengadaan</td>
							<td><?php echo $res['id_pengadaan'];?></td>
						</tr>
						<tr>
							<td>Identitas Barang</td>
							<td><?php echo $res['id_barang'];?></td>
						</tr>
						<tr>
							<td>Identitas Jadwal</td>
							<td><?php echo $res['id_jadwal'];?></td>
						</tr>
						<tr>
							<td>Nama Paket</td>
							<td><?php echo $res['paket_name'];?></td>
						</tr>
						<tr>
							<td>No Pengadaan</td>
							<td><?php echo $res['no_pengadaan'];?></td>
						</tr>
						<tr>
							<td>Tanggal Surat</td>
							<td><?php echo $res['tanggal_surat'];?></td>
						</tr>
						<tr>
							<td>Tanggal Pengadaan</td>
							<td><?php echo $res['tanggal_pengadaan'];?></td>
						</tr>
						<tr>
							<td>Anggaran</td>
							<td><?php echo $res['anggaran'];?></td>
						</tr>
						<tr>
							<td>Anggaran Detail</td>
							<td><?php echo $res['anggaran_detail'];?></td>
						</tr>
						<tr>
							<td>No DIPA</td>
							<td><?php echo $res['no_dipa'];?></td>
						</tr>
						<tr>
							<td>Mata Anggaran Kode</td>
							<td> <?php echo $res['mak'];?></td>
						</tr>
						<tr>
							<td>Satuan Kerja</td>
							<td> <?php echo $res['satker'];?></td>
						</tr>
						<tr>
							<td>Lokasi Pengadaan</td>
							<td><?php echo $res['lokasi'];?></td>
						</tr>
						<tr>
							<td>Tahun Anggaran</td>
							<td> <?php echo $res['ta_anggaran'];?></td>
						</tr>

					</table>
				</div>
				<?php echo form_open('ppk/save_word'); ?>
					<input type="hidden" name="id_pengadaan" value=<?php echo $res['id_pengadaan']; ?>>
					<input type="hidden" name="id_barang" value=<?php echo $res['id_barang']; ?>>
					<input type="hidden" name="id_jadwal" value=<?php echo $res['id_jadwal']; ?>>
					<input type="hidden" name="paket_name" value=<?php echo $res['paket_name']; ?>>
					<input type="hidden" name="no_pengadaan" value=<?php echo $res['no_pengadaan']; ?>>
					<input type="hidden" name="tanggal_surat" value=<?php echo $res['tanggal_surat']; ?>>
					<input type="hidden" name="tanggal_pengadaan" value=<?php echo $res['tanggal_pengadaan']; ?>>
					<input type="hidden" name="anggaran" value=<?php echo $res['anggaran']; ?>>
					<input type="hidden" name="anggaran_detail" value=<?php echo $res['anggaran_detail']; ?>>
					<input type="hidden" name="no_dipa" value=<?php echo $res['no_dipa']; ?>>
					<input type="hidden" name="mak" value=<?php echo $res['mak']; ?>>
					<input type="hidden" name="satker" value=<?php echo $res['satker']; ?>>
					<input type="hidden" name="lokasi" value=<?php echo $res['lokasi']; ?>>
					<input type="hidden" name="ta_anggaran" value=<?php echo $res['ta_anggaran']; ?>>
					<div class="col-md-3 text-right">
					<button id="print" name="print" class="btn btn-group" type="submit" value="print">Print</button>
					<?php echo form_close();?>
					<?php echo form_open('ppk/alah');?>
					<button id="lanjut" name="lanjut" class="btn btn-group" type="submit" value="lanjut">Lanjut</button>
					<?php echo form_close(); ?>
					</div>


					
			</div>
			
		</div>