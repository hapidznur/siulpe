<!-- start: Content -->
<div id="content">
    <div class="container-fluid">

        <ul class="breadcrumb">
            <li><?php echo set_breadcrumb();?>
        </li>
        </ul>

        <div class="row-fluid sortable">        
            <div class="box span12">
                <div class="box-header" data-original-title>
                    <h2><span class="break"></span>Harga Perkiraan & Spesifikasi </h2>
                    <!--<div class="box-icon">
                        <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                        <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                        <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                    </div>-->
                </div>
                <div class="box-content">
                    <table class="table table-striped table-bordered bootstrap-datatable datatable">
                      <thead>
                          <tr>
                              <th>No</th>
                              <th>Uraian</th>
                              <th>Unit/Satuan</th>
                              <th>Quantity</th>
                              <th>Harga Satuan</th>
                              <th>Jumlah</th>
                          </tr>
                      </thead>   
                      <tbody>
                      </tr>
                      <tr>
                        <td class="col-md-1">1</td>
                        <td>Dennis Ji</td>
                        <td class="center">2012/03/01</td>
                        <td class="col-md-2"><span class="label label-warning">Pending</span></td>
                        <td class="col-md-2">
                            Rp. 10.000,-
                        </td>
                        <td>Banyak</td>
                    </tr>
                    
                </tbody>
            </table>            
        </div>
    </div><!--/span-->

        </div><!--/row-->
    </div>

</div>

<!-- </div>/.fluid-container -->

<!-- end: Content -->
<!-- </div>/#content.span10 -->
<!-- </div>/fluid-row -->
