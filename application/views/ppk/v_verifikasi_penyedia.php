<div class="col-sm-10 col-sm-offset-2 col-md-10 col-lg-offset-2 main">
      <ol class="breadcrumb"><?php //echo ; ?></ol>
      <div class="row">
        <div class="col-sm-8 col-md-8 col-lg-8">
          <h3>Verifikasi Penyedia</h3>
        </div><!--end of col-8 -->
      </div><!-- end of row-->
 <div class="row">
  	<div class="col-sm-12 col-md-12 col-lg-12">
  		<table class="table table-bordered" id="table-verifikasi-penyedia">
  			<thead>
  				<tr>
  				<th colspan="2">Detail Pengadaan</th>
  				</tr>
  			</thead>
  			<tbody >
  				<tr>
  					<td class="col-sm-4 col-md-4 col-lg-4 text-left">Nama Pengadaan</td>
  					<td class="text-left"><?php echo array_values($seluruh_penawaran)[0]['paket_name']; ?></td>
  				</tr>
  				<tr>
  					<td class="col-sm-4 col-md-4 col-lg-4 text-left">Lokasi</td>
  					<td class="text-left"><?php echo array_values($seluruh_penawaran)[0]['lokasi']; ?></td>
  				</tr>
  				<tr>
  					<td class="col-sm-4 col-md-4 col-lg-4 text-left">Jumlah Penawar</td>
  					<td><span id="jumlahPenawar" class="jumlahPenawar"><?php echo count($seluruh_penawaran); ?></span></td>
  				</tr>
  				<tr>
  					<td class="col-sm-4 col-md-4 col-lg-4 text-left">Status</td>
  					<td><code><span id="statusPengadaan" name="statusPengadaan"></span></code></td>
  				</tr>
  			</tbody>
  		</table>
  		<table class="table table-bordered">
  			<thead>
  				<tr>
  				<th>Nama Penyedia</th>
  				<th>Terverifikasi</th>
  				<th>Total Harga</th>
  				<th colspan="2">Aksi</th>
  				</tr>
  			</thead>
  			<tbody>
  				<?php foreach ($seluruh_penawaran as $value) { ?>
  				<tr>
  					<td class="col-sm-4 col-md-4 col-lg-4 text-left"><?php echo $value['nama_penyedia']; ?></td>
  					<td class="text-center"><span id="statsVerifikasi" name="statsVerifikasi"><?php echo $value['verifikasi']; ?></span></td>
  					<td class="col-sm-4 col-md-4 col-lg-4 text-center"><?php echo $value['total_harga']; ?></td>
  					<td class="text-center">
              <?php echo form_open("ppk/penawaran_terverifikasi"); ?>
              <input type="hidden" name="id_penawaran" value=<?php echo $value['id_penawaran']; ?>>
              <input type="submit" value="Verifikasi Saja" class="btn btn-success" id="btn-verifikasi" name="btn-verifikasi">
              <?php echo form_close(); ?>
            </td>
  					<td class="col-sm-4 col-md-4 col-lg-4 text-center">
              <?php echo form_open('ppk/detail_tawaran_penyedia'); ?>
              <input type="hidden" name="id_pengadaan" value=<?php echo $value['id_pengadaan']; ?>>
              <input type="hidden" name="id_penawaran" value=<?php echo $value['id_penawaran']; ?>>
              <input type="submit" value="Detail Tawaran" class="btn btn-info">
              <?php echo form_close(); ?>
            </td>
  				</tr>
  				<?php } ?>
  			</tbody>
  		</table>
  	</div>
</div>