<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<pkg:package xmlns:pkg="http://schemas.microsoft.com/office/2006/xmlPackage">
	<pkg:part pkg:name="/_rels/.rels" pkg:contentType="application/vnd.openxmlformats-package.relationships+xml" pkg:padding="512">
		<pkg:xmlData>
			<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
				<Relationship Id="rId3" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties" Target="docProps/app.xml"/>
				<Relationship Id="rId2" Type="http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties" Target="docProps/core.xml"/>
				<Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument" Target="word/document.xml"/>
			</Relationships>
		</pkg:xmlData>
	</pkg:part>
	<pkg:part pkg:name="/word/_rels/document.xml.rels" pkg:contentType="application/vnd.openxmlformats-package.relationships+xml" pkg:padding="256">
		<pkg:xmlData>
			<Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
				<Relationship Id="rId8" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable" Target="fontTable.xml"/>
				<Relationship Id="rId3" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings" Target="settings.xml"/>
				<Relationship Id="rId7" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/footer" Target="footer1.xml"/>
				<Relationship Id="rId2" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles" Target="styles.xml"/>
				<Relationship Id="rId1" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/numbering" Target="numbering.xml"/>
				<Relationship Id="rId6" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/endnotes" Target="endnotes.xml"/>
				<Relationship Id="rId5" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/footnotes" Target="footnotes.xml"/>
				<Relationship Id="rId4" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/webSettings" Target="webSettings.xml"/>
				<Relationship Id="rId9" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme" Target="theme/theme1.xml"/>
			</Relationships>
		</pkg:xmlData>
	</pkg:part>
	<pkg:part pkg:name="/word/document.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml">
		<pkg:xmlData>
			<w:document mc:Ignorable="w14 w15 wp14" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape">
				<w:body>
					<w:p w:rsidR="00F91D2A" w:rsidRPr="006D5A96" w:rsidRDefault="00F91D2A" w:rsidP="00791790">
						<w:pPr>
							<w:ind w:left="9990" w:firstLine="90"/>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t>Lampiran</w:t>
						</w:r>
						<w:r w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
							<w:t xml:space="preserve">: Surat Permintaan </w:t>
						</w:r>
						<w:r w:rsidR="00AA197E" w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t xml:space="preserve">Informasi </w:t>
						</w:r>
						<w:r w:rsidR="00B80B11" w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t xml:space="preserve">Harga </w:t>
						</w:r>
					</w:p>
					<w:p w:rsidR="00F91D2A" w:rsidRPr="00D331AA" w:rsidRDefault="00F96D2F" w:rsidP="00D331AA">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="900"/>
								<w:tab w:val="left" w:pos="1260"/>
							</w:tabs>
							<w:ind w:left="3600"/>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="en-US"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00791790">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00791790">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00791790">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00791790">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00791790">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00791790">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00791790">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00791790">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t xml:space="preserve">Nomor </w:t>
						</w:r>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00F91D2A" w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t xml:space="preserve">: </w:t>
						</w:r>
						<w:r w:rsidR="00D331AA">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="en-US"/>
							</w:rPr>
							<w:t><?php foreach ($detail_pengadaan as $key => $value) {
								echo $value['no_pengadaan'];
							} ?></w:t>
						</w:r>
					</w:p>
					<w:p w:rsidR="00F91D2A" w:rsidRPr="00E03050" w:rsidRDefault="00F91D2A" w:rsidP="00E03050">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="900"/>
								<w:tab w:val="left" w:pos="1260"/>
							</w:tabs>
							<w:ind w:left="3600"/>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="en-US"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00791790">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00791790">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00791790">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00791790">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00791790">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00791790">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00791790">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00791790">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t xml:space="preserve">Tanggal </w:t>
						</w:r>
						<w:r w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
							<w:t xml:space="preserve">: </w:t>
						</w:r>
						<w:r w:rsidR="00E03050">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="en-US"/>
							</w:rPr>
							<w:t><?php foreach ($detail_pengadaan as $key => $value) {
								echo $value['tanggal_surat'];
							} ?></w:t>
						</w:r>
					</w:p>
					<w:p w:rsidR="00F91D2A" w:rsidRPr="006D5A96" w:rsidRDefault="00F91D2A" w:rsidP="00F91D2A">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="900"/>
								<w:tab w:val="left" w:pos="1260"/>
							</w:tabs>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
					</w:p>
					<w:p w:rsidR="00F96D2F" w:rsidRDefault="00F96D2F" w:rsidP="00F91D2A">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="900"/>
								<w:tab w:val="left" w:pos="1260"/>
							</w:tabs>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:b/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
						</w:pPr>
					</w:p>
					<w:p w:rsidR="00F91D2A" w:rsidRPr="006D5A96" w:rsidRDefault="00F91D2A" w:rsidP="00F91D2A">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="900"/>
								<w:tab w:val="left" w:pos="1260"/>
							</w:tabs>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:b/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:b/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t>Rincian  Anggaran Biaya (RAB)</w:t>
						</w:r>
					</w:p>
					<w:p w:rsidR="00F91D2A" w:rsidRPr="006D5A96" w:rsidRDefault="00F91D2A" w:rsidP="00F91D2A">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="900"/>
								<w:tab w:val="left" w:pos="1260"/>
							</w:tabs>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:b/>
								<w:sz w:val="14"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
						</w:pPr>
					</w:p>
					<w:p w:rsidR="00F91D2A" w:rsidRPr="0043456B" w:rsidRDefault="004434CA" w:rsidP="00E03050">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="1985"/>
							</w:tabs>
							<w:ind w:left="2324" w:hanging="2324"/>
							<w:jc w:val="both"/>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi" w:cstheme="minorHAnsi"/>
								<w:b/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t xml:space="preserve">Pekerjaan </w:t>
						</w:r>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00F91D2A" w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t xml:space="preserve">: </w:t>
						</w:r>
						<w:r w:rsidR="00E03050">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi" w:cstheme="minorHAnsi"/>
								<w:b/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
							</w:rPr>
							<w:t><?php foreach ($detail_pengadaan as $key => $value) {
								echo $value['paket_name'];
							} ?></w:t>
						</w:r>
					</w:p>
					<w:p w:rsidR="00F91D2A" w:rsidRPr="006D5A96" w:rsidRDefault="004434CA" w:rsidP="00E03050">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="900"/>
								<w:tab w:val="left" w:pos="1985"/>
							</w:tabs>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t>Lokasi</w:t>
						</w:r>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
							<w:t xml:space="preserve">: </w:t>
						</w:r>
						<w:r w:rsidR="00E03050">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:b/>
								<w:sz w:val="20"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="en-US"/>
							</w:rPr>
							<w:t><?php foreach ($detail_pengadaan as $key => $value) {
								echo $value['lokasi'];
							} ?></w:t>
						</w:r>
					</w:p>
					<w:p w:rsidR="00F91D2A" w:rsidRPr="00E03050" w:rsidRDefault="004434CA" w:rsidP="004434CA">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="1985"/>
							</w:tabs>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:b/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="en-US"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t xml:space="preserve">Tahun Anggaran </w:t>
						</w:r>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:tab/>
						</w:r>
						<w:r w:rsidR="00F91D2A" w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t xml:space="preserve">: </w:t>
						</w:r>
						<w:r w:rsidR="00E03050">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:b/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="en-US"/>
							</w:rPr>
							<w:t><?php foreach ($detail_pengadaan as $key => $value) {
								echo $value['tahun'];
							} ?></w:t>
						</w:r>
					</w:p>
					<w:p w:rsidR="001C2357" w:rsidRDefault="001C2357" w:rsidP="004434CA">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="1985"/>
							</w:tabs>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:b/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
							</w:rPr>
						</w:pPr>
					</w:p>
					<w:tbl>
						<w:tblPr>
							<w:tblW w:w="16302" w:type="dxa"/>
							<w:tblInd w:w="108" w:type="dxa"/>
							<w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/>
						</w:tblPr>
						<w:tblGrid>
							<w:gridCol w:w="433"/>
							<w:gridCol w:w="5521"/>
							<w:gridCol w:w="820"/>
							<w:gridCol w:w="1138"/>
							<w:gridCol w:w="1238"/>
							<w:gridCol w:w="7152"/>
						</w:tblGrid>
						<w:tr w:rsidR="00D331AA" w:rsidRPr="00E42F09" w:rsidTr="00D331AA">
							<w:trPr>
								<w:trHeight w:val="612"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="433" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="4D4D4D"/>
										<w:left w:val="double" w:sz="6" w:space="0" w:color="4D4D4D"/>
										<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="4D4D4D"/>
									</w:tcBorders>
									<w:shd w:val="clear" w:color="CCFFCC" w:fill="DDDDDD"/>
									<w:vAlign w:val="center"/>
									<w:hideMark/>
								</w:tcPr>
								<w:p w:rsidR="00D331AA" w:rsidRPr="00E42F09" w:rsidRDefault="00D331AA" w:rsidP="00E42F09">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:b/>
											<w:bCs/>
											<w:color w:val="000000"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:r w:rsidRPr="00E42F09">
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:b/>
											<w:bCs/>
											<w:color w:val="000000"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t>No</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="5521" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="4D4D4D"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="4D4D4D"/>
									</w:tcBorders>
									<w:shd w:val="clear" w:color="CCFFCC" w:fill="DDDDDD"/>
									<w:vAlign w:val="center"/>
									<w:hideMark/>
								</w:tcPr>
								<w:p w:rsidR="00D331AA" w:rsidRPr="00E42F09" w:rsidRDefault="00D331AA" w:rsidP="00E42F09">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:b/>
											<w:bCs/>
											<w:color w:val="000000"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:b/>
											<w:bCs/>
											<w:color w:val="000000"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t>Nama Barang</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="820" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="4D4D4D"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="4D4D4D"/>
									</w:tcBorders>
									<w:shd w:val="clear" w:color="CCFFCC" w:fill="DDDDDD"/>
									<w:vAlign w:val="center"/>
									<w:hideMark/>
								</w:tcPr>
								<w:p w:rsidR="00D331AA" w:rsidRPr="00E42F09" w:rsidRDefault="00D331AA" w:rsidP="00E42F09">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:b/>
											<w:bCs/>
											<w:color w:val="000000"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:r w:rsidRPr="00E42F09">
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:b/>
											<w:bCs/>
											<w:color w:val="000000"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t>Volume</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1138" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="4D4D4D"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="4D4D4D"/>
									</w:tcBorders>
									<w:shd w:val="clear" w:color="CCFFCC" w:fill="DDDDDD"/>
									<w:vAlign w:val="center"/>
									<w:hideMark/>
								</w:tcPr>
								<w:p w:rsidR="00D331AA" w:rsidRPr="00E42F09" w:rsidRDefault="00D331AA" w:rsidP="00E42F09">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:b/>
											<w:bCs/>
											<w:color w:val="000000"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:r w:rsidRPr="00E42F09">
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:b/>
											<w:bCs/>
											<w:color w:val="000000"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t>Harga Satuan (Rp)</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1238" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="4D4D4D"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="4D4D4D"/>
									</w:tcBorders>
									<w:shd w:val="clear" w:color="CCFFCC" w:fill="DDDDDD"/>
									<w:vAlign w:val="center"/>
									<w:hideMark/>
								</w:tcPr>
								<w:p w:rsidR="00D331AA" w:rsidRPr="00E42F09" w:rsidRDefault="00D331AA" w:rsidP="00E42F09">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:b/>
											<w:bCs/>
											<w:color w:val="000000"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:r w:rsidRPr="00E42F09">
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:b/>
											<w:bCs/>
											<w:color w:val="000000"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t>Jumlah (Rp)</w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="7152" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="double" w:sz="6" w:space="0" w:color="4D4D4D"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
										<w:right w:val="double" w:sz="6" w:space="0" w:color="4D4D4D"/>
									</w:tcBorders>
									<w:shd w:val="clear" w:color="CCFFCC" w:fill="DDDDDD"/>
								</w:tcPr>
								<w:p w:rsidR="00D331AA" w:rsidRPr="00791790" w:rsidRDefault="00D331AA" w:rsidP="00D331AA">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:b/>
											<w:bCs/>
											<w:color w:val="000000"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="id-ID"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:b/>
											<w:bCs/>
											<w:color w:val="000000"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="id-ID"/>
										</w:rPr>
										<w:t xml:space="preserve">Keterangan </w:t>
									</w:r>
								</w:p>
								<w:p w:rsidR="00D331AA" w:rsidRPr="00791790" w:rsidRDefault="00D331AA" w:rsidP="00E42F09">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:b/>
											<w:bCs/>
											<w:color w:val="000000"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="id-ID"/>
										</w:rPr>
									</w:pPr>
								</w:p>
							</w:tc>
						</w:tr>
						<?php foreach ($detail_barang as $key => $value) {?>
						<w:tr w:rsidR="00D331AA" w:rsidRPr="00E42F09" w:rsidTr="00D331AA">
							<w:trPr>
								<w:trHeight w:val="480"/>
							</w:trPr>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="433" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="single" w:sz="4" w:space="0" w:color="000000"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="000000"/>
									</w:tcBorders>
									<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
									<w:hideMark/>
								</w:tcPr>
								<w:p w:rsidR="00D331AA" w:rsidRPr="00E42F09" w:rsidRDefault="00D331AA" w:rsidP="00E42F09">
									<w:pPr>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:r w:rsidRPr="00E42F09">
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t><?php echo $key; ?></w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="5521" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="000000"/>
									</w:tcBorders>
									<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
									<w:hideMark/>
								</w:tcPr>
								<w:p w:rsidR="00D331AA" w:rsidRPr="00E42F09" w:rsidRDefault="00D331AA" w:rsidP="002454F8">
									<w:pPr>
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:r w:rsidRPr="00E42F09">
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t><?php echo $value['nama_barang']; ?></w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="820" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="000000"/>
									</w:tcBorders>
									<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
									<w:hideMark/>
								</w:tcPr>
								<w:p w:rsidR="00D331AA" w:rsidRPr="00E42F09" w:rsidRDefault="00D331AA" w:rsidP="002454F8">
									<w:pPr>
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:r w:rsidRPr="00E42F09">
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t><?php echo $value['volume']; ?></w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1138" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="000000"/>
									</w:tcBorders>
									<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
									<w:hideMark/>
								</w:tcPr>
								<w:p w:rsidR="00D331AA" w:rsidRPr="00E42F09" w:rsidRDefault="00D331AA" w:rsidP="002454F8">
									<w:pPr>
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:r w:rsidRPr="00E42F09">
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t> </w:t>
									</w:r>
									<w:r>
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t><?php echo $value['harga']; ?></w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="1238" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="000000"/>
									</w:tcBorders>
									<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
									<w:hideMark/>
								</w:tcPr>
								<w:p w:rsidR="00D331AA" w:rsidRPr="00E42F09" w:rsidRDefault="00D331AA" w:rsidP="002454F8">
									<w:pPr>
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t><?php echo $value['jumlah_harga']; ?></w:t>
									</w:r>
								</w:p>
							</w:tc>
							<w:tc>
								<w:tcPr>
									<w:tcW w:w="7152" w:type="dxa"/>
									<w:tcBorders>
										<w:top w:val="nil"/>
										<w:left w:val="nil"/>
										<w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/>
										<w:right w:val="single" w:sz="4" w:space="0" w:color="000000"/>
									</w:tcBorders>
								</w:tcPr>
								<w:p w:rsidR="00D331AA" w:rsidRPr="00E42F09" w:rsidRDefault="00D331AA" w:rsidP="002454F8">
									<w:pPr>
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
									</w:pPr>
									<w:r>
										<w:rPr>
											<w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/>
											<w:sz w:val="18"/>
											<w:szCs w:val="18"/>
											<w:lang w:val="en-US"/>
										</w:rPr>
										<w:t><?php echo $value['spesifikasi']; ?></w:t>
									</w:r>
								</w:p>
							</w:tc>
						</w:tr>
						<?php }  ?>
					</w:tbl>
					<w:p w:rsidR="005C061A" w:rsidRDefault="005C061A" w:rsidP="004434CA">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="1985"/>
							</w:tabs>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
						</w:pPr>
					</w:p>
					<w:p w:rsidR="00875476" w:rsidRDefault="00875476" w:rsidP="00875476">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="1985"/>
							</w:tabs>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t>NB :</w:t>
						</w:r>
					</w:p>
					<w:p w:rsidR="00875476" w:rsidRPr="00474CE0" w:rsidRDefault="00856FEC" w:rsidP="00474CE0">
						<w:pPr>
							<w:pStyle w:val="ListParagraph"/>
							<w:numPr>
								<w:ilvl w:val="0"/>
								<w:numId w:val="19"/>
							</w:numPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="1985"/>
							</w:tabs>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t>Harga sudah termasuk pajak</w:t>
						</w:r>
					</w:p>
					<w:p w:rsidR="001C2357" w:rsidRPr="00AB5566" w:rsidRDefault="000A0482" w:rsidP="00474CE0">
						<w:pPr>
							<w:pStyle w:val="ListParagraph"/>
							<w:numPr>
								<w:ilvl w:val="0"/>
								<w:numId w:val="19"/>
							</w:numPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="1985"/>
							</w:tabs>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="en-US"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t>M</w:t>
						</w:r>
						<w:r w:rsidR="008B19D4">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t>encamtumkan spesifikasi secara l</w:t>
						</w:r>
						<w:r w:rsidR="00875476" w:rsidRPr="00474CE0">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t>engkap</w:t>
						</w:r>
					</w:p>
					<w:p w:rsidR="00AB5566" w:rsidRPr="008D2091" w:rsidRDefault="00AB5566" w:rsidP="00474CE0">
						<w:pPr>
							<w:pStyle w:val="ListParagraph"/>
							<w:numPr>
								<w:ilvl w:val="0"/>
								<w:numId w:val="19"/>
							</w:numPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="1985"/>
							</w:tabs>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="en-US"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="en-US"/>
							</w:rPr>
							<w:t>Seluruh buku menggunakan tahun penerbitan t</w:t>
						</w:r>
						<w:r w:rsidR="00F70D5D">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="en-US"/>
							</w:rPr>
							<w:t>erakhir</w:t>
						</w:r>
					</w:p>
					<w:p w:rsidR="008D2091" w:rsidRPr="00474CE0" w:rsidRDefault="008D2091" w:rsidP="00474CE0">
						<w:pPr>
							<w:pStyle w:val="ListParagraph"/>
							<w:numPr>
								<w:ilvl w:val="0"/>
								<w:numId w:val="19"/>
							</w:numPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="1985"/>
							</w:tabs>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="en-US"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t xml:space="preserve">Melampirkan surat pernyataan kesediaan barang </w:t>
						</w:r>
						<w:r w:rsidR="00515BD4">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t>(</w:t>
						</w:r>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t>bermaterai</w:t>
						</w:r>
						<w:r w:rsidR="00515BD4">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t>)</w:t>
						</w:r>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="22"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t xml:space="preserve">
							</w:t>
						</w:r>
					</w:p>
					<w:p w:rsidR="00D87AB8" w:rsidRDefault="00D87AB8" w:rsidP="00F91D2A">
						<w:pPr>
							<w:spacing w:before="120"/>
							<w:ind w:left="5040" w:firstLine="720"/>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
						</w:pPr>
					</w:p>
					<w:p w:rsidR="00D87AB8" w:rsidRDefault="00D87AB8" w:rsidP="00F91D2A">
						<w:pPr>
							<w:spacing w:before="120"/>
							<w:ind w:left="5040" w:firstLine="720"/>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
						</w:pPr>
					</w:p>
					<w:p w:rsidR="00F91D2A" w:rsidRPr="006D5A96" w:rsidRDefault="005D009F" w:rsidP="00791790">
						<w:pPr>
							<w:spacing w:before="120"/>
							<w:ind w:left="10800" w:firstLine="720"/>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t>Pejabat Pembuat Komitmen</w:t>
						</w:r>
						<w:r w:rsidR="00F91D2A" w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t>,</w:t>
						</w:r>
					</w:p>
					<w:p w:rsidR="00F91D2A" w:rsidRDefault="00F91D2A" w:rsidP="00F91D2A">
						<w:pPr>
							<w:spacing w:before="120"/>
							<w:ind w:left="5040" w:firstLine="720"/>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
						</w:pPr>
					</w:p>
					<w:p w:rsidR="00D87AB8" w:rsidRPr="006D5A96" w:rsidRDefault="00D87AB8" w:rsidP="00F91D2A">
						<w:pPr>
							<w:spacing w:before="120"/>
							<w:ind w:left="5040" w:firstLine="720"/>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
						</w:pPr>
					</w:p>
					<w:p w:rsidR="00F91D2A" w:rsidRDefault="00F91D2A" w:rsidP="00F91D2A">
						<w:pPr>
							<w:jc w:val="right"/>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi" w:cstheme="minorHAnsi"/>
								<w:lang w:val="en-US"/>
							</w:rPr>
						</w:pPr>
					</w:p>
					<w:p w:rsidR="008050AA" w:rsidRDefault="00E03050" w:rsidP="00791790">
						<w:pPr>
							<w:ind w:left="10800" w:firstLine="720"/>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi" w:cstheme="minorHAnsi"/>
								<w:color w:val="000000" w:themeColor="text1"/>
								<w:lang w:val="pt-BR"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi" w:cstheme="minorHAnsi"/>
								<w:color w:val="000000" w:themeColor="text1"/>
								<w:lang w:val="pt-BR"/>
							</w:rPr>
							<w:t><?php echo $ppk_profil[0]['ppk_name']; ?></w:t>
						</w:r>
					</w:p>
					<w:p w:rsidR="00024264" w:rsidRPr="006D5A96" w:rsidRDefault="008050AA" w:rsidP="00E03050">
						<w:pPr>
							<w:ind w:left="10800" w:firstLine="720"/>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
							</w:rPr>
						</w:pPr>
						<w:bookmarkStart w:id="0" w:name="_GoBack"/>
						<w:bookmarkEnd w:id="0"/>
						<w:r w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t>NIP</w:t>
						</w:r>
						<w:r>
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:lang w:val="en-US"/>
							</w:rPr>
							<w:t>.</w:t>
						</w:r>
						<w:r w:rsidRPr="006D5A96">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi"/>
								<w:lang w:val="id-ID"/>
							</w:rPr>
							<w:t xml:space="preserve">
							</w:t>
						</w:r>
						<w:r w:rsidR="00E03050">
							<w:rPr>
								<w:rFonts w:asciiTheme="majorHAnsi" w:hAnsiTheme="majorHAnsi" w:cstheme="minorHAnsi"/>
								<w:color w:val="000000" w:themeColor="text1"/>
								<w:lang w:val="sv-SE"/>
							</w:rPr>
							<w:t><?php echo $ppk_profil[0]['nip']; ?></w:t>
						</w:r>
					</w:p>
					<w:sectPr w:rsidR="00024264" w:rsidRPr="006D5A96" w:rsidSect="00791790">
						<w:footerReference w:type="default" r:id="rId7"/>
						<w:pgSz w:w="18722" w:h="12242" w:orient="landscape" w:code="258"/>
						<w:pgMar w:top="1327" w:right="1440" w:bottom="1440" w:left="1440" w:header="720" w:footer="720" w:gutter="0"/>
						<w:cols w:space="720"/>
						<w:docGrid w:linePitch="360"/>
					</w:sectPr>
				</w:body>
			</w:document>
		</pkg:xmlData>
	</pkg:part>
	<pkg:part pkg:name="/word/footnotes.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.footnotes+xml">
		<pkg:xmlData>
			<w:footnotes mc:Ignorable="w14 w15 wp14" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape">
				<w:footnote w:type="separator" w:id="-1">
					<w:p w:rsidR="00F45040" w:rsidRDefault="00F45040" w:rsidP="00E42F09">
						<w:r>
							<w:separator/>
						</w:r>
					</w:p>
				</w:footnote>
				<w:footnote w:type="continuationSeparator" w:id="0">
					<w:p w:rsidR="00F45040" w:rsidRDefault="00F45040" w:rsidP="00E42F09">
						<w:r>
							<w:continuationSeparator/>
						</w:r>
					</w:p>
				</w:footnote>
			</w:footnotes>
		</pkg:xmlData>
	</pkg:part>
	<pkg:part pkg:name="/word/endnotes.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.endnotes+xml">
		<pkg:xmlData>
			<w:endnotes mc:Ignorable="w14 w15 wp14" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape">
				<w:endnote w:type="separator" w:id="-1">
					<w:p w:rsidR="00F45040" w:rsidRDefault="00F45040" w:rsidP="00E42F09">
						<w:r>
							<w:separator/>
						</w:r>
					</w:p>
				</w:endnote>
				<w:endnote w:type="continuationSeparator" w:id="0">
					<w:p w:rsidR="00F45040" w:rsidRDefault="00F45040" w:rsidP="00E42F09">
						<w:r>
							<w:continuationSeparator/>
						</w:r>
					</w:p>
				</w:endnote>
			</w:endnotes>
		</pkg:xmlData>
	</pkg:part>
	<pkg:part pkg:name="/word/footer1.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.footer+xml">
		<pkg:xmlData>
			<w:ftr mc:Ignorable="w14 w15 wp14" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape">
				<w:sdt>
					<w:sdtPr>
						<w:id w:val="249474747"/>
						<w:docPartObj>
							<w:docPartGallery w:val="Page Numbers (Bottom of Page)"/>
							<w:docPartUnique/>
						</w:docPartObj>
					</w:sdtPr>
					<w:sdtEndPr>
						<w:rPr>
							<w:sz w:val="16"/>
							<w:szCs w:val="16"/>
						</w:rPr>
					</w:sdtEndPr>
					<w:sdtContent>
						<w:sdt>
							<w:sdtPr>
								<w:rPr>
									<w:sz w:val="16"/>
									<w:szCs w:val="16"/>
								</w:rPr>
								<w:id w:val="1728636285"/>
								<w:docPartObj>
									<w:docPartGallery w:val="Page Numbers (Top of Page)"/>
									<w:docPartUnique/>
								</w:docPartObj>
							</w:sdtPr>
							<w:sdtEndPr/>
							<w:sdtContent>
								<w:p w:rsidR="00E42F09" w:rsidRPr="00E42F09" w:rsidRDefault="00E42F09">
									<w:pPr>
										<w:pStyle w:val="Footer"/>
										<w:jc w:val="center"/>
										<w:rPr>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
									</w:pPr>
									<w:r w:rsidRPr="00E42F09">
										<w:rPr>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
										<w:t xml:space="preserve">Page </w:t>
									</w:r>
									<w:r w:rsidRPr="00E42F09">
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
										<w:fldChar w:fldCharType="begin"/>
									</w:r>
									<w:r w:rsidRPr="00E42F09">
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
										<w:instrText xml:space="preserve"> PAGE </w:instrText>
									</w:r>
									<w:r w:rsidRPr="00E42F09">
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
										<w:fldChar w:fldCharType="separate"/>
									</w:r>
									<w:r w:rsidR="00813B79">
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:noProof/>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
									<w:r w:rsidRPr="00E42F09">
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
										<w:fldChar w:fldCharType="end"/>
									</w:r>
									<w:r w:rsidRPr="00E42F09">
										<w:rPr>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
										<w:t xml:space="preserve"> of </w:t>
									</w:r>
									<w:r w:rsidRPr="00E42F09">
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
										<w:fldChar w:fldCharType="begin"/>
									</w:r>
									<w:r w:rsidRPr="00E42F09">
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
										<w:instrText xml:space="preserve"> NUMPAGES  </w:instrText>
									</w:r>
									<w:r w:rsidRPr="00E42F09">
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
										<w:fldChar w:fldCharType="separate"/>
									</w:r>
									<w:r w:rsidR="00813B79">
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:noProof/>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
										<w:t>1</w:t>
									</w:r>
									<w:r w:rsidRPr="00E42F09">
										<w:rPr>
											<w:b/>
											<w:bCs/>
											<w:sz w:val="16"/>
											<w:szCs w:val="16"/>
										</w:rPr>
										<w:fldChar w:fldCharType="end"/>
									</w:r>
								</w:p>
							</w:sdtContent>
						</w:sdt>
					</w:sdtContent>
				</w:sdt>
				<w:p w:rsidR="00E42F09" w:rsidRDefault="00E42F09">
					<w:pPr>
						<w:pStyle w:val="Footer"/>
					</w:pPr>
				</w:p>
			</w:ftr>
		</pkg:xmlData>
	</pkg:part>
	<pkg:part pkg:name="/word/theme/theme1.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.theme+xml">
		<pkg:xmlData>
			<a:theme name="Office Theme" xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">
				<a:themeElements>
					<a:clrScheme name="Office">
						<a:dk1>
							<a:sysClr val="windowText" lastClr="000000"/>
						</a:dk1>
						<a:lt1>
							<a:sysClr val="window" lastClr="FFFFFF"/>
						</a:lt1>
						<a:dk2>
							<a:srgbClr val="1F497D"/>
						</a:dk2>
						<a:lt2>
							<a:srgbClr val="EEECE1"/>
						</a:lt2>
						<a:accent1>
							<a:srgbClr val="4F81BD"/>
						</a:accent1>
						<a:accent2>
							<a:srgbClr val="C0504D"/>
						</a:accent2>
						<a:accent3>
							<a:srgbClr val="9BBB59"/>
						</a:accent3>
						<a:accent4>
							<a:srgbClr val="8064A2"/>
						</a:accent4>
						<a:accent5>
							<a:srgbClr val="4BACC6"/>
						</a:accent5>
						<a:accent6>
							<a:srgbClr val="F79646"/>
						</a:accent6>
						<a:hlink>
							<a:srgbClr val="0000FF"/>
						</a:hlink>
						<a:folHlink>
							<a:srgbClr val="800080"/>
						</a:folHlink>
					</a:clrScheme>
					<a:fontScheme name="Office">
						<a:majorFont>
							<a:latin typeface="Cambria"/>
							<a:ea typeface=""/>
							<a:cs typeface=""/>
							<a:font script="Jpan" typeface="ＭＳ ゴシック"/>
							<a:font script="Hang" typeface="맑은 고딕"/>
							<a:font script="Hans" typeface="宋体"/>
							<a:font script="Hant" typeface="新細明體"/>
							<a:font script="Arab" typeface="Times New Roman"/>
							<a:font script="Hebr" typeface="Times New Roman"/>
							<a:font script="Thai" typeface="Angsana New"/>
							<a:font script="Ethi" typeface="Nyala"/>
							<a:font script="Beng" typeface="Vrinda"/>
							<a:font script="Gujr" typeface="Shruti"/>
							<a:font script="Khmr" typeface="MoolBoran"/>
							<a:font script="Knda" typeface="Tunga"/>
							<a:font script="Guru" typeface="Raavi"/>
							<a:font script="Cans" typeface="Euphemia"/>
							<a:font script="Cher" typeface="Plantagenet Cherokee"/>
							<a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
							<a:font script="Tibt" typeface="Microsoft Himalaya"/>
							<a:font script="Thaa" typeface="MV Boli"/>
							<a:font script="Deva" typeface="Mangal"/>
							<a:font script="Telu" typeface="Gautami"/>
							<a:font script="Taml" typeface="Latha"/>
							<a:font script="Syrc" typeface="Estrangelo Edessa"/>
							<a:font script="Orya" typeface="Kalinga"/>
							<a:font script="Mlym" typeface="Kartika"/>
							<a:font script="Laoo" typeface="DokChampa"/>
							<a:font script="Sinh" typeface="Iskoola Pota"/>
							<a:font script="Mong" typeface="Mongolian Baiti"/>
							<a:font script="Viet" typeface="Times New Roman"/>
							<a:font script="Uigh" typeface="Microsoft Uighur"/>
							<a:font script="Geor" typeface="Sylfaen"/>
						</a:majorFont>
						<a:minorFont>
							<a:latin typeface="Calibri"/>
							<a:ea typeface=""/>
							<a:cs typeface=""/>
							<a:font script="Jpan" typeface="ＭＳ 明朝"/>
							<a:font script="Hang" typeface="맑은 고딕"/>
							<a:font script="Hans" typeface="宋体"/>
							<a:font script="Hant" typeface="新細明體"/>
							<a:font script="Arab" typeface="Arial"/>
							<a:font script="Hebr" typeface="Arial"/>
							<a:font script="Thai" typeface="Cordia New"/>
							<a:font script="Ethi" typeface="Nyala"/>
							<a:font script="Beng" typeface="Vrinda"/>
							<a:font script="Gujr" typeface="Shruti"/>
							<a:font script="Khmr" typeface="DaunPenh"/>
							<a:font script="Knda" typeface="Tunga"/>
							<a:font script="Guru" typeface="Raavi"/>
							<a:font script="Cans" typeface="Euphemia"/>
							<a:font script="Cher" typeface="Plantagenet Cherokee"/>
							<a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
							<a:font script="Tibt" typeface="Microsoft Himalaya"/>
							<a:font script="Thaa" typeface="MV Boli"/>
							<a:font script="Deva" typeface="Mangal"/>
							<a:font script="Telu" typeface="Gautami"/>
							<a:font script="Taml" typeface="Latha"/>
							<a:font script="Syrc" typeface="Estrangelo Edessa"/>
							<a:font script="Orya" typeface="Kalinga"/>
							<a:font script="Mlym" typeface="Kartika"/>
							<a:font script="Laoo" typeface="DokChampa"/>
							<a:font script="Sinh" typeface="Iskoola Pota"/>
							<a:font script="Mong" typeface="Mongolian Baiti"/>
							<a:font script="Viet" typeface="Arial"/>
							<a:font script="Uigh" typeface="Microsoft Uighur"/>
							<a:font script="Geor" typeface="Sylfaen"/>
						</a:minorFont>
					</a:fontScheme>
					<a:fmtScheme name="Office">
						<a:fillStyleLst>
							<a:solidFill>
								<a:schemeClr val="phClr"/>
							</a:solidFill>
							<a:gradFill rotWithShape="1">
								<a:gsLst>
									<a:gs pos="0">
										<a:schemeClr val="phClr">
											<a:tint val="50000"/>
											<a:satMod val="300000"/>
										</a:schemeClr>
									</a:gs>
									<a:gs pos="35000">
										<a:schemeClr val="phClr">
											<a:tint val="37000"/>
											<a:satMod val="300000"/>
										</a:schemeClr>
									</a:gs>
									<a:gs pos="100000">
										<a:schemeClr val="phClr">
											<a:tint val="15000"/>
											<a:satMod val="350000"/>
										</a:schemeClr>
									</a:gs>
								</a:gsLst>
								<a:lin ang="16200000" scaled="1"/>
							</a:gradFill>
							<a:gradFill rotWithShape="1">
								<a:gsLst>
									<a:gs pos="0">
										<a:schemeClr val="phClr">
											<a:shade val="51000"/>
											<a:satMod val="130000"/>
										</a:schemeClr>
									</a:gs>
									<a:gs pos="80000">
										<a:schemeClr val="phClr">
											<a:shade val="93000"/>
											<a:satMod val="130000"/>
										</a:schemeClr>
									</a:gs>
									<a:gs pos="100000">
										<a:schemeClr val="phClr">
											<a:shade val="94000"/>
											<a:satMod val="135000"/>
										</a:schemeClr>
									</a:gs>
								</a:gsLst>
								<a:lin ang="16200000" scaled="0"/>
							</a:gradFill>
						</a:fillStyleLst>
						<a:lnStyleLst>
							<a:ln w="9525" cap="flat" cmpd="sng" algn="ctr">
								<a:solidFill>
									<a:schemeClr val="phClr">
										<a:shade val="95000"/>
										<a:satMod val="105000"/>
									</a:schemeClr>
								</a:solidFill>
								<a:prstDash val="solid"/>
							</a:ln>
							<a:ln w="25400" cap="flat" cmpd="sng" algn="ctr">
								<a:solidFill>
									<a:schemeClr val="phClr"/>
								</a:solidFill>
								<a:prstDash val="solid"/>
							</a:ln>
							<a:ln w="38100" cap="flat" cmpd="sng" algn="ctr">
								<a:solidFill>
									<a:schemeClr val="phClr"/>
								</a:solidFill>
								<a:prstDash val="solid"/>
							</a:ln>
						</a:lnStyleLst>
						<a:effectStyleLst>
							<a:effectStyle>
								<a:effectLst>
									<a:outerShdw blurRad="40000" dist="20000" dir="5400000" rotWithShape="0">
										<a:srgbClr val="000000">
											<a:alpha val="38000"/>
										</a:srgbClr>
									</a:outerShdw>
								</a:effectLst>
							</a:effectStyle>
							<a:effectStyle>
								<a:effectLst>
									<a:outerShdw blurRad="40000" dist="23000" dir="5400000" rotWithShape="0">
										<a:srgbClr val="000000">
											<a:alpha val="35000"/>
										</a:srgbClr>
									</a:outerShdw>
								</a:effectLst>
							</a:effectStyle>
							<a:effectStyle>
								<a:effectLst>
									<a:outerShdw blurRad="40000" dist="23000" dir="5400000" rotWithShape="0">
										<a:srgbClr val="000000">
											<a:alpha val="35000"/>
										</a:srgbClr>
									</a:outerShdw>
								</a:effectLst>
								<a:scene3d>
									<a:camera prst="orthographicFront">
										<a:rot lat="0" lon="0" rev="0"/>
									</a:camera>
									<a:lightRig rig="threePt" dir="t">
										<a:rot lat="0" lon="0" rev="1200000"/>
									</a:lightRig>
								</a:scene3d>
								<a:sp3d>
									<a:bevelT w="63500" h="25400"/>
								</a:sp3d>
							</a:effectStyle>
						</a:effectStyleLst>
						<a:bgFillStyleLst>
							<a:solidFill>
								<a:schemeClr val="phClr"/>
							</a:solidFill>
							<a:gradFill rotWithShape="1">
								<a:gsLst>
									<a:gs pos="0">
										<a:schemeClr val="phClr">
											<a:tint val="40000"/>
											<a:satMod val="350000"/>
										</a:schemeClr>
									</a:gs>
									<a:gs pos="40000">
										<a:schemeClr val="phClr">
											<a:tint val="45000"/>
											<a:shade val="99000"/>
											<a:satMod val="350000"/>
										</a:schemeClr>
									</a:gs>
									<a:gs pos="100000">
										<a:schemeClr val="phClr">
											<a:shade val="20000"/>
											<a:satMod val="255000"/>
										</a:schemeClr>
									</a:gs>
								</a:gsLst>
								<a:path path="circle">
									<a:fillToRect l="50000" t="-80000" r="50000" b="180000"/>
								</a:path>
							</a:gradFill>
							<a:gradFill rotWithShape="1">
								<a:gsLst>
									<a:gs pos="0">
										<a:schemeClr val="phClr">
											<a:tint val="80000"/>
											<a:satMod val="300000"/>
										</a:schemeClr>
									</a:gs>
									<a:gs pos="100000">
										<a:schemeClr val="phClr">
											<a:shade val="30000"/>
											<a:satMod val="200000"/>
										</a:schemeClr>
									</a:gs>
								</a:gsLst>
								<a:path path="circle">
									<a:fillToRect l="50000" t="50000" r="50000" b="50000"/>
								</a:path>
							</a:gradFill>
						</a:bgFillStyleLst>
					</a:fmtScheme>
				</a:themeElements>
				<a:objectDefaults/>
				<a:extraClrSchemeLst/>
			</a:theme>
		</pkg:xmlData>
	</pkg:part>
	<pkg:part pkg:name="/word/settings.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml">
		<pkg:xmlData>
			<w:settings mc:Ignorable="w14 w15" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:sl="http://schemas.openxmlformats.org/schemaLibrary/2006/main">
				<w:zoom w:percent="100"/>
				<w:defaultTabStop w:val="720"/>
				<w:characterSpacingControl w:val="doNotCompress"/>
				<w:footnotePr>
					<w:footnote w:id="-1"/>
					<w:footnote w:id="0"/>
				</w:footnotePr>
				<w:endnotePr>
					<w:endnote w:id="-1"/>
					<w:endnote w:id="0"/>
				</w:endnotePr>
				<w:compat>
					<w:compatSetting w:name="compatibilityMode" w:uri="http://schemas.microsoft.com/office/word" w:val="12"/>
				</w:compat>
				<w:rsids>
					<w:rsidRoot w:val="00F91D2A"/>
					<w:rsid w:val="00000ACA"/>
					<w:rsid w:val="00007714"/>
					<w:rsid w:val="00024264"/>
					<w:rsid w:val="00070DCA"/>
					<w:rsid w:val="000A0482"/>
					<w:rsid w:val="000C7157"/>
					<w:rsid w:val="000E0F52"/>
					<w:rsid w:val="00116C46"/>
					<w:rsid w:val="00117592"/>
					<w:rsid w:val="00132759"/>
					<w:rsid w:val="001358D5"/>
					<w:rsid w:val="00166C6D"/>
					<w:rsid w:val="00196D57"/>
					<w:rsid w:val="001C2357"/>
					<w:rsid w:val="001C5FE6"/>
					<w:rsid w:val="001D698D"/>
					<w:rsid w:val="001D73C5"/>
					<w:rsid w:val="00202700"/>
					<w:rsid w:val="0020691D"/>
					<w:rsid w:val="00234D53"/>
					<w:rsid w:val="002454F8"/>
					<w:rsid w:val="002669F2"/>
					<w:rsid w:val="00270425"/>
					<w:rsid w:val="00283FB9"/>
					<w:rsid w:val="00294A05"/>
					<w:rsid w:val="00294E51"/>
					<w:rsid w:val="002B0D47"/>
					<w:rsid w:val="002B5E59"/>
					<w:rsid w:val="002D09B7"/>
					<w:rsid w:val="002E3A8C"/>
					<w:rsid w:val="00330F79"/>
					<w:rsid w:val="00350FB3"/>
					<w:rsid w:val="0035704E"/>
					<w:rsid w:val="00361445"/>
					<w:rsid w:val="00370984"/>
					<w:rsid w:val="003717F9"/>
					<w:rsid w:val="003A5990"/>
					<w:rsid w:val="003C4CF9"/>
					<w:rsid w:val="003E3E02"/>
					<w:rsid w:val="00402049"/>
					<w:rsid w:val="00417895"/>
					<w:rsid w:val="004203F1"/>
					<w:rsid w:val="0043456B"/>
					<w:rsid w:val="004434CA"/>
					<w:rsid w:val="00463F46"/>
					<w:rsid w:val="00472125"/>
					<w:rsid w:val="00474846"/>
					<w:rsid w:val="00474CE0"/>
					<w:rsid w:val="00495653"/>
					<w:rsid w:val="004A6376"/>
					<w:rsid w:val="004E3CB5"/>
					<w:rsid w:val="004F3BE7"/>
					<w:rsid w:val="00515BD4"/>
					<w:rsid w:val="0052253D"/>
					<w:rsid w:val="00523B71"/>
					<w:rsid w:val="005369AC"/>
					<w:rsid w:val="00560FB1"/>
					<w:rsid w:val="00575468"/>
					<w:rsid w:val="00577667"/>
					<w:rsid w:val="005B4985"/>
					<w:rsid w:val="005C061A"/>
					<w:rsid w:val="005D009F"/>
					<w:rsid w:val="005F67BE"/>
					<w:rsid w:val="0061372D"/>
					<w:rsid w:val="00616320"/>
					<w:rsid w:val="006444BE"/>
					<w:rsid w:val="00667619"/>
					<w:rsid w:val="006901E4"/>
					<w:rsid w:val="00697D34"/>
					<w:rsid w:val="006A3CB0"/>
					<w:rsid w:val="006C3ADE"/>
					<w:rsid w:val="006D5A96"/>
					<w:rsid w:val="00711C84"/>
					<w:rsid w:val="00735BC7"/>
					<w:rsid w:val="00744075"/>
					<w:rsid w:val="0075600B"/>
					<w:rsid w:val="00775799"/>
					<w:rsid w:val="007759EE"/>
					<w:rsid w:val="00777E2E"/>
					<w:rsid w:val="00791790"/>
					<w:rsid w:val="007B344B"/>
					<w:rsid w:val="007C4A0B"/>
					<w:rsid w:val="007C704D"/>
					<w:rsid w:val="007D06E2"/>
					<w:rsid w:val="007D5E59"/>
					<w:rsid w:val="007E1A1C"/>
					<w:rsid w:val="007F2A58"/>
					<w:rsid w:val="008050AA"/>
					<w:rsid w:val="0081153E"/>
					<w:rsid w:val="00813B79"/>
					<w:rsid w:val="00852129"/>
					<w:rsid w:val="00856FEC"/>
					<w:rsid w:val="00873A0A"/>
					<w:rsid w:val="00875476"/>
					<w:rsid w:val="00876ACA"/>
					<w:rsid w:val="0088496F"/>
					<w:rsid w:val="008851C2"/>
					<w:rsid w:val="008B19D4"/>
					<w:rsid w:val="008D2091"/>
					<w:rsid w:val="008E18C9"/>
					<w:rsid w:val="008F1501"/>
					<w:rsid w:val="00924ECB"/>
					<w:rsid w:val="0092680B"/>
					<w:rsid w:val="00926A0D"/>
					<w:rsid w:val="00964034"/>
					<w:rsid w:val="009831CC"/>
					<w:rsid w:val="0099321D"/>
					<w:rsid w:val="009A6FFB"/>
					<w:rsid w:val="009C026D"/>
					<w:rsid w:val="009C0716"/>
					<w:rsid w:val="00A0037A"/>
					<w:rsid w:val="00A07361"/>
					<w:rsid w:val="00A1062A"/>
					<w:rsid w:val="00A359BD"/>
					<w:rsid w:val="00A50579"/>
					<w:rsid w:val="00A74295"/>
					<w:rsid w:val="00A93319"/>
					<w:rsid w:val="00A9781E"/>
					<w:rsid w:val="00AA1357"/>
					<w:rsid w:val="00AA197E"/>
					<w:rsid w:val="00AA36E1"/>
					<w:rsid w:val="00AB1935"/>
					<w:rsid w:val="00AB5566"/>
					<w:rsid w:val="00AC2ADB"/>
					<w:rsid w:val="00AC33A5"/>
					<w:rsid w:val="00AD1DF3"/>
					<w:rsid w:val="00AF076F"/>
					<w:rsid w:val="00AF2400"/>
					<w:rsid w:val="00B23B42"/>
					<w:rsid w:val="00B53BC9"/>
					<w:rsid w:val="00B8040E"/>
					<w:rsid w:val="00B80B11"/>
					<w:rsid w:val="00B83A82"/>
					<w:rsid w:val="00B85DA7"/>
					<w:rsid w:val="00B90779"/>
					<w:rsid w:val="00BB0A6D"/>
					<w:rsid w:val="00BC4EBB"/>
					<w:rsid w:val="00BD1C66"/>
					<w:rsid w:val="00BE44FF"/>
					<w:rsid w:val="00C0561D"/>
					<w:rsid w:val="00C109DB"/>
					<w:rsid w:val="00C1297C"/>
					<w:rsid w:val="00C3261C"/>
					<w:rsid w:val="00C603B2"/>
					<w:rsid w:val="00C63267"/>
					<w:rsid w:val="00C736DD"/>
					<w:rsid w:val="00C75ED0"/>
					<w:rsid w:val="00C90EBE"/>
					<w:rsid w:val="00D02771"/>
					<w:rsid w:val="00D331AA"/>
					<w:rsid w:val="00D801C0"/>
					<w:rsid w:val="00D87330"/>
					<w:rsid w:val="00D87AB8"/>
					<w:rsid w:val="00D91978"/>
					<w:rsid w:val="00D95590"/>
					<w:rsid w:val="00DB77BE"/>
					<w:rsid w:val="00DE2028"/>
					<w:rsid w:val="00DF7645"/>
					<w:rsid w:val="00E03050"/>
					<w:rsid w:val="00E14AE6"/>
					<w:rsid w:val="00E2157A"/>
					<w:rsid w:val="00E2451C"/>
					<w:rsid w:val="00E314D7"/>
					<w:rsid w:val="00E42F09"/>
					<w:rsid w:val="00E837F2"/>
					<w:rsid w:val="00E907EF"/>
					<w:rsid w:val="00EA6625"/>
					<w:rsid w:val="00ED50A8"/>
					<w:rsid w:val="00ED734A"/>
					<w:rsid w:val="00ED7865"/>
					<w:rsid w:val="00EE243C"/>
					<w:rsid w:val="00F05CC6"/>
					<w:rsid w:val="00F4300D"/>
					<w:rsid w:val="00F45040"/>
					<w:rsid w:val="00F46A33"/>
					<w:rsid w:val="00F55E99"/>
					<w:rsid w:val="00F57E0E"/>
					<w:rsid w:val="00F61184"/>
					<w:rsid w:val="00F66BC7"/>
					<w:rsid w:val="00F700FD"/>
					<w:rsid w:val="00F70D5D"/>
					<w:rsid w:val="00F8677F"/>
					<w:rsid w:val="00F91D2A"/>
					<w:rsid w:val="00F94E43"/>
					<w:rsid w:val="00F96D2F"/>
					<w:rsid w:val="00FA2859"/>
					<w:rsid w:val="00FA568D"/>
					<w:rsid w:val="00FD42AB"/>
				</w:rsids>
				<m:mathPr>
					<m:mathFont m:val="Cambria Math"/>
					<m:brkBin m:val="before"/>
					<m:brkBinSub m:val="--"/>
					<m:smallFrac/>
					<m:dispDef/>
					<m:lMargin m:val="0"/>
					<m:rMargin m:val="0"/>
					<m:defJc m:val="centerGroup"/>
					<m:wrapIndent m:val="1440"/>
					<m:intLim m:val="subSup"/>
					<m:naryLim m:val="undOvr"/>
				</m:mathPr>
				<w:themeFontLang w:val="en-GB" w:bidi="ar-SA"/>
				<w:clrSchemeMapping w:bg1="light1" w:t1="dark1" w:bg2="light2" w:t2="dark2" w:accent1="accent1" w:accent2="accent2" w:accent3="accent3" w:accent4="accent4" w:accent5="accent5" w:accent6="accent6" w:hyperlink="hyperlink" w:followedHyperlink="followedHyperlink"/>
				<w:shapeDefaults>
					<o:shapedefaults v:ext="edit" spidmax="1026"/>
					<o:shapelayout v:ext="edit">
						<o:idmap v:ext="edit" data="1"/>
					</o:shapelayout>
				</w:shapeDefaults>
				<w:decimalSymbol w:val="."/>
				<w:listSeparator w:val=","/>
				<w15:docId w15:val="{F717FBA7-F0B6-4EBC-920C-387BEDBC221A}"/>
			</w:settings>
		</pkg:xmlData>
	</pkg:part>
	<pkg:part pkg:name="/word/webSettings.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml">
		<pkg:xmlData>
			<w:webSettings mc:Ignorable="w14 w15" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml">
				<w:divs>
					<w:div w:id="55711623">
						<w:bodyDiv w:val="1"/>
						<w:marLeft w:val="0"/>
						<w:marRight w:val="0"/>
						<w:marTop w:val="0"/>
						<w:marBottom w:val="0"/>
						<w:divBdr>
							<w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
							<w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
							<w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
							<w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
						</w:divBdr>
					</w:div>
					<w:div w:id="484709787">
						<w:bodyDiv w:val="1"/>
						<w:marLeft w:val="0"/>
						<w:marRight w:val="0"/>
						<w:marTop w:val="0"/>
						<w:marBottom w:val="0"/>
						<w:divBdr>
							<w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
							<w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
							<w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
							<w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
						</w:divBdr>
					</w:div>
					<w:div w:id="1010257158">
						<w:bodyDiv w:val="1"/>
						<w:marLeft w:val="0"/>
						<w:marRight w:val="0"/>
						<w:marTop w:val="0"/>
						<w:marBottom w:val="0"/>
						<w:divBdr>
							<w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
							<w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
							<w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
							<w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
						</w:divBdr>
					</w:div>
					<w:div w:id="1085882544">
						<w:bodyDiv w:val="1"/>
						<w:marLeft w:val="0"/>
						<w:marRight w:val="0"/>
						<w:marTop w:val="0"/>
						<w:marBottom w:val="0"/>
						<w:divBdr>
							<w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
							<w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
							<w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
							<w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
						</w:divBdr>
					</w:div>
					<w:div w:id="1635745445">
						<w:bodyDiv w:val="1"/>
						<w:marLeft w:val="0"/>
						<w:marRight w:val="0"/>
						<w:marTop w:val="0"/>
						<w:marBottom w:val="0"/>
						<w:divBdr>
							<w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
							<w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
							<w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
							<w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
						</w:divBdr>
					</w:div>
				</w:divs>
				<w:optimizeForBrowser/>
				<w:relyOnVML/>
				<w:allowPNG/>
			</w:webSettings>
		</pkg:xmlData>
	</pkg:part>
	<pkg:part pkg:name="/word/styles.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml">
		<pkg:xmlData>
			<w:styles mc:Ignorable="w14 w15" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml">
				<w:docDefaults>
					<w:rPrDefault>
						<w:rPr>
							<w:rFonts w:asciiTheme="minorHAnsi" w:eastAsiaTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi" w:cstheme="minorBidi"/>
							<w:sz w:val="22"/>
							<w:szCs w:val="22"/>
							<w:lang w:val="en-US" w:eastAsia="en-US" w:bidi="ar-SA"/>
						</w:rPr>
					</w:rPrDefault>
					<w:pPrDefault>
						<w:pPr>
							<w:spacing w:after="200" w:line="276" w:lineRule="auto"/>
						</w:pPr>
					</w:pPrDefault>
				</w:docDefaults>
				<w:latentStyles w:defLockedState="0" w:defUIPriority="99" w:defSemiHidden="0" w:defUnhideWhenUsed="0" w:defQFormat="0" w:count="371">
					<w:lsdException w:name="Normal" w:uiPriority="0" w:qFormat="1"/>
					<w:lsdException w:name="heading 1" w:uiPriority="9" w:qFormat="1"/>
					<w:lsdException w:name="heading 2" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
					<w:lsdException w:name="heading 3" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
					<w:lsdException w:name="heading 4" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
					<w:lsdException w:name="heading 5" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
					<w:lsdException w:name="heading 6" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
					<w:lsdException w:name="heading 7" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
					<w:lsdException w:name="heading 8" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
					<w:lsdException w:name="heading 9" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1" w:qFormat="1"/>
					<w:lsdException w:name="index 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="index 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="index 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="index 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="index 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="index 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="index 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="index 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="index 9" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="toc 1" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="toc 2" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="toc 3" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="toc 4" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="toc 5" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="toc 6" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="toc 7" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="toc 8" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="toc 9" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Normal Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="footnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="annotation text" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="header" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="footer" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="index heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="caption" w:semiHidden="1" w:uiPriority="35" w:unhideWhenUsed="1" w:qFormat="1"/>
					<w:lsdException w:name="table of figures" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="envelope address" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="envelope return" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="footnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="annotation reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="line number" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="page number" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="endnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="endnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="table of authorities" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="macro" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="toa heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List Bullet" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List Number" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List Bullet 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List Bullet 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List Bullet 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List Bullet 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List Number 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List Number 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List Number 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List Number 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Title" w:uiPriority="10" w:qFormat="1"/>
					<w:lsdException w:name="Closing" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Default Paragraph Font" w:semiHidden="1" w:uiPriority="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Body Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Body Text Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List Continue" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List Continue 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List Continue 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List Continue 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="List Continue 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Message Header" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Subtitle" w:uiPriority="11" w:qFormat="1"/>
					<w:lsdException w:name="Salutation" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Date" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Body Text First Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Body Text First Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Note Heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Body Text 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Body Text 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Body Text Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Body Text Indent 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Block Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Hyperlink" w:semiHidden="1" w:uiPriority="0" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="FollowedHyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Strong" w:uiPriority="22" w:qFormat="1"/>
					<w:lsdException w:name="Emphasis" w:uiPriority="20" w:qFormat="1"/>
					<w:lsdException w:name="Document Map" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Plain Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="E-mail Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="HTML Top of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="HTML Bottom of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Normal (Web)" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="HTML Acronym" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="HTML Address" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="HTML Cite" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="HTML Code" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="HTML Definition" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="HTML Keyboard" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="HTML Preformatted" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="HTML Sample" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="HTML Typewriter" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="HTML Variable" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Normal Table" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="annotation subject" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="No List" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Outline List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Outline List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Outline List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Simple 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Simple 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Simple 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Classic 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Classic 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Classic 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Classic 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Colorful 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Colorful 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Colorful 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Columns 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Columns 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Columns 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Columns 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Columns 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Grid 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Grid 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Grid 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Grid 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Grid 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Grid 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Grid 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Grid 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table List 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table List 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table List 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table 3D effects 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table 3D effects 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table 3D effects 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Contemporary" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Elegant" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Professional" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Subtle 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Subtle 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Web 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Web 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Web 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Balloon Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Table Grid" w:uiPriority="59"/>
					<w:lsdException w:name="Table Theme" w:semiHidden="1" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="Placeholder Text" w:semiHidden="1"/>
					<w:lsdException w:name="No Spacing" w:uiPriority="1" w:qFormat="1"/>
					<w:lsdException w:name="Light Shading" w:uiPriority="60"/>
					<w:lsdException w:name="Light List" w:uiPriority="61"/>
					<w:lsdException w:name="Light Grid" w:uiPriority="62"/>
					<w:lsdException w:name="Medium Shading 1" w:uiPriority="63"/>
					<w:lsdException w:name="Medium Shading 2" w:uiPriority="64"/>
					<w:lsdException w:name="Medium List 1" w:uiPriority="65"/>
					<w:lsdException w:name="Medium List 2" w:uiPriority="66"/>
					<w:lsdException w:name="Medium Grid 1" w:uiPriority="67"/>
					<w:lsdException w:name="Medium Grid 2" w:uiPriority="68"/>
					<w:lsdException w:name="Medium Grid 3" w:uiPriority="69"/>
					<w:lsdException w:name="Dark List" w:uiPriority="70"/>
					<w:lsdException w:name="Colorful Shading" w:uiPriority="71"/>
					<w:lsdException w:name="Colorful List" w:uiPriority="72"/>
					<w:lsdException w:name="Colorful Grid" w:uiPriority="73"/>
					<w:lsdException w:name="Light Shading Accent 1" w:uiPriority="60"/>
					<w:lsdException w:name="Light List Accent 1" w:uiPriority="61"/>
					<w:lsdException w:name="Light Grid Accent 1" w:uiPriority="62"/>
					<w:lsdException w:name="Medium Shading 1 Accent 1" w:uiPriority="63"/>
					<w:lsdException w:name="Medium Shading 2 Accent 1" w:uiPriority="64"/>
					<w:lsdException w:name="Medium List 1 Accent 1" w:uiPriority="65"/>
					<w:lsdException w:name="Revision" w:semiHidden="1"/>
					<w:lsdException w:name="List Paragraph" w:uiPriority="34" w:qFormat="1"/>
					<w:lsdException w:name="Quote" w:uiPriority="29" w:qFormat="1"/>
					<w:lsdException w:name="Intense Quote" w:uiPriority="30" w:qFormat="1"/>
					<w:lsdException w:name="Medium List 2 Accent 1" w:uiPriority="66"/>
					<w:lsdException w:name="Medium Grid 1 Accent 1" w:uiPriority="67"/>
					<w:lsdException w:name="Medium Grid 2 Accent 1" w:uiPriority="68"/>
					<w:lsdException w:name="Medium Grid 3 Accent 1" w:uiPriority="69"/>
					<w:lsdException w:name="Dark List Accent 1" w:uiPriority="70"/>
					<w:lsdException w:name="Colorful Shading Accent 1" w:uiPriority="71"/>
					<w:lsdException w:name="Colorful List Accent 1" w:uiPriority="72"/>
					<w:lsdException w:name="Colorful Grid Accent 1" w:uiPriority="73"/>
					<w:lsdException w:name="Light Shading Accent 2" w:uiPriority="60"/>
					<w:lsdException w:name="Light List Accent 2" w:uiPriority="61"/>
					<w:lsdException w:name="Light Grid Accent 2" w:uiPriority="62"/>
					<w:lsdException w:name="Medium Shading 1 Accent 2" w:uiPriority="63"/>
					<w:lsdException w:name="Medium Shading 2 Accent 2" w:uiPriority="64"/>
					<w:lsdException w:name="Medium List 1 Accent 2" w:uiPriority="65"/>
					<w:lsdException w:name="Medium List 2 Accent 2" w:uiPriority="66"/>
					<w:lsdException w:name="Medium Grid 1 Accent 2" w:uiPriority="67"/>
					<w:lsdException w:name="Medium Grid 2 Accent 2" w:uiPriority="68"/>
					<w:lsdException w:name="Medium Grid 3 Accent 2" w:uiPriority="69"/>
					<w:lsdException w:name="Dark List Accent 2" w:uiPriority="70"/>
					<w:lsdException w:name="Colorful Shading Accent 2" w:uiPriority="71"/>
					<w:lsdException w:name="Colorful List Accent 2" w:uiPriority="72"/>
					<w:lsdException w:name="Colorful Grid Accent 2" w:uiPriority="73"/>
					<w:lsdException w:name="Light Shading Accent 3" w:uiPriority="60"/>
					<w:lsdException w:name="Light List Accent 3" w:uiPriority="61"/>
					<w:lsdException w:name="Light Grid Accent 3" w:uiPriority="62"/>
					<w:lsdException w:name="Medium Shading 1 Accent 3" w:uiPriority="63"/>
					<w:lsdException w:name="Medium Shading 2 Accent 3" w:uiPriority="64"/>
					<w:lsdException w:name="Medium List 1 Accent 3" w:uiPriority="65"/>
					<w:lsdException w:name="Medium List 2 Accent 3" w:uiPriority="66"/>
					<w:lsdException w:name="Medium Grid 1 Accent 3" w:uiPriority="67"/>
					<w:lsdException w:name="Medium Grid 2 Accent 3" w:uiPriority="68"/>
					<w:lsdException w:name="Medium Grid 3 Accent 3" w:uiPriority="69"/>
					<w:lsdException w:name="Dark List Accent 3" w:uiPriority="70"/>
					<w:lsdException w:name="Colorful Shading Accent 3" w:uiPriority="71"/>
					<w:lsdException w:name="Colorful List Accent 3" w:uiPriority="72"/>
					<w:lsdException w:name="Colorful Grid Accent 3" w:uiPriority="73"/>
					<w:lsdException w:name="Light Shading Accent 4" w:uiPriority="60"/>
					<w:lsdException w:name="Light List Accent 4" w:uiPriority="61"/>
					<w:lsdException w:name="Light Grid Accent 4" w:uiPriority="62"/>
					<w:lsdException w:name="Medium Shading 1 Accent 4" w:uiPriority="63"/>
					<w:lsdException w:name="Medium Shading 2 Accent 4" w:uiPriority="64"/>
					<w:lsdException w:name="Medium List 1 Accent 4" w:uiPriority="65"/>
					<w:lsdException w:name="Medium List 2 Accent 4" w:uiPriority="66"/>
					<w:lsdException w:name="Medium Grid 1 Accent 4" w:uiPriority="67"/>
					<w:lsdException w:name="Medium Grid 2 Accent 4" w:uiPriority="68"/>
					<w:lsdException w:name="Medium Grid 3 Accent 4" w:uiPriority="69"/>
					<w:lsdException w:name="Dark List Accent 4" w:uiPriority="70"/>
					<w:lsdException w:name="Colorful Shading Accent 4" w:uiPriority="71"/>
					<w:lsdException w:name="Colorful List Accent 4" w:uiPriority="72"/>
					<w:lsdException w:name="Colorful Grid Accent 4" w:uiPriority="73"/>
					<w:lsdException w:name="Light Shading Accent 5" w:uiPriority="60"/>
					<w:lsdException w:name="Light List Accent 5" w:uiPriority="61"/>
					<w:lsdException w:name="Light Grid Accent 5" w:uiPriority="62"/>
					<w:lsdException w:name="Medium Shading 1 Accent 5" w:uiPriority="63"/>
					<w:lsdException w:name="Medium Shading 2 Accent 5" w:uiPriority="64"/>
					<w:lsdException w:name="Medium List 1 Accent 5" w:uiPriority="65"/>
					<w:lsdException w:name="Medium List 2 Accent 5" w:uiPriority="66"/>
					<w:lsdException w:name="Medium Grid 1 Accent 5" w:uiPriority="67"/>
					<w:lsdException w:name="Medium Grid 2 Accent 5" w:uiPriority="68"/>
					<w:lsdException w:name="Medium Grid 3 Accent 5" w:uiPriority="69"/>
					<w:lsdException w:name="Dark List Accent 5" w:uiPriority="70"/>
					<w:lsdException w:name="Colorful Shading Accent 5" w:uiPriority="71"/>
					<w:lsdException w:name="Colorful List Accent 5" w:uiPriority="72"/>
					<w:lsdException w:name="Colorful Grid Accent 5" w:uiPriority="73"/>
					<w:lsdException w:name="Light Shading Accent 6" w:uiPriority="60"/>
					<w:lsdException w:name="Light List Accent 6" w:uiPriority="61"/>
					<w:lsdException w:name="Light Grid Accent 6" w:uiPriority="62"/>
					<w:lsdException w:name="Medium Shading 1 Accent 6" w:uiPriority="63"/>
					<w:lsdException w:name="Medium Shading 2 Accent 6" w:uiPriority="64"/>
					<w:lsdException w:name="Medium List 1 Accent 6" w:uiPriority="65"/>
					<w:lsdException w:name="Medium List 2 Accent 6" w:uiPriority="66"/>
					<w:lsdException w:name="Medium Grid 1 Accent 6" w:uiPriority="67"/>
					<w:lsdException w:name="Medium Grid 2 Accent 6" w:uiPriority="68"/>
					<w:lsdException w:name="Medium Grid 3 Accent 6" w:uiPriority="69"/>
					<w:lsdException w:name="Dark List Accent 6" w:uiPriority="70"/>
					<w:lsdException w:name="Colorful Shading Accent 6" w:uiPriority="71"/>
					<w:lsdException w:name="Colorful List Accent 6" w:uiPriority="72"/>
					<w:lsdException w:name="Colorful Grid Accent 6" w:uiPriority="73"/>
					<w:lsdException w:name="Subtle Emphasis" w:uiPriority="19" w:qFormat="1"/>
					<w:lsdException w:name="Intense Emphasis" w:uiPriority="21" w:qFormat="1"/>
					<w:lsdException w:name="Subtle Reference" w:uiPriority="31" w:qFormat="1"/>
					<w:lsdException w:name="Intense Reference" w:uiPriority="32" w:qFormat="1"/>
					<w:lsdException w:name="Book Title" w:uiPriority="33" w:qFormat="1"/>
					<w:lsdException w:name="Bibliography" w:semiHidden="1" w:uiPriority="37" w:unhideWhenUsed="1"/>
					<w:lsdException w:name="TOC Heading" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1" w:qFormat="1"/>
					<w:lsdException w:name="Plain Table 1" w:uiPriority="41"/>
					<w:lsdException w:name="Plain Table 2" w:uiPriority="42"/>
					<w:lsdException w:name="Plain Table 3" w:uiPriority="43"/>
					<w:lsdException w:name="Plain Table 4" w:uiPriority="44"/>
					<w:lsdException w:name="Plain Table 5" w:uiPriority="45"/>
					<w:lsdException w:name="Grid Table Light" w:uiPriority="40"/>
					<w:lsdException w:name="Grid Table 1 Light" w:uiPriority="46"/>
					<w:lsdException w:name="Grid Table 2" w:uiPriority="47"/>
					<w:lsdException w:name="Grid Table 3" w:uiPriority="48"/>
					<w:lsdException w:name="Grid Table 4" w:uiPriority="49"/>
					<w:lsdException w:name="Grid Table 5 Dark" w:uiPriority="50"/>
					<w:lsdException w:name="Grid Table 6 Colorful" w:uiPriority="51"/>
					<w:lsdException w:name="Grid Table 7 Colorful" w:uiPriority="52"/>
					<w:lsdException w:name="Grid Table 1 Light Accent 1" w:uiPriority="46"/>
					<w:lsdException w:name="Grid Table 2 Accent 1" w:uiPriority="47"/>
					<w:lsdException w:name="Grid Table 3 Accent 1" w:uiPriority="48"/>
					<w:lsdException w:name="Grid Table 4 Accent 1" w:uiPriority="49"/>
					<w:lsdException w:name="Grid Table 5 Dark Accent 1" w:uiPriority="50"/>
					<w:lsdException w:name="Grid Table 6 Colorful Accent 1" w:uiPriority="51"/>
					<w:lsdException w:name="Grid Table 7 Colorful Accent 1" w:uiPriority="52"/>
					<w:lsdException w:name="Grid Table 1 Light Accent 2" w:uiPriority="46"/>
					<w:lsdException w:name="Grid Table 2 Accent 2" w:uiPriority="47"/>
					<w:lsdException w:name="Grid Table 3 Accent 2" w:uiPriority="48"/>
					<w:lsdException w:name="Grid Table 4 Accent 2" w:uiPriority="49"/>
					<w:lsdException w:name="Grid Table 5 Dark Accent 2" w:uiPriority="50"/>
					<w:lsdException w:name="Grid Table 6 Colorful Accent 2" w:uiPriority="51"/>
					<w:lsdException w:name="Grid Table 7 Colorful Accent 2" w:uiPriority="52"/>
					<w:lsdException w:name="Grid Table 1 Light Accent 3" w:uiPriority="46"/>
					<w:lsdException w:name="Grid Table 2 Accent 3" w:uiPriority="47"/>
					<w:lsdException w:name="Grid Table 3 Accent 3" w:uiPriority="48"/>
					<w:lsdException w:name="Grid Table 4 Accent 3" w:uiPriority="49"/>
					<w:lsdException w:name="Grid Table 5 Dark Accent 3" w:uiPriority="50"/>
					<w:lsdException w:name="Grid Table 6 Colorful Accent 3" w:uiPriority="51"/>
					<w:lsdException w:name="Grid Table 7 Colorful Accent 3" w:uiPriority="52"/>
					<w:lsdException w:name="Grid Table 1 Light Accent 4" w:uiPriority="46"/>
					<w:lsdException w:name="Grid Table 2 Accent 4" w:uiPriority="47"/>
					<w:lsdException w:name="Grid Table 3 Accent 4" w:uiPriority="48"/>
					<w:lsdException w:name="Grid Table 4 Accent 4" w:uiPriority="49"/>
					<w:lsdException w:name="Grid Table 5 Dark Accent 4" w:uiPriority="50"/>
					<w:lsdException w:name="Grid Table 6 Colorful Accent 4" w:uiPriority="51"/>
					<w:lsdException w:name="Grid Table 7 Colorful Accent 4" w:uiPriority="52"/>
					<w:lsdException w:name="Grid Table 1 Light Accent 5" w:uiPriority="46"/>
					<w:lsdException w:name="Grid Table 2 Accent 5" w:uiPriority="47"/>
					<w:lsdException w:name="Grid Table 3 Accent 5" w:uiPriority="48"/>
					<w:lsdException w:name="Grid Table 4 Accent 5" w:uiPriority="49"/>
					<w:lsdException w:name="Grid Table 5 Dark Accent 5" w:uiPriority="50"/>
					<w:lsdException w:name="Grid Table 6 Colorful Accent 5" w:uiPriority="51"/>
					<w:lsdException w:name="Grid Table 7 Colorful Accent 5" w:uiPriority="52"/>
					<w:lsdException w:name="Grid Table 1 Light Accent 6" w:uiPriority="46"/>
					<w:lsdException w:name="Grid Table 2 Accent 6" w:uiPriority="47"/>
					<w:lsdException w:name="Grid Table 3 Accent 6" w:uiPriority="48"/>
					<w:lsdException w:name="Grid Table 4 Accent 6" w:uiPriority="49"/>
					<w:lsdException w:name="Grid Table 5 Dark Accent 6" w:uiPriority="50"/>
					<w:lsdException w:name="Grid Table 6 Colorful Accent 6" w:uiPriority="51"/>
					<w:lsdException w:name="Grid Table 7 Colorful Accent 6" w:uiPriority="52"/>
					<w:lsdException w:name="List Table 1 Light" w:uiPriority="46"/>
					<w:lsdException w:name="List Table 2" w:uiPriority="47"/>
					<w:lsdException w:name="List Table 3" w:uiPriority="48"/>
					<w:lsdException w:name="List Table 4" w:uiPriority="49"/>
					<w:lsdException w:name="List Table 5 Dark" w:uiPriority="50"/>
					<w:lsdException w:name="List Table 6 Colorful" w:uiPriority="51"/>
					<w:lsdException w:name="List Table 7 Colorful" w:uiPriority="52"/>
					<w:lsdException w:name="List Table 1 Light Accent 1" w:uiPriority="46"/>
					<w:lsdException w:name="List Table 2 Accent 1" w:uiPriority="47"/>
					<w:lsdException w:name="List Table 3 Accent 1" w:uiPriority="48"/>
					<w:lsdException w:name="List Table 4 Accent 1" w:uiPriority="49"/>
					<w:lsdException w:name="List Table 5 Dark Accent 1" w:uiPriority="50"/>
					<w:lsdException w:name="List Table 6 Colorful Accent 1" w:uiPriority="51"/>
					<w:lsdException w:name="List Table 7 Colorful Accent 1" w:uiPriority="52"/>
					<w:lsdException w:name="List Table 1 Light Accent 2" w:uiPriority="46"/>
					<w:lsdException w:name="List Table 2 Accent 2" w:uiPriority="47"/>
					<w:lsdException w:name="List Table 3 Accent 2" w:uiPriority="48"/>
					<w:lsdException w:name="List Table 4 Accent 2" w:uiPriority="49"/>
					<w:lsdException w:name="List Table 5 Dark Accent 2" w:uiPriority="50"/>
					<w:lsdException w:name="List Table 6 Colorful Accent 2" w:uiPriority="51"/>
					<w:lsdException w:name="List Table 7 Colorful Accent 2" w:uiPriority="52"/>
					<w:lsdException w:name="List Table 1 Light Accent 3" w:uiPriority="46"/>
					<w:lsdException w:name="List Table 2 Accent 3" w:uiPriority="47"/>
					<w:lsdException w:name="List Table 3 Accent 3" w:uiPriority="48"/>
					<w:lsdException w:name="List Table 4 Accent 3" w:uiPriority="49"/>
					<w:lsdException w:name="List Table 5 Dark Accent 3" w:uiPriority="50"/>
					<w:lsdException w:name="List Table 6 Colorful Accent 3" w:uiPriority="51"/>
					<w:lsdException w:name="List Table 7 Colorful Accent 3" w:uiPriority="52"/>
					<w:lsdException w:name="List Table 1 Light Accent 4" w:uiPriority="46"/>
					<w:lsdException w:name="List Table 2 Accent 4" w:uiPriority="47"/>
					<w:lsdException w:name="List Table 3 Accent 4" w:uiPriority="48"/>
					<w:lsdException w:name="List Table 4 Accent 4" w:uiPriority="49"/>
					<w:lsdException w:name="List Table 5 Dark Accent 4" w:uiPriority="50"/>
					<w:lsdException w:name="List Table 6 Colorful Accent 4" w:uiPriority="51"/>
					<w:lsdException w:name="List Table 7 Colorful Accent 4" w:uiPriority="52"/>
					<w:lsdException w:name="List Table 1 Light Accent 5" w:uiPriority="46"/>
					<w:lsdException w:name="List Table 2 Accent 5" w:uiPriority="47"/>
					<w:lsdException w:name="List Table 3 Accent 5" w:uiPriority="48"/>
					<w:lsdException w:name="List Table 4 Accent 5" w:uiPriority="49"/>
					<w:lsdException w:name="List Table 5 Dark Accent 5" w:uiPriority="50"/>
					<w:lsdException w:name="List Table 6 Colorful Accent 5" w:uiPriority="51"/>
					<w:lsdException w:name="List Table 7 Colorful Accent 5" w:uiPriority="52"/>
					<w:lsdException w:name="List Table 1 Light Accent 6" w:uiPriority="46"/>
					<w:lsdException w:name="List Table 2 Accent 6" w:uiPriority="47"/>
					<w:lsdException w:name="List Table 3 Accent 6" w:uiPriority="48"/>
					<w:lsdException w:name="List Table 4 Accent 6" w:uiPriority="49"/>
					<w:lsdException w:name="List Table 5 Dark Accent 6" w:uiPriority="50"/>
					<w:lsdException w:name="List Table 6 Colorful Accent 6" w:uiPriority="51"/>
					<w:lsdException w:name="List Table 7 Colorful Accent 6" w:uiPriority="52"/>
				</w:latentStyles>
				<w:style w:type="paragraph" w:default="1" w:styleId="Normal">
					<w:name w:val="Normal"/>
					<w:qFormat/>
					<w:rsid w:val="00F91D2A"/>
					<w:pPr>
						<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					</w:pPr>
					<w:rPr>
						<w:rFonts w:ascii="Times New Roman" w:eastAsia="Times New Roman" w:hAnsi="Times New Roman" w:cs="Times New Roman"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:val="en-GB"/>
					</w:rPr>
				</w:style>
				<w:style w:type="character" w:default="1" w:styleId="DefaultParagraphFont">
					<w:name w:val="Default Paragraph Font"/>
					<w:uiPriority w:val="1"/>
					<w:semiHidden/>
					<w:unhideWhenUsed/>
				</w:style>
				<w:style w:type="table" w:default="1" w:styleId="TableNormal">
					<w:name w:val="Normal Table"/>
					<w:uiPriority w:val="99"/>
					<w:semiHidden/>
					<w:unhideWhenUsed/>
					<w:tblPr>
						<w:tblInd w:w="0" w:type="dxa"/>
						<w:tblCellMar>
							<w:top w:w="0" w:type="dxa"/>
							<w:left w:w="108" w:type="dxa"/>
							<w:bottom w:w="0" w:type="dxa"/>
							<w:right w:w="108" w:type="dxa"/>
						</w:tblCellMar>
					</w:tblPr>
				</w:style>
				<w:style w:type="numbering" w:default="1" w:styleId="NoList">
					<w:name w:val="No List"/>
					<w:uiPriority w:val="99"/>
					<w:semiHidden/>
					<w:unhideWhenUsed/>
				</w:style>
				<w:style w:type="paragraph" w:styleId="ListParagraph">
					<w:name w:val="List Paragraph"/>
					<w:basedOn w:val="Normal"/>
					<w:uiPriority w:val="34"/>
					<w:qFormat/>
					<w:rsid w:val="00F91D2A"/>
					<w:pPr>
						<w:ind w:left="720"/>
					</w:pPr>
				</w:style>
				<w:style w:type="character" w:styleId="Hyperlink">
					<w:name w:val="Hyperlink"/>
					<w:basedOn w:val="DefaultParagraphFont"/>
					<w:rsid w:val="00F91D2A"/>
					<w:rPr>
						<w:rFonts w:cs="Times New Roman"/>
						<w:color w:val="0000FF"/>
						<w:u w:val="single"/>
					</w:rPr>
				</w:style>
				<w:style w:type="paragraph" w:customStyle="1" w:styleId="Style1">
					<w:name w:val="Style 1"/>
					<w:basedOn w:val="Normal"/>
					<w:uiPriority w:val="99"/>
					<w:rsid w:val="003C4CF9"/>
					<w:pPr>
						<w:widowControl w:val="0"/>
						<w:autoSpaceDE w:val="0"/>
						<w:autoSpaceDN w:val="0"/>
						<w:adjustRightInd w:val="0"/>
					</w:pPr>
					<w:rPr>
						<w:rFonts w:eastAsiaTheme="minorEastAsia"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:val="en-US"/>
					</w:rPr>
				</w:style>
				<w:style w:type="character" w:customStyle="1" w:styleId="CharacterStyle1">
					<w:name w:val="Character Style 1"/>
					<w:uiPriority w:val="99"/>
					<w:rsid w:val="003C4CF9"/>
					<w:rPr>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
					</w:rPr>
				</w:style>
				<w:style w:type="paragraph" w:customStyle="1" w:styleId="Style2">
					<w:name w:val="Style 2"/>
					<w:basedOn w:val="Normal"/>
					<w:uiPriority w:val="99"/>
					<w:rsid w:val="00E2451C"/>
					<w:pPr>
						<w:widowControl w:val="0"/>
						<w:autoSpaceDE w:val="0"/>
						<w:autoSpaceDN w:val="0"/>
						<w:spacing w:before="180" w:line="208" w:lineRule="auto"/>
						<w:ind w:left="864"/>
					</w:pPr>
					<w:rPr>
						<w:rFonts w:ascii="Arial" w:eastAsiaTheme="minorEastAsia" w:hAnsi="Arial" w:cs="Arial"/>
						<w:sz w:val="21"/>
						<w:szCs w:val="21"/>
						<w:lang w:val="en-US"/>
					</w:rPr>
				</w:style>
				<w:style w:type="character" w:customStyle="1" w:styleId="CharacterStyle2">
					<w:name w:val="Character Style 2"/>
					<w:uiPriority w:val="99"/>
					<w:rsid w:val="00E2451C"/>
					<w:rPr>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
					</w:rPr>
				</w:style>
				<w:style w:type="paragraph" w:styleId="BalloonText">
					<w:name w:val="Balloon Text"/>
					<w:basedOn w:val="Normal"/>
					<w:link w:val="BalloonTextChar"/>
					<w:uiPriority w:val="99"/>
					<w:semiHidden/>
					<w:unhideWhenUsed/>
					<w:rsid w:val="00D87330"/>
					<w:rPr>
						<w:rFonts w:ascii="Segoe UI" w:hAnsi="Segoe UI" w:cs="Segoe UI"/>
						<w:sz w:val="18"/>
						<w:szCs w:val="18"/>
					</w:rPr>
				</w:style>
				<w:style w:type="character" w:customStyle="1" w:styleId="BalloonTextChar">
					<w:name w:val="Balloon Text Char"/>
					<w:basedOn w:val="DefaultParagraphFont"/>
					<w:link w:val="BalloonText"/>
					<w:uiPriority w:val="99"/>
					<w:semiHidden/>
					<w:rsid w:val="00D87330"/>
					<w:rPr>
						<w:rFonts w:ascii="Segoe UI" w:eastAsia="Times New Roman" w:hAnsi="Segoe UI" w:cs="Segoe UI"/>
						<w:sz w:val="18"/>
						<w:szCs w:val="18"/>
						<w:lang w:val="en-GB"/>
					</w:rPr>
				</w:style>
				<w:style w:type="paragraph" w:styleId="Header">
					<w:name w:val="header"/>
					<w:basedOn w:val="Normal"/>
					<w:link w:val="HeaderChar"/>
					<w:uiPriority w:val="99"/>
					<w:unhideWhenUsed/>
					<w:rsid w:val="00E42F09"/>
					<w:pPr>
						<w:tabs>
							<w:tab w:val="center" w:pos="4680"/>
							<w:tab w:val="right" w:pos="9360"/>
						</w:tabs>
					</w:pPr>
				</w:style>
				<w:style w:type="character" w:customStyle="1" w:styleId="HeaderChar">
					<w:name w:val="Header Char"/>
					<w:basedOn w:val="DefaultParagraphFont"/>
					<w:link w:val="Header"/>
					<w:uiPriority w:val="99"/>
					<w:rsid w:val="00E42F09"/>
					<w:rPr>
						<w:rFonts w:ascii="Times New Roman" w:eastAsia="Times New Roman" w:hAnsi="Times New Roman" w:cs="Times New Roman"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:val="en-GB"/>
					</w:rPr>
				</w:style>
				<w:style w:type="paragraph" w:styleId="Footer">
					<w:name w:val="footer"/>
					<w:basedOn w:val="Normal"/>
					<w:link w:val="FooterChar"/>
					<w:uiPriority w:val="99"/>
					<w:unhideWhenUsed/>
					<w:rsid w:val="00E42F09"/>
					<w:pPr>
						<w:tabs>
							<w:tab w:val="center" w:pos="4680"/>
							<w:tab w:val="right" w:pos="9360"/>
						</w:tabs>
					</w:pPr>
				</w:style>
				<w:style w:type="character" w:customStyle="1" w:styleId="FooterChar">
					<w:name w:val="Footer Char"/>
					<w:basedOn w:val="DefaultParagraphFont"/>
					<w:link w:val="Footer"/>
					<w:uiPriority w:val="99"/>
					<w:rsid w:val="00E42F09"/>
					<w:rPr>
						<w:rFonts w:ascii="Times New Roman" w:eastAsia="Times New Roman" w:hAnsi="Times New Roman" w:cs="Times New Roman"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:val="en-GB"/>
					</w:rPr>
				</w:style>
			</w:styles>
		</pkg:xmlData>
	</pkg:part>
	<pkg:part pkg:name="/word/numbering.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.numbering+xml">
		<pkg:xmlData>
			<w:numbering mc:Ignorable="w14 w15 wp14" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape">
				<w:abstractNum w:abstractNumId="0">
					<w:nsid w:val="01B24694"/>
					<w:multiLevelType w:val="singleLevel"/>
					<w:tmpl w:val="5B5257A9"/>
					<w:lvl w:ilvl="0">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%1)"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:tabs>
								<w:tab w:val="num" w:pos="360"/>
							</w:tabs>
							<w:ind w:left="576"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
							<w:snapToGrid/>
							<w:spacing w:val="8"/>
							<w:sz w:val="21"/>
							<w:szCs w:val="21"/>
						</w:rPr>
					</w:lvl>
				</w:abstractNum>
				<w:abstractNum w:abstractNumId="1">
					<w:nsid w:val="039BDA06"/>
					<w:multiLevelType w:val="singleLevel"/>
					<w:tmpl w:val="2DA496E3"/>
					<w:lvl w:ilvl="0">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%1)"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:tabs>
								<w:tab w:val="num" w:pos="288"/>
							</w:tabs>
							<w:ind w:left="936" w:hanging="288"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
							<w:snapToGrid/>
							<w:spacing w:val="2"/>
							<w:sz w:val="21"/>
							<w:szCs w:val="21"/>
						</w:rPr>
					</w:lvl>
				</w:abstractNum>
				<w:abstractNum w:abstractNumId="2">
					<w:nsid w:val="054DFCC3"/>
					<w:multiLevelType w:val="singleLevel"/>
					<w:tmpl w:val="1BD83A01"/>
					<w:lvl w:ilvl="0">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%1)"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:tabs>
								<w:tab w:val="num" w:pos="360"/>
							</w:tabs>
							<w:ind w:left="1008" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
							<w:snapToGrid/>
							<w:spacing w:val="8"/>
							<w:sz w:val="21"/>
							<w:szCs w:val="21"/>
						</w:rPr>
					</w:lvl>
				</w:abstractNum>
				<w:abstractNum w:abstractNumId="3">
					<w:nsid w:val="0731E7A9"/>
					<w:multiLevelType w:val="singleLevel"/>
					<w:tmpl w:val="04090011"/>
					<w:lvl w:ilvl="0">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%1)"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="1224" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:snapToGrid/>
							<w:spacing w:val="11"/>
							<w:sz w:val="21"/>
							<w:szCs w:val="21"/>
						</w:rPr>
					</w:lvl>
				</w:abstractNum>
				<w:abstractNum w:abstractNumId="4">
					<w:nsid w:val="0E631CE4"/>
					<w:multiLevelType w:val="hybridMultilevel"/>
					<w:tmpl w:val="B060C1C6"/>
					<w:lvl w:ilvl="0" w:tplc="04090019">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%1."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2016" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%2."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2736" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%3."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="3456" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%4."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="4176" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%5."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="4896" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%6."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="5616" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%7."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="6336" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%8."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="7056" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%9."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="7776" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
				</w:abstractNum>
				<w:abstractNum w:abstractNumId="5">
					<w:nsid w:val="105C4E8F"/>
					<w:multiLevelType w:val="hybridMultilevel"/>
					<w:tmpl w:val="5156B52A"/>
					<w:lvl w:ilvl="0" w:tplc="98AA3F86">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%1."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="1008" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:i w:val="0"/>
							<w:iCs w:val="0"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="1" w:tplc="A87620D6">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%2)"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="1728" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%3."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="2448" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%4."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="3168" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%5."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="3888" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%6."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="4608" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%7."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="5328" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%8."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="6048" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%9."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="6768" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
				</w:abstractNum>
				<w:abstractNum w:abstractNumId="6">
					<w:nsid w:val="12713C32"/>
					<w:multiLevelType w:val="hybridMultilevel"/>
					<w:tmpl w:val="6EE825F2"/>
					<w:lvl w:ilvl="0" w:tplc="04210009">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="v"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="720" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="1" w:tplc="04210009">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="v"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="1440" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="2" w:tplc="04210005" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="§"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2160" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="3" w:tplc="04210001" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2880" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="4" w:tplc="04210003" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="o"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="3600" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="5" w:tplc="04210005" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="§"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="4320" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="6" w:tplc="04210001" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="5040" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="7" w:tplc="04210003" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="o"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="5760" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="8" w:tplc="04210005" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="§"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="6480" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
						</w:rPr>
					</w:lvl>
				</w:abstractNum>
				<w:abstractNum w:abstractNumId="7">
					<w:nsid w:val="257D541D"/>
					<w:multiLevelType w:val="hybridMultilevel"/>
					<w:tmpl w:val="B060C1C6"/>
					<w:lvl w:ilvl="0" w:tplc="04090019">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%1."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2016" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%2."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2736" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%3."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="3456" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%4."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="4176" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%5."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="4896" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%6."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="5616" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%7."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="6336" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%8."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="7056" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%9."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="7776" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
				</w:abstractNum>
				<w:abstractNum w:abstractNumId="8">
					<w:nsid w:val="359B2B12"/>
					<w:multiLevelType w:val="hybridMultilevel"/>
					<w:tmpl w:val="C72C619A"/>
					<w:lvl w:ilvl="0" w:tplc="0409000F">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%1."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="720" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%2."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="1440" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%3."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="2160" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%4."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2880" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%5."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="3600" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%6."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="4320" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%7."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="5040" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%8."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="5760" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%9."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="6480" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
				</w:abstractNum>
				<w:abstractNum w:abstractNumId="9">
					<w:nsid w:val="3EF04493"/>
					<w:multiLevelType w:val="hybridMultilevel"/>
					<w:tmpl w:val="FBE40F98"/>
					<w:lvl w:ilvl="0" w:tplc="0409000D">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="ü"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="720" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="1" w:tplc="04090003" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="o"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="1440" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="2" w:tplc="04090005" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="§"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2160" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="3" w:tplc="04090001" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2880" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="4" w:tplc="04090003" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="o"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="3600" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="5" w:tplc="04090005" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="§"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="4320" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="6" w:tplc="04090001" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="5040" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="7" w:tplc="04090003" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="o"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="5760" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="8" w:tplc="04090005" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="§"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="6480" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
						</w:rPr>
					</w:lvl>
				</w:abstractNum>
				<w:abstractNum w:abstractNumId="10">
					<w:nsid w:val="40CA4A5C"/>
					<w:multiLevelType w:val="hybridMultilevel"/>
					<w:tmpl w:val="C72C619A"/>
					<w:lvl w:ilvl="0" w:tplc="0409000F">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%1."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="720" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%2."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="1440" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%3."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="2160" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%4."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2880" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%5."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="3600" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%6."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="4320" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%7."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="5040" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%8."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="5760" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%9."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="6480" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
				</w:abstractNum>
				<w:abstractNum w:abstractNumId="11">
					<w:nsid w:val="422827E5"/>
					<w:multiLevelType w:val="hybridMultilevel"/>
					<w:tmpl w:val="9C2A6096"/>
					<w:lvl w:ilvl="0" w:tplc="F2206386">
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="-"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="720" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Cambria" w:eastAsia="Times New Roman" w:hAnsi="Cambria" w:cs="Arial" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="1" w:tplc="04090003" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="o"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="1440" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="2" w:tplc="04090005" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="§"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2160" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="3" w:tplc="04090001" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2880" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="4" w:tplc="04090003" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="o"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="3600" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="5" w:tplc="04090005" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="§"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="4320" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="6" w:tplc="04090001" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="5040" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="7" w:tplc="04090003" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="o"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="5760" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="8" w:tplc="04090005" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="§"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="6480" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
						</w:rPr>
					</w:lvl>
				</w:abstractNum>
				<w:abstractNum w:abstractNumId="12">
					<w:nsid w:val="47781CDC"/>
					<w:multiLevelType w:val="hybridMultilevel"/>
					<w:tmpl w:val="EF66D990"/>
					<w:lvl w:ilvl="0" w:tplc="500C621A">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%1."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="648" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:hint="default"/>
							<w:i w:val="0"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%2."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="1368" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%3."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="2088" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%4."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2808" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%5."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="3528" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%6."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="4248" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%7."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="4968" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%8."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="5688" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%9."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="6408" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
				</w:abstractNum>
				<w:abstractNum w:abstractNumId="13">
					<w:nsid w:val="50520D73"/>
					<w:multiLevelType w:val="hybridMultilevel"/>
					<w:tmpl w:val="39FCC1D4"/>
					<w:lvl w:ilvl="0" w:tplc="AB4E6670">
						<w:numFmt w:val="none"/>
						<w:lvlText w:val=""/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="720" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="1" w:tplc="04090003" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="o"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="1440" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="2" w:tplc="04090005" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="§"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2160" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="3" w:tplc="04090001" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2880" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="4" w:tplc="04090003" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="o"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="3600" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="5" w:tplc="04090005" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="§"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="4320" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="6" w:tplc="04090001" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="5040" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="7" w:tplc="04090003" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="o"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="5760" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="8" w:tplc="04090005" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="§"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="6480" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
						</w:rPr>
					</w:lvl>
				</w:abstractNum>
				<w:abstractNum w:abstractNumId="14">
					<w:nsid w:val="5AC343FE"/>
					<w:multiLevelType w:val="hybridMultilevel"/>
					<w:tmpl w:val="E5A47858"/>
					<w:lvl w:ilvl="0" w:tplc="5A7A6C8E">
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="-"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="720" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="1" w:tplc="04210003" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="o"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="1440" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="2" w:tplc="04210005" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="§"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2160" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="3" w:tplc="04210001" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2880" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="4" w:tplc="04210003" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="o"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="3600" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="5" w:tplc="04210005" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="§"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="4320" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="6" w:tplc="04210001" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="5040" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="7" w:tplc="04210003" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="o"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="5760" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="8" w:tplc="04210005" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="§"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="6480" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/>
						</w:rPr>
					</w:lvl>
				</w:abstractNum>
				<w:abstractNum w:abstractNumId="15">
					<w:nsid w:val="639355C9"/>
					<w:multiLevelType w:val="hybridMultilevel"/>
					<w:tmpl w:val="B060C1C6"/>
					<w:lvl w:ilvl="0" w:tplc="04090019">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%1."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2016" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%2."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2736" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%3."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="3456" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%4."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="4176" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%5."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="4896" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%6."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="5616" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%7."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="6336" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%8."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="7056" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%9."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="7776" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
				</w:abstractNum>
				<w:abstractNum w:abstractNumId="16">
					<w:nsid w:val="6FE6133B"/>
					<w:multiLevelType w:val="hybridMultilevel"/>
					<w:tmpl w:val="FCD41CCA"/>
					<w:lvl w:ilvl="0" w:tplc="04090011">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%1)"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="1296" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="1" w:tplc="04090019">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%2."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="2016" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%3."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="2736" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%4."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="3456" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%5."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="4176" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%6."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="4896" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="decimal"/>
						<w:lvlText w:val="%7."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="5616" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerLetter"/>
						<w:lvlText w:val="%8."/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:ind w:left="6336" w:hanging="360"/>
						</w:pPr>
					</w:lvl>
					<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="lowerRoman"/>
						<w:lvlText w:val="%9."/>
						<w:lvlJc w:val="right"/>
						<w:pPr>
							<w:ind w:left="7056" w:hanging="180"/>
						</w:pPr>
					</w:lvl>
				</w:abstractNum>
				<w:abstractNum w:abstractNumId="17">
					<w:nsid w:val="78D9607B"/>
					<w:multiLevelType w:val="multilevel"/>
					<w:tmpl w:val="5226EAFC"/>
					<w:lvl w:ilvl="0">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:tabs>
								<w:tab w:val="num" w:pos="501"/>
							</w:tabs>
							<w:ind w:left="501" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
							<w:sz w:val="20"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="1" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:tabs>
								<w:tab w:val="num" w:pos="1440"/>
							</w:tabs>
							<w:ind w:left="1440" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
							<w:sz w:val="20"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="2" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:tabs>
								<w:tab w:val="num" w:pos="2160"/>
							</w:tabs>
							<w:ind w:left="2160" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
							<w:sz w:val="20"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="3" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:tabs>
								<w:tab w:val="num" w:pos="2880"/>
							</w:tabs>
							<w:ind w:left="2880" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
							<w:sz w:val="20"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="4" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:tabs>
								<w:tab w:val="num" w:pos="3600"/>
							</w:tabs>
							<w:ind w:left="3600" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
							<w:sz w:val="20"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="5" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:tabs>
								<w:tab w:val="num" w:pos="4320"/>
							</w:tabs>
							<w:ind w:left="4320" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
							<w:sz w:val="20"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="6" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:tabs>
								<w:tab w:val="num" w:pos="5040"/>
							</w:tabs>
							<w:ind w:left="5040" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
							<w:sz w:val="20"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="7" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:tabs>
								<w:tab w:val="num" w:pos="5760"/>
							</w:tabs>
							<w:ind w:left="5760" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
							<w:sz w:val="20"/>
						</w:rPr>
					</w:lvl>
					<w:lvl w:ilvl="8" w:tentative="1">
						<w:start w:val="1"/>
						<w:numFmt w:val="bullet"/>
						<w:lvlText w:val="·"/>
						<w:lvlJc w:val="left"/>
						<w:pPr>
							<w:tabs>
								<w:tab w:val="num" w:pos="6480"/>
							</w:tabs>
							<w:ind w:left="6480" w:hanging="360"/>
						</w:pPr>
						<w:rPr>
							<w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/>
							<w:sz w:val="20"/>
						</w:rPr>
					</w:lvl>
				</w:abstractNum>
				<w:num w:numId="1">
					<w:abstractNumId w:val="6"/>
				</w:num>
				<w:num w:numId="2">
					<w:abstractNumId w:val="11"/>
				</w:num>
				<w:num w:numId="3">
					<w:abstractNumId w:val="13"/>
				</w:num>
				<w:num w:numId="4">
					<w:abstractNumId w:val="10"/>
				</w:num>
				<w:num w:numId="5">
					<w:abstractNumId w:val="8"/>
				</w:num>
				<w:num w:numId="6">
					<w:abstractNumId w:val="3"/>
				</w:num>
				<w:num w:numId="7">
					<w:abstractNumId w:val="0"/>
				</w:num>
				<w:num w:numId="8">
					<w:abstractNumId w:val="2"/>
				</w:num>
				<w:num w:numId="9">
					<w:abstractNumId w:val="1"/>
				</w:num>
				<w:num w:numId="10">
					<w:abstractNumId w:val="1"/>
					<w:lvlOverride w:ilvl="0">
						<w:lvl w:ilvl="0">
							<w:numFmt w:val="decimal"/>
							<w:lvlText w:val="%1)"/>
							<w:lvlJc w:val="left"/>
							<w:pPr>
								<w:tabs>
									<w:tab w:val="num" w:pos="360"/>
								</w:tabs>
								<w:ind w:left="1008" w:hanging="360"/>
							</w:pPr>
							<w:rPr>
								<w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
								<w:snapToGrid/>
								<w:spacing w:val="2"/>
								<w:sz w:val="21"/>
								<w:szCs w:val="21"/>
							</w:rPr>
						</w:lvl>
					</w:lvlOverride>
				</w:num>
				<w:num w:numId="11">
					<w:abstractNumId w:val="5"/>
				</w:num>
				<w:num w:numId="12">
					<w:abstractNumId w:val="12"/>
				</w:num>
				<w:num w:numId="13">
					<w:abstractNumId w:val="16"/>
				</w:num>
				<w:num w:numId="14">
					<w:abstractNumId w:val="15"/>
				</w:num>
				<w:num w:numId="15">
					<w:abstractNumId w:val="7"/>
				</w:num>
				<w:num w:numId="16">
					<w:abstractNumId w:val="4"/>
				</w:num>
				<w:num w:numId="17">
					<w:abstractNumId w:val="17"/>
				</w:num>
				<w:num w:numId="18">
					<w:abstractNumId w:val="14"/>
				</w:num>
				<w:num w:numId="19">
					<w:abstractNumId w:val="9"/>
				</w:num>
			</w:numbering>
		</pkg:xmlData>
	</pkg:part>
	<pkg:part pkg:name="/docProps/core.xml" pkg:contentType="application/vnd.openxmlformats-package.core-properties+xml" pkg:padding="256">
		<pkg:xmlData>
			<cp:coreProperties xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:dcmitype="http://purl.org/dc/dcmitype/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
				<dc:title/>
				<dc:creator>ulp admin</dc:creator>
				<cp:lastModifiedBy>Al</cp:lastModifiedBy>
				<cp:revision>2</cp:revision>
				<cp:lastPrinted>2015-09-03T04:15:00Z</cp:lastPrinted>
				<dcterms:created xsi:type="dcterms:W3CDTF">2015-12-16T03:41:00Z</dcterms:created>
				<dcterms:modified xsi:type="dcterms:W3CDTF">2015-12-16T03:41:00Z</dcterms:modified>
			</cp:coreProperties>
		</pkg:xmlData>
	</pkg:part>
	<pkg:part pkg:name="/word/fontTable.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml">
		<pkg:xmlData>
			<w:fonts mc:Ignorable="w14 w15" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml">
				<w:font w:name="Arial">
					<w:panose1 w:val="020B0604020202020204"/>
					<w:charset w:val="00"/>
					<w:family w:val="swiss"/>
					<w:pitch w:val="variable"/>
					<w:sig w:usb0="E0002AFF" w:usb1="C0007843" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF" w:csb1="00000000"/>
				</w:font>
				<w:font w:name="Times New Roman">
					<w:panose1 w:val="02020603050405020304"/>
					<w:charset w:val="00"/>
					<w:family w:val="roman"/>
					<w:pitch w:val="variable"/>
					<w:sig w:usb0="E0002EFF" w:usb1="C0007843" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF" w:csb1="00000000"/>
				</w:font>
				<w:font w:name="Wingdings">
					<w:panose1 w:val="05000000000000000000"/>
					<w:charset w:val="02"/>
					<w:family w:val="auto"/>
					<w:pitch w:val="variable"/>
					<w:sig w:usb0="00000000" w:usb1="10000000" w:usb2="00000000" w:usb3="00000000" w:csb0="80000000" w:csb1="00000000"/>
				</w:font>
				<w:font w:name="Symbol">
					<w:panose1 w:val="05050102010706020507"/>
					<w:charset w:val="02"/>
					<w:family w:val="roman"/>
					<w:pitch w:val="variable"/>
					<w:sig w:usb0="00000000" w:usb1="10000000" w:usb2="00000000" w:usb3="00000000" w:csb0="80000000" w:csb1="00000000"/>
				</w:font>
				<w:font w:name="Courier New">
					<w:panose1 w:val="02070309020205020404"/>
					<w:charset w:val="00"/>
					<w:family w:val="modern"/>
					<w:pitch w:val="fixed"/>
					<w:sig w:usb0="E0002AFF" w:usb1="C0007843" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF" w:csb1="00000000"/>
				</w:font>
				<w:font w:name="Cambria">
					<w:panose1 w:val="02040503050406030204"/>
					<w:charset w:val="00"/>
					<w:family w:val="roman"/>
					<w:pitch w:val="variable"/>
					<w:sig w:usb0="E00002FF" w:usb1="400004FF" w:usb2="00000000" w:usb3="00000000" w:csb0="0000019F" w:csb1="00000000"/>
				</w:font>
				<w:font w:name="Calibri">
					<w:panose1 w:val="020F0502020204030204"/>
					<w:charset w:val="00"/>
					<w:family w:val="swiss"/>
					<w:pitch w:val="variable"/>
					<w:sig w:usb0="E00002FF" w:usb1="4000ACFF" w:usb2="00000001" w:usb3="00000000" w:csb0="0000019F" w:csb1="00000000"/>
				</w:font>
				<w:font w:name="Segoe UI">
					<w:panose1 w:val="020B0502040204020203"/>
					<w:charset w:val="00"/>
					<w:family w:val="swiss"/>
					<w:notTrueType/>
					<w:pitch w:val="variable"/>
					<w:sig w:usb0="00000003" w:usb1="00000000" w:usb2="00000000" w:usb3="00000000" w:csb0="00000001" w:csb1="00000000"/>
				</w:font>
			</w:fonts>
		</pkg:xmlData>
	</pkg:part>
	<pkg:part pkg:name="/docProps/app.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.extended-properties+xml" pkg:padding="256">
		<pkg:xmlData>
			<Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties" xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">
				<Template>Normal</Template>
				<TotalTime>0</TotalTime>
				<Pages>1</Pages>
				<Words>86</Words>
				<Characters>494</Characters>
				<Application>Microsoft Office Word</Application>
				<DocSecurity>0</DocSecurity>
				<Lines>4</Lines>
				<Paragraphs>1</Paragraphs>
				<ScaleCrop>false</ScaleCrop>
				<HeadingPairs>
					<vt:vector size="2" baseType="variant">
						<vt:variant>
							<vt:lpstr>Title</vt:lpstr>
						</vt:variant>
						<vt:variant>
							<vt:i4>1</vt:i4>
						</vt:variant>
					</vt:vector>
				</HeadingPairs>
				<TitlesOfParts>
					<vt:vector size="1" baseType="lpstr">
						<vt:lpstr/>
					</vt:vector>
				</TitlesOfParts>
				<Company/>
				<LinksUpToDate>false</LinksUpToDate>
				<CharactersWithSpaces>579</CharactersWithSpaces>
				<SharedDoc>false</SharedDoc>
				<HyperlinksChanged>false</HyperlinksChanged>
				<AppVersion>15.0000</AppVersion>
			</Properties>
		</pkg:xmlData>
	</pkg:part>
</pkg:package>