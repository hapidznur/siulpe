<div class="col-sm-10 col-sm-offset-2 col-md-10 col-lg-offset-2 main">
      <ol class="breadcrumb"><?php //echo ; ?></ol>
      <div class="row">
        <div class="col-sm-8 col-md-8 col-lg-8">
          <h3>Detail Tawaran Penyedia</h3>
        </div><!--end of col-8 -->
      </div><!-- end of row-->
 <div class="row">
  	<div class="col-sm-12 col-md-12 col-lg-12">
  		<table class="table table-bordered">
  			<thead>
  				<tr>
  				<th colspan="3">Detail Pengadaan</th>
  				</tr>
  			</thead>
  			<tbody >
  				<tr>
  					<td class="col-sm-4 col-md-4 col-lg-4 text-left">Nama Pengadaan</td>
  					<td colspan="2" class="text-left"><?php echo array_values($detail_penawaran_penyedia)[0]['paket_name']; ?></td>
  				</tr>
  				<tr>
  					<td class="col-sm-4 col-md-4 col-lg-4 text-left">Lokasi</td>
  					<td colspan="2" class="text-left"><?php echo array_values($detail_penawaran_penyedia)[0]['lokasi']; ?></td>
  				</tr>
  				<tr>
  					<td class="col-sm-4 col-md-4 col-lg-4 text-left">Nama Penyedia</td>
  					<td><?php echo array_values($detail_penawaran_penyedia)[0]['nama_penyedia']; ?></td>
  					<td><input type="submit" value="Detail Penyedia" class="btn btn-default"></td>
  				</tr>
  				<tr>
  					<td class="col-sm-4 col-md-4 col-lg-4 text-left">Status</td>
  					<td colspan="2" class="text-left"><?php echo array_values($detail_penawaran_penyedia)[0]['verifikasi']; ?></td>
  				</tr>
  				<tr>
  					<td class="col-sm-4 col-md-4 col-lg-4 text-left">Surat Penawaran</td>
  					<td colspan="2" class="text-left"><?php echo array_values($detail_penawaran_penyedia)[0]['surat_penawaran']; ?></td>
  				</tr>
  				<tr>
  					<td class="col-sm-4 col-md-4 col-lg-4 text-left">Aksi</td>
  					<td class="text-center"><input type="submit" value="Tidak Terverifikasi"></td>
  					<td class="text-center"><input type="submit" value="Verifikasikan"></td>
  				</tr>
  			</tbody>
  		</table>
  		<table class="table table-bordered">
  			<thead>
  				<tr>
  				<th>Nama Barang</th>
  				<th>Spesifikasi Rekanan</th>
  				<th>Spesifikasi Penyedia</th>
  				<th>Harga Tawar</th>
  				</tr>
  			</thead>
  			<tbody>
  				<?php foreach ($detail_penawaran_penyedia as $value) { ?>
  				<tr>
  					<td class="col-sm-4 col-md-4 col-lg-4 text-left"><?php echo $value['nama_barang']; ?></td>
  					<td class="text-center"><?php echo $value['spesifikasi_rekanan']; ?></td>
  					<td class="col-sm-4 col-md-4 col-lg-4 text-center"><?php echo $value['spesifikasi']; ?></td>
  					<td class="text-center"><?php echo $value['harga_tawar']; ?></td>
  				</tr>
  				<?php } ?>
  			</tbody>
  		</table>
  	</div>
</div>