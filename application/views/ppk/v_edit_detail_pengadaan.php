<!-- start: Content -->
<?php echo form_open('ppk/update_pengadaan','class="btn-group"');?>
<?php foreach ($pengadaan_sendiri as $set_value_pengadaan) :
?>      
<?php //print_r($set_value_pengadaan); ?>
<div class="col-xs-12 col-sm-12 col-md-12">
  <div class="form-group ">
    <label class="col-md-2 control-label" for="paket_name">Id Pengadaan</label>  
    <div class="col-md-10">
      <input id="paket_name" name="id_pengadaan" type="text" placeholder="" class="form-control input-md" value="<?php echo $set_value_pengadaan['id_pengadaan']; ?>" editable="false">
        <span class="help-block"> </span>
    </div>
  </div>
  <div class="form-group ">
    <label class="col-md-2 control-label" for="paket_name">Nama Paket</label>  
    <div class="col-md-10">
      <input id="paket_name" name="paket_name" type="text" placeholder="" class="form-control input-md" value="<?php echo $set_value_pengadaan['paket_name']; ?>">
      <span class="help-block">Penulisan nama paket sesuai pada POK</span>  
    </div>
  </div>
  <div class="form-group">
    <label class="col-md-2 control-label" for="no_pengadaan">No Pengadaan</label>  
    <div class="col-md-10">
      <input id="no_pengadaan" name="no_pengadaan" type="text" placeholder="" class="form-control input-md" value="<?php echo $set_value_pengadaan['no_pengadaan']; ?>">
      <span class="help-block">Pada bagian ini tuliskan nomer pengadaan</span>  
    </div>
  </div>
  <div class="form-group">
    <label class="col-md-2 control-label" for="tanggal_surat">Tanggal surat</label>  
    <div class="col-md-4">
      <input id="tanggal_surat" name="tanggal_surat" type="text" placeholder="" class="form-control input-md" value="<?php echo $set_value_pengadaan['tanggal_surat']; ?>" >
      <span class="help-block">bagian ini merupakan tanggal surat dikeluarkan</span>
    </div>

    <label class="col-md-2 control-label" for="tanggal_pengadaan">Tanggal Permintaan</label>  
    <div class="col-md-4">
      <input id="tanggal_permintaan" name="tanggal_permintaan" type="text" placeholder="" class="form-control input-md" value="<?php echo $set_value_pengadaan['tanggal_permintaan']; ?>">
      <span class="help-block">bagian ini merupakan tanggal barang diterima</span>

    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="tanggal_pengadaan">Jam</label>  
      <div class="col-md-4">
        <input id="jam" name="jam" type="text" placeholder="" class="form-control input-md" value="<?php echo $set_value_pengadaan['jam']; ?>">
        <span class="help-block">bagian ini merupakan jam barang diterima</span> 
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="anggaran detail">Anggaran Detail</label>
      <div class="col-md-4">
        <select id="anggaran_detail" name="anggaran_detail" class="form-control">
          <option value="rm">RM</option>
          <option value="bop">BOP</option>
          <option value="pnbp">PNBP</option>
        </select>
        <span class="help-block"> </span> 
      </div>
      <label class="col-md-2 control-label" for="tanggal_dipa">Tanggal DIPA</label>  
      <div class="col-md-4">
        <input id="tanggal_dipa" name="tanggal_dipa" type="text" placeholder="" class="form-control input-md" value="<?php echo $set_value_pengadaan['tanggal_dipa']; ?>">
        <span class="help-block"> </span>
      </div>

    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="no_dipa">No DIPA</label>  
      <div class="col-md-4">
        <input id="no_dipa" name="no_dipa" type="text" placeholder="" class="form-control input-md" value="<?php echo $set_value_pengadaan['no_dipa']; ?>">
        <span class="help-block"> </span>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="mak">Mata Anggaran Kode</label>  
      <div class="col-md-4">
        <input id="mak" name="mak" type="text" placeholder="" class="form-control input-md" value="<?php echo $set_value_pengadaan['mak']; ?>">
        <span class="help-block">harap di isi 6 digit</span>  
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="lokasi">Lokasi Pengadaan</label>  
      <div class="col-md-10">
        <input id="lokasi" name="lokasi" type="text" placeholder="" class="form-control input-md" value="<?php echo $set_value_pengadaan['lokasi']; ?>">
        <span class="help-block"> </span>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="ta_anggaran">Tahun Anggaran</label>  
      <div class="col-md-8">
        <input id="ta_anggaran" name="ta_anggaran" type="text" placeholder="" class="form-control input-md" value="<?php echo $set_value_pengadaan['tahun']; ?>">
        <span class="help-block"> </span>
      </div>
      <div class="col-md-6"></div>
    </div>
    <br>
    <div class="form-group">
      <div class="col-md-8">
        <!-- <input type="hidden" name="id_pengadaan"value="<?php //echo $set_value_pengadaan['id_pengadaan']; ?>"> -->
        <button id="submit" name="submit" class="btn btn-primary" type="submit" value="upload">Submit</button>
      <?php endforeach; ?>
      <?php echo form_close(); ?>
      <?php echo form_open('ppk/edit_barang', 'class="btn-group inline"');?>
      <input type="hidden" name="id_pengadaan"value="<?php echo $set_value_pengadaan['id_pengadaan']; ?>">
      <button id="submit" name="submit" class="btn btn-primary" type="submit" value="upload">Edit Pengadaan</button>
      <?php echo form_close(); ?>
      
    </div>
  </div>
</div>

</div>
</div>
<!-- </div> -->
</body>
