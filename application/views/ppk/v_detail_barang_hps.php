<?php 
/**
 * @Author: Al
 * @Date:   2016-01-21 16:45:46
 * @Last Modified by:   Al
 * @Last Modified time: 2016-03-17 21:46:29
 */
?>
<body>
	<div class="col-xs-10 col-sm-10 col-md-10">
		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<tbody>
					<?php foreach ($user_detail_barang_hps as $nilai_data) {
						?>
						<tr>
							<td class="text-left">PA/KPA</td>
							<td class="text-left">Rektor UIN Maulana Malik Ibrahim Malang</td>
						</tr>
						<tr>
							<td class="text-left">K/L/D/I</td>
							<td class="text-left">UIN Maulana Malik Ibrahim Malang</td>
						</tr>
						<tr>
							<td class="text-left">Satker/SKPD</td>
							<td class="text-left"><?php echo $nilai_data['satker']; ?></td>
						</tr>
						<tr>
							<td class="text-left">PPK</td>
							<td class="text-left"><?php echo $nilai_data['ppk_name']; ?></td>
						</tr>
						<tr>
							<td class="text-left">Pekerjaan</td>
							<td class="text-left"><?php echo $nilai_data['paket_name']; ?></td>
						</tr>
						<tr>
							<td class="text-left">Lokasi</td>
							<td class="text-left"><?php echo $nilai_data['lokasi']; ?></td>
						</tr>
						<tr>
							<td class="text-left">Tahun Anggaran</td>
							<td class="text-left"><?php echo $nilai_data['tahun']; ?></td>
						</tr>
						<?php } ?>
					</tbody>

				</table>
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Nama Barang</th>
						<?php foreach ($detail_header as $value) {
							?>
							<th><?php echo $value['nama_penyedia']; ?></th>
							<?php } ?>
							<th >Harga</th>
						</tr>
					</thead>
					<tbody>
						  <?php foreach ($penawar_terendah_satu as $key => $value) {
						  	// echo $key;
						  ?>
						 
						<tr>
							<td class="text-left"><?php echo $value['nama_barang']; ?></td>
							<td><?php echo array_values($penawar_terendah_satu)[$key]['harga_tawar']; ?></td>
							<td><?php echo array_values($penawar_terendah_dua)[$key]['harga_tawar']; ?></td>
							<td><?php echo array_values($penawar_terendah_tiga)[$key]['harga_tawar']; ?></td>
							<td><?php echo array_values($total_tawar)[$key]/3; ?></td>
						</tr>
						<?php } ?>
					</tbody>

				</table>
			</div>
		</div>                  

	</body>


