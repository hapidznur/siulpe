<?php
/**
 * @Author: Al
 * @Date:   2016-02-27 13:11:23
 * @Last Modified by:   Al
 * @Last Modified time: 2016-02-27 19:23:52
 */
?>
<div class="col-xs-10 col-sm-10 col-md-10">
	<div class="table-responsive">
		<table class="table table-hover table-responsive table-bordered table-pengadaan">
			<thead>
				<tr>
					<th>No</th>
					<th>Proses</th>
					<th>Status</th>
					<th>Tanggal Mulai</th>
					<th>Tanggal Akhir</th>
				</tr>
			</thead>
			<tbody>
				<?php
				// print_r($timeline_pengadaan);

				 foreach ($timeline_pengadaan as $key => $value) {
				 	for ($i=1; $i < 21 ; $i++) {
					 ?>
					<tr>
					<td><?php echo $i;?></td>
					<td><?php echo $tahap_pengadaan[$i];?></td>
					<td><?php echo $value['status_pengadaan']; ?></td>
					<td><?php echo $value['p_'.$i.'_mulai']; ?></td>
					<td><?php echo $value['p_'.$i.'_akhir']; ?></td>
					</tr>
				<?php 	}
				}	 ?>

				
			</tbody>
		</table>
	</div><!--end table-responsive -->
</div><!--end col-xs-10 col-sm-10 col-md-10-->
</div> <!--end row-->
</div> <!--end container-fluid content-->