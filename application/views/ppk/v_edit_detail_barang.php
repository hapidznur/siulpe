<?php
/**
 * @Author: elfarqy
 * @Date:   2015-11-27 21:08:37
 * @Last Modified by:   Al
 * @Last Modified time: 2015-11-27 21:15:17
 */
?>
<body>
 <div class="col-xs-10 col-sm-10 col-md-10">
  <div class="panel panel-default">
    <div class="table-responsive">
    <table class="table table-barang">
      <thead>
        <tr>
          <th>Nama Barang</th>
          <th>Spesifikasi</th>
          <th>Volume</th>
          <th>Satuan</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($barang_sendiri as $detail_barang) : ?>
        <tr>
          <td><?php echo $detail_barang['nama_barang']; ?></td>
          <td><?php echo $detail_barang['spesifikasi']; ?></td>
          <td><?php echo $detail_barang['volume']; ?></td>
           <td><?php echo $detail_barang['satuan']; ?></td>
        </tr>

      </tbody>
    <?php endforeach; ?>
      
    </table>
    <?php 
          // echo $halaman;
            ?>
    </div>
  </div>
</div>                  

</body>


