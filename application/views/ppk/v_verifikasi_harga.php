<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Pengadaan
    </h1>
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Pengadaan tersedia</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
      <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <tr>
                <th class="col-md-6">Nama Pengadaan</th>
                <th class="col-md-3">Lokasi</th>
                <th class="col-md-3" colspan="2">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($pengadaan_sendiri as $count_data):
            ?>
          <tr>
            <td><?php echo $count_data['paket_name']; ?></td>
            <td><?php echo $count_data['lokasi']; ?></td>
            <td id="action">
              <p>
                <a href="<?php echo base_url().'ppk/detail_pengadaan/'.$count_data['id_pengadaan'];?>" class="btn btn-default" > Detail</a>
                <a href="<?php echo base_url().'ppk/verifikasi_penyedia_yang_menawar/'.$count_data['id_pengadaan'];?>" class="btn btn-default" > Verifikasi Pengadaan</a>
              </p></td>
            </tr>
            <?php
            endforeach;
            ?>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->