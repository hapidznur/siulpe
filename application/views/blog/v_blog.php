<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/_site/bootstrap/css/bootstrap_theme.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/_site/bootstrap/css/normalize.css')?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/_site/bootstrap/css/style.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/_site/bootstrap/css/bootstrap.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/_site/bootstrap/css/simple-sidebar.css');?>">
	<title>Pengadaan</title>
	<style type="text/css">
	th{
		text-align: center;
	}
	</style>
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="blog-masthead">
				<div class="row">
					<div class="col-sm-12">
						<div class="blog-header text-center">
							<h1 class="blog-title">Unit Layanan Pengadaan</h1>
							<p class="lead blog-description">Pengadaan yang Kredibel Sejahterakan Bangsa</p>
						</div>
					</div>


				</div>
			</div>
			<div class="col-sm-3 blog-sidebar">
				<div class="sidebar-module sidebar-module-inset">
					<h4>About</h4>
					<p>Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
				</div>
				<div class="sidebar-module">
					<h4>Accordion menu here..</h4>
					<ol class="list-unstyled" id="accordion" role="tablist" aria-multiselectable="true">
						<li><ol class=list-unstyled role="tab" id="headingOne">
							<h5><a href="">Regulasi</a></h5>
							<li><a href="#">Undang Undang</a></li>
							<li><a href="#">PP</a></li>
							<li><a href="#">Perpres</a></li>
							<li><a href="#">Lain Lain</a></li>
						</ol></li>
						<li><ol class=list-unstyled>
							<h5>Sitemap</h5>
							<li><a href="#">Pengumuman</a></li>
							<li><a href="#">Berita</a></li>
							<li><a href="#">Petunjuk penggunaan</a></li>
							<li><a href="#">SOP</a></li>
							<li><a href="#">Rencana Umum</a></li>
							<li><a href="#">Alur Pengadaan</a></li>
						</ol></li>
						<li><ol class=list-unstyled>
							<h5>Tentang Kami</h5>
							<li><a href="#">Profil</a></li>
							<li><a href="#">Visi</a></li>
							<li><a href="#">Misi</a></li>
							<li><a href="#">Struktur</a></li>
							<li><a href="#">Tupoksi</a></li>
						</ol></li>
						<li><h5><a href="">Hubungi kami</a></h5></li>
						<li><h5><a href="">Daftar hitam</a></h5></li>
						<li><h5><a href="">Login</a></h5></li>

					</ol>
				</div>

			</div><!-- /.blog-main -->

			<div class="col-sm-8 blog-main">
				<div class="blog-post">
					<?php echo form_open_multipart('upload/do_upload');?>
					<input type="file" id="file_upload" name="userfile" size="20" />
					<br />
					<input type="submit" value="Upload" />
					<?php echo form_close();?>
					
				</div><!-- /.blog-post -->

				<nav>
					<ul class="pager">
						<li><a href="#">Previous</a></li>
						<li><a href="#">Next</a></li>
					</ul>
				</nav>
			</div><!-- /.row -->

		</div><!-- /.blog-sidebar -->


		<footer class="blog-footer">
			<p>Copyright 2015</p>
			<p>
				<a href="#">Back to top</a>
			</p>
		</footer>
	</div>




    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
     <!--<script src="<?php echo base_url('ext/_site/bootstrap/js/jquery.min.js');?>    "></script>
     <script src="<?php echo base_url('ext/_site/bootstrap/js/bootstrap.min.js');?>"></script>-->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>


</body>
</html>
