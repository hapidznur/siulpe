@extends('template')

@section('title', 'Konfirmasi Pembayaran')

@section('body')
  <div class="container">
    <div class="gap-small">
    </div>
    <h2>Konfirmasi Pembayaran</h2>
    <form class="" action="{{base_url("konfirmasi-pembayaran/process")}}" method="post" enctype="multipart/form-data">
      <div class="row">
        <div class="box">
            <div class="col-md-4">
              <div class="form-group">
                <label>Kode Invoice</label>
                <input type="text" name="nomor_invoice" placeholder="Masukkan kode invoice" class="form-control" value="{{@$controller->session->flashdata('old')['nomor_invoice']}}" required="">
              </div>
              <div class="form-group">
                <label>Nama Pengirim</label>
                <input type="text" name="nama" placeholder="Nama pengirim" class="form-control" value="{{@$controller->session->flashdata('old')['nama']}}" required="">
              </div>
              <div class="form-group">
                <label>Email</label>
                <input type="email" name="email" placeholder="Email pengirim" class="form-control" value="{{@$controller->session->flashdata('old')['email']}}" required="">
              </div>
              <div class="form-group">
                <label>Bank Pengirim</label>
                <input type="text" name="bank" placeholder="Nama bank yang digunakan untuk transfer" class="form-control" value="{{@$controller->session->flashdata('old')['bank']}}" required="">
              </div>
              <div class="form-group">
                <label>Rekening Pengirim</label>
                <input type="text" name="rekening_pengirim" placeholder="No rekening anda" class="form-control" value="{{@$controller->session->flashdata('old')['rekening_pengirim']}}" required="">
              </div>
              <div class="form-group">
                <label>Total Transfer</label>
                <input type="number" min="0" name="total_transfer" placeholder="Total Transfer" class="form-control" required="">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Tanggal Pembayaran</label>
                <input type="text" name="tanggal_pembayaran" placeholder="Tanggal pembayaran" class="date-pick form-control" required="" autocomplete="off">
              </div>
              <div class="form-group">
                <label>Bukti Transfer</label>
                <img id="img-konfirmasi" src="http://nemanjakovacevic.net/wp-content/uploads/2013/07/placeholder.png" alt="" />
                <p><span><strong>(Ukuran maksimal gambar 5MB)</strong></span></p>
                <input id="input-img-konfirmasi" type="file" name="img_konfirmasi" style="margin-top: 10px; display: block" required="">
              </div>
            </div>
            <div class="col-md-4">
              <div style="width: 302px">
                <div class="form-group">
                  <label>Recaptcha</label>
                  {{-- <div class="g-recaptcha" data-sitekey="6LdTCyETAAAAAC2oAzUCfdXMI_J6zJ_OIIM-IyBG"></div> --}}
                  <img src="{{$captcha->inline()}}" style="width: 180px" /><br><br>
                  <input type="text" name="captcha" class="form-control" placeholder="Enter the captcha code" required="">
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-lg" style="float: right">Submit</button>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
        </div>
      </div>
    </form>
  </div>
@endsection

@section('modal')
  @if($controller->session->flashdata('errors'))
    <div id="myModal" class="modal modal-errors fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pesan</h4>
          </div>
          <div class="modal-body">
            <div class="alert alert-danger">
              @foreach($controller->session->flashdata('errors') as $key => $errors)
                @foreach($errors as $key => $error)
                  <p>
                    {!!$error!!}
                  </p>
                @endforeach
              @endforeach
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>
  @endif
@endsection

@section('script')
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <script src="{{base_url("assets/js/aksa.js")}}" charset="utf-8"></script>
  <script type="text/javascript">
  ImageReader
    .init({
      'img': $('#img-konfirmasi'),
      'input': $('#input-img-konfirmasi')
    })
    .commit();

    @if ($controller->session->flashdata('errors'))
      $('.modal.modal-errors').modal('show');
    @endif
  </script>
@endsection
