@extends('template')

@section('title', 'Konfirmasi Success')

@section('body')
  <div class="container">
    <div class="gap"></div>
    <div class="gap"></div>
    <div class="gap"></div>
    <div class="text-center">
      <h3>Konfirmasi telah berhasil. Kami akan memberitahukan secepatnya lewat email :)</h3>
    </div>
  </div>
@endsection
