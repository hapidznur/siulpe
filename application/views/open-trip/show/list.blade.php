<div class="clearfix"></div>
<div class="col-md-12" style="float:none;">
  <div class="box nofloat" style="background:none;border:0;">
    <h4><b> {{ucwords($openTrip->masterTrip->title)}}</b></h4>
    <div class="col-md-6 nopadding">
       <div class="img-preview-list">
            @if($openTrip->garansi_berangkat)
              <div class="head-label">
                <span  style="font-size:10px;">
                      Garansi Berangkat
                 </span>
               </div>
            @endif
          <a class="hover-img" href="{{base_url("open-trip/show/{$openTrip->id}")}}">
            <img src="{{$openTrip->cover}}" alt="Image Alternative text" title="Upper Lake in New York Central Park" />
            <i class="fa fa-plus box-icon-white box-icon-border hover-icon-top-right round"></i>
          </a>
          @if($openTrip->is_closed)
            <div class="head-label-price">
              <h3 class="nomargin" style="color:#000"><b>Closed</b></h3>
            </div>
          @elseif (!$openTrip->sisa_quota)
            <div class="head-label-price">
              <b>Sold Out</b>
            </div>
          @else
            <!-- Status Sold E Ga Ero Aku -->
          @endif

        </div>
    </div>
    <div class="col-md-6">
      <table style="width:100%">
        <tr>
          <td><b>Deadline Booking</b></td>
          <td align="right"><b>{{$openTrip->tanggal_deadline_pendaftaran}}</b></td>
        </tr>
      </table>
      <div class="gap-sm"></div>
      <div class="box" style="padding:5px;">
        <h3 class="text-center nomargin" style="font-weight:400;line-height: 1;padding: 8px;">{{$openTrip->price}} <span class="text-normal">/Person </span></h3>
      </div>
      <div class="div-gap"></div>
      <div class="box">
          <p class="text-center text-small"><b>Trip Number : {{$openTrip->kode}}</b></p>
          <hr class="gap-sm">
          <table style="width:100%">
            <tr>
              <td class="text-small" width="50%"><b>Departure</b></td>
              <td class="text-small" width="50%" align="right"><b>Arrival</b></td>
            </tr>
            <tr>
              <td width="50%">
                {{$openTrip->tanggal_berangkat_manusia}} <br>
                {{$openTrip->jam_berangkat}}
              </td>
              <td width="50%" align="right">
                {{$openTrip->tanggal_pulang_manusia}}  <br>
                {{$openTrip->jam_pulang}}
              </td>
              <!-- Tanggal Kedatangan Yo Ga Nemu Cak Hud -->
            </tr>
            <tr>
              <td colspan="2"><hr class="gap-sm"></td>
            </tr>
            <tr>
              <td class="text-small" width="50%"><b>Meeting Point</b></td>
              <td class="text-small" width="50%" align="right"><b>Quota</b></td>
            </tr>
            <tr>
              <td width="50%" title="{{$openTrip->meeting_point}}">{{read_more($openTrip->meeting_point,14)}}</td>
              <td width="50%" align="right">{{$openTrip->quota_masuk}}/{{$openTrip->quota}}</td>
            </tr>
          </table>
      </div>
      <div class="div-gap"></div>
      @if($openTrip->status_open_trip == 'Ditutup')
        <a href="{{base_url("open-trip/show/{$openTrip->id}")}}" class="btn btn-danger btn-block">
          View Trip Detail
        </a>
      @else
        <a href="{{base_url("open-trip/show/{$openTrip->id}")}}" class="btn btn-primary btn-block">
          View Trip Detail
        </a>
      @endif
    </div>
    <div class="clearfix"></div>
  </div>
  <hr class="nomargin">
</div>
