<div class="col-md-3 div-gap"  >
  <div class="box nopadding" style="overflow-x:hidden">
    <div class="header">
      <b > {{ucwords($openTrip->masterTrip->title)}}</b>
    </div>
    <div class="img-preview" style="border-radius: 0px">
      @if($openTrip->garansi_berangkat)
        <div class="head-label">
          <span  style="font-size:10px;">
            Garansi Berangkat
          </span>
        </div>
      @endif
      <a class="hover-img" href="{{base_url("open-trip/show/{$openTrip->id}")}}">
        <img src="{{$openTrip->cover}}" alt="{{$openTrip->masterTrip->title}}" title="{{$openTrip->masterTrip->title}}" />
        <i class="fa fa-plus box-icon-white box-icon-border hover-icon-top-right round"></i>
      </a>
      @if($openTrip->is_closed)
        <div class="head-label-price">
          <b>Closed</b>
        </div>
      @elseif (!$openTrip->sisa_quota)
        <div class="head-label-price">
          <b>Sold Out</b>
        </div>
      @else

      @endif

    </div>
    <div class="body">
      <table style="width:100%">
        <tr>
          <td width="50%">Deadline Booking</td>
          <td width="50%" align="right">{{$openTrip->tanggal_deadline_pendaftaran}}</td>
          <!-- Cak Hud ndukur iki aku nggae tanggal berangkat manusia , aku nggolek deadline booking ga ero sing endi -->
        </tr>
      </table>
      <hr class="gap-sm">
      <h5 style="font-weight:400" class="text-center">{{$openTrip->price}} <span class="text-normal">/Person </span></h5>
      <p class="text-center text-small">Trip Number : {{$openTrip->kode}}</p>
      <hr class="gap-sm">
      <table style="width:100%">
        <tr>
          <td class="text-small" width="50%"><b>Departure</b></td>
          <td class="text-small" width="50%" align="right"><b>Arrival</b></td>
        </tr>
        <tr>
          <td width="50%">
            {{$openTrip->tanggal_berangkat_manusia}} <br>
            {{$openTrip->jam_berangkat}}
          </td>
          <td width="50%" align="right">
            {{$openTrip->tanggal_pulang_manusia}} <br>
            {{$openTrip->jam_pulang}}
          </td>
          <!-- Tanggal Kedatangan Yo Ga Nemu Cak Hud -->
        </tr>
        <tr>
          <td colspan="2"><hr class="gap-sm"></td>
        </tr>
        <tr>
          <td class="text-small" width="50%"><b>Meeting Point</b></td>
          <td class="text-small" width="50%" align="right"><b>Quota</b></td>
        </tr>
        <tr>
          <td width="50%" title="{{$openTrip->meeting_point}}">{{read_more($openTrip->meeting_point,14)}}</td>
          <td width="50%" align="right">{{$openTrip->quota_masuk}}/{{$openTrip->quota}}</td>
        </tr>
      </table>
    </div>
  </div>
  <div class="text-center gap-sm">
    @if($openTrip->status_open_trip == 'Ditutup')
      <a href="{{base_url("open-trip/show/{$openTrip->id}")}}" class="btn btn-danger btn-sm btn-block">
        View Trip Detail
      </a>
    @else
      <a href="{{base_url("open-trip/show/{$openTrip->id}")}}" class="btn btn-primary btn-sm btn-block">
        View Trip Detail
      </a>
    @endif
  </div>
</div>
