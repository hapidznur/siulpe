@extends('template')

@section('title')
  {{$openTrip->masterTrip->title}}
@endsection

@section('style')
{{-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css"> --}}
{{-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/> --}}
{{-- <link rel="stylesheet" type="text/css" href="http://kenwheeler.github.io/slick/slick/slick-theme.css"> --}}
<link href="/assets/jquery.bxslider.css" rel="stylesheet" />
@endsection

@section('body')
  <div class="container">

    <ul class="breadcrumb">
      <li><a href="{{url('/')}}">Home</a>
      </li>
      <li><a href="{{url('open-trip')}}">Open Trip</a>
      </li>
      <li><a href="#">{{$openTrip->masterTrip->title}}</a>
      </li>
    </ul>
    <div class="gap-small">
      <div class="row">
        <div class="col-md-3">
          @include('open-trip.pieces.search-sidebar')
        </div>
        <div class="col-md-9">
          <div class="booking-item-details">
            <header class="booking-item-header" style="margin-top:0">
              <div class="row">
                <div class="col-md-8">
                  <h4 class="lh1em" style="font-weight:400">{{$openTrip->masterTrip->title}}</h4>
                </div>
                <div class="col-md-4">
                  <table style="width:100%">
                    <tr>
                      <td width="50%">Deadline Booking</td>
                      <td width="50%" align="right"><b>{{$openTrip->tanggal_deadline_pendaftaran}}</b></td>
                    </tr>
                  </table>
                </div>
              </div>
            </header>
            <div class="row">
              <div class="col-md-7">
                <div class="detail" >
                <div class="tab-content">
                  @if($openTrip->garansi_berangkat)
                    <div class="head-label">
                      <span  style="font-size:14px;">
                        <b>Garansi Berangkat</b>
                      </span>
                    </div>
                  @endif
                  <ul class="covers">
                  @foreach($openTrip->covers as $key => $cover)
                    <li><img src="{{$cover->cover_src}}" alt="{{$openTrip->masterTrip->title}}" title="{{$openTrip->masterTrip->title}}" /></li>
                  @endforeach
                  </ul>
                  @if($openTrip->is_closed)
                    <div class="head-label-price">
                      <b>Closed</b>
                    </div>
                  @elseif ($openTrip->quota_masuk == $openTrip->quota)
                    <div class="head-label-price">
                      <b>Sold Out</b>
                    </div>
                  @else
                  @endif                  
                </div>
                </div>
              </div>
              <div class="col-md-5">
                <div class="box rounded-full pad-10">
                  <h3 class="bold nomargin text-center" style="text-decoration: none">{{$openTrip->price}} <span class="text-normal">/Person </span></h3>
                </div>
                <div class="box rounded-full pad-10 div-gap-20">
                  <article class="text-center">Trip Number <b>{{$openTrip->kode}}</b></article>
                  <hr class="gap-sm">
                  <table style="width:100%">
                    <tr>
                      <td class="text-small" width="50%"><b>Departure</b></td>
                      <td class="text-small" width="50%" align="right"><b>Arrival</b></td>
                    </tr>
                    <tr>
                      <td width="50%">
                        {{$openTrip->tanggal_berangkat_manusia}}<br>
                        {{$openTrip->jam_berangkat}}
                      </td>
                      <td width="50%" align="right">
                        {{$openTrip->tanggal_pulang_manusia}}<br>
                        {{$openTrip->jam_pulang}}
                      </td>
                      <!-- Tanggal Kedatangan Yo Ga Nemu Cak Hud -->
                    </tr>
                    <tr>
                      <td colspan="2"><hr class="gap-sm"></td>
                    </tr>
                    <tr>
                      <td class="text-small" width="50%"><b>Meeting Point</b></td>
                      <td class="text-small" width="50%" align="right"><b>Quota</b></td>
                    </tr>
                    <tr>
                      <td width="50%" title="{{$openTrip->meeting_point}}">{{read_more($openTrip->meeting_point,20)}}</td>
                      <td width="50%" align="right">{{$openTrip->quota_masuk}}/{{$openTrip->quota}}</td>
                    </tr>
                  </table>
                </div>
                <div>
                  @if($openTrip->status !== 'ditutup' && $openTrip->sisa_quota)
                    <a href="{{base_url("open-trip/book/{$openTrip->id}")}}" class="btn btn-primary btn-block">Booking</a>
                  @else
                    <button class="btn btn-default btn-block" disabled="">Ditutup</button>
                  @endif
                  <a href="{{$openTrip->blog_link}}" class="btn btn-primary btn-block"><i class="fa fa-external-link"></i> All About This Trip</a>
                </div>
              </div>
            </div>
            {{-- <div class="gap gap-small"></div> --}}
            <div class="col-md-12 nopadding box-tab-ku box rounded-full">
              <div class="tabbable">
                <ul class="nav nav-tabs tabs-ku">
                  <li>
                    <a data-toggle="tab" href="#home" id="itenary">
                      Itinerary
                    </a>
                  </li>
                  <li>
                    <a data-toggle="tab" href="#fasilitas">
                      Facilities
                    </a>
                  </li>
                  <li>
                    <a data-toggle="tab" href="#info-tambahan">
                      Additional Information
                    </a>
                  </li>
                  <li>
                    <a data-toggle="tab" href="#menu2">
                      Term of Services
                    </a>
                  </li>
                </ul>
                <div class="tab-content-parent">
                  <div class="tab-content">
                    <div id="home" class="tab-pane fade ">
                      <article>
                        @if($openTrip->masterTrip->itenary_file)
                          <a href="{{$openTrip->masterTrip->itenary_file_src}}" target="_blank" class="btn btn-default"><i class="fa fa-download"></i> Download Itinerary</a>
                        @endif
                        <br><br>
                        {!!$openTrip->masterTrip->itenary!!}
                      </article>
                    </div>
                    <div id="fasilitas" class="tab-pane fade">
                      <article>
                        {!!$openTrip->masterTrip->fasilitas!!}
                      </article>
                    </div>
                    <div id="info-tambahan" class="tab-pane fade">
                      <article>
                        {!!$openTrip->masterTrip->additional_information!!}
                      </article>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                      <article>
                        {!!$openTrip->masterTrip->term_of_service!!}
                      </article>
                    </div>
                  </div>
                </div>

              </div>
            </div>

          </div>
          <div class="gap gap-small"></div>
          <div class="show-on-small">
            @foreach($leftBarAds as $key => $ads)
              <div class="" style="margin-top: 10px">
                @if($ads->jenis === 'gambar')
                  <img src="{{$ads->image_src}}" style="width: 100%" alt="{{$ads->nama}}" />
                @else
                  {!!$ads->script!!}
                @endif
              </div>
            @endforeach
          </div>
          <div class="">
            @foreach($bottomBodyAds as $key => $ads)
              <div class="" style="margin-top: 10px">
                @if($ads->jenis === 'gambar')
                  <img src="{{$ads->image_src}}" style="max-width: 100%" alt="{{$ads->nama}}" />
                @else
                  {!!$ads->script!!}
                @endif
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection


@section('script')
<script type="text/javascript">
  (function () {
    $('#itenary').click()
  })()
</script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.min.js"></script> --}}
{{-- <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script> --}}
<script src="/assets/jquery.bxslider.min.js"></script>
<script type="text/javascript">
  $('ul.covers').bxSlider();
</script>
@endsection