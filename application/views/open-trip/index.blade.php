@extends('template')

@section('title', 'Compas Petualang | Open Trip')

@section('body')

          <div class="container">
              {{-- <ul class="breadcrumb">
                  <li><a href="{{url('')}}">Home</a>
                  </li>
                  <li><a href="#">Open Trip</a>
                  </li>
              </ul> --}}
              @include('open-trip.pieces.search-modal')
              <div class="gap-small">

              </div>
              <div class="row">
                  {{-- <div class="col-md-3">
                    @include('open-trip.pieces.search-sidebar')
                  </div> --}}
                  <div class="">
                    <form id="form-search-open-trip"></form>
                    <div class="col-xs-8" style="padding-right: 0">
                      <input form="form-search-open-trip" type="text" name="destination" class="form-control input-lg" placeholder="Type keyword..." value="{{$controller->input->get('destination')}}">
                    </div>
                    <div class="col-xs-3" style="padding-left: 0; padding-right: 0">
                      <select class="form-control input-lg" form="form-search-open-trip" name="month">
                        <option value="1" {{@$monthActive === 1 ? 'selected' : ''}}>Januari</option>
                        <option value="2" {{@$monthActive === 2 ? 'selected' : ''}}>Februari</option>
                        <option value="3" {{@$monthActive === 3 ? 'selected' : ''}}>Maret</option>
                        <option value="4" {{@$monthActive === 4 ? 'selected' : ''}}>April</option>
                        <option value="5" {{@$monthActive === 5 ? 'selected' : ''}}>Mei</option>
                        <option value="6" {{@$monthActive === 6 ? 'selected' : ''}}>Juni</option>
                        <option value="7" {{@$monthActive === 7 ? 'selected' : ''}}>Juli</option>
                        <option value="8" {{@$monthActive === 8 ? 'selected' : ''}}>Agustus</option>
                        <option value="9" {{@$monthActive === 9 ? 'selected' : ''}}>September</option>
                        <option value="10" {{@$monthActive === 10 ? 'selected' : ''}}>Oktober</option>
                        <option value="11" {{@$monthActive === 11 ? 'selected' : ''}}>Nopember</option>
                        <option value="12" {{@$monthActive === 12 ? 'selected' : ''}}>Desember</option>
                      </select>
                    </div>
                    <div class="col-xs-1" style="padding-left: 0">
                      <button class="btn btn-lg btn-default" type="submit" form='form-search-open-trip'><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                  <div class="col-md-12" id="open-trip-app">
                        <div class="row" id="open-trip-list-ndek-kene">
                          @if(count($openTrips) > 0)
                            @foreach($openTrips as $key => $openTrip)
                               @include('open-trip.show.thumbnails')
                               {{-- @if($view !== 'list')
                               @else
                                 @include('open-trip.show.list')
                               @endif --}}
                            @endforeach
                          @else
                            <div class="col-md-12">
                              <div class="box text-center" style="margin-top: 30px">
                                Trip tidak ditemukan.
                              </div>
                            </div>
                          @endif
                          
                        </div>
                      <div class="row div-gap">
                          {{-- <div class="col-md-12">
                              <ul class="pagination">
                                  @if($minimumPage > 1)
                                    <li class="previous"><a page="{{$page - 1}}">Previous Page</a>
                                    </li>
                                  @endif
                                  @for($i=$minimumPage; $i <= $maximumPage; $i++)
                                    <li class="{{$page == $i ? 'active':''}}"><a page="{{$i}}">{{$i}}</a></li>
                                  @endfor
                                  @if($maximumPage < $pageCount)
                                    <li class="dots">...</li>
                                    <li><a page="{{$pageCount}}">{{$pageCount}}</a></li>
                                    <li class="next"><a page="{{$page + 1}}">Next Page</a>
                                    </li>
                                  @endif
                              </ul>
                          </div> --}}
                          {{-- <div class="col-md-6 text-right">
                              <p>Not what you're looking for? <a class="popup-text" href="#search-dialog" data-effect="mfp-zoom-out">Try your search again</a>
                              </p>
                          </div> --}}
                      </div>
                      <div class="gap"></div>
                      <div class="show-on-small">
                        @foreach($leftBarAds as $key => $ads)
                          <div class="" style="margin-top: 10px">
                            @if($ads->jenis === 'gambar')
                              <img src="{{$ads->image_src}}" style="width: 100%" alt="{{$ads->nama}}" />
                            @else
                              {!!$ads->script!!}
                            @endif
                          </div>
                        @endforeach
                      </div>
                      <div class="">
                        @foreach($bottomBodyAds as $key => $ads)
                          <div class="" style="margin-top: 10px">
                            @if($ads->jenis === 'gambar')
                              <img src="{{$ads->image_src}}" style="max-width: 100%" alt="{{$ads->nama}}" />
                            @else
                              {!!$ads->script!!}
                            @endif
                          </div>
                        @endforeach
                      </div>
                  </div>
              </div>
          </div>

@endsection

@section('script')
  <script src="{{base_url('assets/js/regional.js')}}" charset="utf-8"></script>
  {{-- <script src="{{base_url('assets/js/admin/new-master-trip.js')}}" charset="utf-8"></script> --}}
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <script src="https://cdn.jsdelivr.net/vue.resource/1.2.0/vue-resource.min.js"></script>
  <script>
    (function () {

      Regional
        .init()
        .provinsi($('[name=provinsi]'), '{{$controller->input->get('provinsi')}}', true)
        .kota($('[name=kota]'), '{{$controller->input->get('kota')}}', true)
        .grab();

      $('.pagination a[page]').click(paginate);

      function paginate() {
        var page = $(this).attr('page')
        $('input[name=page]').val(page)
        $('#search-open-trip-form').submit()
      }

      $('.sort-by').click(function () {
        var form = $('#search-open-trip-form')
        var value = $(this).attr('value')
        form.find('input[name=sort_by]').val(value)
        form.submit()
      })

      $('.view').click(function () {
        var form = $('#search-open-trip-form')
        var value = $(this).attr('value')
        form.find('input[name=view]').val(value)
        form.submit()
      })
    })();
  </script>

  <script type="text/javascript">

    var app = new Vue({
      el: '#open-trip-app',
      data: {
        page: 2,
        openTrips: [],
        destination: '{{$controller->input->get('destination')}}',
        month: '{{$monthActive}}',
        loading: false
      },
      methods: {
        getOpenTrips: function () {
          this.loading = true
          this.$http.get('/open-trip', {params: {page: this.page++, destination: this.destination, month: this.month, pieces: 'tes'}}).then((response) => {
            this.loading = false
            $('#open-trip-list-ndek-kene').append(response.body)
          })
        }
      }
    })

    $(window).scroll(function(){
      if  ($(window).scrollTop() >= $(document).height() - $(window).height() - 400){
        if (!app.loading){
          app.getOpenTrips()
        }
      }
    }); 
  </script>
@endsection
