@extends('template')

@section('title', 'Compas Petualang | Kuota Penuh')

@section('body')
  <div class="container">
    <div class="text-center" style="margin-top: 100px">
      <div class="alert alert-danger">
        <h1>Kuota Penuh</h1>
      </div>
    </div>
  </div>
@endsection
