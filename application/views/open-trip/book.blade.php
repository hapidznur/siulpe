@extends('template')

@section('title', "Booking {$openTrip->masterTrip->title}")

@section('body')
<div class="container">
  <div class="gap">
    <h2>{{$openTrip->masterTrip->title}}, <small>{{$openTrip->masterTrip->destination}}</small></h2>
  </div>
  <div class="box">
    <form class="form" action="{{base_url("open-trip/book-process/{$openTrip->id}")}}" method="post">
      @if($controller->session->errors)
        <div class="alert alert-danger">
          Messages
          <ul>
            @foreach($controller->session->errors as $key => $errors)
              @foreach($errors as $key => $error)
                <li>{{$error}}</li>
              @endforeach
            @endforeach
          </ul>
        </div>
      @endif
      @if($controller->session->message)
        <div class="alert alert-success">
          {{$controller->session->message}}
        </div>
      @endif
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" class="form-control" placeholder="Nama" value="{{old('nama')}}" required="">
          </div>
          <div class="form-group">
            <label>No TLP</label>
            <input type="text" name="no_tlp" placeholder="No telepon" class="form-control" value="{{old('no_tlp')}}" required="">
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" placeholder="email" class="form-control" value="{{old('email')}}" required="">
          </div>
          <div class="form-group">
            <label>Kota Tinggal</label>
            <input name="alamat" class="form-control" placeholder="Kota tinggal" value="{{old('alamat')}}" required>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Jumlah Peserta</label>
            <input type="number" name="jumlah_peserta" placeholder="Jumlah Peserta" min="1" max="{{$openTrip->sisa_quota}}" class="form-control" required="">
          </div>
          <div id="kontainer-peserta">

          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Syarat dan Ketentuan</label>
            <div class="checkbox">
                <label>
                  <input class="i-check" name="syarat_dan_ketentuan" type="checkbox" />
                  <p style="padding-left: 15px">
                    Saya menyetujui <a target="_blank" href="{{url('help/syarat-dan-ketentuan')}}">syarat dan ketentuan</a> dari compas petualang
                  </p>
                </label>
            </div>
          </div>
          <div class="form-group">
            {{-- <div class="g-recaptcha" data-sitekey="6LdTCyETAAAAAC2oAzUCfdXMI_J6zJ_OIIM-IyBG"></div> --}}
            <img src="{{$captcha->inline()}}" style="width: 180px" /><br><br>
            <input type="text" name="captcha" class="form-control" placeholder="Enter the captcha code" required="">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary btn-lg" style="float: right">Booking</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection

@section('script')
  <script src="{{base_url('assets/js/regional.js')}}" charset="utf-8"></script>
  <script>
    Regional
      .init()
      .provinsi('select[name=provinsi]')
      .kota('select[name=kota]')
      .kecamatan('select[name=kecamatan]')
      .grab();
  </script>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <script src="{{url('assets/js/open-trip/booking.js')}}" charset="utf-8"></script>
@endsection
