
<aside class="box search-sidebar">
  <form  class="text-white" id="search-open-trip-form" class="" action="{{url("open-trip")}}" method="get">
      <ul class="list booking-filters-list">
          <input type="hidden" name="page" value="{{@$page ? $page : 1}}">
          <input type="hidden" name="sort_by" value="{{@$sortBy}}">
          <input type="hidden" name="view" value="{{@$view}}">
          <li>
              <h5 class="booking-filters-title">Price</h5>
              <input type="text" id="price-slider" name="price_range" from="{{$min}}" to="{{$max}}">
          </li>
          {{-- <li>
            <h5 class="booking-filters-title">Provinsi</h5>
            <select class="form-control" name="provinsi">
            </select>
          </li>
          <li>
            <h5 class="booking-filters-title">Kota</h5>
            <select class="form-control" name="kota">
            </select>
          </li> --}}
          <li>
            <h5 class="booking-filters-title">Destination</h5>
            <input type="text" name="destination" placeholder="Ex: Bromo, Baling" class="form-control" value="{{$controller->input->get('destination')}}">
          </li>
          <li>
            <h5 class="booking-filters-title">Start date</h5>
            <input type="text" name="start_date" placeholder="Start date" value="{{$controller->input->get('start_date')}}" class="date-pick form-control">
          </li>
          <li>
            <h5 class="booking-filters-title">End date</h5>
            <input type="text" name="end_date" placeholder="End date" value="{{$controller->input->get('end_date')}}" class="date-pick form-control">
          </li>
      </ul>
      </form>
</aside>

<div style="margin-top:10px;">
  <button form="search-open-trip-form" type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Search</button>
</div>


<aside class="box" style="margin-top: 10px">
  <article class="text-center"><b>Filter by activities</b></article>
  <hr>
  @foreach($categories as $key => $category)
    <div class="checkbox">
      <label>
        <input form="search-open-trip-form" type="checkbox" name="categories[]" value="{{$category->id}}" {{@$categoriesSelected[$key] ? 'checked':''}}>
        {{$category->nama}}
      </label>
    </div>
  @endforeach
</aside>

<aside class="hidden-on-small">
  @foreach($leftBarAds as $key => $ads)
    <div class="" style="margin-top: 10px">
      @if($ads->jenis === 'gambar')
        <img src="{{$ads->image_src}}" style="width: 100%" alt="{{$ads->nama}}" />
      @else
        {!!$ads->script!!}
      @endif
    </div>
  @endforeach
</aside>
