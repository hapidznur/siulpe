@extends('template')

@section('title')
Compas Petualang | Gallery
@endsection

@section('body')
<div class="container" style="margin-top: 20px">
  <div class="text-center" style="margin-bottom: 20px">
   <div class="col-md-4" style="margin-left:auto;margin-right:auto;float:none">
      <table  style="width:100%">
        <tr>
          <td style="padding:15px;width:50%"  align="center">
            <a href="{{url('gallery/instagram')}}" class="tombol-gallery  active" >
              IG
            </a>
            Photo
          </td>
          <td  style="padding:15px;width:50%;" align="center">
             <a href="{{url('gallery/flickr')}}" class="tombol-gallery">Flickr</a>
             Album
          </td>
        </tr>
      </table>
    </div>
    <div class="clearfix"></div>
  </div>
  <h3 style="margin-bottom: 20px">Instagram</h3>
  <div class="row">
    @if(isset($images->data))
      @foreach($images->data as $key => $item)
        <div class="col-md-3">
          <div class="image-gallery">
            <a class="image-popup-no-margins" href="{{$item->images->standard_resolution->url}}"><img src="{{$item->images->standard_resolution->url}}"/></a>
          </div>
        </div>
      @endforeach
    @else
      <div class="col-md-12">
        <div class="box text-center" style="margin-top: 30px">
          <h4>
            Tidak ada gambar.
          </h4>
        </div>
      </div>
    @endif
  </div>
</div>
@endsection

@section('script')
  <script type="text/javascript">
    $("a.gallery-show").fancybox();
  </script>
  <script type="text/javascript">
  $('.image-popup-no-margins').magnificPopup({
    type: 'image',
    closeOnContentClick: true,
    closeBtnInside: false,
    fixedContentPos: true,
    mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
    image: {
      verticalFit: true
    },
    zoom: {
      enabled: true,
      duration: 300 // don't foget to change the duration also in CSS
    }
  });
  </script>
@endsection
