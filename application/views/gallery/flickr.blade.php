@extends('template')

@section('title')
Compas Petualang | Gallery
@endsection

@section('body')
<div class="container" style="margin-top: 20px">
  <div class="text-center" style="margin-bottom: 20px">
    <div class="col-md-4" style="margin-left:auto;margin-right:auto;float:none">
      <table  style="width:100%">
        <tr>
          <td style="padding:15px;width:50%"  align="center">
            <a href="{{url('gallery/instagram')}}" class="tombol-gallery" >
              IG
            </a>
            Photo
          </td>
          <td  style="padding:15px;width:50%;" align="center">
             <a href="{{url('gallery/flickr')}}" class="tombol-gallery active">Flickr</a>
             Album
          </td>
        </tr>
      </table>
    </div>
    <div class="clearfix"></div>
  </div>
  <h3 style="margin-bottom: 20px">Flickr</h3>
  <div class="row">
    @foreach($albums as $key => $album)
      <div class="col-md-3">
        <div class="image-gallery">
          <a href="{{url("gallery/flickr/album?photoset_id={$album->id}")}}">
            <img src="{{$album->cover}}"/>
          </a>
          <p>
            {{$album->title}}
          </p>
        </div>
      </div>
    @endforeach

    @if(!count($albums))
      <div class="col-md-12" style="margin-top: 30px">
        <div class="box text-center">
          Tidak ada gambar.
        </div>
      </div>
    @endif
  </div>
</div>
@endsection
