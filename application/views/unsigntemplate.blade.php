<!DOCTYPE HTML>
<html>

<head>
    <title>@yield('title')</title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->

    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" type="text/css" href="{{base_url('css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{base_url('css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" type="text/css" href="{{base_url('css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{ base_url('Admin-lte/dist/css/AdminLTE.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{base_url('plugins/iCheck/minimal/blue.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    @yield('style')

</head>

<body class="hold-transition login-page" style="height: auto">
        @yield('pieces')
        @yield('body')
        <!-- jQuery 2.2.3 -->
        <script src="{{base_url('js/jquery.min.js')}}"></script>
        <script src="{{base_url('js/jquery-ui.min.js')}}"></script>
        <script src="{{base_url('js/bootstrap.min.js')}}"></script>
        <script src="{{base_url('plugins/iCheck/icheck.min.js')}}"></script>

        @yield('script')
</body>

</html>
