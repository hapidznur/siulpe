<!-- TOP AREA -->
<div class="top-area show-onload">
    <div class="bg-holder full">
        <div class="bg-mask" style="background:#fff"></div>
        <div class="bg-parallax" {{--style="background-image:url('/assets/images/home-background.jpg');"--}}></div>
        <div class="bg-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 nofloat margin-center">
                        <div class="search-tabs search-tabs-bg mt50">
                            {{-- <div class="logo-head">&nbsp;</div> --}}
                            <div class="text-center">
                              @foreach($categories as $key => $category)
                                <a href="{{url("open-trip/?categories[]={$category->id}")}}">
                                  <img data-toglle='tooltip' title="{{$category->nama}}" src="{{$category->icon_src}}" class="img-circle" style="width: 50px; height: 50px; object-fit: cover; margin: 2px" alt="{{$category->nama}}" />
                                </a>
                              @endforeach
                            </div>
                            <h2 class="text-center" style="font-weight:400">
                                    Find Your Perfect Trip
                            </h2>
                            <div class="box-trip" style="background:#2E3094">
                                <div class="pad-30">
                                    <form action="{{url("open-trip")}}">
                                        <div class="form-group form-group-lg">
                                            <label>Where are you going?</label>
                                            <div class="row">
                                              <div class="col-md-12">
                                                <input name="destination" class="form-control" placeholder="Destination" {{--list="destination-list"--}} />
                                                <datalist id="destination-list">
                                                </datalist>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                        <label class="booking-filters-title">Start date</label>
                                                        <input type="text" name="start_date" value="{{$controller->input->get('start_date')}}" class="date-pick form-control" placeholder="Start date">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                        <label class="booking-filters-title">End date</label>
                                                        <input type="text" name="end_date" value="{{$controller->input->get('end_date')}}" class="date-pick form-control" placeholder="End date">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button class="btn bg-orange btn-lg btn-block" type="submit"><i class="fa fa-search"></i> Search for Trip</button>
                                        <article class="text-center text-small text-white" style="margin-top:10px;">
                                            <b>Compas Petualang</b> , Best Partner Support To Find Your Perfect Trip
                                        </article>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
