<div class="about-us-menu">
  <ul class="list-compas-ku">
    <li class=" {{@$aboutUsMenu == '' ? 'active' : ''}}">
      <a href="{{url('/')}}">Compas Petualang</a>
    </li>
    <li class="{{@$aboutUsMenu == 'about-us' ? 'active' : ''}}"><a href="{{url('about-us')}}">About Us</a></li>
    <li class="{{@$aboutUsMenu == 'contact-us' ? 'active' : ''}}"><a href="{{url('contact-us')}}">Contact Us</a></li>
    <li class="{{@$aboutUsMenu == 'legalitas' ? 'active' : ''}}"><a href="{{url('legalitas')}}">Legalitas</a></li>
    <li class="{{@$aboutUsMenu == 'faq' ? 'active' : ''}}"><a href="{{url('faq')}}">FAQ</a></li>
    <li class="{{@$aboutUsMenu == 'karir' ? 'active' : ''}}"><a href="{{url('karir')}}">Karir</a></li>
    <li class="{{@$aboutUsMenu == 'afiliasi' ? 'active' : ''}}"><a href="{{url('afiliasi')}}">Afiliasi</a></li>
  </ul>
</div>
