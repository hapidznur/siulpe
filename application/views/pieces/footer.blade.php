<footer id="main-footer">
  <div class="container">
    <div class="row row-wrap">
      <div class="col-md-4">
        <a class="logo" href="{{base_url('/')}}">
          <h3 style="color: white">
            <img src="{{base_url('assets/images/'.$identitasWeb->logo.'')}}" style="width:98%;" title="{{$identitasWeb->nama}}" alt="{{$identitasWeb->meta_deskripsi}}">
          </h3>
        </a>
        {{-- <p class="mb20">{{$identitasWeb->meta_deskripsi}}</p> --}}
        <p class="mb20 footer-link">
          <a href="{{url('about-us')}}">About Us</a>
          <a href="{{url('contact-us')}}">Contact Us</a>
          <a href="{{url('legalitas')}}">Legalitas</a>
          <a href="{{url('faq')}}">FAQ</a>
          <a href="{{url('karir')}}">Karir</a>
          <a href="{{url('afiliasi')}}">Afiliasi</a>
        </p>
      </div>

      <div class="col-md-4">
        <h4>Newsletter</h4>
        <form action="{{url("admin/subscribe-post")}}" method="post">
          <label>Enter your E-mail Address</label>
          <input type="email" class="form-control" name="email">
          <p class="mt5"><small>*We Never Send Spam</small>
          </p>
          <input type="submit" class="btn btn-primary" value="Subscribe">
        </form>
      </div>

      <div class="col-md-4">
        <h4>Contact us</h4>
        <p class="mb20">
          {{$identitasWeb->nomor_telepon}}
        </p>
        <p class="mb20">
          {{$identitasWeb->alamat}}
        </p>
        <ul class="list list-horizontal list-space">
            <li>
              <a class="fa fa-facebook box-icon-normal round animate-icon-bottom-to-top" href="{{$identitasWeb->fb}}"></a>
            </li>
            <li>
                <a class="fa fa-twitter box-icon-normal round animate-icon-bottom-to-top" href="{{$identitasWeb->twitter}}"></a>
            </li>
            <li>
                <a class="fa fa-instagram box-icon-normal round animate-icon-bottom-to-top" href="{{$identitasWeb->instagram}}"></a>
            </li>
            <li>
              <a href="mailto:{{$identitasWeb->email}}" class="fa fa-envelope box-icon-normal round animate-icon-bottom-to-top">{{$identitasWeb->email}}</a>
            </li>
        </ul>
      </div>

    </div>
  </div>
  <div class="cta">
    <ul>
      <li>
        <a title="bbm" href="http://pin.bbm.com/{{$identitasWeb->bbm}}">
          <img width="32" height="32" src="http://aksamedia.co.id/files/jasa-pembuatan-website-bbm.png" title="bbm">
        </a>
      </li>
      <li>
        <a title="contact us" href="mailto: {{$identitasWeb->email}}">
          <img width="32" height="32" src="http://aksamedia.co.id/files/jasa-pembuatan-website-mail.png" title="contact us">
        </a>
      </li>
      <li>
        <a title="call-us" href="tel://{{$identitasWeb->nomor_telepon}}">
          <img style="width:32px;height:32px;" src="http://aksamedia.co.id/files/jasa-pembuatan-website-phone-red.png" title="call us">
        </a>
      </li>
      <li>
        <a title="call-us" href="intent://send/{{$identitasWeb->wa}}#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end">
          <img style="width:32px;height:32px;" src="{{url('assets/images/whatsapp-transparent.png')}}" title="call us">
        </a>
      </li>
    </ul>
  </div>
</footer>
