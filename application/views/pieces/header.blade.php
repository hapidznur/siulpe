<header id="main-header" style="background:#fff">
    <div class="header-top text-center" style="overflow: hidden; max-height: 120px;">
      <a href="{{base_url('/')}}" style="display: inline-block; margin: 10px;">
        <img src="{{base_url('assets/images/'.$identitasWeb->logo.'')}}" style="display: inline-block;max-width:400px;" title="{{$identitasWeb->nama}}" alt="{{$identitasWeb->meta_deskripsi}}">
      </a>
    </div>
    <div class="container">
        <div class="nav">
            <ul class="slimmenu col-md-7 nofloat">
                <li {{-- class="{{@$menuActive == 'home' ? 'active': ''}}" --}}><a href="{{base_url('/')}}"><i class="fa fa-home"></i> Home</a></li>
                {{-- <li class="{{@$menuActive == 'open-trip' ? 'active': ''}}"><a href="{{base_url('open-trip')}}">Open Trip</a></li> --}}
                <li {{-- class="{{@$menuActive == 'konfirmasi-pembayaran' ? 'active': ''}}" --}}><a href="{{base_url('konfirmasi-pembayaran')}}">Payment Confirmation</a></li>
                <li {{-- class="{{@$menuActive == 'gallery' ? 'active': ''}}" --}}><a href="{{base_url('gallery')}}">Gallery</a></li>
                <li {{-- class="{{@$menuActive == 'blog' ? 'active': ''}}" --}}><a href="{{base_url('blog')}}">Blog</a></li>
            </ul>
        </div>
    </div>
</header>
