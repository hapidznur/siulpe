<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pengadaan
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
<table class="table table-bordered">
<thead>
	<tr>
	<th>Nama penyedia</th>
	<th>Nama pemilik</th>
	<th>No telepon</th>
	<th>No NPWP</th>
	<th colspan="3">Aksi</th>
	</tr>
</thead>
<tbody>
	<?php foreach ($penyedia_baru as $key => $value) {
	  echo form_open('ulp/penyedia_terverifikasi');?>
	<tr>
  <input type="hidden" name="id_penyedia" value=<?php echo $value['id_penyedia']; ?>>
		<td><?php echo $value['nama_penyedia']; ?></td>
		<td><?php echo $value['pemilik']; ?></td>
		<td><?php echo $value['contact']; ?></td>
		<td><?php echo $value['no_npwp']; ?></td>
		<td class="text-center"><input type="submit" value="Verifikasi" class="btn btn-success"></td><?php  echo form_close();  ?>
		<?php echo form_open('ulp/detail_penyedia'); ?>
    <input type="hidden" name="id_penyedia" value=<?php echo $value['id_penyedia']; ?>>
    <td class="text-center"><input type="submit" value="Detail Penyedia" class="btn btn-info"></td>
    <?php echo form_close(); ?>
    <?php echo form_open('ulp/tolak_penyedia'); ?>
    <input type="hidden" name="email" value=<?php echo $value['email']; ?>>
    <input type="hidden" name="nama_penyedia" value=<?php echo $value['nama_penyedia']; ?>>
    <input type="hidden" name="pemilik" value=<?php echo $value['pemilik']; ?>>
    <td class="text-center"><input type="submit" value="Tolak Penyedia" class="btn btn-danger"></td>
    <?php echo form_close(); ?>

	</tr>
	<?php } ?>
</tbody>
</table>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->