<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pengadaan
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
			<table class="table" id="detailjadwal">
				<thead>
					<tr>
            <th>No</th>
						<th>Proses</th>
						<th>Tanggal Mulai</th>
						<th>Tanggal Akhir</th>
            <th>Action</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="modal_jadwal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><label>Proses</label> <label class="prosesbisnis"></label></h4>
      </div>
      <div class="modal-body">
        <p>
          <label for="edit">Edit Tanggal Proses</label>
        </p>
        <p>
      <form action="#" id="form">
        <div class="form-group">
            <label for="edit">Tanggal Mulai</label>
            <div class="input-group">
            <input type="text" class="form-control datepicker" name="mulai" id="mulai" readonly="readonly" placeholder="Tanggal">
          </div>
        </div>
        <div class="form-group">
            <label for="edit">Tanggal Akhir</label>
            <div class="input-group">
            <input type="text" name="akhir" class="form-control datepicker" id="akhir" readonly="readonly" placeholder="Tanggal">
          </div>
        </div>
        </p>
      </div>
      <input type="hidden" name="id" id="id">
      <input type="hidden" name="proses" id="proses">
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" onClick="simpan()" id="btnSave">Save changes</button>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->