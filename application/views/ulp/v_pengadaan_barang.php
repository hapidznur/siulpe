<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pengadaan
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <table>
      <?php foreach ($detail_pengadaan as $value) {
        print_r($value);
       ?>
      <th class="text-nowrap"><h3>Detail Paket Pengadaan</h3></th>
      <tr><td class="text-justify" >Nama Paket</td><td class="text-justify"><?php echo $value['paket_name']?></td></tr>
      <tr><td class="text-justify">Lokasi</td><td class="text-justify"><?php echo $value['lokasi'];?></td></tr>
      <tr><td class="text-justify">No Pengadaan</td><td class="text-justify"><?php echo $value['no_pengadaan'];?></td></tr>
      <tr><td class="text-justify">Tanggal</td><td class="text-justify"><?php echo $value['tanggal_permintaan'];?></td></tr>
      <tr><td class="text-justify">Anggaran</td><td class="text-justify"><?php echo $value['anggaran'];?></tr>
      <?php } ?>
    </table>
    <br>
    <div class="table-responsive">
    <table class="table table-bordered table-barang">
      <thead>
        <tr>
          <th>Nama Barang</th>
          <th>Spesifikasi</th>
          <th>Volume</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($artikel as $value) : ?>

        <tr>
          <td class="text-justify"><?php echo $value['nama_barang']; ?></td>
          <td class="text-justify"><?php echo $value['spesifikasi']; ?></td>
          <td class="text-center"><?php echo $value['volume']; ?></td>
        </tr>

      </tbody>
    <?php endforeach; ?>

    </table>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->