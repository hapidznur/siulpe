<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pengadaan
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>
    <section class="content">
        <!-- Default box -->
        <div class="box">
			<table class="table table-hover table-pengadaan">
					<thead>
						<tr>
							<th>No</th>
							<th>Pengadaan</th>
							<th>HPS</th>
							<th>Undangan</th>
							<th>Kirim</th>
						</tr>
					</thead>
					<tbody>
						<?php $i=1; foreach ($pengadaan as $pengadaan): ?>
						<tr>
							<td ><?php echo $i++;?> </td>
							<td ><a href="<?php echo base_url().'ulp/undangan?pengadaan='.$pengadaan->id_pengadaan;?>"><?php echo $pengadaan->paket_name;?></a></td>
							<td ><?php echo $pengadaan->hps ?></td>
							<td><input type="file" name="undangan" class="file"></td>
							<td ><button type="submit" class="btn btn-primary" name="simpan">Kirim</button></td>
						</tr>
						<?php endforeach;?>
					</tbody>
				</table>
			<?php
			echo form_close();
			?>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->