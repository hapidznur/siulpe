<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pengadaan
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

    <!-- Default box -->
    <div class="box">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Nama Pengadaan</th>
            <th>Lokasi</th>
            <th>Tanggal Proses</th>
            <th>Lokasi</th>
            <th</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($list_pengadaan_download as $key => $value) {?>
            <tr>
              <td><?php echo $value['paket_name']; ?></td>
              <td><?php echo $value['lokasi']; ?></td>
              <td><?php echo $value['tanggal_permintaan']; ?></td>
              <td><?php echo form_open('ulp/download_file'); ?>
                <input type="hidden" name="id_pengadaan" value=<?php echo $value['id_pengadaan']; ?>>
                <input type="submit" value="download" class="btn btn-default">
                <?php echo form_close(); ?>
              </td>
            <tr>
          <?php }?>
        </tbody>
      </table>
    </div>
    <!-- /.box  -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->