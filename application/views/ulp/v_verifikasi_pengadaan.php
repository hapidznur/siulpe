<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Verifikasi Penawaran
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="col-md-12">

        </div>
        <div class="box box-default collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title">Nama Rekanan</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-6">
                    <div class="box box-primary kelengkapan">
                        <div class="box-header"> <h5><strong>Kelengkapan Rekanan</strong></h5></div>
                        <div class="box-content" style="padding-left: 20px;">
                            <label>
                                <input type="checkbox"> Akte Notaris/Pendirian Perusahaan
                            </label><br />
                            <label>
                                <input type="checkbox" > SIUP
                            </label><br />
                            <label>
                                <input type="checkbox"> NPWP
                            </label><br />
                            <label>
                                <input type="checkbox" > PKP
                            </label><br />
                            <label>
                                <input type="checkbox" > Tanda Daftar Perusahaan
                            </label><br />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-info dokumen">
                        <div class="box-header"> <h5><strong>Dokumen Penawaran</strong></h5></div>
                        <div class="box-content" style="padding-left: 20px;">
                            <label>
                                <input type="checkbox" > Surat penawaran
                            </label><br />
                            <label>
                                <input type="checkbox" > Surat Kuasa
                            </label><br />
                            <label>
                                <input type="checkbox" > Dokumen penawaran teknis
                            </label><br />
                            <label>
                                <input type="checkbox" > Dokumen penawaran harga
                            </label><br />
                            <label>
                                <input type="checkbox" > Pakta Integritas
                            </label><br />
                            <label>
                                <input type="checkbox" > Formulir Isian Kualifikasi
                            </label><br />
                        </div>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    <button class="btn btn-flat btn-info pull-right">Simpan</button>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
        <div class="box box-default collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title">Nama Rekanan</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-6">
                    <div class="box box-primary kelengkapan">
                        <div class="box-header"> <h5><strong>Kelengkapan Rekanan</strong></h5></div>
                        <div class="box-content" style="padding-left: 20px;">
                            <label>
                                <input type="checkbox"> Akte Notaris/Pendirian Perusahaan
                            </label><br />
                            <label>
                                <input type="checkbox" > SIUP
                            </label><br />
                            <label>
                                <input type="checkbox"> NPWP
                            </label><br />
                            <label>
                                <input type="checkbox" > PKP
                            </label><br />
                            <label>
                                <input type="checkbox" > Tanda Daftar Perusahaan
                            </label><br />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-info dokumen">
                        <div class="box-header"> <h5><strong>Dokumen Penawaran</strong></h5></div>
                        <div class="box-content" style="padding-left: 20px;">
                            <label>
                                <input type="checkbox" > Surat penawaran
                            </label><br />
                            <label>
                                <input type="checkbox" > Surat Kuasa
                            </label><br />
                            <label>
                                <input type="checkbox" > Dokumen penawaran teknis
                            </label><br />
                            <label>
                                <input type="checkbox" > Dokumen penawaran harga
                            </label><br />
                            <label>
                                <input type="checkbox" > Pakta Integritas
                            </label><br />
                            <label>
                                <input type="checkbox" > Formulir Isian Kualifikasi
                            </label><br />
                        </div>
                    </div>
                </div>
                <div style="float:right;padding-right: 10px">
                    <button class="btn btn-primary">Simpan</button>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
        <div class="box box-default collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title">Nama Rekanan</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-6">
                    <div class="box box-primary kelengkapan">
                        <div class="box-header"> <h5><strong>Kelengkapan Rekanan</strong></h5></div>
                        <div class="box-content" style="padding-left: 20px;">
                            <label>
                                <input type="checkbox" name="kelengkapan"> Akte Notaris/Pendirian Perusahaan
                            </label><br />
                            <label>
                                <input type="checkbox" > SIUP
                            </label><br />
                            <label>
                                <input type="checkbox"> NPWP
                            </label><br />
                            <label>
                                <input type="checkbox" > PKP
                            </label><br />
                            <label>
                                <input type="checkbox" > Tanda Daftar Perusahaan
                            </label><br />
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-info dokumen">
                        <div class="box-header"> <h5><strong>Dokumen Penawaran</strong></h5></div>
                        <div class="box-content" style="padding-left: 20px;">
                            <label>
                                <input type="checkbox" > Surat penawaran
                            </label><br />
                            <label>
                                <input type="checkbox" > Surat Kuasa
                            </label><br />
                            <label>
                                <input type="checkbox" > Dokumen penawaran teknis
                            </label><br />
                            <label>
                                <input type="checkbox" > Dokumen penawaran harga
                            </label><br />
                            <label>
                                <input type="checkbox" > Pakta Integritas
                            </label><br />
                            <label>
                                <input type="checkbox" > Formulir Isian Kualifikasi
                            </label><br />
                        </div>
                    </div>
                </div>
                <div style="float:right;padding-right: 10px">
                    <button class="btn btn-primary">Simpan</button>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
