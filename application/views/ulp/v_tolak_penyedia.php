<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pengadaan
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

    <?php echo form_open('ulp/do_send'); ?>
    <div class="col-sm-8 col-md-8 col-lg-8">
      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="email">Email</label>
        <div class="col-md-8">
          <input id="email" name="email" type="text" placeholder="" class="form-control input-md" value="<?php echo $tertolak['0']; ?>">
          <span class="help-block">email</span>
        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="Perusahaan">Perusahaan</label>
        <div class="col-md-8">
          <input id="perusahaan" name="perusahaan" type="text" placeholder="" class="form-control input-md" value="<?php echo $tertolak['1']; ?>">
          <span class="help-block">perusahaan </span>
        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="Pemilik">Pemilik</label>
        <div class="col-md-8">
          <input id="pemilik" name="pemilik" type="text" placeholder="" class="form-control input-md" value="<?php echo $tertolak['2']; ?>">
          <span class="help-block">pemilik</span>
        </div>
      </div>

      <!-- Textarea -->
      <div class="form-group">
        <label class="col-md-4 control-label" for="Isi_mail">Isi Email</label>
        <div class="col-md-4">
          <textarea class="form-control" id="isi_mail" name="isi_mail">Saran untuk penyedia/rekanan</textarea>
        </div>
      </div>
      <input type="submit" value="Tolak Penyedia" class="btn btn-danger">
    </div><!--end of col-8 -->

    <?php echo form_close(); ?>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
