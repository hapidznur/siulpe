<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pengadaan
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
                <table class="table table-hover table-pengadaan">
                	<thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Paket</th>
                        <th>PPK</th>
                        <th>HPS</th>
                        <th>Penyedia</th>
                        <th>Harga</th>
                        <th>keterangan</th>
                    </th>
                    </thead>
                    <tbody>
                    <?php echo form_open('ulp/updatepengumuman');?>
                    <?php foreach ($pengumuman as $pengumuman) {?>
                    <tr>
						<td><?php echo $pengumuman ->paket_name;?></td>
						<td><?php echo $pengumuman ->PPK_NAME;?></td>
						<td><?php echo $pengumuman ->HPS;?></td>
						<td>
							<select name="list_penyedia" class="role"><?php foreach ($penyedia as $penyedia) { ?>
								<option value="<?php echo $penyedia->id_penyedia;?>">
								<?php echo $penyedia->penyedia;?>
								</option>
							<?php } ?>
							</select>
						</td>
						<td><input type="text" name="tawaran" id="input-group"></td>
						<td><input type="submit" class="btn btn-primary pengumuman" name="pengumuman"></td>
                    </tr>
                    <input type="hidden" name="pengadaan" value="<?php echo $pengumuman->id_pengadaan;?>">
                    <?php form_close();?>
                	</tbody>
          		</table>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->