<footer class="main-footer" style="height: 10px;">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.6
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
</footer>

<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>

</div>
<!-- Bootstrap Core JavaScript -->
<!-- Core Scripts - Include with every page -->
<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url('ext/js/jquery.min.js'); ?>"></script>
<!--<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>-->
<!-- jQuery UI 1.11.4 -->
<!--<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>-->
<script src="<?php echo base_url('ext/js/jquery-ui.min.js'); ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('ext/js/bootstrap.min.js'); ?>"></script>
<!--<script src="bootstrap/js/bootstrap.min.js"></script>-->
<!-- Morris.js charts -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>-->
<script src="<?php echo base_url('ext/js/raphael.min.js'); ?>"></script>
<!--<script src="plugins/morris/morris.min.js"></script>-->
<script src="<?php echo base_url('ext/js/morris.min.js'); ?>"></script>
<!-- Sparkline -->
<!--<script src="plugins/sparkline/jquery.sparkline.min.js"></script>-->
<script src="<?php echo base_url('ext/js/jquery.sparkline.min.js'); ?>"></script>
<!-- jvectormap -->
<!--<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>-->
<script src="<?php echo base_url('ext/js/jquery-jvectormap.min.js'); ?>"></script>

<!--<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>-->
<!-- jQuery Knob Chart -->
<!--<script src="plugins/knob/jquery.knob.js"></script>-->
<script src="<?php echo base_url('ext/js/jquery.knob.min.js'); ?>"></script>
<!-- daterangepicker -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>-->
<script src="<?php echo base_url('ext/js/moment.min.js'); ?>"></script>
<!--<script src="plugins/daterangepicker/daterangepicker.js"></script>-->
<script src="<?php echo base_url('ext/js/daterangepicker.min.js'); ?>"></script>
<!-- datepicker -->
<!--<script src="plugins/datepicker/bootstrap-datepicker.js"></script>-->
<script src="<?php echo base_url('ext/js/bootstrap-datepicker.js'); ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<!--<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>-->
<script src="<?php echo base_url('ext/js/bootstrap3-wysihtml5.all.min.js'); ?>"></script>
<!-- Slimscroll -->
<!--<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>-->
<script src="<?php echo base_url('ext/js/jquery.slimscroll.min.js'); ?>"></script>
<!-- FastClick -->
<!--<script src="plugins/fastclick/fastclick.js"></script>-->
<script src="<?php echo base_url('ext/js/fastclick.min.js'); ?>"></script>
<!-- AdminLTE App -->
<!--<script src="dist/js/app.min.js"></script>-->
<script src="<?php echo base_url('ext/js/app.min.js'); ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="dist/js/pages/dashboard.js"></script>-->
<script src="<?php echo base_url('ext/js/dashboard.js'); ?>"></script>
<!-- AdminLTE for demo purposes -->
<!--<script src="dist/js/demo.js"></script>-->
<script src="<?php echo base_url('ext/js/demo.js'); ?>"></script>

<!-- Datatable JQuery Plugin-->
<script src="<?php echo base_url('ext/js/jquery.dataTables.js')?>"></script>

<!-- Datatable JQuery UI Bootstrap Plugin-->
<script src="<?php echo base_url('ext/js/dataTables.bootstrap.js')?>"></script>

<!--Datatable-->
<script>
    $('#penjadwalan').DataTable();
</script>
<script>
var table;
var proses;
  table = $('#detailjadwal').DataTable({
    "ajax": {
          "url": "<?php echo site_url('ulp/list_detail_jadwal')?>",
          "type": "POST"
    },
    //Set column definition initialisation properties.
    "columnDefs": [
    {
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
    },
    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex) {
        if (aData[0] == 1) {
            $(nRow).addClass('danger');
        }
    }
  });
  // end of Datatables
  //datepicker
  $('.datepicker').datepicker({
    autoclose: true,
    format: "yyyy-mm-dd",
    todayHighlight: true,
    orientation: "top auto",
    todayBtn: true,
    todayHighlight: true,
  });

  function get_proses(proses){
    $.ajax({
      url : "<?php echo site_url('ulp/get_status/')?>"+ proses,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {
      }
    });
  }

  function reload_table()
  {
      table.ajax.reload(null,false); //reload datatable ajax
  }
  function simpan(){
      // ajax adding data to database
    $.ajax({
        url : "<?php echo site_url('ulp/simpan_jadwal')?>",
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
            }


        }
      });
    }

    function edit_jadwal(id, no){
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('ulp/edit_jadwal/')?>/" + id +"/"+no,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('#mulai').val(data.mulai);
            $('#akhir').val(data.akhir);
            $('#proses').val(data.proses);
            $('#id').val(data.id);
            $('#modal_jadwal').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Kalender Pengadaan'); // Set title to Bootstrap mo dal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
</script>

<!-- Script -->
<script type="text/javascript">
  function downloadFile(str) {
  window.location.href = "<?php echo base_url().'siulpe/'?>" + str;
      // $.ajax({
     //        type: 'POST',
     //        url: 'download_surat',
     //        data: {value: str},
     //        success: function(data) {
     //            alert('ok');
     //        }
     //    });
    }

    $(document).ready(function() {

    //     $('td.proses').dblclick(function (){
    //         var tanggal = $(this).text();
    //         var proses = $(this).data('proses');
    //         var name = $(this).data('user');
    //         var modal = $('#jadwal');
    //         $('#jadwal').modal('show');
    //         $('#aku').text(tanggal);
    //         modal.find('.prosesbisnis').text(proses);
    //         modal.find('.modal-body #editproses').val(tanggal);
    //         modal.find('.modal-body #identify').val(name);

    //     });
    //     $('#editproses').datepicker({
    //       // format: 'dd-mm-yyyy',
    //       minDate: '+5d',
    //       changeMonth: true,
    //       changeYear: true,
    //       altField: '#DetailsHidden',
    //       format: 'yyyy-mm-dd'
    //     });
    //     // $('.modal-dialog').draggable({
    //     //     handle: ".modal-header"
    //     // });
    //     $('button#submit').click(function(){
    //         $('#jadwal').modal('hide');
    //         var proses = $('#identify').val();
    //         var tanggalbaru = $('#DetailsHidden').val();
    //         var userjadwal = $('#userjadwal').val();

    //         $.post(
    //             "<?php echo base_url('ulp/updatekalender');?>",
    //             {
    //                 proses : proses,
    //                 tanggal : tanggalbaru,
    //                 userjadwal : userjadwal
    //             }
    //         );
    //     });

    });
</script>

</body>
</html>