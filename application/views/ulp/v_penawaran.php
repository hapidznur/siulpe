<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pengadaan
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<table class="table table-hover">
			<thead>
				<tr>
					<th style="width: 5%">No</th>
					<th>Pengadaan</th>
					<th>Satker</th>
					<th>Penyedia</th>
					<th>Surat Informasi Harga</th>
					<th>Total Harga</th>
					<th>List Rekanan</th>
				</tr>
			</thead>
			<?php $i=0; foreach ($Verifikasi as $Verifikasi ) {?>
			<tbody>
				<tr>
					<td ><?php echo $i;?></td>
					<td ><?php echo $Verifikasi['paket_name']; ?></td>
					<td ><?php echo $Verifikasi['satker']; ?></td>
					<td ><?php echo $Verifikasi['nama_penyedia']; ?></td>
					<td ><?php echo $Verifikasi['surat_penawaran']; ?></td>
					<td ><?php echo $Verifikasi['total_harga']; ?></td>
					<td ><a href="<?php echo base_url('ulp/penyedia')."?pengadaan=".$Verifikasi['id_pengadaan']?>">Rekanan</a></td>

				</tr>
			</tbody>
			<?php $i++; } ?>
		</table>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->