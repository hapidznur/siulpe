<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistem Informasi Unit Layanan Pengadaan | <?php echo $title; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/css/bootstrap.min.css'); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/css/font-awesome.min.css'); ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/css/ionicons.min.css'); ?>">

    <!-- Theme style -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/css/AdminLTE.min.css'); ?>">

    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/css/_all-skins.min.css'); ?>">

    <!-- iCheck -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/css/blue.css'); ?>">
    <!-- Morris chart -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/css/morris.css'); ?>">
    <!-- jvectormap -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/css/jquery-jvectormap.css'); ?>">
    <!-- Date Picker -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/css/datepicker3.css'); ?>">
    <!-- Daterange picker -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/css/daterangepicker-bs3.min.css'); ?>">
    <!-- bootstrap wysihtml5 - text editor -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/css/bootstrap3-wysihtml5.min.css'); ?>">
    <!-- Datatables JQuery UI plugin   -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/css/dataTables.bootstrap.css')?>">
    <!--  Siulpe css custom  -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('ext/css/siulpe-custom.css')?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--    [if lt IE 9]>-->
    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>-->
    <!--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
    <!--    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="../../index2.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>ULP</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Layanan </b> Pengadaan</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo $photo; ?>" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?php echo $name; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?php echo $photo; ?>" class="img-circle" alt="User Image">
                                <p>
                                    <?php echo $name; ?> - Web Developer
                                    <small>Member since Nov. 2012</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="row">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="#" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo $photo; ?>" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>Alexander Pierce</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MENU UTAMA</li>
                <li>
                    <a href="<?php echo base_url('/ulp/penjadwalan');?>">Kalender Pengadaan</a>
                </li>
                <li>
                    <a href="<?php echo base_url('/ulp/verifikasiharga');?>">Verifikasi Informasi Harga</a>
                </li>
                <li>
                    <a href="<?php echo base_url('/ulp/verifikasi');?>">Verifikasi Pengadaan</a>
                </li>
                <li>
                    <a href="<?php echo base_url('/ulp/verifikasi_penyedia');?>">Verifikasi rekanan</a>
                </li>
                <li>
                    <a href="<?php echo base_url('/ulp/undangan');?>">Kirim Undangan</a>
                </li>
                <li>
                    <a href="<?php echo base_url('/ulp/pengumuman');?>">Pengumuman Penyedia</a>
                </li>
                <li>
                    <a href="<?php echo base_url('/ulp/profil');?>">Profil</a>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>