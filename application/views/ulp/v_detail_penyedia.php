<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pengadaan
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>
    <section class="content">
        <!-- Default box -->
        <div class="box">
    <table class="table table-bordered">
      <thead>
        <tr>
        <td></td>
        <td></td>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($detail_penyedia_baru as $key => $value) {
         ?>
        <tr>
          <td>Nama Penyedia</td>
          <td><?php echo $value['nama_penyedia']; ?></td>
        </tr>
        <tr>
          <td>Pemilik</td>
          <td><?php echo $value['pemilik']; ?></td>
        </tr>
        <tr>
          <td>Alamat</td>
          <td><?php echo $value['alamat']; ?></td>
        </tr>
        <tr>
          <td>email</td>
          <td><?php echo $value['email']; ?></td>
        </tr>
        <tr>
          <td>Kontak</td>
          <td><?php echo $value['contact']; ?></td>
        </tr>
        <tr>
          <td>No NPWP</td>
          <td><?php echo $value['no_npwp']; ?></td>
        </tr>
        <tr>
          <td>Link NPWP</td>
          <td><a><?php echo $value['npwp']; ?></a></td>
        </tr>
        <tr>
          <td>Link PKP</td>
          <td><a><?php echo $value['pkp']; ?></a></td>
        </tr>
        <tr>
          <td>Link SIUP</td>
          <td><a><?php echo $value['siup']; ?></a></td>
        </tr>
        <tr>
          <td>Link Akta Notaris</td>
          <td><a><?php echo $value['akta_notaris']; ?></a></td>
        </tr>
      </tbody>
      <?php } ?>
    </table>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->