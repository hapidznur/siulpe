<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Penjadwalan
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
			<table class="table table-hover" id="penjadwalan">
					<thead>
						<tr>
							<th style="width:5%" >No</th>
							<th>Paket</th>
							<th>Status</th>
							<th>Tanggal</th>
							<th>Detailed</th>
						</tr>
					</thead>
					<tbody>
					<?php $i=1; foreach ($kalender as $kalender) { ?>
					<tr>
						<td ><?php echo $i; ?></td>
						<td ><?php echo $kalender['paket_name'];?></td>
						<td ><span class="label label-danger">
						<?php echo $kalender['status'];?></span></td>
						<td ><?php echo $kalender['OnProses'];?></td>
						<td ><a href="<?php echo base_url('/ulp/editpenjadwalan')."?pengadaan=".$kalender['id_jadwal']; ?>">Detail</a></td>
					</tr>
					<?php } ?>
					</tbody>
				</table>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
