<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1>Aksasystem Reset Password</h1>
    <p>
      Click the link below to reset your password.
    </p>
    <p>
      <a href="{{base_url("auth/reset_password?token={$token}")}}">Reset Password</a>
    </p>
  </body>
</html>
