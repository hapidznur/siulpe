@extends('emails.template')

@section('email-type', 'Keterlambatan Pembayaran')

@section('body')
@include('emails.pieces.dataemail')
 <tr mc:hideable>
  <td align="center" valign="top">
    <!-- CENTERING TABLE // -->
    <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
      <tr>
        <td align="center" valign="top">
          <!-- FLEXIBLE CONTAINER // -->
          <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
            <tr>
              <td align="center" valign="top" width="500" class="flexibleContainerCell">
                <table border="0" cellpadding="30" cellspacing="0" width="100%">
                  <tr>
                    <td align="center" valign="top">

                      <!-- CONTENT TABLE // -->
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="textContent">
                            <!--
                              The "mc:edit" is a feature for MailChimp which allows
                              you to edit certain row. It makes it easy for you to quickly edit row sections.
                              http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
                            -->
                            <h3 mc:edit="header" style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
                              Keterlamabatan Pembayaran Invoice.
                            </h3>
                            <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                              <p>
                                Kepada Pelanggan yang terhormat, dengan ini kami beri tahukan dengan berat hati bahwa pemesanan Open Trip anda dengan kode <b>{{$booking->openTrip->kode}}</b> ({{$booking->openTrip->masterTrip->title}}) yang direncakan akan berangkat pada tanggal <b>{{$booking->openTrip->tanggal_berangkat_manusia}} harus kami batalkan, karena anda belum melakukan pembayaran sampai dengan tanggal yang sudah ditentukan.</b>
                              </p>
                              <p>
                                Demikian pesan ini kami, terima kasih atas perthatiannya dan kami sampaikan mohon maaf.
                              </p>
                            </div>
                          </td>
                        </tr>
                      </table>
                      <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <!-- // FLEXIBLE CONTAINER -->
        </td>
      </tr>
    </table>
    <!-- // CENTERING TABLE -->
  </td>
</tr>

@endsection
