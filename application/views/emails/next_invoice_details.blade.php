@extends('emails.template')

@section('email-type', 'Invoice Detail')

@section('body')
  {{-- Hello this is your invoice code -> {{$invoice->nomor_invoice}} --}}
  @include('emails.pieces.dataemail')
  @if($booking->invoice->status != 'lunas')
  @include('emails.pieces.rekening')
  @endif
  <!-- // MODULE ROW -->
  <!-- MODULE ROW // -->
  <!-- // MODULE ROW -->

@endsection
