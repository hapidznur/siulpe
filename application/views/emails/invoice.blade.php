@extends('emails.template')

@section('email-type', 'Invoice Detail')

@section('body')
  {{-- Hello this is your invoice code -> {{$invoice->nomor_invoice}} --}}
  @include('emails.pieces.dataemail')
  @include('emails.pieces.rekening')
@endsection
