@extends('emails.template')

@section('email-type', 'Konfirmasi Pembayaran')

@section('body')
  <tr>
    <td align="center" valign="top">
      <!-- CENTERING TABLE // -->
      <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
        <tr>
          <td align="center" valign="top">
            <!-- FLEXIBLE CONTAINER // -->
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
              <tr>
                <td align="center" valign="top" width="500" class="flexibleContainerCell">
                  <table border="0" cellpadding="30" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top">

                        <!-- CONTENT TABLE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                            <td valign="top" class="textContent">
                              <!--
                                The "mc:edit" is a feature for MailChimp which allows
                                you to edit certain row. It makes it easy for you to quickly edit row sections.
                                http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
                              -->
                              <h3 mc:edit="header" style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
                                Pembayaran Telah Dikonfirmasi
                              </h3>
                              <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                Terima kasih, pembayaran anda dengan data berikut:
                                <table style="width: 100%;background: rgb(240, 240, 240); margin-top: 20px">
                                  <tr>
                                    <th style="width: 50%;padding: 10px">
                                      Invoice
                                    </th>
                                    <td>
                                      <b>{{$invoiceConfirmation->invoice->nomor_invoice}}</b>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th style="width: 50%;padding: 10px">
                                      Nama
                                    </th>
                                    <td>
                                      {{$invoiceConfirmation->nama}}
                                    </td>
                                  </tr>
                                  <tr>
                                    <th style="width: 50%;padding: 10px">
                                      Bank
                                    </th>
                                    <td>
                                      {{$invoiceConfirmation->bank}}
                                    </td>
                                  </tr>
                                  <tr>
                                    <th style="width: 50%;padding: 10px">
                                      No Rekening
                                    </th>
                                    <td>
                                      {{$invoiceConfirmation->rekening_pengirim}}
                                    </td>
                                  </tr>
                                  <tr>
                                    <th style="width: 50%;padding: 10px">
                                      Tanggal Pembayaran
                                    </th>
                                    <td>
                                      {{$invoiceConfirmation->tanggal_pembayaran}}
                                    </td>
                                  </tr>
                                  <tr>
                                    <th style="width: 50%;padding: 10px">
                                      Total Transfer
                                    </th>
                                    <td>
                                      IDR. {{number_format($invoiceConfirmation->total_transfer)}}
                                    </td>
                                  </tr>
                                </table>
                                <p>
                                  Telah kami konfirmasi. Untuk informasi pembayaran selanjutnya, akan kami kirim pada email setelah ini.
                                </p>
                              </div>
                            </td>
                          </tr>
                        </table>
                        <!-- // CONTENT TABLE -->

                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <!-- // FLEXIBLE CONTAINER -->
          </td>
        </tr>
      </table>
      <!-- // CENTERING TABLE -->
    </td>
  </tr>

@endsection
