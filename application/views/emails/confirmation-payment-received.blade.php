@extends('emails.template')

@section('email-type', 'Konfirmasi Pembayaran')

@section('body')
  <tr>
    <td align="center" valign="top">
      <!-- CENTERING TABLE // -->
      <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
        <tr>
          <td align="center" valign="top">
            <!-- FLEXIBLE CONTAINER // -->
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
              <tr>
                <td align="center" valign="top" width="500" class="flexibleContainerCell">
                  <table border="0" cellpadding="30" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top">

                        <!-- CONTENT TABLE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                            <td valign="top" class="textContent">
                              <!--
                                The "mc:edit" is a feature for MailChimp which allows
                                you to edit certain row. It makes it easy for you to quickly edit row sections.
                                http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
                              -->
                              <div mc:edit="body" style="text-align:justify;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;line-height:135%;">
                                Terimakasih Konfirmasi pembayaran telah kami terima dan akan kami proses maksimal 1x24 jam, selanjutnya bukti pembayaran akan kami kirim via email.
                              </div>
                            </td>
                          </tr>
                        </table>
                        <!-- // CONTENT TABLE -->

                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <!-- // FLEXIBLE CONTAINER -->
          </td>
        </tr>
      </table>
      <!-- // CENTERING TABLE -->
    </td>
  </tr>

@endsection
