<tr>
  <td align="center" valign="top">
    <!-- CENTERING TABLE // -->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td align="center" valign="top">
          <!-- FLEXIBLE CONTAINER // -->
          <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
            <tr>
              <td valign="top" width="500" class="flexibleContainerCell">

                <!-- CONTENT TABLE // -->
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td align="left" valign="top" class="flexibleContainerBox" style="background-color:#494949;">
                      <table border="0" cellpadding="30" cellspacing="0" width="100%" style="max-width:100%;">
                        <tr>
                          <td align="center" class="textContent" style="vertical-align:middle">
                            <img src="{{base_url("assets/images/logo-white.png")}}" style="width:50px;margin-top:8px;max-height:50px;margin-left:auto;margin-right:auto">
                          </td>
                        </tr>
                      </table>
                    </td>
                    <td align="right" valign="top" class="flexibleContainerBox" style="background-color:#494949;">
                      <table class="flexibleContainerBoxNext" border="0" cellpadding="30" cellspacing="0" width="100%" style="max-width:100%;">
                        <tr>
                          <td align="left" class="textContent">
                            <h3 style="color:#FFFFFF;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">Compas Petualang</h3>
                            <div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:12px;margin-bottom:0;color:#FFFFFF;line-height:135%;">
                              {{$identitasWeb->email}}<br>
                              {{$identitasWeb->nomor_telepon}}<br>
                              {{$identitasWeb->alamat}}<br>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
                <!-- // CONTENT TABLE -->

              </td>
            </tr>
          </table>
          <!-- // FLEXIBLE CONTAINER -->
        </td>
      </tr>
    </table>
    <!-- // CENTERING TABLE -->
  </td>
</tr>
