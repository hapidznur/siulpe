<tr>
    <td align="center" valign="top">
      <!-- CENTERING TABLE // -->
      <!--
        The centering table keeps the content
        tables centered in the emailBody table,
        in case its width is set to 100%.
      -->
      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF;" bgcolor="#FFF">
        <tr>
          <td align="center" valign="top">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
              <tr>
                <td align="center" valign="top" width="500" class="flexibleContainerCell">
                  <table border="0" cellpadding="30" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top" class="textContent">
                          <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;color:#555">
                              <tr>
                                <td align="left" class="textContent">
                                  <table style="width:100%;font-size:12px;">
                                      <tr>
                                        <td>Trip Number: {{$booking->openTrip->kode}}</td>
                                        <td align="right">{{\Carbon\Carbon::now()->toFormattedDateString()}}</td>
                                      </tr>
                                  </table>
                                  <br>
                                  <table style="width:100%;font-size:13px;">
                                      <tr>
                                        <th align="left">Customer</th>
                                        <th align="right">Open Trip</th>
                                      </tr>
                                      <tr>
                                        <td>{{$booking->nama}}</td>
                                        <td align="right">
                                          <a href="{{base_url('open-trip/show/'.$booking->openTrip->id)}}" >{{$booking->openTrip->masterTrip->title}}</a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2"><hr  style="border-color:#eee"></td>
                                      </tr>
                                      <tr>
                                        <td align="left"><b>Invoice Number</b></td>
                                        <td align="right"><b>Kode Booking</b></td>
                                      </tr>
                                      <tr>
                                        <td align="left">{{$booking->no_invoice}}</td>
                                        <td align="right">{{$booking->kode}}</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2"><hr  style="border-color:#eee"></td>
                                      </tr>
                                      <tr>
                                        <td align="left"><b>Departure</b></td>
                                        <td align="right"><b>Arrival</b></td>
                                      </tr>
                                      <tr>
                                        <td align="left">{{$booking->openTrip->tanggal_berangkat_manusia}}{{$booking->openTrip->jam_berangkat ? ", {$booking->openTrip->jam_berangkat}" : ''}}</td>
                                        <td align="right">{{$booking->openTrip->tanggal_pulang_manusia}}{{$booking->openTrip->jam_pulang ? ", {$booking->openTrip->jam_pulang}" : ''}}</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2"><hr style="border-color:#eee"></td>
                                      </tr>
                                      <tr>
                                        <td align="left"><b>Meeting Point</b></td>
                                        <td align="right"><b>Jumlah Peserta</b></td>
                                      </tr>
                                      <tr>
                                        <td align="left">{{$booking->openTrip->meeting_point }}</td>
                                        <td align="right">{{$booking->jumlah_peserta}} Orang</td>
                                      </tr>
                                      <tr>
                                        <td colspan="2"><hr  style="border-color:#eee"></td>
                                      </tr>
                                      <tr>
                                        <td align="left"><b>Facility</b></td>
                                        <td align="right"><b>Destination</b></td>
                                      </tr>
                                      <tr>
                                        <td align="left">{!!$booking->openTrip->masterTrip->fasilitas!!}</td>
                                        <td align="right">
                                          @foreach($booking->openTrip->masterTrip->destinations as $destination)
                                            <span>{{$destination->nama}}</span><br>
                                          @endforeach
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2"><hr  style="border-color:#eee"></td>
                                      </tr>
                                      <tr>
                                        <td align="left" colspan="2"><b>Detail Itinerary</b></td>
                                      </tr>
                                      <tr>
                                        <td align="left" colspan="2">
                                        @if($booking->openTrip->masterTrip->itenary_file)
                                        <a href="{{$booking->openTrip->masterTrip->itenary_file_src}}">Download Itinerary</a>
                                        @else
                                        No Itinerary File
                                        @endif
                                        </td>
                                      </tr>
                                       <tr>
                                        <td colspan="2"><hr  style="border-color:#eee"></td>
                                      </tr>
                                      <tr>
                                        <td align="left"><b>Price Per person</b></td>
                                        <td align="right"><b>Total Price</b></td>
                                      </tr>
                                      <tr>
                                        <td align="left">{{$booking->openTrip->price}}</td>
                                        <td align="right">IDR. {{number_format($booking->invoice ? $booking->invoice->total_harga : $booking->jumlah_peserta * $booking->openTrip->harga)}}</td>
                                      </tr>
                                      @if ($booking->invoice && $booking->invoice->status != 'belum_dibayar')
                                      <tr>
                                        <td align="left"><b>Yang sudah dibayar</b></td>
                                        <td align="right"><b>Yang belum dibayar</b></td>
                                      </tr>
                                      <tr>
                                        <td align="left">IDR. {{number_format($booking->invoice->total_yang_sudah_dibayar)}}</td>
                                        <td align="right">IDR. {{number_format($booking->invoice->total_yang_harus_dibayar)}}</td>
                                      </tr>
                                      @endif
                                  </table>
                                </td>
                              </tr>
                            </table>
                      </td>
                    </tr>
                  </table>
                  <!-- // CONTENT TABLE -->

                </td>
              </tr>
            </table>
            <!-- // FLEXIBLE CONTAINER -->
          </td>
        </tr>
      </table>
      <!-- // CENTERING TABLE -->
    </td>
  </tr>           