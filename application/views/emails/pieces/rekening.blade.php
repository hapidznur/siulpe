 <tr mc:hideable>
    <td align="center" valign="top">
      <!-- CENTERING TABLE // -->
      <table border="0" cellpadding="0" cellspacing="0" width="100%" >
        <tr>
          <td align="center" valign="top">
            <!-- FLEXIBLE CONTAINER // -->
            <table border="0" cellpadding="30" cellspacing="0" width="500" class="flexibleContainer">
              <tr>
                <td valign="top" width="500" class="flexibleContainerCell" style="padding-top:0px;">

                  <!-- CONTENT TABLE // -->
                  <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td align="left" valign="top" class="flexibleContainerBox">
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;overflow:hidden;">
                          <tr>
                            <td align="left" class="textContent">
                              <table style="width:100%">
                               <tr>
                                <td><hr  style="border-color:#eee"><hr  style="border-color:#eee"></td>
                              </tr>
                              </table>
                              <div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:13px;margin-bottom:0;line-height:135%;width:100%;overd">
                                <article style="text-align: justify;"><i>
                                @if($booking->invoice->status == 'belum_dibayar')
                                  Pembayaran DP (down payment) sebesar <b><i>IDR. {{number_format($booking->openTrip->dp_minimum * $booking->jumlah_peserta)}}</i></b> dibayarkan maksimal {{$booking->openTrip->sisa_hari > 30 ? '3' : '1'}}x24 jam. Jika Melewati batas waktu tersebut, maka pemesanan kami anggap batal dan silahkan melakukan pemesanan ulang.
                                @elseif ($booking->invoice->status != 'lunas')
                                  Pelunasan sebesar <b><i>IDR. {{number_format($invoice->total_yang_harus_dibayar)}}</i></b> dibayarkan selambatnya tanggal {{$booking->openTrip->jatuh_tempo_pembayaran_manusia}}. Jika melewati batas waktu tersebut, maka pemesanan kami anggap  batal dan uang yang sudah dibayarkan tidak bisa di refund (hangus).
                                @else
                                  Terima kasih telah menggunakan layanan kami.
                                @endif
                                <br> <br>
                                Pembayaran bisa di lakukan via transfer atau setor tunai ke rekening di bawah ini :</i>
                                </article>
                                <table cellpadding="3" style="margin-top:20px;color:#777">
                                  <tr>
                                    <td>Bank Name</td>
                                    <td>
                                      <strong>{{$identitasWeb->bank}}</strong>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>Bank Account Name</td>
                                    <td>
                                      <strong>{{$identitasWeb->atas_nama}}<strong>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>Bank Account Number</td>
                                    <td><strong>
                                        {{$identitasWeb->no_rekening}}
                                      </strong>
                                    </td>
                                  </tr>
                                </table>
                                <hr  style="border-color:#eee"><hr  style="border-color:#eee">
                                <div style="margin-top:10px;text-align: justify;">
                                  <i>Setelah melakukan pembayaran silahkan melakukan konfirmasi dengan memasukan kode invoice di bawah ini ke page konfirmasi di website kami</i>
                                </div>
                                <table style="width:100%;overflow-x:hidden" cellpadding="5">
                                  <tr>
                                    <td width="50%">
                                        <h2 style="margin:0;text-align:center;padding:7px;border:2px solid#bbb">{{$invoice->nomor_invoice}}</h2>
                                    </td>
                                    <td  width="50%">
                                      <a href="{{base_url('konfirmasi-pembayaran')}}" style="background:#2E3094;display:block;padding:10px;border:2px solid#2E3094;color:#fff;font-size:15px;text-align:center">
                                        <b>CONFIRMATION</b>
                                      </a>
                                    </td>
                                  </tr>
                                </table>
                              </div>
                            </td>
                          </tr>

                        </table>
                      </td>
                    </tr>
                  </table>
                  <!-- // CONTENT TABLE -->

                </td>
              </tr>
            </table>
            <!-- // FLEXIBLE CONTAINER -->
          </td>
        </tr>
      </table>
      <!-- // CENTERING TABLE -->
    </td>
  </tr>
