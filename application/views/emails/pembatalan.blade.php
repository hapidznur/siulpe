@extends('emails.template')

@section('email-type', 'Informasi Open Trip')

@section('body')
  @include('emails.pieces.dataemail')
  <tr>
    <td align="center" valign="top">
      <!-- CENTERING TABLE // -->
      <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
        <tr>
          <td align="center" valign="top">
            <!-- FLEXIBLE CONTAINER // -->
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
              <tr>
                <td align="center" valign="top" width="500" class="flexibleContainerCell">
                  <table border="0" cellpadding="30" cellspacing="0" width="100%">
                    <tr>
                      <td align="center" valign="top">

                        <!-- CONTENT TABLE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                            <td valign="top" class="textContent">
                              <!--
                                The "mc:edit" is a feature for MailChimp which allows
                                you to edit certain row. It makes it easy for you to quickly edit row sections.
                                http://kb.mailchimp.com/templates/code/create-editable-content-areas-with-mailchimps-template-language
                              -->
                              <div mc:edit="body" style="font-family:Helvetica,Arial,sans-serif;font-size:13px;margin-bottom:0;line-height:135%;text-align:justify">
                                Mohon Maaf trip yang sebagaimana dijelaskan diatas terpaksa dibatlkan dikarenakan jumlah peserta tidak mencukupi kuota. jika berkenan anda bisa mengikuti jadwal trip selanjutnya. Terimakasih
                              </div>
                            </td>
                          </tr>
                        </table>
                        <!-- // CONTENT TABLE -->

                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <!-- // FLEXIBLE CONTAINER -->
          </td>
        </tr>
      </table>
      <!-- // CENTERING TABLE -->
    </td>
  </tr>

@endsection
