<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1>Welcome to Sistem Informasi Layanan Pengadaan Langsung</h1>
    <p>
      Click the link below to confirm your acccount
    </p>
    <a href="{{base_url("auth/confirmation?email_token={$token}")}}">
        {{base_url("auth/confirmation?email_token={$token}")}}
    </a>
  </body>
</html>
