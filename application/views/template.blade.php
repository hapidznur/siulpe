<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <title>@yield('title')</title>

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ base_url('css/bootstrap.min.css')}}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    
    <link rel="stylesheet" href="{{ base_url('Admin-lte/dist/css/AdminLTE.min.css')}}">

    <!-- Theme style -->
    <link rel="stylesheet" type="text/css" href="{{ base_url('Admin-lte/dist/css/skins/_all-skins.min.css')}}"> 
    <!-- iCheck -->
    <!-- <link rel="stylesheet" type="text/css" href="{{ base_url('plugins/iCheck/flat/black.css')}}"> -->
    <!-- Morris chart -->
    <link rel="stylesheet" type="text/css" href="{{ base_url('plugins/morris/morris.css')}}">
    <!-- jvectormap -->
    <!-- <link rel="stylesheet" type="text/css" href="{{base_url('css/jquery-jvectormap.css')}}"> -->
    <!-- Daterange picker -->
    <link rel="stylesheet" type="text/css" href="{{base_url('plugins/daterangepicker/daterangepicker.css')}}">

    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" type="text/css" href="{{base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

    <script src="{{base_url("assets/templates/js/modernizr.js")}}"></script>
    @yield('style')

</head>

<body class="skin-red sidebar-mini wysihtml5-supported">
    <div class="wrapper">
        @yield('pieces')
        
        @yield('body')
        <div class="gap"></div>
    </div> <!-- End of Wrapper -->


        <!-- Moment JS  -->
        <script src="{{ base_url('js/moment.min.js') }}"></script>

        <!-- jQuery 2.2.3 -->
        <script src="{{ base_url('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>


        <!-- Bootstrap 3.3.6 -->
        <script src="{{ base_url('js/bootstrap.min.js')}}"></script>

        <!-- Morris.js charts -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

        <script src="{{ base_url('plugins/morris/morris.min.js')}}"></script>

        <!-- Sparkline -->
        <script src="{{ base_url('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
        <!-- <script src="{{base_url('js/jquery.sparkline.min.js')}}"></script> -->
        <!-- jvectormap -->
        <!--<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>-->
        <script src="{{base_url('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
        <script src="{{base_url('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>

        <!-- jQuery Knob Chart -->
        <script src="{{ base_url('js/jquery.knob.min.js')}}"></script>
        
        <!-- daterangepicker -->
        <script src="{{base_url('plugins/daterangepicker/daterangepicker.js')}}"></script>

        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{base_url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
        <!-- Slimscroll -->
        <script src="{{base_url('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
        <!-- FastClick -->
        <script src="{{base_url('plugins/fastclick/fastclick.js')}}"></script>
        <!-- AdminLTE App -->
        <script src="{{base_url('Admin-lte/dist/js/app.min.js')}}"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{{base_url('Admin-lte/dist/js/pages/dashboard.js')}}"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="{{base_url('Admin-lte/dist/js/demo.js')}}"></script>

        <!-- Menu Toggle Script -->
        <script>
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        </script>
        <script>
        $(function(){

            $(".dropdown-menu li a").click(function(){

                $(".dropdown-toggle:first-child").text($(this).text());
                $(".dropdown-toggle:first-child").val($(this).text());
            });
        });
        </script>

        <script type="javascript">
        function showPassword() {

            var key_attr = $('#key').attr('type');

            if(key_attr != 'text') {

                $('.checkbox').addClass('show');
                $('#key').attr('type', 'text');

            } else {

                $('.checkbox').removeClass('show');
                $('#key').attr('type', 'password');

            }

        }
    </script>
    @yield('script')
</body>
</html>
