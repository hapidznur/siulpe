@extends('unsigntemplate')

@section('title')
  Forgot password sent
@endsection

@section('body')
<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>SIULPE</b></a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Forgot Password Notification</p>
      <div class="form-group has-feedback">
	      <p style="text-align: center"><label> Email sudah dikirim. Silahkan chek email anda</label></p>
      </div>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->
@endsection
