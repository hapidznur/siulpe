@extends('template')

@section('title')
  Sign up
@stop

@section('body')
  <div class="">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <p class="text-center">Sign up</p>
        @if ($controller->input->get('error') === 'email')
        <div class="alert-danger">
          <p>
            Email already taken
          </p>
        </div>
        @endif
        <form action="{{base_url("auth/aksi-signup") }}" method="post">
          <div class="form-group">
            <label>Nama</label>
            <input class="form-control" type="text" name="nama" placeholder="Nama" required>
          </div>
          <div class="form-group">
            <label>Username</label>
            <input class="form-control" type="text" name="username" placeholder="Username" required>
          </div>
          <div class="form-group">
            <label>Email</label>
            <input class="form-control" type="email" name="email" placeholder="example@host.com" required>
          </div>
          <div class="form-group">
            <label>Password</label>
            <input class="form-control" type="password" name="password" placeholder="Password" required>
          </div>
          <div class="form-group">
            <label>Retype Password</label>
            <input class="form-control" type="password" name="password_confirmation" placeholder="Retype Password" required>
          </div>
          <div class="text-center">
            <button type="submit" class="btn btn-primary" style="width: 100%" >Sign up</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@stop
