@extends('template')

@section('title')
Resend email confimation
@endsection

@section('body')
<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>SIULPE</b></a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Masukkan Email</p>
      <form action="{{base_url('auth/aksi-resend-email-confirmation')}}" method="post">
        <div class="form-group has-feedback">
            <label>Email</label>
            <input type="email" class="form-control" name="email" placeholder="example@host.com" value="{{auth()->user()->email}}" disabled>
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-xs-4 pull-right">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
  </div>
</div>
@endsection
