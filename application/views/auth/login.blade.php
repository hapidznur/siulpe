@extends('unsigntemplate')

@section('style')
<link rel="stylesheet" type="text/css" href="{{base_url('less/style.css')}}">
@endsection


@section('title', 'signin')

@section('body')
<div class="login-box">
    <div class="login-logo">
        <a href="{{ base_url('pengadaan') }}"><b>Sistem Infomasi</b><br><span style="font-size: 0.7em">Unit Layanan Pengadaan</span></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
      @if($controller->session->error_message)
        <div>
          <p style="color: red; text-align: center">
                  {{$controller->session->error_message}}
          </p>
        </div>
        @endif
        <form action="{{ base_url('validation') }}" method="post">
        <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="username" name="name" id="username">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="password" id="password" >
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <select name="role" id="role" class="role form-control">
                <option value="penyedia">Penyedia</option>
                <option value="ulp">ULP</option>
                <option value="ppk">PPK</option>
            </select>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <button type="submit" class="btn btn-success btn-block btn-flat">Sign In</button>
            </div>
            <!-- /.col -->
            <div class="col-xs-6">
                <a href="{{base_url('signup') }}" class="btn btn-primary btn-block btn-flat">Sign
                    Up Penyedia</a>
            </div>
            <!-- /.col -->
        </div>
        </form>
        <div class="social-auth-links text-center">
            <a href="{{base_url('auth/forgot_password')}}">I forgot my password</a><br>
        </div>
    </div>
    <!-- /.login-box-body -->
</div>
@stop