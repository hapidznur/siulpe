@extends('unsigntemplate')

@section('title')
Forgot Password
@endsection

@section('body')
<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>SIULPE</b></a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Masukkan Email</p>

    <form class="text" action="{{base_url("auth/send-email-forgot")}}" method="post">
      <div class="form-group has-feedback">
        <input class="form-control" type="email" name="email" placeholder="example@host.com" required="">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4 pull-right">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->
@endsection
