@extends('unsigntemplate')

@section('title')
  Confirm your email
@stop

@section('body')
 <div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>SIULPE</b></a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Forgot Password Notification</p>
	    <div class="form-group has-feedback">
			  <h1>Oops. Confirm your email first.</h1>
			  <p>
			    Don't receive an email? Check your spam. Or resend an email confirmation <a href="{{base_url("auth/resend-email-confirmation")}}">here</a>.
			  </p>
		</div>
	</div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->
@stop
