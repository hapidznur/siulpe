@extends('template')

@section('title')
  Reset Password
@endsection

@section('body')
<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>SIULPE</b></a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Forgot Password Notification</p>
      <div class="form-group has-feedback">
        <form action="{{base_url("auth/attempt_password")}}" method="post">
          <input type="hidden" name="password_token" value="{{$controller->input->get('token')}}">
          <div class="form-group">
            <label>New Password</label>
            <input class="form-control" type="password" name="password" placeholder="password" required="">
          </div>
          <div class="form-group">
            <label>Confirm Password</label>
            <input class="form-control" type="password" name="password_confirmation" placeholder="retype password" required="">
          </div>
          <div class="row">
          <!-- /.col -->
            <div class="col-xs-4 pull-right">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
            </div>
          <!-- /.col -->
        </div>
        </form>
      </div>
  </div>
</div>
@endsection
