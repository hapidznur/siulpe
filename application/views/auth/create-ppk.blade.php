@extends('unsigntemplate')

@section('style')
<link rel="stylesheet" type="text/css" href="{{base_url('less/style.css')}}">
@endsection

@section('pieces')
@include('pieces.unsignheader')
@stop

@section('title', 'signup')

@section('body')
<div class="col-xs-12 col-sm-12 col-md-12 content-wrapper">
  <div class="container">
    <div class="col-md-offset-4 signup">
      <div class="col-md-6 signup-box">
        <div class="box-title">
          <h3 class="text-center">Register Rekanan</h3>
        </div>
        <hr>
        <form action="{{base_url("auth/ppk_register")}}" method="post">
        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-5 control-label" for="namapenyedia">Nama PPK</label>
          <div class="col-md-12">
            <input id="namapenyedia" name="namapenyedia" type="text" placeholder="Nama Perusahaan dengan jelas" class="form-control input-md" required>
            <span class="help-block"><small>Tuliskan nama perusahaan/CV/Organisasi milik Anda</small></span>
          </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-5 control-label" for="pemilik">Satuan Kerja</label>
          <div class="col-md-12">
            <input id="pemilik" name="pemilik" type="text" placeholder="Nama pemilik dengan jelas" class="form-control input-md" data-maxlength="50" required>
            <span class="help-block"></span>
          </div>
        </div>
        <!-- Textarea -->
<!--         <div class="form-group">
          <label class="col-md-5 control-label" for="alamat">Alamat</label>
          <div class="col-md-12">
            <textarea class="form-control" id="alamat" name="alamat" required></textarea>
            <span class="help-block"> </span>
          </div>
        </div> -->
        <div class="form-group">
          <label class="col-md-5 control-label" for="contact">Role</label>
          <div class="col-md-12">
            <input id="role" name="role" type="text" onblur="checkEmail();" placeholder="Tuliskan alamat email yang bisa dihubungi" class="form-control input-md" required="">
            <span class="help-block email-help">Tuliskan alamat email yang bisa dihubungi</span>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-5 control-label" for="contact">Email</label>
          <div class="col-md-12">
            <input id="email" name="email" type="email" onblur="checkEmail();" placeholder="Tuliskan alamat email yang bisa dihubungi" class="form-control input-md" required="">
            <span class="help-block email-help">Tuliskan alamat email yang bisa dihubungi</span>
          </div>
        </div>
        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-5 control-label" for="pemilik">Password</label>
          <div class="col-md-12">
            <input id="password" name="password" type="password" placeholder="Password" class="form-control input-md" data-maxlength="48" required>
            <span class="help-block"></span>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-5 control-label" for="pemilik">Retype Password</label>
          <div class="col-md-12">
            <input id="password" name="passwordconfirm" type="password" placeholder="Password Confirmation" class="form-control input-md" data-maxlength="48" required>
            <span class="help-block"></span>
          </div>
        </div>        
<!--         <div class="form-group">
          <label class="col-md-5 control-label" for="contact">NPWP</label>
          <div class="col-md-12">
            <input id="npwp" name="npwp" type="url" placeholder="Tuliskan link scan dokumen npwp milik Anda" class="form-control input-md" required="">
            <span class="help-block"><small>Link scan dokumen npwp (Include kan http://)</small></span>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-5 control-label" for="contact">PKP</label>
          <div class="col-md-12">
            <input id="pkp" name="pkp" type="url" placeholder="Tuliskan link scan dokumen PKP milik Anda" class="form-control input-md" required="">
            <span class="help-block"><small>Link scan dokumen npwp (Include kan http://)</small></span>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-5 control-label" for="contact">SIUP</label>
          <div class="col-md-12">
            <input id="siup" name="siup" type="url" placeholder="Tuliskan link scan dokumen SIUP milik Anda" class="form-control input-md" required="">
            <span class="help-block"><small>Link scan dokumen npwp (Include kan http://)</small></span>
           </div>
        </div>
        <div class="form-group">
          <label class="col-md-5 control-label" for="contact">Akta Notaris</label>
          <div class="col-md-12">
            <input id="akta_notaris" name="akta_notaris" type="url" placeholder="Tuliskan link scan dokumen akta notaris milik Anda" class="form-control input-md" required="">
           <span class="help-block"><small>Link scan dokumen npwp (Include kan http://)</small></span>
           </div>
        </div> -->
        <!-- Button -->
        <div class="form-group">
          <div class="col-md-4">
            <button id="daftar" name="daftar" class="btn btn-default">Daftar</button>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
@stop
@section('script')
  <script type="text/javascript" src="{{base_url('js/siulpe.js')}}"></script>
@stop