<div class="col-sm-10 col-sm-offset-2 col-md-10 col-lg-offset-2 main">
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-hover table-responsive table-bordered table-pengadaan">
                    <thead>
                        <tr>
                            <th style="width: 5%;">No</th>
                            <th>Proses</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Akhir</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i=1; foreach ($kalendar as $kalender) {?>
                    <?php foreach ($proses as $bisnis => $value) {?>
                        <tr id="<?php echo $value; ?>">
                            <td ><?php echo $i; ?></td>
                            <td class="col-sm-3 col-md-3" id="<?php echo $bisnis;?>"><?php echo $bisnis;?></td>
                            </td>
                            <td class="proses" id="p_<?php echo $i."_mulai"?>" data-user="p_<?php echo $i."_mulai"?>" data-proses="<?php echo $bisnis;?>"><?php echo $kalender["p_".$i."_mulai"]?></td>
                            <td class="proses" id="p_<?php echo $i."_akhir"?>" data-user="p_<?php echo $i."_akhir"?>" data-proses="<?php echo $bisnis;?>"><?php echo $kalender["p_".$i."_akhir"]?></td>
                        </tr>
                    <?php $i++; } ?>
                    <input type="hidden" id="userjadwal" value="<?php echo $kalender["id_jadwal"]?>">
                    <?php  } ?>
                    </tbody>
                </table>
            </div><!--end table-responsive -->
        </div><!--end col-xs-10 col-sm-10 col-md-10-->
    </div> <!--end row-->
</div> <!--end container-fluid content-->

<div class="modal fade" id="jadwal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><label>Proses</label> <label class="prosesbisnis"></label></h4>
      </div>
      <div class="modal-body">
        <!-- <p>
          <label for="edit">Edit Tanggal Proses</label>
        </p> -->
        <p>
        <h4 style="text-align:center"><div id="alertkosong" ></div></h4>
        <div class="form-group">
            <span><label>Tanggal Baru</label></span>
            <div class="input-group">
                <input type="text" class="form-control datepick" id="editproses" readonly="readonly" placeholder="Tanggal">
            </div>
        </div>
        <input type="hidden" id="identify">
        <input type="hidden" id="DetailsHidden">

        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="submit">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->