@extends('template')

@section('title', 'Lihat Jadwal Pengadaan')

@section('pieces')
  @include('ppk.ppk-menu')
@stop

@section('style')
  <link rel="stylesheet" type="text/css" href="{{base_url('plugins/datatables/jquery.dataTables.css')}}">
  <link rel="stylesheet" type="text/css" href="{{base_url('plugins/datatables/dataTables.bootstrap.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ base_url('plugins/datatables/extensions/Editor/css/editor.dataTables.min.css')}}">
@endsection

@section('body')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Informasi Harga
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">Informasi Harga</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-xs-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Permintaan Informasi Harga</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          	<div class="row">
          		<div class="col-sm-12">
		          	<table id="example2" class="table table-bordered table-hover dataTable" aria-describedby="example2_info">
			            <thead>
			                <tr role="row">
				                <th>ROWID</th>
				                <th>Proses</th>
				                <th>Tanggal</th>
			                </tr>
			            </thead>	            
			        </table>
			    </div>
		    </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer" style="margin-left: 15px;">
          </div>
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (left) -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
@stop

@section('script')
	<script src="{{base_url('plugins/datatables/jquery.dataTables.min.js')}}"></script>

	<script src="{{ base_url('plugins/datatables/dataTables.bootstrap.min.js')}}" type="text/javascript"></script>
	<script src="{{ base_url('plugins/datatables/extensions/Editor/js/dataTables.editor.min.js')}}" type="text/javascript" charset="utf-8" async defer></script>

	<script>
	    $('#example2').DataTable( {
		    paging: false,
			"ordering": false,
			"searching": false,
	        "ajax": {
	            "url": "{{base_url('penjadwalan/datatables')}}",
	            "type": "GET"
	        },
	        "columns": [
	            { "data": "rowID" },	        
	            { "data": "proses" },
	            { "data": "tanggal" }
	        ],
	        "columnDefs": [
            {
                "targets": [0],
                "visible": false
            }
        ]
	    } );
	    $('#example2').on( 'click', 'tbody td:not(:first-child)', function (e) {
			    } );


	</script>
@stop