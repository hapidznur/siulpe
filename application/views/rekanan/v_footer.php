<footer class="main-footer" style="height: 10px;">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.6
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
</footer>

<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>

</div>
<!-- Bootstrap Core JavaScript -->
<!-- Core Scripts - Include with every page -->
<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url('ext/js/jquery.min.js'); ?>"></script>
<!--<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>-->
<!-- jQuery UI 1.11.4 -->
<!--<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>-->
<script src="<?php echo base_url('ext/js/jquery-ui.min.js'); ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('ext/js/bootstrap.min.js'); ?>"></script>
<!--<script src="bootstrap/js/bootstrap.min.js"></script>-->
<!-- Morris.js charts -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>-->
<script src="<?php echo base_url('ext/js/raphael.min.js'); ?>"></script>
<!--<script src="plugins/morris/morris.min.js"></script>-->
<script src="<?php echo base_url('ext/js/morris.min.js'); ?>"></script>
<!-- Sparkline -->
<!--<script src="plugins/sparkline/jquery.sparkline.min.js"></script>-->
<script src="<?php echo base_url('ext/js/jquery.sparkline.min.js'); ?>"></script>
<!-- jvectormap -->
<!--<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>-->
<script src="<?php echo base_url('ext/js/jquery-jvectormap.min.js'); ?>"></script>

<!--<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>-->
<!-- jQuery Knob Chart -->
<!--<script src="plugins/knob/jquery.knob.js"></script>-->
<script src="<?php echo base_url('ext/js/jquery.knob.min.js'); ?>"></script>
<!-- daterangepicker -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>-->
<script src="<?php echo base_url('ext/js/moment.min.js'); ?>"></script>
<!--<script src="plugins/daterangepicker/daterangepicker.js"></script>-->
<script src="<?php echo base_url('ext/js/daterangepicker.min.js'); ?>"></script>
<!-- datepicker -->
<!--<script src="plugins/datepicker/bootstrap-datepicker.js"></script>-->
<script src="<?php echo base_url('ext/js/bootstrap-datepicker.js'); ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<!--<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>-->
<script src="<?php echo base_url('ext/js/bootstrap3-wysihtml5.all.min.js'); ?>"></script>
<!-- Slimscroll -->
<!--<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>-->
<script src="<?php echo base_url('ext/js/jquery.slimscroll.min.js'); ?>"></script>
<!-- FastClick -->
<!--<script src="plugins/fastclick/fastclick.js"></script>-->
<script src="<?php echo base_url('ext/js/fastclick.min.js'); ?>"></script>
<!-- AdminLTE App -->
<!--<script src="dist/js/app.min.js"></script>-->
<script src="<?php echo base_url('ext/js/app.min.js'); ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="dist/js/pages/dashboard.js"></script>-->
<script src="<?php echo base_url('ext/js/dashboard.js'); ?>"></script>
<!-- AdminLTE for demo purposes -->
<!--<script src="dist/js/demo.js"></script>-->
<script src="<?php echo base_url('ext/js/demo.js'); ?>"></script>

<!-- Datatable JQuery Plugin-->
<script src="<?php echo base_url('ext/js/jquery.dataTables.js')?>"></script>

<!-- Datatable JQuery UI Bootstrap Plugin-->
<script src="<?php echo base_url('ext/js/dataTables.bootstrap.js')?>"></script>

<!--Datatable-->
<script>
    $('#example').DataTable();
</script>

<!-- /.Simple Calculator-->
<script>
    $("#hargarekanan").keydown(function(event) {
        // Allow only backspace and delete
        if( event.keyCode == 46 ||
            event.keyCode == 8  ||
            event.keyCode == 13 ||
            event.keyCode == 9)
        {
            // let happen
        }
        else
        {
            // Ensure that it is a number and stop the keypress
            if (event.keyCode < 48 || event.keyCode > 57 )
            {
                event.preventDefault();
            }
        }
    });
    function total() {
        var total1 = 0;
        $('span#jumlah').each(function () {
            var n = parseFloat($(this).text());
            total1 += isNaN(n) ? 0 : n;
        });
        $('#jumlahtotal').text(total1.toFixed(0));
        $('.jumlahtotal').val(total1.toFixed(0));
    }

    $('input#hargarekanan').keyup(function () {
        var val = parseFloat($(this).val());
        val = (val ? val * $(this).data('harga') : '');
        $(this).closest('td').next().find('span#jumlah').text(val);
        $(this).closest('td').next().find('input.totalhidden').val(val);
        total();
    });

    $('input#hargarekanan').focusout(function () {
        var val = parseFloat($(this).val());
        val = (val ? val * $(this).data('harga') : '');
        $(this).closest('td').next().find('span#jumlah').text(val);
        $(this).closest('td').next().find('input.totalhidden').val(val);
        total();
    });

    $('input#hargarekanan').on({
        focus: function () {
            if (this.value == '0') this.value = '';
        },
        blur: function () {
            if (this.value === '') this.value = '0';
        }
    });

    $("button.spesifikasi").click(function(e){
        docCookies.setItem($('#id_barang').val(), $('#spesifikasi').val(), 864e2, "/");
        window.close();
    });
    // $('#submitundangan').submit(function(e){
    //     e.preventDefault();
    //     // var te = $(this).find('input:file').closest();
    //     // console.log(te);
    // });

    // $('input#undangan').on('change', prepareUpload);

    // function prepareUpload(event){
    //     files = event.target.files;
    // }

    // $('form.undangan').on('submit',function(event){
    //     event.stopPropagation();
    //     event.preventDefault();

    //     var data = new FromData();
    //     $.each(files, function (key, value) {
    //         data.append(key,value);
    //     });
    //     $.ajax({
    //         url:'',
    //         type'POST',
    //         cache:false,
    //         dataType:'json',
    //         processData:false,
    //         contentType:false,
    //         success:function(data, textStatus, jqXHR) {
    //             if(typeof data.error === 'undefined') {
    //                 submitForm (event, data);
    //             }
    //             else {
    //                 console.log('error' + data.error)
    //             }
    //         },
    //         error:function(jqXHR,textStatus, errorThrown) {
    //             console.log(textStatus);
    //         }
    //     });
    // });

    // function submitForm(event, data) {
    //     $form = $(event.target);

    //     var formData = $form.serialize();
    //     $.each(data.files,function(key, value){
    //         formData = formData + '&filename[]=' + value;
    //     });
    //      $.ajax({
    //         url:'',
    //         type'POST',
    //         cache:false,
    //         dataType:'json',
    //         success:function(data, textStatus, jqXHR) {
    //             if(typeof data.error === 'undefined') {
    //                 submitForm (event, data);
    //             }
    //             else {
    //                 console.log('error' + data.error)
    //             }
    //         },
    //         error:function(jqXHR,textStatus, errorThrown) {
    //             console.log(textStatus);
    //         },
    //         complete: function(){

    //         }
    // }

</script>

<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
<script>
    $(function () {

        $(".dropdown-menu li a").click(function () {

            $(".dropdown-toggle:first-child").text($(this).text());
            $(".dropdown-toggle:first-child").val($(this).text());
        });
    });
</script>

<script type="text/javascript">
    function showPassword() {

        var key_attr = $('#key').attr('type');

        if (key_attr != 'text') {

            $('.checkbox').addClass('show');
            $('#key').attr('type', 'text');

            } else {

            $('.checkbox').removeClass('show');
            $('#key').attr('type', 'password');

        }

    }
</script>

</body>
</html>
