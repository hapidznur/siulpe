<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Blank page
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Responsive Hover Table</h3>

                            <div class="box-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control pull-right"
                                           placeholder="Search">

                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <thead>
                                    <th>No</th>
                                    <th>Paket</th>
                                    <th>Undangan</th>
                                    <th>Kelengkapan</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>

                                <?php $i = 1;
                                foreach ($undangan as $undangan) { ?>
                                    <?php if ($undangan['undangan'] == 'submit') { ?>
                                        <tr><span class="text-center"> Tidak Ada Undangan</span></tr>
                                    <?php } else { ?>
                                        <tr>
                                        <?php echo form_open_multipart('rekanan/submit_harga','id="submitundangan",data-toggle="validator" role="form"'); ?>
                                            <td ><?php echo $i; ?></td>
                                            <td ><?php echo $undangan['paket_name'] ?></td>
                                            <td >Download Undangan, IKP, LDP</td>
                                            <td ><input type="file" class="form-input" name="suratpenawaran" required></td>
                                            <td >
                                                <input type="hidden" name="pengadaan"
                                                       value="<?php echo $undangan['id_pengadaan']; ?>">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </td>
                                        </tr>
                                <?php form_close();?>
                                    <?php } ?>
                                   <?php } ?>
                                    </tbody>
                            </table>
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>
            <!-- /.box-body -->

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->