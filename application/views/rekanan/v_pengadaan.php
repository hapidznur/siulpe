<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Pengadaan
		</h1>
<!--        Sementara Breadcumb off ->
<!--		<ol class="breadcrumb">-->
<!--			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>-->
<!--			<li><a href="#">Examples</a></li>-->
<!--			<li class="active">Blank page</li>-->
<!--		</ol>-->
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- Default box -->
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Pengadaan tersedia</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body table-responsive no-padding">
				<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
					<tr>
						<th >Paket Pengadaan</th>
						<th >Proses</th>
						<th >Tanggal</th>
						<th >Pukul</th>
						<th >Action</th>
					</tr>
					</thead>
					<tbody>
				<?php
					foreach ($artikel as $count_data):
				?>
					<tr>
						<td class="col-md-6"><?php echo $count_data['paket_name']; ?></td>
						<td ><?php echo $count_data['status_pengadaan'] === '2'? 'Harga Barang': 'Informasi Harga'; ?></td>
						<td ><?php echo $count_data['tanggal_permintaan']; ?></td>
						<td ><?php echo $count_data['jam']; ?></td>
						<td id="action">
                            <p>
                                <a href="<?php echo base_url().'rekanan/detail_barang/'.$count_data['id_pengadaan'];?>" class="btn btn-default" > Detail</a>
                                <a href="<?php echo base_url('rekanan/detail_penawaran').'?pengadaan='.$count_data['id_pengadaan'];?>" class="btn btn-success">Informasi Harga</a>
                            </p></td>
					</tr>
                <?php
	    			endforeach;
            	?>
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->