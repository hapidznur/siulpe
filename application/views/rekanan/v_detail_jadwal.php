<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pengadaan
        </h1>
        <!--        Sementara Breadcumb off ->
        <!--        <ol class="breadcrumb">-->
        <!--            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>-->
        <!--            <li><a href="#">Examples</a></li>-->
        <!--            <li class="active">Blank page</li>-->
        <!--        </ol>-->
    </section>
    <!--    /.content-header -->
    <section class="content">
        <div class="box">
            <table class="table table-hover table-responsive table-bordered">
                <thead>
                    <tr>
                        <th >No</th>
                        <th>Proses</th>
                        <th>Tanggal Mulai</th>
                        <th>Tanggal Akhir</th>
                    </tr>
                </thead>
                <tbody>
                <?php $i=1; foreach ($kalender as $kalender) {?>
                <?php foreach ($proses as $bisnis) {?>
                    <tr <?php echo !empty($aktif) ? $aktif : ''  ;?>>
                        <td ><?php echo $i; ?></td>
                        <td ><?php echo $bisnis['nama'];?></td>
                        <td ><?php echo $kalender["p_".$i."_mulai"]?></td>
                        <td ><?php echo $kalender["p_".$i."_akhir"]?></td>
                    </tr>
                <?php  $i++; } ?>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <!-- /.box    -->
    </section>
    <!-- /.content    -->
</div>
<!-- /.content-wrapper -->