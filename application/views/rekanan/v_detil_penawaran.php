<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pengadaan
        </h1>
        <!--        Sementara Breadcumb off ->
        <!--        <ol class="breadcrumb">-->
        <!--            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>-->
        <!--            <li><a href="#">Examples</a></li>-->
        <!--            <li class="active">Blank page</li>-->
        <!--        </ol>-->
    </section>
    <!--    /.content-header -->
    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Submit Informasi Harga Barang atau Jasa</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th >Barang/Jasa
                        <th class="text-center</th> col-md-1">Rincian</th>
                        <th class="text-center col-md-1">Satuan</th>
                        <th class="text-center col-md-1">Banyak</th>
                        <th >Harga Non PPn</th>
                        <th >Jumlah</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php echo form_open_multipart('rekanan/submit_harga','id="submitharga",data-toggle="validator" role="form"'); ?>
                    <fieldset>
                    <?php $i=0; foreach ($barang as $barang ){ ?>
                        <tr>
                            <td class="text-center"><?php echo $i; ?></td>
                            <td class="text-center"><?php echo $barang->nama_barang; ?></td>
                            <td class="text-center"><a class="btn btn-link" onclick="window.open('<?php echo base_url().'rekanan/spesifikasi?pengadaan='.$barang->id_pengadaan.'&barang='.$barang->id_barang; ?>', 'spesifikasi', 'height=900,width=600')">Rincian</a></td>
                            <td class="text-center"><?php echo $barang->satuan ?></td>
                            <td class="text-center"><span id="volume" name="volume"><?php echo $barang->volume ?></span></td>
                            <td class="col-md-2"><input type="text" name="harga_tawar[]" value="<?php echo set_value('options[]'); ?>" class="form-control input-md" id="hargarekanan" data-harga="<?php echo $barang->volume ?>" required></td>
                            <td class="col-md-3 text-center"><span id="jumlah"></span><input type="hidden" name="jumlah[]" class="totalhidden"></td>
                        </tr>
                        <input type="hidden" name="pengadaan" value="<?php echo $barang->id_pengadaan;?>">
                        <input type="hidden" name="detailed[]" value="<?php echo $barang->id_barang;?>">
                        <input type="hidden" name="jumlahtotal" class="jumlahtotal">
                        <?php $i++;}?>
                    <tr>
                        <td colspan="6"><strong>Jumlah Total</strong></td>
                        <td style="text-align:center"><span id="jumlahtotal"><strong>0</strong></span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div id="surat-informasi" class="box box-danger" style="margin-top: 20px;">
                <div class="box-header" >
                    <h5 class="box-title">Surat Informasi Harga Barang atau Jasa</h5>
                </div>
                <div class="box-body" style="padding-bottom: 20px;">
                    <div class="form-group">
                        <input type="file" name="suratinformasi" id="surat-informasi" class="form-control input-md" required>
                    </div>
                </div>
            </div>
            <div class="submitharga">
                <p style="text-align: center">
                    <button type="submit" class="btn btn-default" name="clear" alt="">Clear</button>
                    <input type="submit" class="btn btn-primary" placeholder="Submit" alt="">
                </p>
            </div>
            </fieldset>
            <?php echo form_close() ?>
        </div>
        <!-- /.box    -->
    </section>
    <!-- /.content    -->
</div>
<!-- /.content-wrapper -->
