@extends('template')

@section('body')
<div class="container" style="margin-top:10px;">
  <div class="row">
    <div class="col-md-9">
      @yield('body-inner')
      <div class="gap"></div>
      <div class="show-on-small">
        @foreach($leftBarAds as $key => $ads)
        <div class="" style="margin-top: 10px">
          @if($ads->jenis === 'gambar')
          <img src="{{$ads->image_src}}" style="width: 100%" alt="{{$ads->nama}}" />
          @else
          {!!$ads->script!!}
          @endif
        </div>
        @endforeach
      </div>
      <div class="">
        @foreach($bottomBodyAds as $key => $ads)
        <div class="" style="margin-top: 10px">
          @if($ads->jenis === 'gambar')
          <img src="{{$ads->image_src}}" style="max-width: 100%" alt="{{$ads->nama}}" />
          @else
          {!!$ads->script!!}
          @endif
        </div>
        @endforeach
      </div>
    </div>
    <div class="col-md-3">
      @yield('ads-gap')
      <aside class="hidden-on-small">
        @foreach($leftBarAds as $key => $ads)
        <div class="" style="margin-top: 10px">
          @if($ads->jenis === 'gambar')
          <img src="{{$ads->image_src}}" style="width: 100%" alt="{{$ads->nama}}" />
          @else
          {!!$ads->script!!}
          @endif
        </div>
        @endforeach
      </aside>
    </div>
    @yield('after-ads')
  </div>
  @endsection