@extends('article.template')

@section('style')
<meta property="og:url" content="{{url("blog/{$blog->id}")}}" />
<meta property="og:title" content="{{$blog->title}}" />
<meta property="og:description" content="{{$blog->summary}}" />
<meta property="og:image" content="{{url("assets/img/blog/{$blog->figure}")}}" />
@endsection

@section('title')
{{$blog->title}}
@endsection

@section('ads-gap')
<div class="gap"></div>
@stop

@section('body-inner')
<ul class="breadcrumb">
    <li><a href="{{url('')}}">Home</a>
    </li>
    <li><a href="{{url('blog')}}">Blog</a>
    </li>
    <li><a href="#">{{$blog->title}}</a>
    </li>
</ul>
<div style="margin-top:20px"></div>
<div class="preview" style="margin-bottom:3px;">
  <img src="{{base_url('assets/img/blog/'.$blog->figure)}}" alt="{{$blog->title}}" title="{{$blog->title}}" style="max-height:500px;width:100%; object-fit: cover; margin-right: 15px">
  <article>
      <div class="gap gap-small"></div>
      <h3 style="margin-left:0px"><i><strong>{{$blog->title}}</strong></i></h3>
      {!! $blog->article !!}
  </article>
  <div class="gap"></div>
  <div>
      <h4>Recommended Article</h4>
      <ul>
          @foreach($recommendedArticles as $article)
          <li><a href="{{url("blog/detail/{$article->id}")}}">{{$article->title}}</a></li>
          @endforeach
      </ul>
  </div>
  <br />
  <div>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <div class="addthis_sharing_toolbox"></div>
  </div>
  <div style="padding: 10px; border: 1px solid rgba(0, 0, 0, 0.4); margin-top: 10px">
    Ditulis oleh Admin
  </div>
</div>
@endsection

@section('after-ads')
<div class="row">
  <div class="col-md-9">
    <div style="margin: 10px">
      <div class="gap"></div>
      <div class="gap">
        <h4>{{count($blog->comments)}} Comments</h4>
        @foreach($blog->comments as $comment)
        <div class="box" style="margin-bottom: 10px">
          <div style="overflow: hidden">
            <h5 style="float: left"><strong>{{$comment->nama}}</strong></h5>
            <span style="float: right">{{$comment->created_at->toFormattedDateString()}}</span>
          </div>
          <hr style="margin-top: 5px; margin-bottom: 5px">
          <p>
            {{$comment->body}}
          </p>
        </div>
        @endforeach
      </div>
      <div class="box" style="margin-top: 10px">
        <h4>Beri Komentar</h4>
        <form method="post" action="{{url("blog/post-comment/{$blog->id}")}}">
          @if($controller->session->errors)
          <div class="alert alert-danger">
            Messages
            <ul>
              @foreach($controller->session->errors as $key => $errors)
              @foreach($errors as $key => $error)
              <li>{{$error}}</li>
              @endforeach
              @endforeach
            </ul>
          </div>
          @endif
          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" class="form-control" placeholder="Nama" required="" value="{{old('nama')}}">
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" class="form-control" placeholder="Nama" required="" value="{{old('email')}}">
          </div>
          <div class="form-group">
            <label>Comment</label>
            <textarea type="text" name="comment" class="form-control" placeholder="Comment" required="">{{old('comment')}}</textarea>
          </div>
          <div class="text-right">
            <div style="overflow: hidden">
              <div style="float: right;" class="g-recaptcha" data-sitekey="6LdTCyETAAAAAC2oAzUCfdXMI_J6zJ_OIIM-IyBG"></div>
            </div>
            <button style="margin-top: 10px" type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@stop

@section('script')
<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57997d977d74077f"></script>
@endsection