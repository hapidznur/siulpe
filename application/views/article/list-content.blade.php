@extends('admin.template')

@section('title', "Blog")
@section('page-title', 'Blog')
@section('style')
<style type="text/css">
a.morelink{
  color: #0254EB
}
a.morelink:visited {
  color: #0254EB
}
.comment {
  width: 400px;
  background-color: #f0f0f0;
  margin: 10px;
 }
a.morelink {
  text-decoration:none;
  outline: none;
}
.morecontent span {
  display: none;

}
</style>
@stop
@section('body')
<div class="col-sm-10 col-xs-10 col-md-12"  style="height:900px">
  <div class="x_panel">
      <div class="x_title">
        <h2>Blog</h2>
        <div class="clearfix"></div>
      </div>
      <div style="display: block;" class="x_content">
        <br>
        @if($controller->session->errors)
          <div class="col-md-offset-3 col-md-7">
            <div class="alert alert-danger">
              <b>Messages</b>
              <ul>
                @foreach($controller->session->errors as $key => $errors)
                  @foreach($errors as $key => $error)
                    <li>{{$error}}</li>
                  @endforeach
                @endforeach
              </ul>
            </div>
          </div>
        @endif
    <div class="col-md-11">
    <table class="table table-stripped">
    <thead>
      <tr>
      <th>No</th>
      <th>Judul</th>
      <th>Cover</th>
      <th class="text-center">Artikel</th>
      <th>Tanggal</th>
      <th>Action</th>
      </tr>
    </thead>
    <tbody>
    @foreach($blog as $blog)
      <tr>
        <td width="1%">{{$blog->id}}</td>
        <td>{{$blog->title}}</td>
        <td><img src="{{base_url('assets/img/covers/'.$blog->figure)}}" style="width:5em;height:5em"></td>
        <td class="col-md-4"><div class="comment less" style="color:#000;">{!!$blog->article!!}</div></td>
        <td>{{$blog->created_at}}</td>
        <td><button class="btn btn-danger" width="100">Delete</button><a href="{{base_url('blog/admin/create/'.$blog->id)}}" class="btn btn-primary" width="100">Update</a></td>
      </tr>
    @endforeach
    </tbody>
  </table>
  </div>
  </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
jQuery(function($){
  var compas = window.compas || {};
  compas.readMore = function(){

  }
  $(document).ready(function() {
    $(window).load(function(){
    var showChar = 100;
    var ellipsestext = "...";
    var moretext = "read more";
    var lesstext = "less";
    $('.comment').each(function() {
      var content = $(this).html();

      if(content.length > showChar) {

        var c = content.substr(0, showChar);
        var h = content.substr(showChar-1, content.length - showChar);

        var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">'+moretext+'</a></span>';

        $(this).html(html);
      }

    });

    $(".morelink").click(function(){
      if($(this).hasClass("more")) {
        $(this).removeClass("more");
        $(this).html(lesstext);
      } else {
        $(this).addClass("more");
        $(this).html(moretext);
      }
      $(this).parent().prev().toggle();
      $(this).prev().toggle();
      return false;
    });
    });
  });
});
</script>
@stop
