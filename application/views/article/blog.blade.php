@extends('article.template')

@section('title')
Compas Petualang | Blog
@endsection

@section('ads-gap')
<div class="gap"></div>
@stop

@section('body-inner')
<ul class="breadcrumb">
    <li><a href="{{url('')}}">Home</a>
    </li>
    <li><a href="#">Blog</a>
    </li>
</ul>
<div style="margin-top:20px"></div>
@foreach($blog as $key => $article)
<div class="preview" style="margin-bottom:33px;">
  <a href="{{url("blog/detail/{$article->id}")}}">
      <img src="{{base_url('assets/img/blog/'.$article->figure)}}" alt="{{$article->title}}" title="{{$article->title}}" style="max-height:300px;width:100%; object-fit: cover; margin-right: 15px;">
  </a>
  <article>
      <div class="gap gap-small"></div>
      <a href="{{url("blog/detail/{$article->id}")}}"><h3 style="margin-left:0px"><i><strong>{{$article->title}}</strong></i></h3></a>
      <hr>
      {!! $article->summary !!}
      <a href="{{url("blog/detail/{$article->id}")}}"><i>Read more</i></a>
  </article>
</div>
@endforeach
@stop