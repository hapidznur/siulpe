@extends('admin.template')

@section('title', "Article")
@section('page-title', 'Buat Artikel')
@section('style')
<style type="text/css">
#update-cover{
  position: relative;
  margin-left: 150px;
}
</style>
@stop
@section('body')
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Artikel</h2>
        <div class="clearfix"></div>
      </div>
      <div style="display: block;" class="x_content">
        <br>
        @if($controller->session->errors)
          <div class="col-md-offset-3 col-md-7">
            <div class="alert alert-danger">
              <b>Messages</b>
              <ul>
                @foreach($controller->session->errors as $key => $errors)
                  @foreach($errors as $key => $error)
                    <li>{{$error}}</li>
                  @endforeach
                @endforeach
              </ul>
            </div>
          </div>
        @endif
        <form  action="{{url('blog/post')}}" method="post" class="form-horizontal form-label-left" enctype="multipart/form-data">
          <input type="hidden" name="id" value="{{isset($id) ? $id : ''}}">
          <div class="form-group">
            <div class="control-label col-md-2">
              <label>Judul</label>
            </div>
              <div class="col-md-5">
                <input type="text" class="form-control" name="judul" placeholder="Judul" value="{{isset($article->title) ? $article->title : ''}}">
              </div>
          </div>
          <div class="form-group">
            <div class="control-label col-md-2">
              <label>Cover</label>
            </div>
            @if(isset($article->figure))
            <div class="col-md-1">
              <img src="{{base_url('assets/img/covers/'.$article->figure)}}" width="100" height="100" style="margin-bottom:10px">
              <input type="file" name="cover" placeholder="Gambar Depan">
            </div>
            @else
            <div class="col-md-1"  style="top:5px">
              <input type="file" name="cover" placeholder="Gambar Depan">
            </div>
            @endif
          </div>
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Artikel<span class="required">*</span></label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <textarea name="content" class="form-control" style="height: 500px" placeholder="Deskripsi" >{{isset($article->article) ? $article->article : ''}}</textarea>
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
              <div class="btn-group">
                <a href="" type="submit" class="btn btn-primary">Cancel</a>
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{base_url('assets/js/admin/tiny-mce.js')}}" charset="utf-8"></script>
  {{-- <script src="{{base_url('assets/js/admin/new-master-trip.js')}}" charset="utf-8"></script> --}}
  <script>
    $(function () {
      startTinyMCE('textarea');
    });

  </script>
@stop