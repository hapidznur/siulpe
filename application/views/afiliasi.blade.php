@extends('template')

@section('title')
Compas Petualang | Afiliasi
@endsection

@section('body')
  <div class="gap">
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        @include('pieces/about-us/sidebar')
      </div>
      <div class="col-md-8">
        <div class="box">
          {!!$aboutUs->afiliasi!!}
        </div>
      </div>
    </div>
  </div>
@endsection
