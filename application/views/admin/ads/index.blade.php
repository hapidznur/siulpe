@extends('admin.template')

@section('title')
Admin | Ads
@endsection

@section('page-title')
Ads
@endsection

@section('body')
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_content">
        <table class="table table-hovered table-striped data-table">
          <thead>
            <tr>
              <th>
                No
              </th>
              <th>
                Nama
              </th>
              <th>
                Posisi
              </th>
              <th>
                Jenis
              </th>
              <th>
                Status
              </th>
              <th>
                Aksi
              </th>
            </tr>
          </thead>
          <tbody>
            @foreach($ads as $key => $ad)
              <tr ads-id="{{$ad->id}}">
                <td>
                  {{$key + 1}}
                </td>
                <td>
                  {{$ad->nama}}
                </td>
                <td>
                  {{$ad->posisi}}
                </td>
                <td>
                  {{$ad->jenis}}
                </td>
                <td>
                  {{$ad->status ? 'Aktif' : 'Tidak Aktif'}}
                </td>
                <td>
                  <div class="btn-group">
                    <a href="{{url("admin/ads-edit/{$ad->id}")}}" class="btn btn-default">Edit</a>
                    <button class="btn btn-danger btn-delete">Delete</button>
                  </div>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@section('modal')
  <div id="modal-delete" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Ads</h4>
        </div>
        <div class="modal-body">
          <p style="color: red">
            Apakah anda yakin akan menghapus data ini?
          </p>
          <form style="display: none" id="delete-form" action="{{url("admin/ads-delete-post")}}" method="post">
            <input type="hidden" name="ads_id" value="">
          </form>
        </div>
        <div class="modal-footer">
          <div class="form-group">
            <div class="btn-group" style="float: right">
              <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
              <button form="delete-form" type="submit" class="btn btn-danger">Delete</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{url('assets/js/admin/ads.js')}}" charset="utf-8"></script>
@endsection
