@extends('admin.template')

@section('title')
Admin | Edit Ads
@endsection

@section('page-title')
Ads
@endsection

@section('body')
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Edit Ads</h2>
        <div class="clearfix"></div>
      </div>
      <div style="display: block;" class="x_content">
        <br>
        @if($controller->session->errors)
          <div class="col-md-12">
            <div class="alert alert-danger">
              <b>Messages</b>
              <ul>
                @foreach($controller->session->errors as $key => $errors)
                  @foreach($errors as $key => $error)
                    <li>{{$error}}</li>
                  @endforeach
                @endforeach
              </ul>
            </div>
          </div>
        @endif
        <form id="form-new-ads" action="{{url("admin/ads-edit-post/{$ads->id}")}}" method="post" class="form-horizontal form-label-left" enctype="multipart/form-data">
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Nama</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <input name="nama" class="form-control col-md-8 col-xs-12" type="text" value="{{$ads->nama}}" required="">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Posisi</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <select class="form-control" name="posisi" required="">
                <option disabled="" selected="">Pilih Posisi</option>
                <option value="left-bar" {{$ads->posisi === 'left-bar' ? 'selected' : ''}}>Left Bar</option>
                <option value="bottom-body" {{$ads->posisi === 'bottom-body' ? 'selected' : ''}}>Bottom Body</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Jenis</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <select class="form-control" name="jenis" required="">
                <option disabled="" selected="">Pilih Jenis</option>
                <option value="gambar" {{$ads->jenis === 'gambar' ? 'selected' : ''}}>Gambar</option>
                <option value="script" {{$ads->jenis === 'script' ? 'selected' : ''}}>Script</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Status</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <select class="form-control" name="status" required>
                <option value="1" {{$ads->status == 1 ? 'selected' : ''}}>Aktif</option>
                <option value="0" {{$ads->status == 0 ? 'selected' : ''}}>Tidak Aktif</option>
              </select>
            </div>
          </div>
          <div id="input-gambar" class="form-group" style="display: none">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Gambar</label>
            <div class="col-md-8 col-sm-8 col-xs-12" >
              {{-- <input class="form-control" type="file" name="script"> --}}
              <input class="form-control" type="url" name="script" value="{{$ads->script}}" placeholder="Paste link gambar di sini">
              <div style="margin-top: 20px">
                <img src="{{$ads->image_src}}" style="width: 100%; height: auto; ">
              </div>
            </div>
          </div>
          <div id="input-script" class="form-group" style="display: none">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Script</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <textarea class="form-control" name="script" style="min-height: 250px" placeholder="Script">{{$ads->script}}</textarea>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-10 col-sm-10 col-xs-12">
              <div class="text-right">
                <div class="btn-group">
                  <a href="{{url('admin/ads')}}" type="submit" class="btn btn-default">Cancel</a>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{url('assets/js/admin/new-ads.js')}}" charset="utf-8"></script>
@endsection
