@extends('admin.template')

@section('title', 'Admin | Syarat dan Ketentuan')

@section('page-title', 'Syarat dan Ketentuan')

@section('body')
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Syarat dan Ketentuan</h2>
        <div class="clearfix"></div>
      </div>
      <div style="display: block;" class="x_content">
        <br>
        @if($controller->session->errors)
          <div class="col-md-offset-3 col-md-7">
            <div class="alert alert-danger">
              <b>Messages</b>
              <ul>
                @foreach($controller->session->errors as $key => $errors)
                  @foreach($errors as $key => $error)
                    <li>{{$error}}</li>
                  @endforeach
                @endforeach
              </ul>
            </div>
          </div>
        @endif
        <form id="form-new-master-trip" action="{{url("admin/syarat-dan-ketentuan-post")}}" method="post" class="form-horizontal form-label-left">
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Syarat dan Ketentuan <span class="required">*</span></label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <textarea name="syarat_dan_ketentuan" class="form-control" style="height: 200px" placeholder="Deskripsi">{{$identitasWeb->syarat_dan_ketentuan}}</textarea>
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
              <div class="btn-group">
                <a href="{{base_url('admin/syarat-dan-ketentuan')}}" type="submit" class="btn btn-primary">Cancel</a>
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{base_url('assets/js/admin/tiny-mce.js')}}" charset="utf-8"></script>
  {{-- <script src="{{base_url('assets/js/admin/new-master-trip.js')}}" charset="utf-8"></script> --}}
  <script>
    (function () {
      startTinyMCE('textarea');
    })();
  </script>
@endsection
