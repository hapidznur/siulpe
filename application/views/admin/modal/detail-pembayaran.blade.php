<div id="modal-detail-pembayaran" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Pembayaran</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label>Nama</label>
              <p id="nama">
                Loading..
              </p>
            </div>
            <div class="form-group">
              <label>Email</label>
              <p id="email">
                Loading..
              </p>
            </div>
            <div class="form-group">
              <label>Bank</label>
              <p id="bank">
                Loading..
              </p>
            </div>
            <div class="form-group">
              <label>Rekening Pengirim</label>
              <p id="rekening_pengirim">
                Loading..
              </p>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Total Transfer</label>
              <p id="total_transfer">
                Loading..
              </p>
            </div>
            <div class="form-group">
              <label>Status</label>
              <p id="status">
                Loading..
              </p>
            </div>
            <div class="form-group">
              <label>Tanggal Pembayaran</label>
              <p id="tanggal_pembayaran">
                Loading..
              </p>
            </div>
          </div>
          <div class="col-md-4">
            <img id="bukti_transfer" src="http://orig02.deviantart.net/cd44/f/2016/152/2/d/placeholder_3_by_sketchymouse-da4ny84.png" style="width: 100%; height:auto" alt="" />
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
