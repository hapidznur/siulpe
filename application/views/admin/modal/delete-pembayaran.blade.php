<div id="modal-delete-pembayaran" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Hapus Konfirmasi Pembayaran</h4>
      </div>
      <div class="modal-body">
        Apakah anda yakin akan menghapus pembayaran ini?
        <form style="display: none" id="delete-invoice-confirmation-form" action="{{url("admin/konfirmasi-pembayaran-delete")}}" method="post">
          <input type="text" name="invoice_confirmation_id" value="">
        </form>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <div class="btn-group" style="float: right">
            <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
            <button form="delete-invoice-confirmation-form" type="submit" class="btn btn-danger">Hapus</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
