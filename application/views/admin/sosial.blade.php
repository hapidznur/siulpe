@extends('admin.template')

@section('title', 'Admin | Media Sosial')

@section('page-title', 'Media Sosial')

@section('body')
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Media Sosial</h2>
        <div class="clearfix"></div>
      </div>
      <div style="display: block;" class="x_content">
        <br>
        @if($controller->session->errors)
          <div class="col-md-offset-3 col-md-7">
            <div class="alert alert-danger">
              <b>Messages</b>
              <ul>
                @foreach($controller->session->errors as $key => $errors)
                  @foreach($errors as $key => $error)
                    <li>{{$error}}</li>
                  @endforeach
                @endforeach
              </ul>
            </div>
          </div>
        @endif
        <form id="form-new-master-trip" action="{{url("admin/media-sosial-post")}}" method="post" class="form-horizontal form-label-left" >
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Facebook</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <input name="fb" class="form-control col-md-8 col-xs-12" type="text" value="{{$identitasWeb->fb}}">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Twitter</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <input name="twitter" class="form-control col-md-8 col-xs-12" type="text" value="{{$identitasWeb->twitter}}">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Instagram</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <input name="instagram" class="form-control col-md-8 col-xs-12" type="text" value="{{$identitasWeb->instagram}}">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Line</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <input name="line" class="form-control col-md-8 col-xs-12" type="text" value="{{$identitasWeb->line}}">
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
              <div class="btn-group">
                <a href="{{base_url('admin/media-sosial')}}" type="submit" class="btn btn-primary">Cancel</a>
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
