<table class="table">
  <thead>
    <tr>
      <th style="border: 1px solid black" colspan="2">
        Keterangan
      </th>
      <th style="border: 1px solid black">
        Total
      </th>
      <th></th>
      <th style="border: 1px solid black">Keterangan</th>
      <th style="border: 1px solid black">Detail</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="border: 1px solid black" colspan="2">
        Jumlah Invoice
      </td>
      <td style="border: 1px solid black">
        {{count($invoices)}}
      </td>
      <th></th>
      <th style="border: 1px solid black">Open Trip</th>
      <th style="border: 1px solid black">{{@$_GET['open-trip']}}</th>
    </tr>
    <tr>
      <td style="border: 1px solid black" colspan="2">
        Belum Dibayar
      </td>
      <td style="border: 1px solid black">
        {{$invoices->where('status', 'belum_dibayar')->count()}}
      </td>
      <th></th>
      <th style="border: 1px solid black">Bulan</th>
      <th style="border: 1px solid black">{{@$months[$_GET['month']-1]}}</th>
    </tr>
    <tr>
      <td style="border: 1px solid black" colspan="2">
        Yang sudah lunas
      </td>
      <td style="border: 1px solid black">
        {{$invoices->where('status', 'lunas')->count()}}
      </td>
      <th></th>
      <th style="border: 1px solid black">Tahun</th>
      <th style="border: 1px solid black">{{@$_GET['year']}}</th>
    </tr>
    <tr>
      <td style="border: 1px solid black" colspan="2">
        Total Kas Masuk
      </td>
      <td style="border: 1px solid black">
        <b>{{$totalKasMasuk}}</b>
      </td>
    </tr>
  </tbody>
</table>
<table>
  <tr></tr>
  <tr></tr>
</table>
<table class="table table-hovered table-striped data-table" style="min-width: 1500px" border="1">
  <thead>
    <tr>
      <th>
        No
      </th>
      <th>
        Kode Open trip
      </th>
      <th>
        Invoice
      </th>
      <th>
        Nama
      </th>
      <th>
        Email
      </th>
      <th>
        Tanggal Booking
      </th>
      <th>
        Pembayaran 1
      </th>
      <th>
        Tanggal
      </th>
      <th>
        Pembayaran 2
      </th>
      <th>
        Tanggal
      </th>
      <th>
        Pembayaran 3
      </th>
      <th>
        Tanggal
      </th>
    </tr>
  </thead>
  <tbody>
    @foreach($invoices as $key => $invoice)
    <tr>
      <td>
        {{$key + 1}}
      </td>
      <td>
        {{$invoice->booking->openTrip->kode}}
      </td>
      <td>
        {{$invoice->nomor_invoice}}
      </td>
      <td>
        {{$invoice->booking->nama}}
      </td>
      <td>
        {{$invoice->booking->email}}
      </td>
      <td>
        {{$invoice->booking->tanggal_booking}}
      </td>
      <td>
        @if(@$invoice->confirmations[0])
        {{$invoice->confirmations[0]->total_transfer_str}}
        @else
        belum
        @endif
      </td>
      <td>
        @if(@$invoice->confirmations[0])
        {{$invoice->confirmations[0]->tanggal_pembayaran}}
        @else
        belum
        @endif
      </td>
      <td>
        @if(@$invoice->confirmations[1])
        {{$invoice->confirmations[1]->total_transfer_str}}
        @else
        belum
        @endif
      </td>
      <td>
        @if(@$invoice->confirmations[1])
        {{$invoice->confirmations[1]->tanggal_pembayaran}}
        @else
        belum
        @endif
      </td>
      <td>
        @if(@$invoice->confirmations[2])
        {{$invoice->confirmations[2]->total_transfer_str}}
        @else
        belum
        @endif
      </td>
      <td>
        @if(@$invoice->confirmations[2])
        {{$invoice->confirmations[2]->tanggal_pembayaran}}
        @else
        belum
        @endif
      </td>
    </tr>
    @endforeach
  </tbody>
</table>