@extends('admin.template')

@section('title', 'Edit Open Trip')

@section('page-title', 'Open Trip')

@section('body')
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Edit Open Trip</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <!-- Smart Wizard -->
          <div id="wizard" class="form_wizard wizard_horizontal">
            <ul class="wizard_steps">
              <li>
                <a href="#step-1">
                  <span class="step_no">1</span>
                  <span class="step_descr">
                    Step 1<br />
                    <small>Pilih Master Trip</small>
                  </span>
                </a>
              </li>
              <li>
                <a href="#step-2">
                  <span class="step_no">2</span>
                  <span class="step_descr">
                    Step 2<br />
                    <small>Deskripsi Open Trip</small>
                  </span>
                </a>
              </li>
              <li>
                <a href="#step-3">
                  <span class="step_no">3</span>
                  <span class="step_descr">
                    Step 3<br />
                    <small>Durasi Pendaftaran</small>
                  </span>
                </a>
              </li>
              <li>
                <a href="#step-4">
                  <span class="step_no">4</span>
                  <span class="step_descr">
                    Step 4<br />
                    <small>Price Detail</small>
                  </span>
                </a>
              </li>
            </ul>
            <div id="step-1">
              <div class="text-center" style="padding-top: 50px; padding-bottom: 80px">
                <button style="min-width: 200px" class="btn btn-lg btn-default btn-pilih-master-trip">
                  <p>
                    <b>{{$openTrip->masterTrip->title}}</b><br>
                    <small>{{$openTrip->masterTrip->destination}}</small>
                  </p>
                </button>
              </div>
            </div>
            <div id="step-2">
              <form id="open-trip" action="" method="post" class="form-horizontal form-label-left" edit-open-trip="true" open-trip-id="{{$openTrip->id}}" master-trip-id="{{$openTrip->masterTrip->id}}">
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Meeting Point <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input value="{{$openTrip->meeting_point}}" name="meeting_point" class="form-control has-feedback-left col-md-7 col-xs-12 input" placeholder="meeting point" required="required" type="text">
                    <span class="fa fa-map-marker form-control-feedback left"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Kuota <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input value="{{$openTrip->quota}}" name="quota" class="form-control has-feedback-left col-md-7 col-xs-12 input" placeholder="kuota" required="required" type="number" min="1">
                    <span class="fa fa-users form-control-feedback left"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Review link
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input value="{{$openTrip->blog_link}}" name="blog_link" class="form-control has-feedback-left col-md-7 col-xs-12" placeholder="url review link" type="url">
                    <span class="fa fa-link form-control-feedback left"></span>
                  </div>
                </div>
              </form>
            </div>
            <div id="step-3">
              <div class="form-horizontal form-label-left">
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Status <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select form="open-trip" name="status" class="form-control col-md-7 col-xs-12 input" required="required">
                      <option value="pendaftaran" {{$openTrip->status == 'pendaftaran' ? 'selected':''}}>Belum Pasti Berangkat</option>
                      <option value="pasti_berangkat" {{$openTrip->status == 'pasti_berangkat' ? 'selected':''}}>Pasti Berangkat</option>
                      <option value="dibatalkan" {{$openTrip->status == 'dibatalkan' ? 'selected':''}}>Dibatalkan</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Garansi Berangkat
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="checkbox">
                      <label>
                        <input form="open-trip" type="checkbox" name="garansi_berangkat" value="1" {{$openTrip->garansi_berangkat ? 'checked':''}}>
                        Ya
                      </label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Top Destination
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="checkbox">
                      <label>
                        <input form="open-trip" type="checkbox" name="top_destination" value="1" {{$openTrip->top_destination ? 'checked':''}}>
                        Ya
                      </label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Berangkat <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input value="{{$openTrip->tanggal_berangkat}}" form="open-trip" name="tanggal_berangkat" class="form-control date-picker has-feedback-left col-md-7 col-xs-12 input" required="required" type="text">
                    <span class="fa fa-calendar form-control-feedback left"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Jam Berangkat <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input value="{{$openTrip->jam_berangkat}}" form="open-trip" name="jam_berangkat" class="form-control has-feedback-left col-md-7 col-xs-12 input" required="required" type="text">
                    <span class="fa fa-clock-o form-control-feedback left"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Pulang <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input value="{{$openTrip->tanggal_pulang}}" form="open-trip" name="tanggal_pulang" class="form-control input date-picker has-feedback-left col-md-7 col-xs-12" required="required" type="text">
                    <span class="fa fa-calendar form-control-feedback left"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Jam Pulang <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input value="{{$openTrip->jam_pulang}}" form="open-trip" name="jam_pulang" class="form-control has-feedback-left col-md-7 col-xs-12 input" required="required" type="text">
                    <span class="fa fa-clock-o form-control-feedback left"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">End date pendaftaran <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input value="{{$openTrip->status_tutup_otomatis}}" form="open-trip" name="status_tutup_otomatis" class="form-control input date-picker has-feedback-left col-md-7 col-xs-12" required="required" type="text">
                    <span class="fa fa-calendar form-control-feedback left"></span>
                  </div>
                </div>
              </div>
            </div>
            <div id="step-4">
              <div class="form-horizontal form-label-left">
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Harga <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input value="{{$openTrip->harga}}" form="open-trip" name="harga" class="form-control has-feedback-left col-md-7 col-xs-12 input" required="required" type="number" placeholder="100000" step="1000" min="0">
                    <span class="fa fa-money form-control-feedback left"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Minimal DP <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input value="{{$openTrip->dp_minimum}}" form="open-trip" name="dp_minimum" class="form-control has-feedback-left col-md-7 col-xs-12 input" required="required" type="number" placeholder="100000" step="1000" min="0">
                    <span class="fa fa-money form-control-feedback left"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Jumlah Termin <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input value="{{$openTrip->angsuran_maksimum}}" form="open-trip" name="angsuran_maksimum" class="form-control has-feedback-left col-md-7 col-xs-12 input" placeholder="3 kali" required="required" type="number" min="1">
                    <span class="fa fa-calendar form-control-feedback left"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Jatuh Tempo<span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input value="{{$openTrip->jatuh_tempo_pembayaran}}" form="open-trip" name="jatuh_tempo" class="form-control date-picker has-feedback-left col-md-7 col-xs-12 input" required="required" type="text">
                    <span class="fa fa-calendar form-control-feedback left"></span>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <!-- End SmartWizard Content -->
        </div>
      </div>
    </div>
  </div>
@endsection

@section('modal')
  <div class="modal fade pilih-master-trip" role="dialog">
  	<div class="modal-dialog">
  		<div class="modal-content">
  			<div class="modal-header">
  				<button type="button" class="close" data-dismiss="modal">&times;</button>
  				<h4 class="modal-title">Pilih Master Trip</h4>
  			</div>
  			<div class="modal-body">
          <div class="text-right">
            <div class="row">
              <div class="col-md-4 col-md-offset-8">
                <div class="form-group">
                  <input type="text" name="q" placeholder="search" class="form-control">
                </div>
              </div>
            </div>
          </div>
          <ul class="list-group master-trip">

          </ul>
  			</div>
  		</div>
  	</div>
  </div>
@endsection

@section('script')
  <script src="{{base_url('assets/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js')}}"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $('.date-picker').daterangepicker({
      singleDatePicker: true,
      calender_style: "picker_4"
    }, function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
    });
  });

  $(document).ready(function() {
    $('#wizard').smartWizard();

    $('#wizard_verticle').smartWizard({
      transitionEffect: 'slide'
    });

    $('.buttonNext').addClass('btn btn-default');
    $('.buttonPrevious').addClass('btn btn-default');
    $('.buttonFinish').addClass('btn btn-primary input').text('Submit');
  });
  </script>
  <script src="{{base_url('assets/js/admin/tiny-mce.js')}}" charset="utf-8"></script>
  {{-- <script src="{{base_url('assets/js/admin/new-master-trip.js')}}" charset="utf-8"></script> --}}
  <script>
    (function () {
      startTinyMCE('textarea');
    })();
  </script>

  <script src="{{base_url('assets/js/admin/new-open-trip.js')}}" charset="utf-8"></script>
@endsection
