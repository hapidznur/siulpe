@extends('admin.template')

@section('title', 'Admin | Open Trip')

@section('page-title', 'Open Trip')

@section('body')
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_content">
        <table class="table table-stripped table-hovered data-table">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode</th>
              <th>Nama Trip</th>
              <th>Tujuan Trip</th>
              <th>Harga</th>
              <th>Status</th>
              <th>Quota</th>
              <th>Tanggal Berangkat</th>
              <th>Jumlah Booking</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach($openTrips as $key => $openTrip)
              <tr open-trip-id="{{$openTrip->id}}">
                <td>{{$key+1}}</td>
                <td>{{$openTrip->kode}}</td>
                <td>{{$openTrip->masterTrip->title}}</td>
                <td>{{$openTrip->masterTrip->destinations_lists}}</td>
                <td>{{$openTrip->price}}</td>
                <td>
                  @if($openTrip->status === 'pendaftaran')
                    <span class="fa fa-envelope-o green" data-toggle="tooltip" title="Pendaftaran"></span>
                  @elseif($openTrip->status === 'pasti_berangkat')
                    <span class="fa fa-check blue" data-toggle="tooltip" title="Pasti Berangkat"></span>
                  @elseif($openTrip->status === 'dibatalkan')
                    <span class="fa fa-times red" data-toggle="tooltip" title="Dibatalkan"></span>
                  @else
                    <span class="fa fa-times red" data-toggle="tooltip" title="Ditutup"></span>
                  @endif
                </td>
                <td>{{$openTrip->quota_masuk}}/{{$openTrip->quota}}</td>
                <td>{{$openTrip->tanggal_berangkat}}</td>
                <td>{{$openTrip->bookings()->count()}}</td>
                <td>
                  <div class="btn-group">
                    <a href="{{url("admin/edit-open-trip/{$openTrip->id}")}}" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                    <a href="{{url("admin/open-trip-show/{$openTrip->id}")}}" class="btn btn-default"><i class="fa fa-eye"></i></a>
                    <button class="btn btn-danger btn-delete"><i class="fa fa-trash"></i></button>
                  </div>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@section('modal')
  <div id="modal-delete" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Open Trip</h4>
        </div>
        <div class="modal-body">
          Apakah anda yakin akan menghapus data ini?
          <small>data yang sudah dihapus tidak bisa dikembalikan lagi.</small>
          <form style="display: none" id="open-trip-delete-form" action="{{url("admin/open-trip-delete-post")}}" method="post">
            <input type="hidden" name="open_trip_id" value="">
          </form>
        </div>
        <div class="modal-footer">
          <div class="form-group">
            <div class="btn-group" style="float: right">
              <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
              <button form="open-trip-delete-form" type="submit" class="btn btn-danger">Delete</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{url('assets/js/admin/open-trip.js')}}" charset="utf-8"></script>
@endsection
