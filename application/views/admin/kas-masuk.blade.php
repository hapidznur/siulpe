@extends('admin.template')

@section('title', 'Admin | Kas Masuk')

@section('page-title', 'Kas Masuk')

@section('body')
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_content">
        <table class="table">
          <thead>
            <tr>
              <th>
                Keterangan
              </th>
              <th>
                Total
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                Jumlah Invoice
              </td>
              <td>
                {{count($invoices)}}
              </td>
            </tr>
            <tr>
              <td>
                Belum Dibayar
              </td>
              <td>
                {{$invoices->where('status', 'belum_dibayar')->count()}}
              </td>
            </tr>
            <tr>
              <td>
                Yang sudah lunas
              </td>
              <td>
                {{$invoices->where('status', 'lunas')->count()}}
              </td>
            </tr>
            <tr>
              <td>
                Total Kas Masuk
              </td>
              <td>
                <b>{{$totalKasMasuk}}</b>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="x_panel">
      <div class="x_content">
        <div style="overflow-x: auto">
          <div class="form">
            <form class="" action="" method="get" id="form-kas-masuk">
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Open Trip</label>
                    <select class="form-control" name="open-trip">
                      <option value="semua">Semua</option>
                      @foreach($openTrips as $key => $openTrip)
                        <option value="{{$openTrip->id}}" {{@$_GET['open-trip'] == $openTrip->id ? 'selected' : ''}}>{{$openTrip->kode}} - {{$openTrip->masterTrip->title}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Bulan</label>
                    <select class="form-control" name="month">
                      <option value="semua">Semua</option>
                      @foreach($months as $key => $month)
                        <option value="{{$key + 1}}" {{@$_GET['month'] == $key + 1 ? 'selected' : ''}}>{{$month}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>Tahun</label>
                    <select class="form-control" name="year">
                      <option value="semua">Semua</option>
                      @for($i=(int) date('Y'); $i >= (int) date('Y') - 5; $i--)
                        <option value="{{$i}}" {{@$_GET['year'] == $i ? 'selected' : ''}}>{{$i}}</option>
                      @endfor
                    </select>
                  </div>
                </div>
                <div class="col-md-3">
                  <label></label>
                  <div style="margin-top: 4px">
                    <button type="submit" class="btn btn-primary" id="search">Search</button>
                    <a class="btn btn-success" id="export">Export to Excel</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <table class="table table-hovered table-striped data-table" style="min-width: 1500px">
            <thead>
              <tr>
                <th>
                  No
                </th>
                <th>
                  Kode Open trip
                </th>
                <th>
                  Invoice
                </th>
                <th>
                  Nama
                </th>
                <th>
                  Email
                </th>
                <th>
                  Tanggal Booking
                </th>
                <th>
                  Pembayaran 1
                </th>
                <th>
                  Tanggal
                </th>
                <th>
                  Pembayaran 2
                </th>
                <th>
                  Tanggal
                </th>
                <th>
                  Pembayaran 3
                </th>
                <th>
                  Tanggal
                </th>
                <th>
                  Aksi
                </th>
              </tr>
            </thead>
            <tbody>
              @foreach($invoices as $key => $invoice)
                <tr>
                  <td>
                    {{$key + 1}}
                  </td>
                  <td>
                    {{$invoice->booking->openTrip->kode}}
                  </td>
                  <td>
                    {{$invoice->nomor_invoice}}
                  </td>
                  <td>
                    {{$invoice->booking->nama}}
                  </td>
                  <td>
                    {{$invoice->booking->email}}
                  </td>
                  <td>
                    {{$invoice->booking->tanggal_booking}}
                  </td>
                  <td>
                    @if(@$invoice->confirmations[0])
                      {{$invoice->confirmations[0]->total_transfer_str}}
                    @else
                      belum
                    @endif
                  </td>
                  <td>
                    @if(@$invoice->confirmations[0])
                      {{$invoice->confirmations[0]->tanggal_pembayaran}}
                    @else
                      belum
                    @endif
                  </td>
                  <td>
                    @if(@$invoice->confirmations[1])
                      {{$invoice->confirmations[1]->total_transfer_str}}
                    @else
                      belum
                    @endif
                  </td>
                  <td>
                    @if(@$invoice->confirmations[1])
                      {{$invoice->confirmations[1]->tanggal_pembayaran}}
                    @else
                      belum
                    @endif
                  </td>
                  <td>
                    @if(@$invoice->confirmations[2])
                      {{$invoice->confirmations[2]->total_transfer_str}}
                    @else
                      belum
                    @endif
                  </td>
                  <td>
                    @if(@$invoice->confirmations[2])
                      {{$invoice->confirmations[2]->tanggal_pembayaran}}
                    @else
                      belum
                    @endif
                  </td>
                  <td>
                    <div class="btn-group">
                      <a href="{{url("admin/booking-show/{$invoice->booking->id}")}}" class="btn btn-default"><span class="fa fa-eye"></span></a>
                    </div>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript">
  $('#export').click(function() {
    $('#form-kas-masuk').append(
      '<input type="hidden" value="true" name="export">'
    ).submit()
  })

  $('#search').click(function() {
    $('[name=export]').remove()
  })
</script>
@stop