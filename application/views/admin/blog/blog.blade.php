@extends('admin.template')

@section('title', 'Admin | Blog')

@section('page-title', 'Blog')

@section('body')
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_content">

      <a class="btn btn-default" href="{{url("admin/blog-baru")}}" data-toggle="tooltip" title="Add Blog"><i class="fa fa-edit"></i>Add Blog </a>
        <table class="table table-hovered table-striped table-stripped data-table">
          <thead>
            <tr>
              <th>No</th>
              <th>Judul Artikel</th>
              <th>User</th>
              <th>Komentar</th>
              <th>Tanggal Posting</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach($blog as $key => $blogs)
              <tr article-id="{{$blogs->id}}">
                <td>{{$key + 1}}</td>
                <td>{{$blogs->title}}</td>
                <td>{{$blogs->user}}</td>
                <td><a style="text-decoration: underline; color: blue" href="{{url("admin/blog-comment/{$blogs->id}")}}">{{$blogs->comments()->count()}}</a></td>
                <td>{{$blogs->created_at->toFormattedDateString()}}</td>
                <td>
                  <div class="btn-group">
                    <a class="btn btn-default" target="_blank" href="{{url("blog/{$blogs->id}")}}" data-toggle="tooltip" title="Show"><i class="fa fa-eye"></i></a>
                    <a class="btn btn-default" href="{{url("admin/blog-edit/{$blogs->id}")}}" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
                    <button class="btn btn-danger btn-delete" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>
                  </div>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@section('modal')
  <div id="modal-delete" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Artikel</h4>
        </div>
        <div class="modal-body">
          Apakah anda yakin akan menghapus data ini?
          <small>data yang sudah dihapus tidak bisa dikembalikan lagi.</small>
          <form style="display: none" id="article-delete-form" action="{{url("admin/blog_delete")}}" method="post">
            <input type="hidden" name="article_id" value="">
          </form>
        </div>
        <div class="modal-footer">
          <div class="form-group">
            <div class="btn-group" style="float: right">
              <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
              <button form="article-delete-form" type="submit" class="btn btn-danger">Delete</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript">
  var modal = $('#modal-delete')

  $('body').on('click', '.btn-delete', function () {
    var id = $(this).parent().parent().parent().attr('article-id');
    modal.find('input').val(id)
    modal.modal('show');
  })
</script>
@endsection
