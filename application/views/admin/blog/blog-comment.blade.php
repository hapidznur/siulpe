@extends('admin.template')

@section('title', 'Admin | Blog')

@section('page-title', 'Blog')

@section('body')
<div class="col-md-12">
  <div class="x_panel">
    <div class="x_content">
      <table class="table table-hovered table-striped table-stripped data-table">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Komentar</th>
            <th>Tanggal Posting</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          @foreach($blog->comments as $key => $comment)
            <tr>
              <td>{{$key + 1}}</td>
              <td>{{$comment->nama}}</td>
              <td>{{$comment->email}}</td>
              <td>{{read_more($comment->body, 200)}}</td>
              <td>{{$comment->created_at->toFormattedDateString()}}</td>
              <td>
                <button href="" class="btn btn-danger btn-delete" comment-id="{{$comment->id}}"><i class="fa fa-trash"></i></button>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@stop

@section('modal')
<div id="modal-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Komentar</h4>
      </div>
      <div class="modal-body">
        <p style="color: red">
          Apakah anda yakin akan menghapus komentar ini?</b>
        </p>
        <form style="display: none" id="delete-form" action="{{url("admin/blog-comment-delete/{$blog->id}")}}" method="post">
          <input type="hidden" name="comment_id" value="">
        </form>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <div class="btn-group" style="float: right">
            <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
            <button form="delete-form" type="submit" class="btn btn-danger">Delete</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('script')
<script type="text/javascript">
  (function() {
    var modal = $('.modal')
    addListeners()

    function addListeners() {
      $('body').on('click', '.btn-delete', onBtnDeleteClick)
    }

    function onBtnDeleteClick() {
      modal.modal('show');
      modal.find('input').val($(this).attr('comment-id'))
    }
  })()
</script>
@stop