@extends('admin.template')

@section('title', 'Admin | Add Blog')

@section('page-title', 'Add Blog')

@section('body')
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Blog</h2>
        <div class="clearfix"></div>
      </div>
      <div style="display: block;" class="x_content">
        <br>
        @if($controller->session->errors)
          <div class="col-md-offset-3 col-md-7">
            <div class="alert alert-danger">
              <b>Messages</b>
              <ul>
                @foreach($controller->session->errors as $key => $errors)
                  @foreach($errors as $key => $error)
                    <li>{{$error}}</li>
                  @endforeach
                @endforeach
              </ul>
            </div>
          </div>
        @endif
        <form id="form-new-master-trip" action="{{url("admin/blog-post")}}" method="post" class="form-horizontal form-label-left" enctype="multipart/form-data">
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Judul </label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <input name="user" class="form-control col-md-8 col-xs-12" type="hidden" value="{{auth()->admin()->nama}}">
              <input name="title" class="form-control col-md-8 col-xs-12" type="text" placeholder="Judul">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12">Deskripsi</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <textarea name="deskripsi" class="form-control" style="height: 200px"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Gambar</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <input name="figure" class="col-md-8 col-xs-12" type="file" id="upload_valid">
            </div>
          </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
              <div class="btn-group">
                <a href="{{base_url('admin/blog')}}" type="submit" class="btn btn-primary">Cancel</a>
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('modal')
<div class="tiny-gallery-modal modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
@stop

@section('script')
  <script src="{{base_url('assets/js/admin/tiny-mce.js')}}" charset="utf-8"></script>
  {{-- <script src="{{base_url('assets/js/admin/new-master-trip.js')}}" charset="utf-8"></script> --}}
  <script>
    (function () {
      startTinyMCE('textarea');
    })();

    $("#upload_valid").bind('change', function() {
        var size = parseInt(this.files[0].size);
        if(size>900000){
          alert("Opss! File Upload Harus Di Bawah 900KB");
          $(this).val('');  
        }
      });
  </script>
@endsection