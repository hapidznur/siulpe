<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <!-- Bootstrap -->
    <link href="{{base_url('assets/vendors/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{base_url('assets/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="{{base_url('assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{base_url('assets/vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css">
    <link href="{{base_url('assets/css/custom.css')}}" rel="stylesheet">
    <link href="{{base_url('assets/css/admin-custom.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{base_url('assets/css/my-style.css')}}" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="shortcut icon" href="{{url("assets/images/{$identitasWeb->favicon}")}}" type="image/x-icon" />
    <link rel="stylesheet" href="{{url('assets/css/selectize.css')}}" media="screen" title="no title" charset="utf-8">
    @yield('style')
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        @include('admin.pieces.sidebar')

        @include('admin.pieces.top-navigation')

        <!-- page content -->
        <div class="right_col" role="main">
          <div>
            <div class="page-title">
              <div class="title_left">
                <h3>@yield('page-title')</h3>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              @yield('body')
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>

    <div>
      @yield('modal')
    </div>

    <!-- jQuery -->
    <script src="{{base_url('assets/vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- jQuery Tags Input -->
    <script src="{{base_url('assets/vendors/jquery.tagsinput/src/jquery.tagsinput.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{base_url('assets/vendors/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{base_url('assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{base_url('assets/js/moment/moment.min.js')}}"></script>
    <script src="{{base_url('assets/js/datepicker/daterangepicker.js')}}"></script>
    <!-- Select2 -->
    <script src="{{base_url('assets/vendors/select2/dist/js/select2.full.min.js')}}"></script>

    <script src="{{base_url('assets/js/aksa.js')}}" charset="utf-8"></script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" charset="utf-8"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js" charset="utf-8"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{base_url('assets/js/custom.js')}}"></script>
    <script src="{{url('assets/js/selectize.js')}}" charset="utf-8"></script>
    <script type="text/javascript">
      $('[data-toggle=tooltip]').tooltip();
      $('.data-table').DataTable();
    </script>
    @yield('script')
  </body>
</html>
