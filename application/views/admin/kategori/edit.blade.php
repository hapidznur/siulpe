@extends('admin.template')

@section('title', 'Admin | Edit Kategori')

@section('page-title', 'Kategori')

@section('body')
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_content">
        <h2>Edit</h2>
        <form enctype="multipart/form-data" action="{{url("admin/kategori-edit-post/{$category->id}")}}" method="post" class="form-horizontal form-label-left">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Kategori <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input name="nama" required="required" class="form-control col-md-7 col-xs-12" type="text" placeholder="Nama trip" value="{{$category->nama}}">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Icon <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <img id="gambar-icon" src="{{$category->icon_src}}" style="width: 80px; height: 80px" />
              <small> ^ direkomendasikan ukuran gambar adalah 80x80 pixels</small>
              <br><br>
              <input name="icon" class="form-control col-md-7 col-xs-12" type="file" >
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Deskripsi <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea name="deskripsi" required="required" class="form-control col-md-7 col-xs-12" placeholder="Deskripsi">{{$category->deskripsi}}</textarea>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-9">
              <div class="text-right">
                <button type="submit" class="btn btn-primary">Update</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    ImageReader
      .init({
        img: $('#gambar-icon'),
        input: $('[type=file]')
      })
      .commit()
  </script>
@endsection
