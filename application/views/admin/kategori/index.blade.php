@extends('admin.template')

@section('title', 'Admin | Kategori')

@section('page-title', 'Kategori')

@section('body')
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_content">
        <div class="text-left">
          <a class="btn btn-primary" href="{{url('admin/kategori-baru')}}">Tambah Kategori</a>
        </div>
        <div>
          <table class="table table-hovered table-striped data-table">
            <thead>
              <tr>
                <th>
                  No
                </th>
                <th>
                  Icon
                </th>
                <th>
                  Nama
                </th>
                <th>
                  Deskripsi
                </th>
                <th>
                  Aksi
                </th>
              </tr>
            </thead>
            <tbody>
              @foreach($categories as $key => $category)
                <tr category-id="{{$category->id}}">
                  <td>
                    {{$key + 1}}
                  </td>
                  <td>
                    <img src="{{$category->icon_src}}" width="80px" />
                  </td>
                  <td>
                    {{$category->nama}}
                  </td>
                  <td>
                    {{$category->deskripsi}}
                  </td>
                  <td>
                    <div class="btn-group">
                      <a href="{{url("admin/kategori-edit/{$category->id}")}}" class="btn btn-default"><span class="fa fa-edit"></span></a>
                      <button href="#" class="btn btn-danger btn-delete"><span class="fa fa-trash"></span></button>
                    </div>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('modal')
  <div id="modal-delete" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Kategori</h4>
        </div>
        <div class="modal-body">
          Apakah anda yakin akan menghapus data ini?
          <small>data yang sudah dihapus tidak bisa dikembalikan lagi.</small>
          <form style="display: none" id="kategori-delete-form" action="{{url("admin/kategori-delete-post")}}" method="post">
            <input type="hidden" name="category_id" value="">
          </form>
        </div>
        <div class="modal-footer">
          <div class="form-group">
            <div class="btn-group" style="float: right">
              <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
              <button form="kategori-delete-form" type="submit" class="btn btn-danger">Delete</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    var modal = $('#modal-delete')
    var form = $('#kategori-delete-form')

    $('body').on('click', '.btn-delete', function () {
      var id = $(this).parent().parent().parent().attr('category-id')
      modal.find('input').val(id)
      modal.modal('show');
    })
  </script>
@endsection
