@extends('admin.template')

 @section('title', 'Admin | Identitas Website')

 @section('page-title', 'Identitas Website')

 @section('body')
   <div class="col-md-12 col-sm-12 col-xs-12">
     <div class="x_panel">
       <div class="x_title">
         <h2>Identitas Website</h2>
         <div class="clearfix"></div>
       </div>
       <div style="display: block;" class="x_content">
         <br>
         @if($controller->session->errors)
           <div class="col-md-offset-3 col-md-7">
             <div class="alert alert-danger">
               <b>Messages</b>
               <ul>
                 @foreach($controller->session->errors as $key => $errors)
                   @foreach($errors as $key => $error)
                     <li>{{$error}}</li>
                   @endforeach
                 @endforeach
               </ul>
             </div>
           </div>
         @endif
         <form id="form-new-master-trip" action="{{url("admin/identitas_post")}}" method="post" class="form-horizontal form-label-left" enctype="multipart/form-data">
           <div class="form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Nama</label>
             <div class="col-md-8 col-sm-8 col-xs-12">
               <input name="nama" class="form-control col-md-8 col-xs-12" type="text" value="{{$identitasWeb->nama}}">
             </div>
           </div>
           <div class="form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12">Meta Deskripsi</label>
             <div class="col-md-8 col-sm-8 col-xs-12">
               <textarea name="meta_deskripsi" class="form-control" style="height: 200px">{{$identitasWeb->meta_deskripsi}}</textarea>
             </div>
           </div>
           <div class="form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12">Meta Keyword</label>
             <div class="col-md-8 col-sm-8 col-xs-12">
               <textarea name="meta_keyword" class="form-control" style="height: 200px">{{$identitasWeb->meta_keyword}}</textarea>
             </div>
           </div>
           <div class="form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Logo</label>
             <div class="col-md-8 col-sm-8 col-xs-12">
               <img src="{{base_url().'assets/images/'.$identitasWeb->logo}}" width="100">
             </div>
           </div>
           <div class="form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Ganti Logo</label>
             <div class="col-md-8 col-sm-8 col-xs-12">
               <input name="logo" class="col-md-8 col-xs-12" type="file" id="upload_valid">
             </div>
           </div>
           <div class="form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">favicon</label>
             <div class="col-md-8 col-sm-8 col-xs-12">
               <img src="{{base_url().'assets/images/'.$identitasWeb->favicon}}" width="100">
             </div>
           </div>
           <div class="form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Ganti Favicon</label>
             <div class="col-md-8 col-sm-8 col-xs-12">
               <input name="favicon" class="col-md-8 col-xs-12" type="file" id="upload_valid2">
             </div>
           </div>
           <div class="ln_solid"></div>
           <div class="form-group">
             <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
               <div class="btn-group">
                 <a href="{{base_url('admin/identitas')}}" type="submit" class="btn btn-primary">Cancel</a>
                 <button type="submit" class="btn btn-success">Submit</button>
               </div>
             </div>
           </div>
         </form>
       </div>
     </div>
   </div>
 @endsection

 @section('script')
   <script>
     $("#upload_valid").bind('change', function() {
         var size = parseInt(this.files[0].size);
         if(size>900000){
           alert("Opss! File Upload Harus Di Bawah 900KB");
           $(this).val('');
         }
       });
     $("#upload_valid2").bind('change', function() {
         var size = parseInt(this.files[0].size);
         if(size>900000){
           alert("Opss! File Upload Harus Di Bawah 900KB");
           $(this).val('');
         }
       });
   </script>
 @endsection
