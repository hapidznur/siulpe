@extends('admin.template')

@section('title', 'Admin')

@section('body')
  <div class="row top_tiles">
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <div class="tile-stats">
        <div class="icon"><i class="fa fa-plane"></i></div>
        <div class="count">{{$masterTripCount}}</div>
        <h3><a href="{{url('admin/master-trip')}}">Master Trip</a></h3>
        <p>Data template perjalanan.</p>
      </div>
    </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <div class="tile-stats">
        <div class="icon"><i class="fa fa-car"></i></div>
        <div class="count">{{$openTripCount}}</div>
        <h3><a href="{{url('admin/open-trip')}}">Open Trip</a></h3>
        <p>Data perjalanan.</p>
      </div>
    </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <div class="tile-stats">
        <div class="icon"><i class="fa fa-book"></i></div>
        <div class="count">{{$bookCount}}</div>
        <h3><a href="{{url('admin/booking')}}">Booking</a></h3>
        <p>Data booking yang masuk.</p>
      </div>
    </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <div class="tile-stats">
        <div class="icon"><i class="fa fa-money"></i></div>
        <div class="count">{{$pembayaranCount}}</div>
        <h3>Pembayaran</h3>
        <p>Daftar pembayaran yang masuk.</p>
      </div>
    </div>
  </div>
@endsection
