@extends('admin.template')

@section('title', 'Admin | Fasilitas Baru')

@section('page-title', 'Fasilitas')

@section('body')
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Fasilitas Baru</h2>
        <div class="clearfix"></div>
      </div>
      <div style="display: block;" class="x_content">
        <br>
        @if($controller->session->errors)
          <div class="col-md-offset-3 col-md-7">
            <div class="alert alert-danger">
              <b>Messages</b>
              <ul>
                @foreach($controller->session->errors as $key => $errors)
                  @foreach($errors as $key => $error)
                    <li>{{$error}}</li>
                  @endforeach
                @endforeach
              </ul>
            </div>
          </div>
        @endif
        <form id="form-new-master-trip" action="{{url("admin/fasilitas-baru-post")}}" method="post" class="form-horizontal form-label-left">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Fasilitas<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input name="title" required="required" class="form-control col-md-7 col-xs-12" type="text" placeholder="Nama fasilitas" value="{{old('title')}}">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Description <span class="required">*</span></label>
            <div class="col-md-7 col-sm-7 col-xs-12">
              <textarea name="description" class="form-control" style="height: 120px" placeholder="Deskripsi">{{old('description')}}</textarea>
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <div class="btn-group">
                <a href="{{base_url('admin/master-trip')}}" type="submit" class="btn btn-primary">Cancel</a>
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('modal')
@endsection
