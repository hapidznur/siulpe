@extends('admin.template')

@section('title', "Booking {$openTrip->masterTrip->title}")

@section('body')
  <div class="">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <div class="gap">
            <h2>{{$openTrip->masterTrip->title}}, <small>{{$openTrip->masterTrip->destination}}</small></h2>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <form class="form" action="{{base_url("admin/book-process/{$openTrip->id}")}}" method="post">
            @if($controller->session->errors)
              <div class="alert alert-danger">
                Messages
                <ul>
                  @foreach($controller->session->errors as $key => $errors)
                    @foreach($errors as $key => $error)
                      <li>{{$error}}</li>
                    @endforeach
                  @endforeach
                </ul>
              </div>
            @endif
            @if($controller->session->message)
              <div class="alert alert-success">
                {{$controller->session->message}}
              </div>
            @endif
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama" value="{{old('nama')}}" required="">
                </div>
                <div class="form-group">
                  <label>No TLP</label>
                  <input type="text" name="no_tlp" placeholder="No telepon" class="form-control" value="{{old('no_tlp')}}" required="">
                </div>
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" name="email" placeholder="email" class="form-control" value="{{old('email')}}" required="">
                </div>
                <div class="form-group">
                  <label>Kota Tinggal</label>
                  <input name="alamat" class="form-control" placeholder="Kota tinggal" value="{{old('alamat')}}" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Jumlah Peserta</label>
                  <input type="number" name="jumlah_peserta" value="{{old('jumlah_peserta')}}" placeholder="Jumlah Peserta" min="1" class="form-control" required="">
                </div>
                <div id="kontainer-peserta">

                </div>
                <button type="submit" class="btn btn-primary btn-lg" style="float: right">Booking</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{base_url('assets/js/regional.js')}}" charset="utf-8"></script>
  <script>
    Regional
      .init()
      .provinsi('select[name=provinsi]')
      .kota('select[name=kota]')
      .kecamatan('select[name=kecamatan]')
      .grab();

  </script>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <script src="{{url('assets/js/open-trip/booking.js')}}" charset="utf-8"></script>
@endsection
