@extends('admin.template')

@section('title', 'Admin | Gallery Instagram')

@section('page-title', '<i class="fa fa-image"></i> Gallery Instagram')

@section('body')
  <hr>
  <div class="col-md-12 col-sm-12 col-xs-12">
    @foreach($gallery as $image)
      <div class="col-md-3">
        <div class="x_panel nopadding">
          <div style="height:200px;overflow:hidden">
            <a class="gallery-show"  rel="group1" href="{{$image->gambar}}">
            <img class="img-responsive" src="{{$image->gambar}}" style="object-fit:cover;height:200px;">
            </a>
          </div>
          <div class="pad-10">
            <b>{{ucwords($image->title)}}</b>
            <hr class="gap-sm">
            <article class="text-muted text-small text-justify">
              {{read_more('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris finibus eu arcu id iaculis. In orci dui, efficitur dapibus nibh sed, euismod mollis purus. Praesent luctus molestie diam, vel tincidunt tortor. Nullam a tempus purus. Ut sed sem libero. Sed sodales tristique consectetur. Aliquam in est ac nibh sodales luctus eu nec urna. Cras porta hendrerit lobortis. Nulla placerat vestibulum nunc, at accumsan sapien.')}}
            </article>
          </div>
        </div>
      </div>
    @endforeach
    <div class="clearfix"></div>
    <div class="text-center">
      <ul class="pagination margin-center">
        <li ><a href="#"><i class="fa fa-arrow-left"></i></a></li>
        <li class="active"><a href="#">1</a></li>
        <li ><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li ><a href="#"><i class="fa fa-arrow-right"></i></a></li>
      </ul>
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript">
  $("a.gallery-show").fancybox();
</script>
@endsection
