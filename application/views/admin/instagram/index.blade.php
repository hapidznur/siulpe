@extends('admin.template')

@section('title', 'Admin | Gallery Instagram')

@section('page-title', 'Gallery Instagram')

@section('body')
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div style="display: block;" class="x_content">
        <p style="font-size: 13pt">
        @if(!$user)
          <p style="font-size: 13pt">
            Anda belum login.
          </p>
          <p style="margin-bottom:10px">
            Untuk bisa menampilkan foto-foto instagram di gallery, anda normalnya harus login satu kali saja dan memberi izin compas-petualang untuk menampilkan foto-foto instagram anda.
          </p>
          <div class="text-center" style="margin-top: 40px; margin-bottom: 40px">
            <a href="{{url('instagram/login')}}" class="btn btn-default" style="display: inline-block">
              <img src="{{url('assets/images/Instagram-v051916.png')}}" style="width: 30px;" />
              Login with instagram
            </a>
          </div>
        @else
          Welcome {{$user->full_name}}
          <div class="">
            <p>
              Foto-fotoyang ditampilkan di menu gallery adalah foto-foto akun isntagram dari <b>{{"@{$user->username}"}}</b>. Jika anda ingin mengganti dengan akun instagram lain, silakan logout dan login dengan akun tersebut.
            </p>
            <a class="btn btn-default" href="{{url('instagram/logout')}}">Logout</a>
          </div>
        @endif
        </p>
      </div>
    </div>
  </div>
  <div class="page-title" style="margin-left: 10px">
    <div class="title_left">
      <h3>Gallery Flickr</h3>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div style="display: block;" class="x_content">
        <form class="" action="{{url('admin/gallery-post')}}" method="post">
          <div class="form-group">
            <label>Username</label>
            <input class="form-control" type="text" name="flickr" value="{{$identitasWeb->flickr}}" placeholder="username flickr">
          </div>
          <div class="text-right">
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('script')
@endsection
