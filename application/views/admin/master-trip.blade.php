@extends('admin.template')

@section('title', 'Admin | Master Trip')

@section('page-title', 'Master Trip')

@section('body')
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_content">
        <table class="table table-hovered table-striped data-table">
          <thead>
            <tr>
              <th>
                No
              </th>
              <th>
                Kode
              </th>
              <th>
                Judul
              </th>
              <th>
                Category
              </th>
              <th>
                Fasilitas
              </th>
              <th>
                Destinasi
              </th>
              <th>
                Aksi
              </th>
            </tr>
          </thead>
          <tbody>
            @foreach($masterTrips as $key => $masterTrip)
              <tr master-trip-id="{{$masterTrip->id}}">
                <td>
                  {{$key + 1}}
                </td>
                <td>
                  {{$masterTrip->kode}}
                </td>
                <td>
                  {{$masterTrip->title}}
                </td>
                <td>
                  {{read_more($masterTrip->category_list, 30)}}
                </td>
                <td>
                  {{read_more(strip_tags($masterTrip->fasilitas), 30)}}
                </td>
                <td>
                  {{$masterTrip->destinations_lists}}
                </td>
                <td>
                  <div class="btn-group">
                    <a class="btn btn-default" href="{{url("admin/edit-master-trip/{$masterTrip->id}")}}"><span class="fa fa-pencil"></span></a>
                    <a class="btn btn-default" href="{{url("admin/show-master-trip/{$masterTrip->id}")}}"><span class="fa fa-eye"></span></a>
                    <button class="btn btn-danger btn-delete"><span class="fa fa-trash"></span></button>
                  </div>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@section('modal')
  <div id="modal-delete" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Master Trip</h4>
        </div>
        <div class="modal-body">
          <p style="color: red">
            Apakah anda yakin akan menghapus master trip ini? Jika anda menghapus master trip ini, <b>Maka otomatis open trip yang bersangkutan, serta data booking, pembayaran, kas masuk, dan sebagainya akan ikut hilang dan tidak bisa dikembalikan</b>
          </p>
          <form style="display: none" id="master-trip-delete-form" action="{{url("admin/master-trip-delete-post")}}" method="post">
            <input type="hidden" name="master_trip_id" value="">
          </form>
        </div>
        <div class="modal-footer">
          <div class="form-group">
            <div class="btn-group" style="float: right">
              <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
              <button form="master-trip-delete-form" type="submit" class="btn btn-danger">Delete</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    var modal = $('#modal-delete')

    $('body').on('click', '.btn-delete', function () {
      var id = $(this).parent().parent().parent().attr('master-trip-id');
      modal.modal('show')
      $('input[name=master_trip_id]').val(id)
    })
  </script>
@endsection
