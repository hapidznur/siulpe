@extends('admin.template')

@section('title', 'Admin | Top Destinations')

@section('page-title', 'Top Destinations')

@section('body')
  <div class="col-md-12">
    <div class="x_panel">
      <div class="text-right">
        <a href="{{url('admin/new-top-destination')}}" class="btn btn-primary">Add Top Destinations</a>
      </div>
      <div class="x_content">
        @if($controller->session->message)
          <div class="alert alert-success">
            {{$controller->session->message}}
          </div>
        @endif
        <table class="table table-hovered table-striped table-stripped data-table">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode</th>
              <th>Nama Trip</th>
              <th>Tujuan Trip</th>
              <th>Harga</th>
              <th>Status</th>
              <th>Quota</th>
              <th>Tanggal Berangkat</th>
              <th>Jumlah Booking</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach($openTrips as $key => $openTrip)
              <tr open-trip-id="{{$openTrip->id}}">
                <td>{{$key+1}}</td>
                <td>{{$openTrip->kode}}</td>
                <td>{{$openTrip->masterTrip->title}}</td>
                <td>{{$openTrip->masterTrip->destinations_lists}}</td>
                <td>{{$openTrip->price}}</td>
                <td>
                  @if($openTrip->status === 'pendaftaran')
                    <span class="fa fa-envelope-o green" data-toggle="tooltip" title="Pendaftaran"></span>
                  @elseif($openTrip->status === 'pasti_berangkat')
                    <span class="fa fa-check blue" data-toggle="tooltip" title="Pasti Berangkat"></span>
                  @elseif($openTrip->status === 'dibatalkan')
                    <span class="fa fa-times red" data-toggle="tooltip" title="Dibatalkan"></span>
                  @else
                    <span class="fa fa-times red" data-toggle="tooltip" title="Ditutup"></span>
                  @endif
                </td>
                <td>{{$openTrip->quota_masuk}}/{{$openTrip->quota}}</td>
                <td>{{$openTrip->tanggal_berangkat}}</td>
                <td>{{$openTrip->bookings()->count()}}</td>
                <td>
                  <div class="btn-group">
                    <a class="btn btn-danger btn-delete" href="{{url("/admin/delete-top-destination/{$openTrip->id}")}}"><i class="fa fa-trash"></i></a>
                  </div>
                </td>
              </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@section('modal')
@endsection

@section('script')
@endsection
