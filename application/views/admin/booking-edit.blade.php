@extends('admin.template')

@section('title', "Booking {$booking->openTrip->masterTrip->title}")

@section('body')
  <div class="">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <div class="gap">
            <h2>{{$booking->openTrip->masterTrip->title}}, <small>{{$booking->openTrip->masterTrip->destination}}</small></h2>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <form class="form" action="{{url("admin/booking-edit-post")}}" method="post">
            @if($controller->session->errors)
              <div class="alert alert-danger">
                Messages
                <ul>
                  @foreach($controller->session->errors as $key => $errors)
                    @foreach($errors as $key => $error)
                      <li>{{$error}}</li>
                    @endforeach
                  @endforeach
                </ul>
              </div>
            @endif
            @if($controller->session->message)
              <div class="alert alert-success">
                {{$controller->session->message}}
              </div>
            @endif
            <div class="row">
              <div class="col-md-6">
                <input type="hidden" name="booking_id" value="{{$booking->id}}">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama" value="{{$booking->nama}}" required="">
                </div>
                <div class="form-group">
                  <label>No TLP</label>
                  <input type="text" name="no_tlp" placeholder="No telepon" class="form-control" value="{{$booking->no_tlp}}" required="">
                </div>
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" name="email" placeholder="email" class="form-control" value="{{$booking->email}}" required="">
                </div>
                <div class="form-group">
                  <label>Kota Tinggal</label>
                  <input name="alamat" class="form-control" placeholder="Kota tinggal" value="{{$booking->alamat}}" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Jumlah Peserta</label>
                  <input type="number" name="jumlah_peserta" value="{{$booking->jumlah_peserta}}" placeholder="Jumlah Peserta" min="1" max="{{$booking->openTrip->sisa_quota}}" class="form-control" required="">
                </div>
                <div id="kontainer-peserta">
                  @foreach($booking->nama_peserta as $key => $nama_peserta)
                    <div class="form-group">
                      <label>Nama Peserta</label>
                      <input class="form-control" type="text" name="nama_peserta[]" value="{{$nama_peserta}}" required="">
                    </div>
                  @endforeach
                </div>
                <button type="submit" class="btn btn-primary btn-lg" style="float: right">Booking</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{base_url('assets/js/regional.js')}}" charset="utf-8"></script>
  <script>
    Regional
      .init()
      .provinsi('select[name=provinsi]')
      .kota('select[name=kota]')
      .kecamatan('select[name=kecamatan]')
      .grab();
  </script>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <script src="{{url('assets/js/open-trip/booking.js')}}" charset="utf-8"></script>
@endsection
