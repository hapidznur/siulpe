@extends('admin.template')

@section('title', 'Admin | Edit User')

@section('page-title', "Users")

@section('body')
<div class="col-md-12 col-sm-12 col-xs-12">
     <div class="x_panel">
       <div class="x_title">
         <h2>Contact</h2>
         <div class="clearfix"></div>
       </div>
       <div style="display: block;" class="x_content">
         <br>
         @if($controller->session->errors)
           <div class="col-md-offset-3 col-md-7">
             <div class="alert alert-danger">
               <b>Messages</b>
               <ul>
                 @foreach($controller->session->errors as $key => $errors)
                   @foreach($errors as $key => $error)
                     <li>{{$error}}</li>
                   @endforeach
                 @endforeach
               </ul>
             </div>
           </div>
         @endif
         <form id="form-new-master-trip" action="{{url("admin/user-edit-post")}}" method="post" class="form-horizontal form-label-left">
           <div class="form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Nama</label>
             <div class="col-md-8 col-sm-8 col-xs-12">
               <input name="nama" class="form-control col-md-8 col-xs-12" type="text" value="{{auth()->admin()->nama}}" required="">
             </div>
           </div>
           <div class="form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Username</label>
             <div class="col-md-8 col-sm-8 col-xs-12">
               <input name="username" class="form-control col-md-8 col-xs-12" type="text" value="{{auth()->admin()->username}}" required="">
             </div>
           </div>
           <div class="form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Email</label>
             <div class="col-md-8 col-sm-8 col-xs-12">
               <input name="email" class="form-control col-md-8 col-xs-12" type="email" value="{{auth()->admin()->email}}">
             </div>
           </div>
           <div class="form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Password
             </label>
             <div class="col-md-8 col-sm-8 col-xs-12">
               <input name="password" class="form-control col-md-8 col-xs-12" type="password"  required="">
             </div>
           </div>
           <div class="form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Password Confirmation</label>
             <div class="col-md-8 col-sm-8 col-xs-12">
               <input name="password_confirmation" class="form-control col-md-8 col-xs-12" type="password" value="" required="">
             </div>
           </div>
           <div class="ln_solid"></div>
           <div class="form-group">
             <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
               <div class="btn-group">
                 <a href="{{base_url('admin/contact')}}" type="submit" class="btn btn-primary">Cancel</a>
                 <button type="submit" class="btn btn-success">Submit</button>
               </div>
             </div>
           </div>
         </form>
       </div>
     </div>
   </div>
@endsection