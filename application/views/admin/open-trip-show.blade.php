@extends('admin.template')

@section('title')
  Admin | {{$openTrip->masterTrip->title}}
@endsection

@section('page-title', 'Open Trip')

@section('body')
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_title">
        <div class="row">
          <div class="col-md-6">
            <h4>{{$openTrip->masterTrip->title}} <small>{{$openTrip->kode}}</small></h4>
          </div>
        </div>
      </div>
      <div style="position: absolute; right: 20px; top: 12px">
        <div class="btn-group">
          <a href="?export=true" class="btn btn-success">Export</a>
        </div>
        <div class="btn-group">
          <a href="{{url("admin/edit-open-trip/{$openTrip->id}")}}" class="btn btn-default" data-toggle="tooltip" title="Edit Open Trip"><span class="fa fa-edit"></span></a>
          <button id="btn-delete-open-trip" href="{{url('')}}" class="btn btn-danger" data-toggle="tooltip" title="Hapus Open Trip"><span class="fa fa-trash"></span></button>
        </div>
      </div>
      <div class="x_content">
        <div class="col-md-6">
          <div class="form-group">
            <label><span class="fa fa-map-marker"></span> Destinasi</label>
            <p>
              {{$openTrip->masterTrip->destinations_lists}}
            </p>
          </div>
          <div class="form-group">
            <label><span class="fa fa-car"></span> Meeting point</label>
            <p>
              {{$openTrip->meeting_point}}
            </p>
          </div>
          <div class="form-group">
            <label><span class="fa fa-check"></span> Status</label>
            <p>
              {{$openTrip->status}}
            </p>
          </div>
          <div class="form-group">
            <label><span class="fa fa-money"></span> Harga</label>
            <p>
              {{$openTrip->price}}
            </p>
          </div>
          <div class="form-group">
            <label><i class="fa fa-money"></i> DP Minimum</label>
            <p>
              IDR. {{number_format($openTrip->dp_minimum)}}
            </p>
          </div>
          <div class="form-group">
            <label><i class="fa fa-calendar"></i> Jumlah Termin</label>
            <p>
              {{number_format($openTrip->angsuran_maksimum)}}
            </p>
          </div>
          <div class="form-group">
            <label><i class="fa fa-calendar"></i> Jatuh Tempo Pembayaran</label>
            <p>
              {{$openTrip->jatuh_tempo_pembayaran}}
            </p>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label><span class="fa fa-star"></span> Fasilitas</label>
            <p>
              {{-- @foreach($openTrip->masterTrip->facilities as $key => $facility)
                <span class="label label-lg label-warning">{{$facility->title}}</span>
              @endforeach --}}
              {!!$openTrip->masterTrip->fasilitas!!}
            </p>
          </div>
          <div class="form-group">
            <label><span class="fa fa-calendar"></span> Tanggal Berangkat</label>
            <p>
              {{$openTrip->tanggal_berangkat_manusia}} <br>
              {{$openTrip->jam_berangkat}}
            </p>
          </div>
          <div class="form-group">
            <label><span class="fa fa-calendar"></span> Tanggal Pulang</label>
            <p>
              {{$openTrip->tanggal_pulang_manusia}} <br>
              {{$openTrip->jam_pulang}}
            </p>
          </div>
          <div class="form-group">
            <label><i class="fa fa-calendar"></i> End Date Pendafaran</label>
            <p>
              {{$openTrip->status_tutup_otomatis}}
            </p>
          </div>
          <div class="form-group">
            <label><span class="fa fa-users"></span> Kuota</label>
            <p>
              {{$openTrip->quota_masuk}}/{{$openTrip->quota}}
            </p>
          </div>
          <div>
            <label><i class="fa fa-external-link"></i> Review Link</label>
            <p>
              <a target="_blank" href="{{$openTrip->blog_link}}">{{read_more($openTrip->blog_link, 30)}}</a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_title">
        <h4>Daftar Pemesan</h4>
      </div>
      <div class="x_content">
        <table class="table table-striped table-hover data-table">
          <thead>
            <tr>
              <th>
                No
              </th>
              <th>
                Invoice
              </th>
              <th>
                Nama
              </th>
              <th>
                Email
              </th>
              <th>
                Jumlah Peserta
              </th>
              <th>
                Status
              </th>
              <th>
                Total
              </th>
              <th>
                <span class="fa fa-fire orange"></span>
              </th>
            </tr>
          </thead>
          <tbody>
            @foreach($openTrip->bookings as $key => $booking)
              <tr booking-id="{{$booking->id}}">
                <td>
                  {{$key + 1}}
                </td>
                <td>
                  {{$booking->no_invoice}}
                </td>
                <td>
                  {{$booking->nama}}
                </td>
                <td>
                  {{$booking->email}}
                </td>
                <td>
                  {{$booking->jumlah_peserta}}
                </td>
                <td>
                  {{$booking->status_pembayaran}}
                </td>
                <td>
                  IDR. {{number_format($booking->total_harga)}}
                </td>
                <td>
                  <div class="btn-group">
                    <a href="{{url("admin/booking-show/{$booking->id}")}}" class="btn btn-sm btn-default" data-toggle="tooltip" title="Lihat detil pembayaran"><span class="fa fa-eye"></span></a>
                    <button class="btn btn-sm btn-danger btn-delete-booking" data-toggle="tooltip" title="Hapus Booking"><span class="fa fa-trash"></span></button>
                  </div>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@section('modal')
  <div id="modal-delete-open-trip" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Open Trip</h4>
        </div>
        <div class="modal-body">
          Apakah anda yakin akan menghapus data ini?
          <small>data yang sudah dihapus tidak bisa dikembalikan lagi.</small>
          <form style="display: none" id="open-trip-delete-form" action="{{url("admin/open-trip-delete-post")}}" method="post">
            <input type="hidden" name="open_trip_id" value="{{$openTrip->id}}">
          </form>
        </div>
        <div class="modal-footer">
          <div class="form-group">
            <div class="btn-group" style="float: right">
              <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
              <button form="open-trip-delete-form" type="submit" class="btn btn-danger">Delete</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="modal-delete-booking" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Data Booking</h4>
        </div>
        <div class="modal-body">
          Apakah anda yakin akan menghapus data ini?
          <small>data yang sudah dihapus tidak bisa dikembalikan lagi.</small>
          <form style="display: none" id="booking-delete-form" action="{{url("admin/booking-delete-post")}}" method="post">
            <input type="hidden" name="booking_id" value="">
            <input type="hidden" name="from_open_trip_show" value="{{$openTrip->id}}">
          </form>
        </div>
        <div class="modal-footer">
          <div class="form-group">
            <div class="btn-group" style="float: right">
              <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
              <button form="booking-delete-form" type="submit" class="btn btn-danger">Delete</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{url('assets/js/admin/open-trip-show.js')}}" charset="utf-8"></script>
@endsection
