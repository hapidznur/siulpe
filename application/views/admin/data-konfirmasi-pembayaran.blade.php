@extends('admin.template')

@section('title', 'Admin | Data Pembayaran')

@section('page-title', 'Data Pembayaran')

@section('body')
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_content">
        @if($controller->session->message)
          <div class="alert alert-success">
            {{$controller->session->message}}
          </div>
        @endif
        <table class="table table-hovered table-striped table-stripped data-table">
          <thead>
            <tr>
              <th>No</th>
              <th>Invoice</th>
              <th>Kode Booking</th>
              <th>Pengirim</th>
              <th>Jumlah Transfer</th>
              <th>Tanggal</th>
              <th>Status</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach($invoiceConfirmations as $key => $invoiceConfirmation)
              <tr invoice-confirmation-id="{{$invoiceConfirmation->id}}" confirmation-id="{{$invoiceConfirmation->id}}">
                <td>{{$key + 1}}</td>
                <td>{{$invoiceConfirmation->invoice->nomor_invoice}}</td>
                <td>{{$invoiceConfirmation->invoice->booking->kode}}</td>
                <td>{{$invoiceConfirmation->nama}}</td>
                <td>{{$invoiceConfirmation->total_transfer_str}}</td>
                <td>{{$invoiceConfirmation->tanggal_pembayaran}}</td>
                <td>{{$invoiceConfirmation->status}}</td>
                <td>
                  <div class="btn-group">
                    @if($invoiceConfirmation->status !== 'sudah dikonfirmasi')
                    <button class="btn btn-success btn-check-pembayaran" data-toggle="tooltip" title="Konfirmasi"><span class="fa fa-check"></span></button>
                    @endif
                    <button class="btn btn-primary btn-detail btn-detail-pembayaran" data-toggle="tooltip" title="Lihat Detail"><span class="fa fa-eye"></span></button>
                    <button class="btn btn-danger btn-delete-pembayaran" data-toggle="tooltip" title="Hapus Data"><span class="fa fa-trash"></span></button>
                  </div>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@section('modal')
  @include('admin.modal.konfirmasi-pembayaran')
  @include('admin.modal.detail-pembayaran')
  @include('admin.modal.delete-pembayaran')
@endsection

@section('script')
  <script src="{{url('assets/js/admin/konfirmasi-pembayaran.js')}}" charset="utf-8"></script>
  <script src="{{url('assets/js/admin/pembayaran.js')}}" charset="utf-8"></script>
@endsection
