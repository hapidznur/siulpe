@extends('admin.template')

@section('title', 'Admin | Edit Master Trip')

@section('page-title', 'Master Trip')

@section('body')
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Edit Master Trip</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div style="display: block;" class="x_content">
          <br>
          <div id="ini-adalah-halaman-edit-master-trip">
          </div>
          <form id="form-new-master-trip" enctype="multipart/form-data" action="{{url("admin/edit-master-trip-post/{$masterTrip->id}")}}" master-trip-id="{{$masterTrip->id}}" method="post" class="form-horizontal form-label-left">
          </form>
          @if($controller->session->errors)
            <div class="col-md-offset-3 col-md-7">
              <div class="alert alert-danger">
                <b>Messages</b>
                <ul>
                  @foreach($controller->session->errors as $key => $errors)
                    @foreach($errors as $key => $error)
                      <li>{{$error}}</li>
                    @endforeach
                  @endforeach
                </ul>
              </div>
            </div>
          @endif
        </div>
        <!-- Smart Wizard -->
        <div id="wizard" class="form_wizard wizard_horizontal">
          <ul class="wizard_steps">
            <li>
              <a href="#step-1">
                <span class="step_no">1</span>
                <span class="step_descr">
                  Step 1<br />
                  <small>Nama, Destinasi, Activity</small>
                </span>
              </a>
            </li>
            <li>
              <a href="#step-2">
                <span class="step_no">2</span>
                <span class="step_descr">
                  Step 2<br />
                  <small>Fasilitas, Itinerary</small>
                </span>
              </a>
            </li>
            <li>
              <a href="#step-3">
                <span class="step_no">3</span>
                <span class="step_descr">
                  Step 3<br />
                  <small>Informasi Tambahan, Ketentuan</small>
                </span>
              </a>
            </li>
            <li>
              <a href="#step-4">
                <span class="step_no">4</span>
                <span class="step_descr">
                  Step 4<br />
                  <small>Foto, Attachment</small>
                </span>
              </a>
            </li>
          </ul>
          <div id="step-1">
            <div class="form-horizontal form-label-left">
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Trip <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input form="form-new-master-trip" name="title" required="required" class="form-control col-md-7 col-xs-12" type="text" placeholder="Nama trip" value="{{$masterTrip->title}}">
                </div>
              </div>
              <div class="form-group">
                <div id="destination-options" style="display: none">
                  <div class="form-group">
                    <div class="col-md-5 col-sm-5 col-md-offset-3 col-sm-offset-3">
                      <select form="form-new-master-trip" class="form-control" name="destinations[]">
                        @foreach($destinations as $key => $destination)
                          <option value="{{$destination->id}}">{{$destination->provinsi}}, {{$destination->kota}}, {{$destination->nama}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-2 col-sm-2">
                      <button class="btn btn-default destination-plus"><i class="fa fa-plus"></i></button>
                      <button class="btn btn-default destination-close"><i class="fa fa-close"></i></button>
                    </div>
                  </div>
                </div>
                <label class="control-label col-md-3 col-sm-3">Destinasi <span class="required"></span></label>
                @foreach($masterTrip->destinations as $key => $destinationFromMasterTrip)
                  @if($key > 0)
                    <div class="form-group">
                  @endif
                  <div class="col-md-5 col-sm-5 {{$key > 0 ? 'col-md-offset-3 col-sm-offset-3' : ''}}">
                    <select form="form-new-master-trip" id="destinations" class="form-control" name="destinations[]" required="">
                      @foreach($destinations as $destination)
                        <option value="{{$destination->id}}" {{$destination->id == $destinationFromMasterTrip->id ? 'selected' : ''}}>{{$destination->provinsi}}, {{$destination->kota}}, {{$destination->nama}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-md-2 col-sm-2">
                    <button class="btn btn-default destination-plus"><i class="fa fa-plus"></i></button>
                    @if($key > 0)
                      <button class="btn btn-default destination-close"><i class="fa fa-close"></i></button>
                    @endif
                  </div>
                  @if($key > 0)
                  </div>
                  @endif
                @endforeach
                <div id="destination-container">
                </div>
              </div>
              <div id="activity-options" style="display: none">
                <div class="form-group">
                  <div class="col-md-5 col-sm-5 col-md-offset-3 col-sm-offset-3">
                    <select form="form-new-master-trip" class="form-control" name="categories[]" required="">
                      @foreach($categories as $key => $category)
                        <option value="{{$category->id}}">
                          {{$category->nama}}
                        </option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-md-2 col-sm-2">
                    <button class="btn btn-default activity-plus"><i class="fa fa-plus"></i></button>
                    <button class="btn btn-default activity-close"><i class="fa fa-close"></i></button>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3">Activity <span class="required"></span></label>
                @foreach($masterTrip->categories as $key => $categoryFromMasterTrip)
                  @if($key > 0)
                    <div class="form-group">
                  @endif
                  <div class="col-md-5 col-sm-5 {{$key > 0 ? 'col-md-offset-3 col-sm-offset-3' : ''}}">
                    <select form="form-new-master-trip" id="categories" class="form-control" name="categories[]" required="">
                      @foreach($categories as $category)
                        <option value="{{$category->id}}" {{$category->id == $categoryFromMasterTrip->id ? 'selected' : ''}}>
                          {{$category->nama}}
                        </option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-md-2 col-sm-2">
                    <button class="btn btn-default activity-plus"><i class="fa fa-plus"></i></button>
                    @if($key > 0)
                      <button class="btn btn-default activity-close"><i class="fa fa-close"></i></button>
                    @endif
                  </div>
                  @if($key > 0)
                  </div>
                  @endif
                @endforeach
              </div>
              <div id="activity-container">
              </div>
            </div>
          </div>
          <div id="step-2">
            <div class="form-horizontal form-label-left">
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Fasilitas <span class="required">*</span></label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <textarea form="form-new-master-trip" name="fasilitas" id="textarea-tiny-mce" class="form-control" style="height: 200px" placeholder="Fasilitas">{{$masterTrip->fasilitas}}</textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Itinerary <span class="required">*</span></label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <textarea form="form-new-master-trip" name="itenary" class="form-control" style="height: 200px" placeholder="Itenary">{{$masterTrip->itenary}}</textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">File Itinerary
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <input form="form-new-master-trip" type="file" name="itenary_file" value="">
                </div>
              </div>
            </div>
          </div>
          <div id="step-3">
            <div class="form-horizontal form-label-left">
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Informasi Tambahan <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <textarea form="form-new-master-trip" name="additional_information" class="form-control col-md-7 col-xs-12" type="text" placeholder="Informasi Tambahan">{{$masterTrip->additional_information}}</textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Ketentuan <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <textarea form="form-new-master-trip" name="term_of_service" class="form-control col-md-7 col-xs-12" type="text" placeholder="Ketenuan">{{$masterTrip->term_of_service}}</textarea>
                </div>
              </div>
            </div>
          </div>
          <div id="step-4">
            <div class="form-horizontal form-label-left">
              <input type="hidden" name="id_cover" form="form-new-master-trip" value="{{$masterTrip->covers()->isCover()->first()}}">
              <div class="row">
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Cover <span class="required">*</span>
                  </label>
                  <div class="col-md-7">
                    <div class="col-md-12 gap gap-small">
                      <button type="button" class="btn btn-primary btn-tambah-gambar"><span class="fa fa-plus"></span> Tambah Gambah</button>
                    </div>
                    <div class="row">
                      @for($i=0; $i < 5; $i++)
                        <div class="col-md-4">
                          <div style="padding-top: 5px; padding-bottom: 5px">
                            <div class="master-trip-cover" {{$i < count($masterTrip->covers) ? "cover-id={$masterTrip->covers[$i]->id}" : ''}}>
                              <div class="btn-group">
                                <button index-image="{{$i}}" class="btn btn-success btn-set-cover" type="button"><span class="fa fa-check"></span></button>
                                <button class="btn btn-danger btn-close" type="button"><span class="fa fa-close"></span></button>
                              </div>
                              @if($i < count($masterTrip->covers))
                                <img src="{{$masterTrip->covers[$i]->cover_src}}" class="{{$masterTrip->id_cover === $i ? 'img cover selected' : ''}}" alt="Cover" style="width: 100%; height: 140px; object-fit: cover" />
                              @else
                                <img class="master-trip-cover-{{$i}}" src="http://nemanjakovacevic.net/wp-content/uploads/2013/07/placeholder.png" alt="Cover" style="width: 100%; height: 140px; object-fit: cover" />
                                <input form="form-new-master-trip" id="master-trip-cover-{{$i}}" type="file" name="cover[]" style="display: none">
                              @endif
                            </div>
                          </div>
                        </div>
                      @endfor
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('modal')
<div class="tiny-gallery-modal modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
			</div>
		</div>
	</div>
</div>
<div class="modal modal-pesan fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Message</h4>
			</div>
			<div class="modal-body">
        <div class="alert">

        </div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade new-facility" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Fasilitas</h4>
      </div>
      <div class="modal-body">
        <form id="form-tambah-fasilitas">
          <div class="form-group">
            <label class="control-label">Nama Fasilitas <span class="required">*</span></label>
            <input class="form-control" type="text" name="facility_name" placeholder="Nama Fasilitas" required="">
          </div>
          <div class="form-group">
            <label class="control-label">Harga <span class="required">*</span></label>
            <input class="form-control" type="number" min="0" name="facility_price" placeholder="100000" required="">
          </div>
          <div class="form-group">
            <label class="control-label">Deskripsi</label>
            <textarea class="form-control" name="facility_description" rows="3" placeholder="Info tambahan mengenai fasilitas"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <div class="btn-group">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary btn-submit-facility" form="form-tambah-fasilitas">Tambah</button>
        </div>
      </div>
    </div>

  </div>
</div>
@endsection

@section('script')
  <script src="{{base_url('assets/js/admin/tiny-mce.js')}}" charset="utf-8"></script>
  <script src="{{base_url('assets/js/regional.js')}}" charset="utf-8"></script>
  <script src="{{base_url('assets/js/admin/new-master-trip.js')}}" charset="utf-8"></script>
  <script src="{{base_url('assets/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js')}}"></script>
  <script>
    (function () {
      startTinyMCE('textarea');

      Regional
        .init()
        .provinsi($('[name=provinsi]'), '{{strtoupper($masterTrip->provinsi)}}')
        .kota($('[name=kota]'), '{{strtoupper($masterTrip->kota)}}')
        .grab();

      $(document).ready(function() {
        $('#wizard').smartWizard();

        $('#wizard_verticle').smartWizard({
          transitionEffect: 'slide'
        });

        $('.buttonNext').addClass('btn btn-default');
        $('.buttonPrevious').addClass('btn btn-default');
        $('.buttonFinish').addClass('btn btn-primary').text('Submit').attr('form', 'form-new-master-trip').click(function () {
          $("#form-new-master-trip").submit()
        });
      });
    })();
  </script>
@endsection
