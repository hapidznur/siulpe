<table border="1">
	<thead>
		<tr>
			<th colspan="8">
				{{$openTrip->kode}} - {{$openTrip->masterTrip->title}} - {{$openTrip->tanggal_berangkat_manusia}}
			</th>
		</tr>
		<tr>
			<th>No</th>
			<th>Kode Booking</th>
			<th>Invoice</th>
			<th>Nama Pemesan</th>
			<th>Email Pemesan</th>
			<th>Nomor Telepon</th>
			<th>Kota Tinggal</th>
			<th>Daftar Peserta</th>
			<th>Tanggal Pemesan</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
		@foreach($openTrip->bookings as $key => $booking)
		<tr style="vertical-align: top">
			<td>
				{{$key + 1}}
			</td>
			<td>
				{{$booking->kode}}
			</td>
			<td>
				{{$booking->no_invoice}}
			</td>
			<td>
				{{$booking->nama}}
            </td>
            <td>
            	{{$booking->email}}
            </td>
            <td>
            	{{$booking->no_tlp}}
            </td>
            <td>
            	{{$booking->alamat}}
            </td>
            <td>
            	@foreach($booking->nama_peserta as $peserta)
            		{{$peserta}}<br>
            	@endforeach
            </td>
            <td>
            	{{$booking->status_pembayaran}}
            </td>
            <td>
            	{{$booking->created_at->toFormattedDateString()}}
            </td>
		</tr>
		@endforeach
	</tbody>
</table>