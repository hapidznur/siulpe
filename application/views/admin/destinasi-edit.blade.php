@extends('admin.template')

@section('title', 'Admin | Edit Destinasi')

@section('page-title', 'Destinasi')

@section('body')
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Edit Destinasi</h2>
        <div class="clearfix"></div>
      </div>
      <div style="display: block;" class="x_content">
        <br>
        @if($controller->session->errors)
          <div class="col-md-offset-3 col-md-7">
            <div class="alert alert-danger">
              <b>Messages</b>
              <ul>
                @foreach($controller->session->errors as $key => $errors)
                  @foreach($errors as $key => $error)
                    <li>{{$error}}</li>
                  @endforeach
                @endforeach
              </ul>
            </div>
          </div>
        @endif
        <form id="form-new-master-trip" action="{{url("admin/destinasi-edit-post/{$destinasi->id}")}}" method="post" class="form-horizontal form-label-left">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Destinasi<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input name="nama" required="required" class="form-control col-md-7 col-xs-12" type="text" placeholder="Nama destinasi" value="{{$destinasi->nama}}">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Provinsi <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select name="provinsi" required="required" class="form-control col-md-7 col-xs-12">
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Kota <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select name="kota" required="required" class="form-control col-md-7 col-xs-12">
              </select>
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <div class="btn-group">
                <a href="{{base_url('admin/destinations')}}" type="submit" class="btn btn-primary">Cancel</a>
                <button type="submit" class="btn btn-success">Update</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{base_url('assets/js/regional.js')}}" charset="utf-8"></script>
  <script>
    (function () {
      Regional
        .init()
        .provinsi($('[name=provinsi]'), '{{strtoupper($destinasi->provinsi)}}')
        .kota($('[name=kota]'), '{{strtoupper($destinasi->kota)}}')
        .grab();
    })();
  </script>
@endsection
