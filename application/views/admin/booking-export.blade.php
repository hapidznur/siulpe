<table class="table table-hovered table-striped table-stripped data-table" border="1">
  <thead>
    <tr>
      <th>No</th>
      <th>Kode</th>
      <th>Open Trip</th>
      <th>Nama Trip</th>
      <th>Nama</th>
      <th>No Telepon</th>
      <th>Kota</th>
      <th>Jumlah Orang</th>
      <th>Nama Peserta</th>
      <th>Status Pembayaran</th>
      <th>Tanggal</th>
    </tr>
  </thead>
  <tbody>
    @foreach($bookings as $key => $booking)
      <tr id-booking="{{$booking->id}}">
        <td>{{$key + 1}}</td>
        <td>{{$booking->kode}}</td>
        <td>{{$booking->openTrip->kode}}</td>
        <td>{{$booking->openTrip->masterTrip->title}}</td>
        <td>{{$booking->nama}}</td>
        <td>{{$booking->no_tlp}}</td>
        <td>{{$booking->alamat}}</td>
        <td>{{$booking->jumlah_peserta}}</td>
        <td>{{$booking->nama_peserta_src}}</td>
        <td>{{$booking->status_pembayaran}}</td>
        <td>{{$booking->tanggal_booking}}</td>
      </tr>
    @endforeach
  </tbody>
</table>