@extends('admin.template')

@section('title', 'Admin | Legalitas')

@section('page-title', 'Legalitas')

@section('body')
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Legalitas</h2>
        <div class="clearfix"></div>
      </div>
      <div style="display: block;" class="x_content">
        <br>
        <form action="{{url("admin/about-us-process")}}" method="post" class="form-horizontal form-label-left">
          <input type="hidden" name="redirect" value="admin/legalitas">
          <div class="form-group">
            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Legalitas</label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <textarea name="legalitas" class="form-control col-md-8 col-xs-12" style="height: 400px" type="text">{{$aboutUs->legalitas}}</textarea>
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-2">
              <div class="btn-group" style="float: right">
                <a href="{{base_url('admin/legalitas')}}" type="submit" class="btn btn-primary">Cancel</a>
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{base_url('assets/js/admin/tiny-mce.js')}}" charset="utf-8"></script>
  <script>
    (function () {
      startTinyMCE('textarea');
    })();
  </script>
@endsection
