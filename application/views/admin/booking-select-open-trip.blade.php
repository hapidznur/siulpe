@extends('admin.template')

@section('title', 'Admin | Booking')

@section('page-title', 'Booking')

@section('body')
  <div class="">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <div class="gap">
            <h2>Order by Admin <small>pilih open trip</small></h2>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <form class="" action="" method="get">
            <div class="form-group">
              <label>Pilih open trip</label>
              <select class="selectpicker form-control" name="open_trip_id" data-live-search="true" required="">
                @foreach($openTrips as $key => $openTrip)
                  <option value="{{$openTrip->id}}">{{$openTrip->kode}} - {{$openTrip->masterTrip->title}} ({{$openTrip->masterTrip->destinations_lists}})</option>
                @endforeach
              </select>
            </div>
            <div class="text-right">
              <button type="submit" class="btn btn-primary">Pilih</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
  @if ($controller->input->get('error'))
  alert("Kuota penuh");
  @endif
  </script>
@endsection
