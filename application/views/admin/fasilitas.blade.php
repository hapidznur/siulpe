@extends('admin.template')

@section('title', 'Admin | Data Fasilitas')

@section('page-title', 'Fasilitas')

@section('body')
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_content">
        @if($controller->session->message)
          <div class="alert alert-success">
            {{$controller->session->message}}
          </div>
        @endif
        <div class="text-right">
          <a class="btn btn-primary" href="{{url('admin/fasilitas-baru')}}"><span class="fa fa-plus"></span> Fasilitas Baru</a>
        </div>
        <table class="table table-hovered table-striped table-stripped data-table">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Deskripsi</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach($facilities as $key => $facility)
              <tr facility-id="{{$facility->id}}" facility-title="{{$facility->title}}" facility-description="{{$facility->description}}">
                <td>{{$key + 1}}</td>
                <td>{{$facility->title}}</td>
                <td>{{read_more($facility->description, 70)}}</td>
                <td>
                  <div class="btn-group">
                    <a href="{{url("admin/fasilitas-edit/{$facility->id}")}}" class="btn btn-default" data-toggle="tooltip" title="Lihat Detail"><span class="fa fa-edit"></span></a>
                    <button class="btn btn-primary btn-detail" data-toggle="tooltip" title="Lihat Detail"><span class="fa fa-eye"></span></button>
                    <button class="btn btn-danger btn-delete" data-toggle="tooltip" title="Hapus Data"><span class="fa fa-trash"></span></button>
                  </div>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@section('modal')
  <div id="modal-detail" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Detail Fasilitas</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="">
              <div class="col-md-2">
                <b>Title</b>
              </div>
              <div class="col-md-10">
                <p id="title"></p>
              </div>
            </div>
            <div class="">
              <div class="col-md-2">
                <b>Description</b>
              </div>
              <div class="col-md-10">
                <p id="description"></p>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="form-group">
            <div class="btn-group" style="float: right">
              <button type="button" data-dismiss="modal" class="btn btn-default">Ok</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="modal-konfirmasi" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Konfirmasi Pembayaran</h4>
        </div>
        <div class="modal-body">
          Apakah anda yakin akan mengkonfirmasi pembayaran ini?
          <form style="display: none" id="invoice-confirmation-form" action="{{url("admin/konfirmasi-pembayaran-update")}}" method="post">
            <input type="hidden" name="invoice_confirmation_id" value="">
          </form>
        </div>
        <div class="modal-footer">
          <div class="form-group">
            <div class="btn-group" style="float: right">
              <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
              <button form="invoice-confirmation-form" type="submit" class="btn btn-primary">Konfirmasi</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="modal-delete" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Hapus Fasilitas</h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-default">
            Apakah anda yakin akan menghapus fasilitas ini? Jika anda menghapus, maka open trip yang bersangkutan dengan fasilitas ini data fasilitasnya akan hilang.
          </div>
          <form style="display: none" id="fasilitas-delete-form" action="{{url("admin/fasilitas-delete")}}" method="post">
            <input type="hidden" name="id" value="">
          </form>
        </div>
        <div class="modal-footer">
          <div class="form-group">
            <div class="btn-group" style="float: right">
              <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
              <button form="fasilitas-delete-form" type="submit" class="btn btn-danger">Delete</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{url('assets/js/admin/fasilitas.js')}}" charset="utf-8"></script>
@endsection
