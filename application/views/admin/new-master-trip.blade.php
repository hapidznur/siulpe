@extends('admin.template')

@section('title', 'Admin | New Master Trip')

@section('page-title', 'Master Trip')

@section('body')
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>New Master Trip</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div style="display: block;" class="x_content">
          <br>
          @if($controller->session->errors)
            <div class="col-md-offset-3 col-md-7">
              <div class="alert alert-danger">
                <b>Messages</b>
                <ul>
                  @foreach($controller->session->errors as $key => $errors)
                    @foreach($errors as $key => $error)
                      <li>{{$error}}</li>
                    @endforeach
                  @endforeach
                </ul>
              </div>
            </div>
          @endif
          <form id="form-new-master-trip" enctype="multipart/form-data" action="{{url('admin/new-master-trip-post')}}" method="post" class="form-horizontal form-label-left">
          </form>
        </div>
        <!-- Smart Wizard -->
        <div id="wizard" class="form_wizard wizard_horizontal">
          <ul class="wizard_steps">
            <li>
              <a href="#step-1">
                <span class="step_no">1</span>
                <span class="step_descr">
                  Step 1<br />
                  <small>Nama, Destinasi, Activity</small>
                </span>
              </a>
            </li>
            <li>
              <a href="#step-2">
                <span class="step_no">2</span>
                <span class="step_descr">
                  Step 2<br />
                  <small>Fasilitas, Itinerary</small>
                </span>
              </a>
            </li>
            <li>
              <a href="#step-3">
                <span class="step_no">3</span>
                <span class="step_descr">
                  Step 3<br />
                  <small>Informasi Tambahan, Ketentuan</small>
                </span>
              </a>
            </li>
            <li>
              <a href="#step-4">
                <span class="step_no">4</span>
                <span class="step_descr">
                  Step 4<br />
                  <small>Foto, Attachment</small>
                </span>
              </a>
            </li>
          </ul>
          <div id="step-1">
            <div class="form-horizontal form-label-left">
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nama Trip <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input form="form-new-master-trip" name="title" required="required" class="form-control col-md-7 col-xs-12" type="text" placeholder="Nama trip" value="{{old('title')}}">
                </div>
              </div>
              <div class="form-group">
                <div id="destination-options" style="display: none">
                  <div class="form-group">
                    <div class="col-md-5 col-sm-5 col-md-offset-3 col-sm-offset-3">
                      <select form="form-new-master-trip" class="form-control" name="destinations[]">
                        @foreach($destinations as $key => $destination)
                          <option value="{{$destination->id}}">{{$destination->provinsi}}, {{$destination->kota}}, {{$destination->nama}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-2 col-sm-2">
                      <button class="btn btn-default destination-plus"><i class="fa fa-plus"></i></button>
                      <button class="btn btn-default destination-close"><i class="fa fa-close"></i></button>
                    </div>
                  </div>
                </div>
                <label class="control-label col-md-3 col-sm-3">Destinasi <span class="required"></span></label>
                <div class="col-md-5 col-sm-5">
                  <select form="form-new-master-trip" id="destinations" class="form-control" name="destinations[]">
                    @foreach($destinations as $key => $destination)
                      <option value="{{$destination->id}}">{{$destination->provinsi}}, {{$destination->kota}}, {{$destination->nama}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-1 col-sm-1">
                  <button class="btn btn-default destination-plus"><i class="fa fa-plus"></i></button>
                </div>
              </div>
              <div id="destination-container">

              </div>
              <div id="activity-options" style="display: none">
                <div class="form-group">
                  <div class="col-md-5 col-sm-5 col-md-offset-3 col-sm-offset-3">
                    <select form="form-new-master-trip" class="form-control" name="categories[]" required="">
                      @foreach($categories as $key => $category)
                        <option value="{{$category->id}}">
                          {{$category->nama}}
                        </option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-md-2 col-sm-2">
                    <button class="btn btn-default activity-plus"><i class="fa fa-plus"></i></button>
                    <button class="btn btn-default activity-close"><i class="fa fa-close"></i></button>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3">Activity <span class="required"></span></label>
                <div class="col-md-5 col-sm-5">
                  <select form="form-new-master-trip" id="categories" class="form-control" name="categories[]" required="">
                    @foreach($categories as $key => $category)
                      <option value="{{$category->id}}">
                        {{$category->nama}}
                      </option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-2 col-sm-2">
                  <button class="btn btn-default activity-plus"><i class="fa fa-plus"></i></button>
                </div>
              </div>
              <div id="activity-container">

              </div>
            </div>
          </div>
          <div id="step-2">
            <div class="form-horizontal form-label-left">
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Fasilitas <span class="required">*</span></label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <textarea form="form-new-master-trip" name="fasilitas" id="textarea-tiny-mce" class="form-control" style="height: 200px" placeholder="Fasilitas">{{old('fasilitas')}}</textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Itinerary <span class="required">*</span></label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <textarea form="form-new-master-trip" name="itenary" id="textarea-tiny-mce" class="form-control" style="height: 200px" placeholder="Itinerary">{{old('itenary')}}</textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="form-horizontal form-label-left">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">File Itinerary <span class="required">*</span>
                  </label>
                  <div class="col-md-7 col-sm-7 col-xs-12">
                    <input form="form-new-master-trip" type="file" name="itenary_file" value="">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="step-3">
            <div class="form-horizontal form-label-left">
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Informasi Tambahan <span class="required">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <textarea form="form-new-master-trip" name="additional_information" class="form-control col-md-7 col-xs-12" type="text" placeholder="Informasi Tambahan">{{old('additional_information')}}</textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Ketentuan <span class="required">*</span>
                </label>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <textarea form="form-new-master-trip" name="term_of_service" class="form-control col-md-7 col-xs-12" type="text" placeholder="Ketenuan">{{old('term_of_service')}}</textarea>
                </div>
              </div>
            </div>
          </div>
          <div id="step-4">
            <input type="hidden" form="form-new-master-trip" name="id_cover">
            <div class="form-horizontal form-label-left">
              <div class="row">
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Cover <span class="required">*</span>
                  </label>
                  <div class="col-md-7">
                    <div class="col-md-12 gap gap-small">
                      <button type="button" class="btn btn-primary btn-tambah-gambar"><span class="fa fa-plus"></span> Tambah Gambah</button>
                    </div>
                    <div class="row">
                      @for($i=0; $i < 5; $i++)
                        <div class="col-md-4">
                          <div style="padding-top: 5px; padding-bottom: 5px">
                            <div class="master-trip-cover">
                              <div class="btn-group">
                                <button index-image="{{$i}}" class="btn btn-success btn-set-cover" type="button"><span class="fa fa-check"></span></button>
                                <button class="btn btn-danger btn-close" type="button"><span class="fa fa-close"></span></button>
                              </div>
                              <img class="master-trip-cover-{{$i}}" src="http://nemanjakovacevic.net/wp-content/uploads/2013/07/placeholder.png" alt="Cover" style="width: 100%; height: 140px; object-fit: cover" />
                              <input form="form-new-master-trip" id="master-trip-cover-{{$i}}" type="file" name="cover[]" style="display: none">
                            </div>
                          </div>
                        </div>
                      @endfor
                    </div>
                    <div class="alert alert-info">
                      Pilih cover master trip dengan mencentang gambar.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('modal')
<div class="tiny-gallery-modal modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
			</div>
		</div>
	</div>
</div>
<div class="modal modal-pesan fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Message</h4>
			</div>
			<div class="modal-body">
        <div class="alert">

        </div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
  <script src="{{base_url('assets/js/admin/tiny-mce.js')}}" charset="utf-8"></script>
  <script src="{{base_url('assets/js/regional.js')}}" charset="utf-8"></script>
  <script src="{{base_url('assets/js/admin/new-master-trip.js')}}" charset="utf-8"></script>
  <script src="{{base_url('assets/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js')}}"></script>
  <script>
    (function () {
      startTinyMCE('textarea');

      Regional
        .init()
        .provinsi($('[name=provinsi]'))
        .kota($('[name=kota]'))
        .grab();
    })();

    $(document).ready(function() {
      $('#wizard').smartWizard();

      $('#wizard_verticle').smartWizard({
        transitionEffect: 'slide'
      });

      $('.buttonNext').addClass('btn btn-default');
      $('.buttonPrevious').addClass('btn btn-default');
      $('.buttonFinish').addClass('btn btn-primary').text('Submit').attr('form', 'form-new-master-trip').click(function () {
        $("#form-new-master-trip").submit()
      });
    });
  </script>
@endsection
