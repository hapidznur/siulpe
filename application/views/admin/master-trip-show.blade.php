@extends('admin.template')

@section('title')
  Admin | {{$masterTrip->title}}
@endsection

@section('page-title', 'Master Trip')

@section('body')
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_title">
        <h4>{{$masterTrip->title}}</h4>
      </div>
      <div class="x_content">
        <div class="row">
          <div class="col-md-8">
            <div class="">
              <label>Destinasi</label>
              <p>
                {{$masterTrip->destinations_lists}}
              </p>
            </div>
            <div class="">
              <label>Kategori</label>
              <p>
                {{$masterTrip->category_list}}
              </p>
            </div>
            @if($masterTrip->itenary_file)
              <div class="">
                <label for="">Download Itinerary</label>
                <div class="">
                  <a href="{{$masterTrip->itenary_file_src}}" target="_blank" class="btn btn-default"><i class="fa fa-download"></i> Download Itinerary</a>
                </div>
              </div>
            @endif
            <div class="tabbable" style="margin-top: 20px">
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home"><i class="fa fa-building-o"></i> Itinerary</a></li>
                <li><a data-toggle="tab" href="#menu4"><i class="fa fa-star-o"></i> Facilities</a></li>
                <li><a data-toggle="tab" href="#menu1"><i class="fa fa-money"></i> Informasi Tambahan</a></li>
                <li><a data-toggle="tab" href="#menu2"><i class="fa fa-question"></i> Term of Services</a></li>
              </ul>
              <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                  <div class="gap gap-small"></div>
                  <h3>Itinerary</h3>
                  <div class="">
                    <p>
                      {!!$masterTrip->itenary!!}
                    </p>
                  </div>
                </div>
                <div id="menu4" class="tab-pane fade in">
                  <div class="gap gap-small"></div>
                  <h3>Facilities</h3>
                  <div class="">
                    <p>
                      {!!$masterTrip->fasilitas!!}
                    </p>
                  </div>
                </div>
                <div id="menu1" class="tab-pane fade">
                  <div class="gap gap-small"></div>
                  <h3>Informasi Tambahan</h3>
                  <div class="">
                    <p>
                      {!!$masterTrip->additional_information!!}
                    </p>
                  </div>                </div>
                <div id="menu2" class="tab-pane fade">
                  <div class="gap gap-small"></div>
                  <h3>Term of Services</h3>
                  <article class="">
                    <p>
                      {!!$masterTrip->term_of_service!!}
                    </p>
                  </article>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="row">
              @foreach($masterTrip->covers as $key => $cover)
                <div class="col-md-4">
                  <div style="padding-top: 5px; padding-bottom: 5px">
                    <a href="{{$cover->cover_src}}"><img src="{{$cover->cover_src}}" style="width: 100%" /></a>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
