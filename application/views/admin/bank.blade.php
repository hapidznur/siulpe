@extends('admin.template')

 @section('title', 'Admin | Bank')

 @section('page-title', 'Bank')

 @section('body')
   <div class="col-md-12 col-sm-12 col-xs-12">
     <div class="x_panel">
       <div class="x_title">
         <h2>Bank</h2>
         <div class="clearfix"></div>
       </div>
       <div style="display: block;" class="x_content">
         <br>
         @if($controller->session->errors)
           <div class="col-md-offset-3 col-md-7">
             <div class="alert alert-danger">
               <b>Messages</b>
               <ul>
                 @foreach($controller->session->errors as $key => $errors)
                   @foreach($errors as $key => $error)
                     <li>{{$error}}</li>
                   @endforeach
                 @endforeach
               </ul>
             </div>
           </div>
         @endif
         <form id="form-new-master-trip" action="{{url("admin/bank-post")}}" method="post" class="form-horizontal form-label-left">
           <div class="form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Bank</label>
             <div class="col-md-8 col-sm-8 col-xs-12">
               <input name="bank" class="form-control col-md-8 col-xs-12" type="text" value="{{$identitasWeb->bank}}">
             </div>
           </div>
           <div class="form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Atas Nama</label>
             <div class="col-md-8 col-sm-8 col-xs-12">
               <input name="atas_nama" class="form-control col-md-8 col-xs-12" type="text" value="{{$identitasWeb->atas_nama}}">
             </div>
           </div>
           <div class="form-group">
             <label class="control-label col-md-2 col-sm-2 col-xs-12" for="first-name">Nomor Rekening</label>
             <div class="col-md-8 col-sm-8 col-xs-12">
               <input name="no_rekening" class="form-control col-md-8 col-xs-12" type="text" value="{{$identitasWeb->no_rekening}}">
             </div>
           </div>
           <div class="ln_solid"></div>
           <div class="form-group">
             <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
               <div class="btn-group">
                 <a href="{{base_url('admin/contact')}}" type="submit" class="btn btn-primary">Cancel</a>
                 <button type="submit" class="btn btn-success">Submit</button>
               </div>
             </div>
           </div>
         </form>
       </div>
     </div>
   </div>
 @endsection
