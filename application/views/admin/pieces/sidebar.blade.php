<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="{{base_url('admin')}}" class="site_title"><i class="fa fa-home"></i> <span>Compas Petualang</span></a>
    </div>

    <div class="clearfix"></div>

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <ul class="nav side-menu">
          <li>
            <a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{base_url('admin')}}">Dashboard</a>
              </li>
            </ul>
          </li>
          <li>
            <a><i class="fa fa-plane"></i> Master Trip <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{base_url('admin/new-master-trip')}}">Master Trip Baru</a>
              </li>
              <li><a href="{{base_url('admin/master-trip')}}">Data Master Trip</a>
              </li>
              <li><a href="{{base_url('admin/kategori')}}">Master Kategori</a>
              </li>
              <li><a href="{{base_url('admin/destinations')}}">Master Destinasi</a>
              </li>
            </ul>
          </li>
          <li><a><i class="fa fa-car"></i> Open Trip <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{base_url('admin/new-open-trip')}}">Open Trip Baru</a>
              </li>
              <li><a href="{{base_url('admin/open-trip')}}">Data Open Trip</a>
              </li>
              <li><a href="{{base_url('admin/top-destinations')}}">Top Destinations</a>
              </li>
            </ul>
          </li>
          <li><a><i class="fa fa-book"></i> Booking <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{base_url('admin/booking-by-admin')}}">Order by Admin</a></li>
              <li><a href="{{base_url('admin/booking')}}">Data Booking Masuk</a></li>
              <li><a href="{{base_url('admin/data-konfirmasi-pembayaran')}}">Konfirmasi Pembayaran</a></li>
            </ul>
          </li>
          <li>
            <a>
              <i class="fa fa-balance-scale"></i> Kas Masuk <span class="fa fa-chevron-down"></span>
            </a>
            <ul class="nav child_menu">
              <li><a href="{{url('admin/kas-masuk')}}">Data Kas Masuk</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-globe"></i> Website <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{base_url('admin/bank')}}">Bank</a></li>
              <li><a href="{{base_url('admin/blog')}}">Blog</a></li>
              <li><a href="{{base_url('admin/contact')}}">Contact</a></li>
              <li><a href="{{base_url('admin/gallery')}}">Gallery</a></li>
              <li><a href="{{base_url('admin/media-sosial')}}">Media Sosial</a></li>
              <li><a href="{{base_url('admin/identitas')}}">Identitas Website</a></li>
              <li><a href="{{base_url('admin/subscribe')}}">Subscribe</a></li>
              <li><a href="{{base_url('admin/syarat-dan-ketentuan')}}">Syarat dan Ketentuan</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-map"></i> About us <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{base_url('admin/about-us')}}">About us</a></li>
              <li><a href="{{base_url('admin/contact-us')}}">Contact us</a></li>
              <li><a href="{{base_url('admin/legalitas')}}">Legalitas</a></li>
              <li><a href="{{base_url('admin/faq')}}">FAQ</a></li>
              <li><a href="{{base_url('admin/karir')}}">Karir</a></li>
              <li><a href="{{base_url('admin/afiliasi')}}">Afiliasi</a></li>
            </ul>
          </li>
          <li>
            <a><i class="fa fa-buysellads"></i> Ads <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{url('admin/ads-create')}}">New Ads</a></li>
              <li><a href="{{url('admin/ads')}}">Data Ads</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
    <!-- /sidebar menu -->
  </div>
</div>
