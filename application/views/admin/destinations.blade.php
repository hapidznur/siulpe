@extends('admin.template')

@section('title', 'Admin | Master Destinasi')

@section('page-title', 'Destinasi')

@section('body')
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_content">
        @if($controller->session->message)
          <div class="alert alert-success">
            {{$controller->session->message}}
          </div>
        @endif
        <div class="text-left">
          <a class="btn btn-primary" href="{{url('admin/destinasi-baru')}}"><span class="fa fa-plus"></span> Destinasi Baru</a>
        </div>
        <table class="table table-hovered table-striped table-stripped data-table">
          <thead>
            <tr>
              <th>No</th>
              <th>Provinsi</th>
              <th>Kota</th>
              <th>Tujuan</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach($destinations as $key => $destinasi)
              <tr destinasi-id="{{$destinasi->id}}">
                <td>
                  {{$key + 1}}
                </td>
                <td>
                  {{$destinasi->provinsi}}
                </td>
                <td>
                  {{$destinasi->kota}}
                </td>
                <td>
                  {{$destinasi->nama}}
                </td>
                <td>
                  <div class="btn-group">
                    <a href="{{url("admin/destinasi-edit/{$destinasi->id}")}}" class="btn btn-default">Edit</a>
                    <button class="btn btn-danger btn-delete">Hapus</button>
                  </div>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@section('modal')
  <div id="modal-delete" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Hapus Destinasi</h4>
        </div>
        <div class="modal-body">
          <div class="alert alert-default">
            Apakah anda yakin akan menghapus destinasi ini? Jika anda menghapus, maka open trip yang bersangkutan dengan fasilitas ini data fasilitasnya akan hilang.
          </div>
          <form style="display: none" id="destinasi-delete-form" action="{{url("admin/destinasi-delete")}}" method="post">
            <input type="hidden" name="id" value="">
          </form>
        </div>
        <div class="modal-footer">
          <div class="form-group">
            <div class="btn-group" style="float: right">
              <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
              <button form="destinasi-delete-form" type="submit" class="btn btn-danger">Delete</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{url("assets/js/admin/destinations.js")}}" charset="utf-8"></script>
@endsection
