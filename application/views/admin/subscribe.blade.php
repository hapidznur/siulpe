@extends('admin.template')

@section('title', 'Admin | Subscribe')

@section('page-title', 'Subscribe')

@section('body')
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_content">
        <table class="table table-hovered table-striped table-stripped data-table">
          <thead>
            <tr>
              <th>#</th>
              <th>Email</th>
              <th>Tanggal</th>
              <!-- <th>Aksi</th> -->
            </tr>
          </thead>
          <tbody>
            @foreach($subscribe as $key => $subscribes)
              <tr article-id="{{$subscribes->id}}">
                <td> </td>
                <td>{{$subscribes->email}}</td>
                <td>{{$subscribes->created_at}}</td>
               <!--  <td>
                  <div class="btn-group">
                    <button class="btn btn-danger btn-delete" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>
                  </div>
                </td> -->
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@section('modal')
  <div id="modal-delete" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Subscribe</h4>
        </div>
        <div class="modal-body">
          Apakah anda yakin akan menghapus data ini?
          <small>data yang sudah dihapus tidak bisa dikembalikan lagi.</small>
          <form style="display: none" id="article-delete-form" action="{{url("admin/subscribe_delete")}}" method="post">
            <input type="hidden" name="article_id" value="">
          </form>
        </div>
        <div class="modal-footer">
          <div class="form-group">
            <div class="btn-group" style="float: right">
              <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
              <button form="article-delete-form" type="submit" class="btn btn-danger">Delete</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script type="text/javascript">
  var modal = $('#modal-delete')

  $('body').on('click', '.btn-delete', function () {
    var id = $(this).parent().parent().parent().attr('article-id');
    modal.find('input').val(id)
    modal.modal('show');
  })
</script>
@endsection
