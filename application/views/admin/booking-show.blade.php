@extends('admin.template')

@section('title')
  Admin | Detail Booking
@endsection

@section('page-title', 'Booking')

@section('body')
  <div iki-id-booking-e-rek="{{$booking->id}}">
  </div>
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_title">
        <div class="row">
          <div class="col-md-6">
            <h4>{{$booking->openTrip->masterTrip->title}}</h4>
          </div>
        </div>
      </div>
      <div style="position: absolute; right: 20px; top: 12px">
        <div class="btn-group">
          <a href="{{url("admin/booking-edit/{$booking->id}")}}" class="btn btn-default" data-toggle="tooltip" title="Edit Booking"><span class="fa fa-edit"></span></a>
          <button id="btn-delete-open-trip" href="{{url('')}}" class="btn btn-danger" data-toggle="tooltip" title="Hapus Booking"><span class="fa fa-trash"></span></button>
        </div>
      </div>
      <div class="x_content">
        <div class="col-md-4">
          <div class="form-group">
            <label>Kode Open Trip</label>
            <p>
              {{$booking->openTrip->kode}}
            </p>
          </div>
          <div class="form-group">
            <label>Destinasi</label>
            <p>
              {{$booking->openTrip->masterTrip->destination}}
            </p>
          </div>
          <div class="form-group">
            <label>Tanggal Berangkat</label>
            <p>
              {{$booking->openTrip->tanggal_berangkat}}
            </p>
          </div>
          <div class="form-group">
            <label>Kuota</label>
            <p>
              {{$booking->openTrip->quota_masuk}}/{{$booking->openTrip->quota}}
            </p>
          </div>
          <div class="form-group">
            <label>Harga</label>
            <p>
              {{$booking->openTrip->price}}
            </p>
          </div>
          <div class="form-group">
            <label>Total Harga</label>
            <p>
              IDR. {{number_format($booking->openTrip->harga * $booking->jumlah_peserta)}}
            </p>
          </div>
          <div class="form-group">
            <label>DP Minimum</label>
            <p>
              IDR. {{number_format($booking->openTrip->dp_minimum)}}
            </p>
          </div>
          <div class="form-group">
            <label>Status Pembayaran</label>
            <p>
              {{$booking->status_pembayaran}}
            </p>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Nomor Invoice</label>
            <p>
              {{$booking->no_invoice}}
            </p>
          </div>
          <div class="form-group">
            <label>Nama Pemesan</label>
            <p>
              {{$booking->nama}}
            </p>
          </div>
          <div class="form-group">
            <label>Email Pemesan</label>
            <p>
              {{$booking->email}}
            </p>
          </div>
          <div class="form-group">
            <label>No HP</label>
            <p>
              {{$booking->no_tlp}}
            </p>
          </div>
          <div class="form-group">
            <label>Tanggal Booking</label>
            <p>
              {{$booking->tanggal_booking}}
            </p>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Alamat Pemesan</label>
            <p>
              {{$booking->alamat}}
            </p>
          </div>
          <div class="form-group">
            <label>Jumlah Peserta</label>
            <p>
              {{$booking->jumlah_peserta}}
            </p>
          </div>
          <div class="form-group">
            <label>Nama Peserta</label>
            <p>
              {{$booking->nama_peserta_src}}
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_title">
        <h4>Daftar Pembayaran</h4>
      </div>
      <div class="x_content">
        <table class="table table-striped table-hover data-table">
          <thead>
            <tr>
              <th>
                No
              </th>
              <th>
                Nama
              </th>
              <th>
                Bank
              </th>
              <th>
                No Rekening
              </th>
              <th>
                Jumlah Transfer
              </th>
              <th>
                Tanggal
              </th>
              <th>
                Status
              </th>
              <th>
                Aksi
              </th>
            </tr>
          </thead>
          <tbody>
            @if($booking->invoice)
              @foreach($booking->invoice->confirmations as $key => $confirmation)
                <tr confirmation-id="{{$confirmation->id}}">
                  <td>
                    {{$key + 1}}
                  </td>
                  <td>
                    {{$confirmation->nama}}
                  </td>
                  <td>
                    {{$confirmation->bank}}
                  </td>
                  <td>
                    {{$confirmation->rekening_pengirim}}
                  </td>
                  <td>
                    {{$confirmation->total_transfer_str}}
                  </td>
                  <td>
                    {{$confirmation->tanggal_pembayaran}}
                  </td>
                  <td>
                    {{$confirmation->status}}
                  </td>
                  <td>
                    <div class="btn-group">
                      @if($confirmation->status !== 'sudah dikonfirmasi')
                      <button class="btn btn-success btn-check-pembayaran" data-toggle="tooltip" title="Konfirmasi"><span class="fa fa-check"></span></button>
                      @endif
                      <button class="btn btn-default btn-detail-pembayaran" data-toggle="tooltip" title="Lihat Detail"><span class="fa fa-eye"></span></button>
                      <button class="btn btn-danger btn-delete-pembayaran" data-toggle="tooltip" title="Hapus Data"><span class="fa fa-trash"></span></button>
                    </div>
                  </td>
                </tr>
              @endforeach
            @else
              <tr>
                <td colspan="8">
                  <div class="text-center">
                    Maaf, open trip ini masih belum pasti berangkat.
                  </div>
                </td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@section('modal')
  <div id="modal-delete" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Data Booking</h4>
        </div>
        <div class="modal-body">
          Apakah anda yakin akan menghapus data ini?
          <small>data yang sudah dihapus tidak bisa dikembalikan lagi.</small>
          <form style="display: none" id="booking-delete-form" action="{{url("admin/booking-delete-post")}}" method="post">
            <input type="hidden" name="booking_id" value="">
          </form>
        </div>
        <div class="modal-footer">
          <div class="form-group">
            <div class="btn-group" style="float: right">
              <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
              <button form="booking-delete-form" type="submit" class="btn btn-danger">Delete</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('admin.modal.konfirmasi-pembayaran')
  @include('admin.modal.detail-pembayaran')
  @include('admin.modal.delete-pembayaran')
@endsection

@section('script')
  <script src="{{url('assets/js/admin/booking-show.js')}}" charset="utf-8"></script>
  <script src="{{base_url('assets/js/admin/pembayaran.js')}}" charset="utf-8"></script>
@endsection
