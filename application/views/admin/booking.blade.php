@extends('admin.template')

@section('title', 'Admin | Booking')

@section('page-title', 'Booking')

@section('style')
<style type="text/css">
  #DataTables_Table_0_filter {
    margin-right: 100px
  }
</style>
@stop

@section('body')
  <div class="col-md-12">
    <div class="x_panel">
      <div class="x_content">
        <a href="?export=true" class="btn btn-success" id="export" style="position: absolute; right: 10px;z-index: 9999">Export</a>
        <table class="table table-hovered table-striped table-stripped data-table">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode</th>
              <th>Open Trip</th>
              <th>Nama Trip</th>
              <th>Nama</th>
              <th>No Telepon</th>
              <th>Kota</th>
              <th>Jumlah Orang</th>
              <th>Nama Peserta</th>
              <th>Status Pembayaran</th>
              <th>Tanggal</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach($bookings as $key => $booking)
              <tr id-booking="{{$booking->id}}">
                <td>{{$key + 1}}</td>
                <td>{{$booking->kode}}</td>
                <td>{{$booking->openTrip->kode}}</td>
                <td>{{$booking->openTrip->masterTrip->title}}</td>
                <td>{{$booking->nama}}</td>
                <td>{{$booking->no_tlp}}</td>
                <td>{{$booking->alamat}}</td>
                <td>{{$booking->jumlah_peserta}}</td>
                <td>{{$booking->nama_peserta_src}}</td>
                <td>{{$booking->status_pembayaran}}</td>
                <td>{{$booking->tanggal_booking}}</td>
                <td>
                  <div class="btn-group">
                    <a class="btn btn-default" href="{{url("admin/booking-edit/{$booking->id}")}}" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>
                    <button class="btn btn-default btn-lihat-detail" data-toggle="tooltip" title="Lihat Detail"><i class="fa fa-eye"></i></button>
                    <button class="btn btn-danger btn-delete" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>
                  </div>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@section('modal')
  <!-- Modal -->
<div id="booking-detail" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Booking</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-4">
            <div class="">
              <h2 id="nama-open-trip">Loading...</h2>
              <table>
                <tr>
                  <td style="padding-right: 30px">Kode Open Trip</td>
                  <td id="kode-open-trip">Loading...</td>
                </tr>
                <tr>
                  <td style="padding-right: 30px">Destinasi</td>
                  <td id="destination">Loading...</td>
                </tr>
                <tr>
                  <td style="padding-right: 30px">Tanggal berangkat</td>
                  <td id="tanggal-berangkat">Loading...</td>
                </tr>
                <tr>
                  <td style="padding-right: 30px">Kuota</td>
                  <td id="quota">Loading...</td>
                </tr>
                <tr>
                  <td style="padding-right: 30px">Harga</td>
                  <td id="harga">Loading...</td>
                </tr>
                {{-- <tr>
                  <td style="padding-right: 30px">Fasilitas</td>
                  <td id="fasilitas">Loading...</td>
                </tr> --}}
                {{-- <tr>
                  <td style="padding-right: 30px">Status</td>
                  <td id="status-open-trip">Loading...</td>
                </tr> --}}
              </table>
            </div>
          </div>
          <div class="col-md-5">
            <h2>Booking detail</h2>
            <table>
              <tr>
                <td style="padding-right: 30px">Kode Booking</td>
                <td id="kode-booking">Loading...</td>
              </tr>
              <tr>
                <td style="padding-right: 30px">Nama Pemesan</td>
                <td id="nama-pemesan">Loading...</td>
              </tr>
              <tr>
                <td style="padding-right: 30px">Email Pemesan</td>
                <td id="email-pemesan">Loading...</td>
              </tr>
              <tr>
                <td style="padding-right: 30px">No HP</td>
                <td id="no-hp">Loading...</td>
              </tr>
              <tr>
                <td style="padding-right: 30px">Alamat</td>
                <td id="alamat">Loading...</td>
              </tr>
              <tr>
                <td style="padding-right: 30px">Tanggal Booking</td>
                <td id="tanggal-booking">Loading...</td>
              </tr>
            </table>
          </div>
          <div class="col-md-3">
            <a id="link-new-tab" target="_blank" href="" class="btn btn-default"><span class="fa fa-external-link"></span> Buka di tab baru</a>
          </div>
          <div class="col-md-6">
            <h2>Detail Pembayaran</h2>
            <table>
              <tr>
                <td style="padding-right: 30px">
                  Invoice
                </td>
                <td id="invoice">
                  Loading..
                </td>
              </tr>
              <tr>
                <td style="padding-right: 30px">
                  Status
                </td>
                <td id="status-pembayaran">
                  Loading..
                </td>
              </tr>
            </table>
            <div style="margin-top: 20px">
              <button id="btn-lihat-detail-pembayaran" class="btn btn-default" name="button">Lihat Detail</button>
            </div>
          </div>
          <div class="col-md-12">
            <table id="table-pembayaran" class="table" style="display: none">
              <thead>
                <tr>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Bank</th>
                  <th>Rek Pengirim</th>
                  <th>Total Transfer</th>
                  <th>Status</th>
                  <th>Tanggal</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div id="modal-delete" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Data Booking</h4>
      </div>
      <div class="modal-body">
        Apakah anda yakin akan menghapus data ini?
        <small>data yang sudah dihapus tidak bisa dikembalikan lagi.</small>
        <form style="display: none" id="booking-delete-form" action="{{url("admin/booking-delete-post")}}" method="post">
          <input type="hidden" name="booking_id" value="">
        </form>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <div class="btn-group" style="float: right">
            <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
            <button form="booking-delete-form" type="submit" class="btn btn-danger">Delete</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
  <script src="{{base_url('assets/js/regional.js')}}" charset="utf-8"></script>
  <script src="{{base_url('assets/js/admin/booking.js')}}" charset="utf-8"></script>
@endsection
