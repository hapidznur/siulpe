<?php

function auth()
{
  return get_instance()->grauth;
}

function blade()
{
  return get_instance()->blade;
}

function url($url)
{
  return base_url($url);
}

function response_json($item)
{
  header("Content-Type: application/json");
  echo json_encode($item);
}

function str_rand($limit)
{
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $limit; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
  }
  return $randomString;
}

function date_reverse($string, $delimiter = '/', $new_delimiter = '-', $from_database = false)
{
  $date = explode($delimiter, $string);

  if (count($date) < 3) {
    return null;
  }
  # bulan-tanggal-tahun
  # tahun-bulan-tanggal
  # tanggal-tahun-bulan
  if (!$from_database) {
    return "{$date[2]}{$new_delimiter}{$date[0]}{$new_delimiter}{$date[1]}";
  }

  return "{$date[1]}{$new_delimiter}{$date[2]}{$new_delimiter}{$date[0]}";
}

function read_more($string, $limit = 100)
{
  $length = strlen(strip_tags($string));
  if ($length>$limit){
    return substr($string,0,$limit).'...';
  }
  else {
    return $string;
  }
}

function request_flash($request)
{
  $session = get_instance()->session;
  $session->set_flashdata('old', $request);
}


function old($key)
{
  $session = get_instance()->session;

  return isset($session->old[$key]) ? $session->old[$key] : '';
}

function seo($s) {
  $c = array (' ');
  $d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
  $s = str_replace($d, '', $s); // Hilangkan karakter yang telah disebutkan di array $d
  $s = strtolower(str_replace($c, '-', $s)); // Ganti spasi dengan tanda - dan ubah hurufnya menjadi kecil semua
  return $s;
}
