<?php
class Penjadwalan extends CI_controller
{
	
	function __construct()
	{
		parent::__construct();
	    blade()->sebarno('controller', $this);
        $profil = UsersORM::find($this->session->ppk);

	    $array = array('nama'=>$profil->nama, 'photo'=>$profil->photo);
	    blade()->shared('profil',$array);
	}


	public function read(){
	    $this->middleware->auth_ppk();

		$this->input->post('pengadaan');
		blade()->nggambar('penjadwalan.index');
	}

	public function datatables(){
		$data = array(
	array(
		'rowID' => '1',
		'proses' =>	 'Informasi Harga',
		'tanggal' => '19-09-2017'
		),
    array(
		'rowID' => '2',
    	'proses' =>  'Harga Barang',
		'tanggal' => '19-09-2017'
		),
    array(
		'rowID' => '3',
    	'proses' =>  'HPS dan Spesifikasi',
		'tanggal' => '19-09-2017'
		),
  //   array('proses' =>  'Undangan Pengadaan Langsung',
		// 'tanggal' => '19-09-2017'
		// ),
  //   array('proses' =>  'Surat Penawaran',
		// 'tanggal' => '19-09-2017'
		// ),
  //   array('proses' =>  'Pakta Integritas',
		// 'tanggal' => '19-09-2017',
		// 'active' => 'true'
		// ),
  //   array('proses' =>  'Formulir Isian Kualifikasi',
		// 'tanggal' => '19-09-2017'
		// ),
  //   array('proses' =>  'Evaluasi Penawaran dan Kualifikasi',
		// 'tanggal' => '19-09-2017'
		// ),
  //   array('proses' =>  'B.A Klarifikasi dan Negosiasi',
		// 'tanggal' => '19-09-2017'
		// ),
  //   array('proses' =>  'Surat Penetapan Penyedia',
		// 'tanggal' => '19-09-2017'
		// ),
  //   array('proses' =>  'Pengumuman Penyedia',
		// 'tanggal' => '19-09-2017'
		// ),
  //   array('proses' =>  'B.A Hasil Pengadaan Langsung',
		// 'tanggal' => '19-09-2017'
		// ),
  //   array('proses' =>  'Surat Perintah Kerja',
		// 'tanggal' => '19-09-2017'
		// ),
  //   array('proses' =>  'Surat Pesanan',
		// 'tanggal' => '19-09-2017'
  //   	),
  //   array('proses' =>  'Surat Jalan',
		// 'tanggal' => '19-09-2017'
  //   	),
  //   array('proses' =>  'B.A Penyelesaian Pekerjaan',
		// 'tanggal' => '19-09-2017'
  //   	),
  //   array('proses' =>  'B.A Pemeriksaan Pekerjaan',
		// 'tanggal' => '19-09-2017'
		// ),
  //   array('proses' =>  'B.A Serah Terima Pekerjaan',
		// 'tanggal' => '19-09-2017'
		// ),
  //   array('proses' =>  'B.A Pembayaran',
		// 'tanggal' => '19-09-2017'
		// ),
  //   array('proses' =>  'Ringkasan Kontrak',
		// 'tanggal' => '19-09-2017'
		// )
      );
		$json = array('draw' => true, 
					  'data' => $data
			);
		echo json_encode($json);
	}
}