<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staff extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }
  function index()
  {
    $this->load->view('staff_ppk/v_header_ppk');
    $this->load->view('staff_ppk/konten_ppk');
    $this->load->view('staff_ppk/v_footer_ppk');
  }
}