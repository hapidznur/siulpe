<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
*/
class Login extends CI_Controller
{
	function __construct()
	{
		parent:: __construct();
		$this->load->helper(array('form','url'));
		$this->load->database();
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('upload');

		blade()->sebarno('controller', $this);
	}

	public function index() {
		// $this->load->view('v_header');
		// $this->load->view('v_login');
		// $this->load->view('v_footer');
		// $this->session->sess_destroy();
	}

	public function register(){

		$this->load->model('PenyediaModel');

		$this->PenyediaModel->create($this->input->post());

		if (is_ajax_request()){
			if (PenyediaORM::where('email', $this->input->post('email'))->count() > 0) {
				return response_json([
					"username" => [
						"username already taken"
					]
				]);
			}
		}

		// $this->auth->confirmation();

		# Uncreated 
		// redirect('penyedia.dashborad');
		blade()->nggambar('auth.email-confirmation');
	}


	function validation()
	{
		$this->form_validation->set_rules('username', 'Username', 'required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'required|xss_clean');
		$this->form_validation->set_error_delimiters('<span class="error">', '</span>');

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$role = $this->input->post('role');

		if($this->form_validation->run()==FALSE)
		{
			$this->load->view('v_header');
			$this->load->view('v_login');
			$this->load->view('v_footer');
		} else {
			if ($role === 'Penyedia')
			{
				$check_penyedia = $this->m_rekanan->get_penyedia($username);
				if(empty($check_penyedia) == FALSE)
				{
					foreach ($check_penyedia as $key)
					{
						$set_session_penyedia['penyedia'] = $key['id_penyedia'];
						$set_session_penyedia['nama_penyedia'] = $key['nama_penyedia'];
						$set_session_penyedia['is_penyedia'] = TRUE;
						$this->session->set_userdata($set_session_penyedia);
					}
					redirect('rekanan/pengadaan');
				}
				else
				{
					echo "Penyedia belum terdaftar";
					var_dump($check_penyedia, $username);
				}
			} else if ($role === 'ulp') {
				$check_ulp = $this->m_ulp->get_ulp($username);
				if (empty($check_ulp)==FALSE) {
					foreach ($check_ulp as $key) {
						$set_session_ulp ['username'] = $key['username'];
						$set_session_ulp ['keycard'] = $key['keycard'];
						$set_session_ulp ['id_user'] = $key['id_user'];
						$set_session_ulp ['is_ulp'] = TRUE;
						$this->session->set_userdata($set_session_ulp);
					}
					redirect('ulp/index');
					//else for check_ppk < 0
				} else {
					echo "ulp belum terdaftar";
				}
			} elseif ($role === '3') {
				// echo "ppk";
				$check_ppk = $this->m_ppk->get_ppk($username);
				if (empty($check_ppk)==FALSE) {
					foreach ($check_ppk as $key) {
						$set_session_ppk ['username'] = $key['username'];
						$set_session_ppk ['keycard'] = $key['keycard'];
						$set_session_ppk ['id_user'] = $key['id_user'];
						$set_session_ppk ['is_ppk'] = TRUE;
						$this->session->set_userdata($set_session_ppk);
					}
					redirect('ppk/index');
					//else for check_ppk < 0
				} else {
					echo $check_ppk;
					echo "ppk belum terdaftar";
				}
			} else {
			echo $role;
			echo "Username yang anda gunakan belum ada pada database";
			}
		}
	}

	function checkEmailRekanan()
	{
		if (PenyediaORM::where('email', $this->input->post('email'))->count() > 0) {
			$email = PenyediaORM::where('email', $this->input->post('email'))->count();
			return response_json([
				"email" => [
					"email already taken"
				]
			]);
		} 
		else
		{
			return response_json([
				"email" => [
					"email already not taken"
				]
			]);
		}
	}

	// /*
	// 	@author elfarqy
	// 	digunakan untuk destroy session.
	//  */
	public function logout()
	{
		$this->session->sess_destroy();

		redirect('login/index');
	}
}
