<?php

/*
* This is generated file by aksamedia
* @author (hapidznur hapidznur@gmail.com)
*/

class Gallery extends CI_Controller {

	function __construct() {
		parent::__construct();

  	# this is where your code is
    $this->load->model('identitasWebModel');
    blade()->sebarno('controller', $this);
    blade()->sebarno('identitasWeb', $this->identitasWebModel->get());    #
	}

	public function index()
	{
		return redirect('gallery/instagram');
	}

	function instagram() {
		$token = $this->identitasWebModel->get()->instagram_access_token;
		$curl = new \Curl\Curl();
		$curl->get("https://api.instagram.com/v1/users/self/media/recent/?access_token={$token}");
		$images = $curl->response;
		// return response_json($images);
    blade()->nggambar('gallery.gallery', compact('images'));
	}

	public function flickr($albumId = false)
	{
		$api_key = '&api_key=' . getenv('FLICKR_KEY');
		$username = $this->identitasWebModel->get()->flickr;
		$method = "flickr.people.findByUsername";
		$base_url = 'https://api.flickr.com/services/rest/?method=';
		$last_url = $api_key . '&format=json&nojsoncallback=1';

		# get user id
		$url = $base_url . $method . '&username=' . $username . $last_url;
		$curl = new \Curl\Curl();
		$response = $curl->get($url);

		if (isset($response->user)) {
			$user_id = $response->user->nsid;
			# get list album
			$method = 'flickr.photosets.getList';
			$url = $base_url . $method . '&user_id=' . $user_id . $last_url;
			$response = $curl->get($url);

			# looping though the album to get the cover // cover is the first image
			$albums = [];
			foreach ($response->photosets->photoset as $key => $photoset) {
				$method = 'flickr.photosets.getPhotos';
				$url = $base_url . $method . '&user_id=' . $user_id . '&photoset_id=' . $photoset->id . $last_url;
				$album = [
					'id' => $photoset->id,
					'title' => $photoset->title->_content
				];

				$response = $curl->get($url);
				$photos = [];
				foreach ($response->photoset->photo as $key => $photo) {
					$photo = [
						'id' => $photo->id,
						'secret' => $photo->secret,
						'server' => $photo->server,
						'farm' => $photo->farm,
						'src'	=> "https://farm{$photo->farm}.staticflickr.com/{$photo->server}/{$photo->id}_{$photo->secret}.jpg",
						'src_big'	=> "https://farm{$photo->farm}.staticflickr.com/{$photo->server}/{$photo->id}_{$photo->secret}_b.jpg"
					];

					array_push($photos, $photo);
				}
				$album['cover'] = $photos[0]['src'];
				$album['photos'] = $photos;
				array_push($albums, $album);
			}
		}
		$albums = !isset($albums) || !$albums ? [] : $albums;
		$albums = json_decode(json_encode($albums));

		if (!$albumId) {
			return blade()->nggambar('gallery.flickr', compact('albums') + ['username' => $this->input->get('username')]);
		}
		else {
			$album = null;
			foreach ($albums as $key => $item) {
				if ($item->id == $this->input->get('photoset_id')) {
					$album = $item;
					break;
				}
			}

			return blade()->nggambar('gallery.flickr-album', compact('album'));
		}
	}

}
