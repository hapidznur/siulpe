<?php

/*
* This is generated file by aksamedia
*/

class Migrate extends CI_Controller {

	function __construct() {
		parent::__construct();
		# this is where your code is
	}

	public function index()
	{
		$this->load->library('migration');

		if (!$this->migration->current()) {
			show_error($this->migration->error_string());
			return;
		}

		echo "migrated";
	}

}
