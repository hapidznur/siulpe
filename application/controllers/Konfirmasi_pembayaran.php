<?php

use Gregwar\Captcha\CaptchaBuilder;

/*
* This is generated file by aksamedia
*/

class Konfirmasi_pembayaran extends CI_Controller {

	function __construct() {
		parent::__construct();

		blade()->sebarno('menuActive', 'konfirmasi-pembayaran');
		blade()->sebarno('controller', $this);
		$this->load->model('identitasWebModel');
 		blade()->sebarno('identitasWeb', $this->identitasWebModel->get());
	}

	public function index()
	{
		# build and load captcha
		$captcha = new CaptchaBuilder;
		$captcha->build();
		# store the Captcha code to the session
		$this->session->set_userdata('captcha', $captcha->getPhrase());

		blade()->nggambar('konfirmasi-pembayaran.index', compact('captcha'));
	}

	public function process()
	{
		$this->load->model('bookingModel');
		$this->load->model('invoiceModel');
		$validator = new \Valitron\Validator($this->input->post());
		$validator->rule('required', [
			'nomor_invoice', 'nama', 'email', 'bank','rekening_pengirim', 'total_transfer',
			'tanggal_pembayaran',
			'captcha'
		]);
		$validator->rule('email', 'email');
		$validator->rule('numeric', ['rekening_pengirim', 'total_transfer']);

		if (!$validator->validate()) {
			$this->session->set_flashdata('old', $this->input->post());
			$this->session->set_flashdata('errors', $validator->errors());
			return redirect('konfirmasi-pembayaran');
		}

		// /**
		// * cek google recaptcha
		// */
		// $curl = new \Curl\Curl();
		// $curl->post("https://www.google.com/recaptcha/api/siteverify", [
		// 	'secret' => getenv('GOOGLE_RECAPTCHA_SECRET'),
		// 	'response' => $this->input->post('g-recaptcha-response'),
		// ]);
		// if (!$curl->response->success) {
		// 	// jika salah
		// 	return response_json([
		// 		'errors' => [
		// 			'google_recaptcha' => ['im not sure you are a human.']
		// 		]
		// 	]);
		// }

		if ($this->input->post('captcha') !== $this->session->captcha) {
			$errors = [
				'captcha' => ['The captcha code is invalid']
			];

			request_flash($this->input->post());
			$this->session->set_flashdata('errors', $errors);

			return redirect("konfirmasi-pembayaran");
		}

		// cek apakah ada foto konfirmasi pembayaran
		if (!isset($_FILES['img_konfirmasi']['name'])) {
			$this->session->set_flashdata('old', $this->input->post());
			$this->session->set_flashdata('errors', ['img_konfirmasi' => ['Bukti pembayaran harus disertakan']]);
			return redirect('konfirmasi-pembayaran');
		}

		# jika lolos dari serentetan pengecekan di atas, maka selanjutnya
		# upload fotonya, lalu simpan datanya
		$this->load->library('imageupload');
		$this->imageupload->prepare([
			'upload_path'	=>	'assets/img/bukti-pembayaran',
			'allowed_types'	=>	'jpg|png',
			'max_size'	=> 5000
		]);

		if ($this->imageupload->do_upload('img_konfirmasi', false)) {
			$img_konfirmasi = $this->imageupload->data()['file_name'];
		}
		else {
			$errors = [
				'picture' => [$this->upload->display_errors()]
			];

			$this->session->set_flashdata('old', $this->input->post());
			$this->session->set_flashdata('errors', $errors);
			// return response_json(compact('errors'));
			return redirect('konfirmasi-pembayaran');
		}

		$confirmation = $this->invoiceModel->addConfirmation($img_konfirmasi, $this->input->post());

		if (!$confirmation) {
			$this->session->set_flashdata('old', $this->input->post());
			$this->session->set_flashdata('errors', ['invoice' => ['Nomor invoice yang anda masukkan tidak terdaftar']]);
			return redirect('konfirmasi-pembayaran');
		}

		# kirim email bahwa konfirmasi telah terkirim dan akan segera diproses


		$this->session->set_flashdata('konfirmasi-success', true);
		return redirect('konfirmasi-pembayaran/success');
	}

	public function success()
	{
		# cek apakah sessionnya masih ada?
		if (!$this->session->flashdata('konfirmasi-success')) {
			# jika tidak, maka redirect ke halmaan konfirmasi
			return redirect('konfirmasi-pembayaran');
		}
		blade()->nggambar('konfirmasi-pembayaran.success');
	}

}
