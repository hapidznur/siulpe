<?php

/*
* This is generated file by aksamedia
*/

class Help extends CI_Controller {

	function __construct() {
		parent::__construct();
		# this is where your code is
		$this->load->model('identitasWebModel');
		blade()->sebarno('controller', $this);
		blade()->sebarno('identitasWeb', $this->identitasWebModel->get());
	}

	public function syarat_dan_ketentuan()
	{
		blade()->nggambar('help.syarat-dan-ketentuan');
	}

}
