<?php

use Gregwar\Captcha\CaptchaBuilder;
/*
* This is generated file by aksamedia
*/

class Open_trip extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('openTripModel');
		blade()->sebarno('controller', $this);
		$this->load->model('identitasWebModel');
		$this->load->model('adsModel');

		$leftBarAds = $this->adsModel->getLeftBarAds();
		$bottomBodyAds = $this->adsModel->getBottomBody();

		blade()->sebarno('leftBarAds', $leftBarAds);
		blade()->sebarno('bottomBodyAds', $bottomBodyAds);
 		blade()->sebarno('identitasWeb', $this->identitasWebModel->get());
	}

	public function index() {
		$this->load->model('categoryModel');
		

		# btw, sorry ya paginationnya manual. Soalnya page ?page= di query stirngya. Nggak pake url-segment
		$perPage = 8;
		$page = $this->input->get('page');
		$page = $page ? $page : 1;
		$openTrips = $this->openTripModel->getPagination($this->input->get('page'), $perPage);
		$rows = $this->openTripModel->getNumberRowsOfSearch();
		$pageCount = ceil($rows / $perPage);
		$minimumPage = 1; # default
		$maximumPage = $pageCount; #default
		$rangePageToDisplay = 10;

		if ($page > floor($rangePageToDisplay / 2) && $maximumPage > $rangePageToDisplay) {
			$minimumPage = $page - floor($rangePageToDisplay / 2);
		}

		if ($maximumPage > $rangePageToDisplay) {
			$maximumPage = $minimumPage + $rangePageToDisplay - 1;
		}

		if ($maximumPage > $pageCount) {
			$maximumPage = $pageCount;
		}

		$monthActive = $this->input->get('month') ? $this->input->get('month') : date('m');
		$monthActive = (int) $monthActive;

		// return response_json(compact('monthActive'));

		// return response_json(compact('openTrips'));

		@$priceRange = explode(';', $this->input->get('price_range'));
		@$min = $priceRange[0];
		@$max = $priceRange[1];

		// echo json_encode($openTrips);
		// // return response_json(compact('openTrips'));
		// return;
		if ($this->input->get('pieces')) {
			return blade()->nggambar('open-trip.show.thumbnails-js', compact('openTrips'));
		}
		if ($this->input->is_ajax_request()) {
			return response_json(compact('openTrips'));
		}
		blade()->sebarno('menuActive', 'open-trip');
		blade()->nggambar('open-trip.index', compact('sortBy', 'view', 'openTrips', 'rows', 'min', 'max', 'page', 'pageCount', 'maximumPage', 'minimumPage', 'categoriesSelected', 'monthActive'));
	}

	public function show($id)
	{


		$openTrip = $this->openTripModel->getById($id);

		if ($this->input->is_ajax_request()) {
			return response_json(compact('openTrip'));
		}

		$this->load->model('categoryModel');
		$categories = $this->categoryModel->getAll();
		$categoriesSelected = [];
		$categoriesFromInput = $this->input->get('categories');
		if (!$categoriesFromInput) {
			$categoriesFromInput = [];
		}
		foreach ($categories as $key => $category) {
			$selected = false;

			foreach ($categoriesFromInput as $key => $fromInput) {
				if ($category->id == $fromInput) {
					$selected = true;
				}
			}

			array_push($categoriesSelected, $selected);
		}

		@$priceRange = explode(';', $this->input->get('price_range'));
		@$min = $priceRange[0];
		@$max = $priceRange[1];


		blade()->nggambar('open-trip.show', compact('openTrip', 'page', 'min', 'max', 'categories'));
	}

	public function book($id)
	{
		$openTrip = $this->openTripModel->getById($id);

		if (!$openTrip) {
			show_error(404);
		}

		if ($openTrip->status === 'ditutup') {
			show_error(404);
		}

		# cek apakah kuota masih ada
		$invoice = $this->openTripModel->getById($id);
		if ($invoice->sisa_quota <= 0) {
			redirect("open-trip/kuota-penuh");
		}

		# build and load captcha
		$captcha = new CaptchaBuilder;
		$captcha->build();
		# store the Captcha code to the session
		$this->session->set_userdata('captcha', $captcha->getPhrase());

		blade()->nggambar('open-trip.book', compact('openTrip', 'captcha'));
	}

	public function book_process($id)
	{
		$this->load->model('bookingModel');
		$validator = new \Valitron\Validator($this->input->post());
		$validator->rule('required', [
			'nama', 'no_tlp', 'email', 'jumlah_peserta', 'alamat', 'nama_peserta', 'syarat_dan_ketentuan', 'captcha'
		]);
		$validator->rule('email', 'email');
		$validator->rule('array', 'nama_peserta');
		$validator->rule('numeric', ['no_ktp', 'jumlah_peserta']);

		if (!$validator->validate()) {
			request_flash($this->input->post());
			$this->session->set_flashdata('errors', $validator->errors());
			return redirect("open-trip/book/{$id}");
		}

		// /**
		// * cek google recaptcha
		// */
		// $curl = new \Curl\Curl();
		// $curl->post("https://www.google.com/recaptcha/api/siteverify", [
		// 	'secret' => getenv('GOOGLE_RECAPTCHA_SECRET'),
		// 	'response' => $this->input->post('g-recaptcha-response'),
		// ]);
		//
		//
		// if (!$curl->response->success) {
		// 	# jika salah
		// 	$errors = [
		// 		'google_recaptcha' => ['Google recaptcha is expired. Check it again.']
		// 	];
		//
		// 	$this->session->set_flashdata('errors', $errors);
		//
		// 	return redirect("open-trip/book/{$id}");
		// }

		if ($this->input->post('captcha') !== $this->session->captcha) {
			$errors = [
				'captcha' => ['The captcha code is invalid']
			];
			request_flash($this->input->post());
			$this->session->set_flashdata('errors', $errors);

			return redirect("open-trip/book/{$id}");
		}

		# cek apakah kuota masih ada
		$openTrip = $this->openTripModel->getById($id);
		$jumlah_peserta = $this->input->post('jumlah_peserta');
		$sisaKuota = $openTrip->sisa_quota;
		if ($jumlah_peserta > $sisaKuota) {
			$errors = [
				'jumlah_peserta' => ['Jumlah peserta melebihi sisa kuota']
			];
			request_flash($this->input->post());
			$this->session->set_flashdata('errors', $errors);

			return redirect("open-trip/book/{$id}");
		}

		# cek apakah statusnya belum tertutup
		if ($openTrip->is_closed) {
			$errors = [
				'status' => ['Status Open Trip sudah tertutup']
			];
			request_flash($this->input->post());
			$this->session->set_flashdata('errors', $errors);

			return redirect("open-trip/book/{$id}");
		}

		$booking = $this->bookingModel->insert($id, $this->input->post());

		$this->session->set_flashdata('message', 'Your booking request already sent');
		// return redirect("open-trip/book/{$id}");
		$invoice = $booking->invoice;
		$this->session->set_flashdata('data_invoice', compact('booking', 'invoice'));
		return redirect('open-trip/invoice');
	}

	public function invoice()
	{
		if (!$this->session->data_invoice) {
			show_error(404);
		}

		$booking = $this->session->data_invoice['booking'];
		$invoice = $this->session->data_invoice['invoice'];
		return blade()->nggambar('emails.invoice', compact('booking', 'invoice'));
	}

	public function kuota_penuh()
	{
		return blade()->nggambar('open-trip.kuota-penuh');
	}

}
