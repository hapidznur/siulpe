<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * class ini adalah controller dari role rekanan
 *
 * @author  hapidznur
*/

class Penyedia extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        // $this->is_login();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('cookie');
        // $this->load->model('m_barang');
        // $this->load->model('m_rekanan');
        // $this->load->model('m_kalender');
        // $this->load->model('m_proses');
        // $this->load->model('m_dtl_penawaran');
        // $this->load->model('m_undangan');
        // $this->load->model('m_penawaran');
        // $this->load->model('m_pengadaan');
    }

    public function is_login(){
        if ($this->session->userdata('is_penyedia') == FALSE) {
            echo "session not set";
            var_dump($this->session->all_userdata());
            echo " <script>
            alert('Ndak berhak ngakses :P');history.go(-1);</script>";
            redirect('login/index');
        }
        else
        {
            $session_penyedia = $this->session->userdata('is_penyedia');
            return($session_penyedia);
        }
    }

    function index(){
        var_dump($this->session->userdata());
    }

    /**
     * Header Rekanan
     */
    function header_rekanan($title) {
        if ($this->session->userdata('is_penyedia') == TRUE) {

            $user_id = $this->session->userdata('penyedia');
            $count_undangan = $this->m_undangan->get_undangan($user_id)->num_rows();
//            $penyedia = $this->m_rekanan->get_profil($user_id)->row();
            $data['photo']= base_url().'tmp/profil.png';
            $data['name'] = $this->session->userdata('nama_penyedia');;
            $data['badges'] = $count_undangan;
            $data['title'] = $title;
            $this->load->view('rekanan/v_header',$data);
        }
    }

    /**
     * List Pengadaan Rekanan
     */
    function pengadaan(){
        $title = 'Pengadaan Barang atau Jasa';
        $this->header_rekanan($title);

        // $config['base_url'] = base_url().'rekanan/pengadaan';
        // $config['total_rows'] = count($this->m_pengadaan->jumlah_pengadaan());
        // $config['per_page'] = 5;
        // $data['artikel'] = $this->m_pengadaan->pengadaan_rekanan($config['per_page']);
        // $this->pagination->initialize($config);
        // $data['halaman'] = $this->pagination->create_links();
        $this->load->view('rekanan/v_pengadaan',$data);
        $this->load->view('rekanan/v_footer');
    }

    /**
     * [detil_pengadaan description]
     * @return [type] [description]
     * @author hapidznur <[hapidznur@gmail.com]>
     */
    function detail_barang($id_pengadaan){
        $this->header_rekanan('Detail Pengadaan');
        $config['base_url'] = base_url().'rekanan/detail_pengadaan';
        $config['total_rows'] = count($this->m_pengadaan->detail_barang_pengadaan($id_pengadaan));
        $config['per_page'] = 5;
        $data['artikel'] = $this->m_pengadaan->lihat_barang($id_pengadaan,$config['per_page']);
        $this->pagination->initialize($config);
        $data['halaman'] = $this->pagination->create_links();

        $data['paket_name'] = $this->input->post('paket_name');
        $data['lokasi'] = $this->input->post('lokasi');
        $data['no_pengadaan'] = $this->input->post('no_pengadaan');
        $data['date'] = $this->input->post('date');
        $data['anggaran'] = $this->input->post('anggaran');

        $data['pengadaan'] = $this->m_pengadaan->get_pengadaan($id_pengadaan);
        $this->load->view('pengadaan/v_detail_barang', $data);
        $this->load->view('rekanan/v_script');
        $this->load->view('rekanan/v_footer');
    }


//    /**
//     * [detil_pengadaan description]
//     * @return [type] [description]
//     * @author hapidznur <[hapidznur@gmail.com]>
//     */
//    function detail_pengadaan($id_pengadaan){
//        $this->header_rekanan('Detail Pengadaan');
//        $config['base_url'] = base_url().'rekanan/detail_pengadaan';
//        $config['total_rows'] = count($this->m_pengadaan->detail_barang_pengadaan($id_pengadaan));
//        $config['per_page'] = 5;
//        $data['artikel'] = $this->m_pengadaan->lihat_barang($id_pengadaan,$config['per_page']);
//        $this->pagination->initialize($config);
//        $data['halaman'] = $this->pagination->create_links();
//
//        $data['paket_name'] = $this->input->post('paket_name');
//        $data['lokasi'] = $this->input->post('lokasi');
//        $data['no_pengadaan'] = $this->input->post('no_pengadaan');
//        $data['date'] = $this->input->post('date');
//        $data['anggaran'] = $this->input->post('anggaran');
//
//        $this->load->view('pengadaan/v_detail_pengadaan', $data);
//        $this->load->view('rekanan/v_script');
//        $this->load->view('rekanan/v_footer');
//    }

    /**
     * [detil_penawaran description]
     * @return [type] [description]
     * @author hapidznur <[<email address>]>
     */
    function detail_penawaran(){
        $id_pengadaan = $this->input->get('pengadaan');
        $this->header_rekanan('Submit Informasi Harga');
        if (empty($id_pengadaan) ==  FALSE)
        {
            $pengadaan = array('id_pengadaan'=>$id_pengadaan);
            $this->session->set_userdata($pengadaan);
            $data['barang']=$this->m_barang->get_barang($id_pengadaan);

            $this->load->view('rekanan/v_detil_penawaran', $data);
            $this->load->view('rekanan/v_footer');
        }
        else
        {
            redirect('rekanan/pengadaan');
        }
    }

    /**
     * [penawaran description]
     * @return  void
     * @author  hapidznur
     */
    function penawaran(){
        $this->header_rekanan('Informasi Harga Rekanan');
        $id_penyedia = $this->session->userdata('penyedia');
        $uri = $this->uri->segment(2);
        $data['header'] = array('Pengadaan', 'Proses', 'Rincian');
        $data['pengadaan'] = $this->m_penawaran->penawaran_penyedia($id_penyedia);
        $this->load->view('rekanan/v_penawaran',$data);
        $this->load->view('rekanan/v_script');
        $this->load->view('rekanan/v_footer');
    }

    /**
     * [kalender description]
     * @return [type] [description]
     */
    function kalender(){
        $this->header_rekanan('Kalender Pengadaan');

        $id_pengadaan = $this->input->get('pengadaan');
        $status = $this->m_penawaran->get_rekanan_status($id_pengadaan)->rekanan_status;
        $jadwal = $this->m_kalender->get_kalender_rekanan($id_pengadaan)->result_array();
        $data['kalender'] = $jadwal;
        $today=strtotime(date("Y-m-d"));
        switch ($status) {
            case '2':
                $mulai = strtotime($jadwal[0]['p_'.$status.'_mulai']);
                $akhir = strtotime($jadwal[0]['p_'.$status.'_akhir']);
                $data['proses'] = $this->m_proses->get_proses_limit(2)->result_array();
                if (($today > $mulai) && ($today < $mulai)){
                    $data['aktif'] = 'class ="active"';
                }
                break;
            case '4':
                $mulai = strtotime($jadwal[0]['p_'.$status.'_mulai']);
                $akhir = strtotime($jadwal[0]['p_'.$status.'_akhir']);
                $data['proses'] = $this->m_proses->get_proses_limit(4)->result_array();
                if (($today > $mulai) && ($today < $mulai)){
                    $data['aktif'] = 'class ="active"';
                }
                break;
            case '10':
                $mulai = strtotime($jadwal[0]['p_'.$status.'_mulai']);
                $akhir = strtotime($jadwal[0]['p_'.$status.'_akhir']);
                $data['proses'] = $this->m_proses->get_proses_limit(10)->result_array();
                if (($today > $mulai) && ($today < $mulai)){
                    $data['aktif'] = 'class ="active"';
                }
                break;

            default:
                $data['proses'] = $this->m_proses->get_proses()->result_array();
                break;

       }

       $this->load->view('rekanan/v_detail_jadwal',$data);
       $this->load->view('rekanan/v_footer');
    }

    /**
     * updatepenawaran
     * @form_Input penawaran
     * @return void
     * @author hapidznur
     */
    function submit_harga(){
        $id_penyedia  = $this->session->userdata('penyedia');
        $id_pengadaan = $this->session->userdata('id_pengadaan');
        $paket_name = $this->m_pengadaan->get_name($id_pengadaan);
        $lampiran = "";

        $folder_pengadaan = './docs/'.$paket_name->paket_name;
        #check folder doesnot exist
        if (!file_exists($folder_pengadaan)) {
            #create folder for pengadaan
            mkdir('./docs/'.$paket_name->paket_name, 0755);
        }

        #Upload config
        $config['upload_path'] = './docs/'.$paket_name->paket_name;
        $config['allowed_types'] = 'doc|docx';
        $config['max_size'] = '200';
        $config['remove_spaces'] = TRUE;
        $config['xss_clean'] = TRUE;
        $this->load->library('upload', $config);

        $this->form_validation->set_rules('harga_tawar[]', 'Silahkan Isi harga barang', 'trim|Harus Diisi');

        if (empty($_FILES['suratinformasi'])){
            $this->form_validation->set_rules('suratinformasi', 'Document', 'required|xss_clean');
        }

        if ($this->form_validation->run() == FALSE){
            echo validation_errors();
            // echo $_FILES['suratinformasi'];
        }else{
            $random = date("i");
            $pengadaan_ex = str_split($id_pengadaan);
            $penyedia_ex = str_split($id_penyedia);

           #id_pengadaan(6 digit pertama) + id_penyedia(3 digit)}
            $id_penawaran = $pengadaan_ex[0].$pengadaan_ex[1].$pengadaan_ex[2].$pengadaan_ex[3].$pengadaan_ex[4].$id_penyedia;

            if ( ! $this->upload->do_upload('suratinformasi')) {
                $error = array('error' => $this->upload->display_errors());
                var_dump($error);
            }
            else {
                $lampiran = $this->upload->file_name;
                // var_dump($lampiran);
            }

            foreach ($this->input->post('detailed') as $val) :
                if ($this->input->cookie($val) != null) {
                    $spesifikasi[] =  $this->input->cookie($val);
                }
                else {
                    $spesifikasi[] = "empty";
                }
                $id_barang[] = $val;
                delete_cookie($val);
            endforeach;

            foreach ($this->input->post('harga_tawar') as $val ) :
                $harga_tawar[] = $val;
            endforeach;

            foreach ($this->input->post('jumlah') as $val ) :
                $jumlah_tawaran[] = $val;
            endforeach;

            $length=count($id_barang);

            $ins_penawaran=array(
            'id_penawaran'=>$id_penawaran,
            'id_penyedia'=>$id_penyedia,
            'id_pengadaan'=>$id_pengadaan,
            'total_harga' => $_POST['jumlahtotal'],
            'surat_informasi_harga'=>$lampiran,
            'rekanan_status' => '2',
            'verifikasi' => 'Tidak');
            // var_dump($ins_penawaran);
            $this->m_penawaran->insert_penawaran($ins_penawaran);

            $tawar = array();

            for ($i=0; $i < $length ; $i++)
            {
                $tawar[] = array(
                    'id_barang'=>$id_barang[$i],
                    'id_penawaran'=>$id_penawaran,
                    'spesifikasi_rekanan'=>$spesifikasi[$i],
                    'harga_tawar'=>$harga_tawar[$i],
                    'jumlah_tawaran'=>$jumlah_tawaran[$i]);
            }
            $this->db->insert_batch('detail_penawaran',$tawar);
        }
        $this->session->unset_userdata('id_pengadaan');
        redirect(base_url('rekanan/pengadaan'));
    }   

    /**
     *@author hapidznur
    */
    function undangan(){
        $this->header_rekanan('Undangan');
        $id_penyedia = $this->session->userdata('penyedia');

        $allowed = array('doc', 'docx');

        if(isset($_FILES['suratpenawaran']) && $_FILES['suratpenawaran']['error'] == 0){

            $extension = pathinfo($_FILES['suratpenawaran']['name'], PATHINFO_EXTENSION);

            if(!in_array(strtolower($extension), $allowed)){
                echo '{"status":"error"}';
                exit;
            }

            if(move_uploaded_file($_FILES['suratpenawaran']['tmp_name'], 'docs/'.$_FILES['suratpenawaran']['name'])){
                echo '{"status":"success"}';
                exit;
            }

            // echo '{"status":"error"}';
           // exit;
        } else {
            $data['undangan']=$this->m_undangan->get_undangan($id_penyedia)->result_array();
            $this->load->view('rekanan/v_undangan',$data);
        }
        $this->load->view('rekanan/v_script');
        $this->load->view('rekanan/v_footer');
    }

    /**
     * [spesifikasi edit_spesifikasi]
     * @return void
     */
    public function spesifikasi(){
        $pengadaan  = $_GET['pengadaan'];
        $barang = $_GET['barang'];
        $result['spesifikasi'] = $this->m_barang->get_spesifikasi($pengadaan,$barang);
        $result['barang'] = $barang;
        $this->load->view('rekanan/v_mini_header');
        $this->load->view('rekanan/v_spesifikasi', $result);
        $this->load->view('rekanan/v_footer');
    }

    /**
     * [updatenpwp description]
     * @return [type] [description]
     * @author hapidznur <[<email address>]>
     */
    public function updatenpwp(){
        $id_penyedia = $_POST['penyedia'];

        if(!empty($_POST['npwp'])){
        $npwp = $_POST["npwp"];
        $data = array('npwp' => $npwp);
        $this->m_rekanan->update_kelengkapan($data, $id_penyedia);
        echo "sukses";
        }

        if (!empty($_POST['akta'])){
        $akta = $_POST["akta"];
        $data = array('akta_notaris' => $akta);
        $this->m_rekanan->update_kelengkapan($data, $id_penyedia);
        echo "sukses";
        }

        if (!empty($_POST['siup'])){
        $siup = $_POST["siup"];
        $data = array('siup' => $siup);
        $this->m_rekanan->update_kelengkapan($data, $id_penyedia);
        echo "sukses";
        }


        if (!empty($_POST['pkp'])){
        $pkp = $_POST["pkp"];
        $data = array('pkp' => $pkp);
        $this->m_rekanan->update_kelengkapan($data, $id_penyedia);
        echo "sukses";
        }
    }

    /**
     *@author hapidznur
     */
    function profil(){
        if ($this->is_login() == TRUE)
        {
            $this->header_rekanan('Profil');
            $this->session->userdata('penyedia');
            $penyedia = $this->m_rekanan->get_profil(1500);
            $data['rekanan'] = $penyedia->result();
            $this->load->view('rekanan/v_profil', $data);
            $this->load->view('rekanan/v_script');
            $this->load->view('rekanan/v_footer');
            // $coba = $this->m_rekanan->last_id('id_penawaran', 'penawaran');
        }
    }

    function download(){
        $this->header_rekanan('Informasi Harga Rekanan');
        $this->load->view('v_download');
        $this->load->view('rekanan/v_script');
        $this->load->view('rekanan/v_footer');
    }

    /**
     * [_push_file description]
     * @param  [type] $path [description]
     * @param  [type] $name [description]
     * @return [type]       [description]
     */
    function _push_file($path, $name) {
        $this->load->helper('download');
        // make sure it's a file before doing anything!
        if (is_file($path)) {
            // required for IE
            if (ini_get('zlib.output_compression')) {
                ini_set('zlib.output_compression', 'Off');
            }

            // get the file mime type using the file extension
            $this->load->helper('file');
            $mime = get_mime_by_extension($path);
            // Build the headers to push out the file properly.
            header('Pragma: public');     // required
            header('Expires: 0');         // no cache
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($path)) . ' GMT+7');
            header('Cache-Control: private', false);
            header('Content-Type: ' . $mime);  // Add the mime type from Code igniter.
            header('Content-Disposition: attachment; filename="' . basename($name) . '"');  // Add the file name
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: ' . filesize($path)); // provide file size
            header('Connection: close');
            readfile($path); // push it out
            exit();
        }
    }

    /**
     * [download_files description]
     * @return [type] [description]
     */
    public function download_files() {
        $filename = $this->uri->segment(2);
        if ($filename == 'pakta') {
            $file_path = 'docs/pakta_integritas.doc';
            var_dump(is_file($path));
            $this->_push_file($file_path, 'pakta_integritas');
        } elseif ($filename == 'form') {
            $file_path = 'docs/form_kualifikasi.doc';
            $this->_push_file($file_path, 'form_kualifikasi');
        }
    }

    /**
     * @return mixed
     */
    public function test()
    {
        $this->load->view('rekanan/v_test');
        $this->load->view('rekanan/v_footer');
    }

}

/* End of file rekanan.php */
/* Location: ./application/controllers/rekanan.php */