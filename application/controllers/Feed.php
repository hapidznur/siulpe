<?php

/*
* This is generated file by aksamedia
*/

class Feed extends CI_Controller {

	function __construct() {
		parent::__construct();
		# this is where your code is
	}

	function index() {
		$this->load->model('identitasWebModel');

		$about = $this->identitasWebModel->get();

		$this->load->library('aksarss', [
			'title' => $about->nama,
			'description' => $about->meta_deskripsi,
			'link' => 'http://compaspetualang.com'
		]);


		// params -> db, primary_key, title, slug, description, pub_date
		$this->aksarss->setDatabaseArticle('oyisam_article', 'id', 'title', 'seo_tag', 'article', 'created_at');
		$this->aksarss->setArticleLinkPattern('/blog/detail/{id}');

		$xml = $this->aksarss->generate();

		echo $xml->asXML();
	}

}

