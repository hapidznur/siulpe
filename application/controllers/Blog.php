<<<<<<< HEAD
<?php
class Blog extends CI_Controller{
   function __construct()
  {
    parent::__construct();
    $this->load->helper(array('form', 'url'));
  }

  function index()
  {
    $this->load->view('upload', array('error' => ' ' ));
  }

  function do_upload()
  {
    $config['upload_path'] = 'uploads/';
    $config['allowed_types'] = 'doc|docx|dot';
    $config['max_size'] = '100';
//    $config['max_width']  = '1024';
 //   $config['max_height']  = '768';

    $this->load->library('upload', $config);

    if ( ! $this->upload->do_upload())
    {
      $error = array('error' => $this->upload->display_errors());

      $this->load->view('upload', $error);
    }
    else
    {
      $data = array('upload_data' => $this->upload->data());

      $this->load->view('success', $data);
    }
  }

    /**
     * @return mixed
     */
    public function test()
    {
//        echo 'hai';
        return $this->load->view('blog/index');
    }

    /**
     * @return mixed
     */
    public function login()
    {
        return $this->load->view('blog/login');
    }

    /**
     * @return mixed
     */
    public function forgot()
    {
        $this->load->view('v_header');
        $this->load->view('v_forgot');
        $this->load->view('v_footer');

    }
}
?>
=======
<?php

/*
* This is generated file by aksamedia
*/

class Blog extends CI_Controller {


    function __construct() {
        parent::__construct();

        # this is where your code is
        $this->load->model('identitasWebModel');
        $this->load->model('adsModel');

        $leftBarAds = $this->adsModel->getLeftBarAds();
        $bottomBodyAds = $this->adsModel->getBottomBody();

        blade()->sebarno('leftBarAds', $leftBarAds);
        blade()->sebarno('bottomBodyAds', $bottomBodyAds);
        blade()->sebarno('controller', $this);
        blade()->sebarno('identitasWeb', $this->identitasWebModel->get());    #
    }

    function index() {
        $this->load->model('ArticleModel');
        $blog = $this->ArticleModel->getAll();
        blade()->nggambar('article.blog', compact('blog'));
    }

    function detail($id=''){
        $this->load->model('ArticleModel');
        $blog = $this->ArticleModel->getByid($id);
        $recommendedArticles = $this->ArticleModel->getRecommendedArticles($blog); 
        blade()->nggambar('article.detail-article', compact('blog', 'recommendedArticles'));
    }

    function admin($case = '', $id = ''){
        $this->middleware->authAdmin();
        $this->load->model('ArticleModel');
        switch ($case) {
            case 'create':
                if (!empty($id)) {
                    $article= ArticleORM::where('id',$id)->first();
                    blade()->nggambar('article.create-content', compact('article','id'));
                } else {
                    blade()->nggambar('article.create-content');
                }
                break;

            default:
                $blog = $this->ArticleModel->getAll();
                blade()->nggambar('article.list-content', compact('blog'));
                break;
        }
    }

    function post(){
        $this->middleware->authAdmin();
        $this->load->model('ArticleModel');
        $this->load->library('imageupload');
        $this->imageupload->prepare([
            'upload_path'   =>  'assets/img/covers',
            'allowed_types' =>  'jpg|png',
            'max_size'  => 5000
        ]);
        $errors = [];
        $covers ='';
        if ($this->imageupload->do_upload('cover', false)) {
            // array_push($covers, $this->imageupload->data()['file_name']);
             $covers = $this->imageupload->data()['file_name'];
        }
        else {
            array_push($errors, $this->upload->display_errors());
            # return the errors
            // return response_json(compact('errors'));
            $this->upload->display_errors();
        }
        if (!empty($this->input->post('id'))) {
            $judul = $this->input->post('judul');
            $seo_tag = strtolower(str_replace(' ','-', $judul));
            $content = $this->input->post('content');
            $this->ArticleModel->update($this->input->post('id'), $covers, $judul, $content, $seo_tag);
        } else {
            var_dump($this->input->post('id'));
            $userdata = UserORM::where('id_user', $this->session->userdata('currentAdmin'))->get(['nama']);
            foreach ($userdata as $key ) {
                $user = $key['nama'];
            }
            $judul = $this->input->post('judul');
            $seo_tag = strtolower(str_replace(' ','-', $judul));
            $content = $this->input->post('content');
            $this->ArticleModel->create($user, $covers, $judul, $content, $seo_tag);
        }

        // redirect('');
    }

    public function post_comment($id)
    {
        $this->load->model('ArticleModel');
        $validator = new \Valitron\Validator($this->input->post());
        $validator->rule('required', [
            'nama', 'email', 'comment', //'g-recaptcha-response',
        ]);
        $validator->rule('email', 'email');

        if (!$validator->validate()) {
            request_flash($this->input->post());
            $this->session->set_flashdata('errors', $validator->errors());
            return redirect("blog/detail/{$id}");
        }

        /**
        * cek google recaptcha
        */


        $curl = new \Curl\Curl();
        $curl->post("https://www.google.com/recaptcha/api/siteverify", [
            'secret' => getenv('GOOGLE_RECAPTCHA_SECRET'),
            'response' => $this->input->post('g-recaptcha-response'),
        ]);


        if (!$curl->response->success) {
            # jika salah
            $errors = [
                'google_recaptcha' => ['Google recaptcha is expired. Check it again.']
            ];

            request_flash($this->input->post());
            $this->session->set_flashdata('errors', $errors);

            return redirect("blog/detail/{$id}");
        }


        # jika lolos
        $this->ArticleModel->addComment($id, $this->input->post());

        return redirect("blog/detail/{$id}");
    }
}
>>>>>>> e3e00e6d0219bef2ffe6eeba9e7658af3af84129
