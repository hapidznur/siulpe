<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ppk extends CI_Controller {
	function __construct()
	{
		parent::__construct();
    blade()->sebarno('controller', $this);
    $profil = UsersORM::find($this->session->ppk);

    $array = array('nama'=>$profil->nama, 'photo'=>$profil->photo);
    blade()->shared('profil',$array);
  } 

  function create_ppk(){
    blade()->nggambar('auth.create-ppk');
  }


  /*
  @author elfarqy
  digunakan ketika user pertama kali login
   */
  function index()
  {

    blade()->nggambar('ppk.test');


    // var_dump($profil->nama);

    // $this->load->view('ppk/v_header_ppk');
    // $this->load->view('ppk/v_test');
    // $this->load->view('ppk/v_footer');
    // echo $this->session->ppk;
  }

  public function informasi_harga(){
    $this->middleware->auth_ppk();
    blade()->nggambar('ppk.informasi-harga');
  }

  function submit_informasi_harga()
  {

    $id_user = UsersORM::find($this->session->ppk);

    // $no_pengadaan = $this->input->post('no_pengadaan');
    // $paket_name = $this->input->post('paket_name');
    // $tanggal_surat = $this->input->post('tanggal_surat');
    // $tanggal_permintaan = $this->input->post('tanggal_permintaan');
    // $jam = $this->input->post('time');
    // $anggaran_detail = $this->input->post('anggaran_detail');
    // $tanggal_dipa = $this->input->post('tanggal_dipa');
    // $no_dipa = $this->input->post('no_dipa');
    // $mak = $this->input->post('mak');
    // $lokasi = $this->input->post('lokasi');
    // $ta_anggaran = $this->input->post('ta_anggaran');

    $this->load->library('spreadsheet');
    $inputFileName = '../public/docs/templates/Book1.xlsx';
    // $inputFileName = $_FILES['filexls']['tmp_name'];
    $barang = $this->spreadsheet->read($inputFileName);
    echo '<pre>';
      print_r($barang[0][0]);
    echo '</pre>';
    echo '<pre>';
    print_r($barang[1][0]);
    echo '</pre>';
    echo '<pre>';
    print_r($barang[2][0]);
    echo '</pre>';
    echo '<pre>';
    print_r($barang[3][0]);
    echo '</pre>';

    // $this->load->library('word');
    // $surat =  array('name' => 'aku',
    //               'date_due' => date('H:i:s'),
    //               'ppk_name' => 'ppk',
    //               'nip' => '123123 231231 12312',
    //               'date_letter' => date('d M Y'),
    //               'day' => date('d'),
    //               'hours' => date('H:i:s'),
    //               'number_letter' => 'sdfadsfasd'
    //                 );

    // $write = $this->word->create_informasi_harga($surat);
    
    // var_dump($this->input->post());
    #loop untuk mendapatkan data dari
    // foreach ($ppk_data as $key) {
    //   $day = date("d-m-y", strtotime($tanggal_surat));
    //   $hour_24 = date("i");
    //   $id_jadwal =implode(explode(":", $hour_24)).$key['id_user'].implode(explode("-", $day));
    //   $id_pengadaan = implode(explode("-", $day)).$key['id_user'].implode(explode(":", $hour_24));
    //   $id_barang = implode(explode("-", $day)).$key['id_user'];
    // }

    // $data_pengadaan['res']= array(
    //   'id_pengadaan' => $id_pengadaan,
    //   'id_barang' => $id_barang,
    //   'id_jadwal' => $id_jadwal,
    //   'no_pengadaan' => $no_pengadaan,
    //   'paket_name' => $paket_name,
    //   'tanggal_surat' => $tanggal_surat,
    //   'tanggal_permintaan' =>$tanggal_permintaan,
    //   'jam' => $jam,
    //   'anggaran_detail' => $anggaran_detail,
    //   'no_dipa' => $no_dipa,
    //   'tanggal_dipa' => $tanggal_dipa,
    //   'mak' => $mak,
    //   'satker' => $key['satker'],
    //   'ppk_name' => $key['ppk_name'],
    //   'nip' => $key['nip'],
    //   'lokasi' => $lokasi,
    //   'ta_anggaran' => $ta_anggaran,
    //   );

    // $datanya_pengadaan = array(
    //   'id_pengadaan' => $id_pengadaan,
    //   'id_ppk' => $key['id_ppk'],
    //   'no_pengadaan' => $no_pengadaan,
    //   'paket_name' => $paket_name,
    //   'lokasi' => $lokasi,
    //   'status_pengadaan' => 2,
    //   'tahun' => $ta_anggaran,
    //   'tanggal_surat' => $tanggal_surat,
    //   'tanggal_permintaan' =>$tanggal_permintaan,
    //   'tanggal_dipa' =>$tanggal_dipa,
    //   'jam' => $jam,
    //   'anggaran' => $anggaran_detail,
    //   'no_dipa' => $no_dipa,
    //   'mak' => $mak,
    //   );
    // // $this->m_pengadaan->insert_pengadaan($datanya_pengadaan);
    // // $this->m_pengadaan->insert_penjadwalan_id($id_jadwal, $id_pengadaan);


    // $j = -1;
    // for ($i=2; $i <= ($data->rowcount($sheet_index=0)); $i++){
    //   $j++;
    //   $nama_barang[$j]   = $data->val($i, 1);
    //   $spesifikasi[$j]    = $data->val($i, 2);
    //   $satuan[$j]  = $data->val($i, 3);
    //   $volume[$j]  = $data->val($i, 4);
    //   $harga[$j]  = $data->val($i, 5);
    //   $jumlah_harga[$j]  = $data->val($i, 6);
    // }
    // $data_excel = array(
    //   'id_pengadaan' => $id_pengadaan,
    //   'id_barang'=> $id_barang,
    //   'nama_barang' => $nama_barang,
    //   'spesifikasi' => $spesifikasi,
    //   'satuan'=>$satuan,
    //   'volume' =>$volume,
    //   'harga' => $harga,
    //   'jumlah_harga' => $jumlah_harga
    //   );
    // $this->m_barang->insert_barang($data_excel);
    // // $this->load->view('ppk/v_header_ppk');
    // $this->load->view('ppk/v_uploaded_pengadaan', $data_pengadaan);
    // $this->load->view('ppk/v_footer');

  }
  /*
  @author elfarqy
  digunakan untuk melakukan akses pada list pengadaan umum
   */
  //pagination here
  function Pengadaan()
  {

    $config['base_url'] = base_url().'ppk/pengadaan';
    $config['total_rows'] = count($this->m_pengadaan->jumlah_pengadaan());
    $config['per_page'] = 3;
    $data['artikel'] = $this->m_pengadaan->pengadaan_rekanan($config['per_page']);
    $this->pagination->initialize($config);
    $data['halaman'] = $this->pagination->create_links();
    $this->load->view('ppk/v_lihatpengadaantest', $data);
    $this->load->view('ppk/v_footer');
  }

  /*
  @author elfarqy
  digunakan untuk melakukan akses pada list pengadaan sesuai user.
   */
  function pengadaan_sendiri()
  {
    $id_user = $this->session->userdata('id_user');
    $config['base_url'] = base_url().'ppk/pengadaan_sendiri';
    $config['total_rows'] = count($this->m_pengadaan->lihat_pengadaan_sendiri($id_user));
    // var_dump($this->m_pengadaan->lihat_pengadaan_sendiri($id_user));
    $config['per_page'] = 5;
    $data['artikel'] = $this->m_pengadaan->lihat_pengadaan_sendiri_details($id_user,$config['per_page']);
    $this->pagination->initialize($config);
    $data['halaman'] = $this->pagination->create_links();
    $this->load->view('ppk/v_lihatpengadaan', $data);
    $this->load->view('ppk/v_footer');
  }


  /*
  @author elfarqy
  digunakan untuk melakukan akses pada detail list pengadaan
   */
  function detail_pengadaan()
  {
    $id_pengadaan = $this->input->post('id_pengadaan');
    $config = array();
    $config['base_url'] = base_url().'ppk/detail_pengadaan/'.$this->uri->segment(3);
    $config['total_rows'] = count($this->m_pengadaan->detail_barang_pengadaan($id_pengadaan));
    // echo $config['total_rows'];
    $config['per_page'] = 5;
    $data['artikel'] = $this->m_pengadaan->lihat_barang($id_pengadaan,$config['per_page']);
    $this->pagination->initialize($config);
    $data['halaman'] = $this->pagination->create_links();
    $id_pengadaan = $this->input->post('id_pengadaan');
    $data['detail_pengadaan'] = $this->m_pengadaan->edit_pengadaan_sendiri_details($id_pengadaan);
    // $this->load->view('ppk/v_header_ppk');
    $this->load->view('ppk/v_detail_pengadaan', $data);
    $this->load->view('ppk/v_footer');
  }

  /*
  @author elfarqy
  method ini digunakan untuk melakukan proses edit detail pengadaan
   */
  function edit_pengadaan()
  {
    $id_pengadaan = $this->input->post('id_pengadaan');
    $data['pengadaan_sendiri'] = $this->m_pengadaan->edit_pengadaan_sendiri_details($id_pengadaan);
    // print_r( $data['pengadaan_sendiri']);
    // $this->load->view('ppk/v_header_ppk');
    $this->load->view('ppk/v_edit_detail_pengadaan',$data);
    $this->load->view('ppk/v_footer');

  }

  function update_pengadaan()
  {
    $id_pengadaan = $this->input->post('id_pengadaan');
    $paket_name = $this->input->post('paket_name');
    $no_pengadaan = $this->input->post('no_pengadaan');
    $tanggal_surat = $this->input->post('tanggal_surat');
    $tanggal_permintaan = $this->input->post('tanggal_permintaan');
    $jam = $this->input->post('jam');
    $anggaran_detail = $this->input->post('anggaran_detail');
    $tanggal_dipa = $this->input->post('tanggal_dipa');
    $no_dipa = $this->input->post('no_dipa');
    $mak = $this->input->post('mak');
    $lokasi = $this->input->post('lokasi');
    $ta_anggaran = $this->input->post('ta_anggaran');

    $datanya_pengadaan = array(
      'id_pengadaan' => $id_pengadaan,
      'no_pengadaan' => $no_pengadaan,
      'paket_name' => $paket_name,
      'lokasi' => $lokasi,
      'status_pengadaan' => "Harga Barang",
      'tahun' => $ta_anggaran,
      'tanggal_surat' => $tanggal_surat,
      'tanggal_permintaan' =>$tanggal_permintaan,
      'tanggal_dipa' =>$tanggal_dipa,
      'jam' => $jam,
      'anggaran' => $anggaran_detail,
      'no_dipa' => $no_dipa,
      'mak' => $mak,
      );
    $this->m_pengadaan->update_pengadaan($datanya_pengadaan);

    // $this->load->view('ppk/v_header_ppk');
    $this->load->view('ppk/v_footer');
  }
  /*
  @author elfarqy
  digunakan untuk melakukan proses edit pada barang yang sudah di submit
   */
  function edit_barang()
  {
    $id_pengadaan = $this->input->post('id_pengadaan');
    $data['barang_sendiri'] = $this->m_barang->edit_barang($id_pengadaan);
    // $this->load->view('ppk/v_header_ppk');
    $this->load->view('ppk/v_edit_detail_barang',$data);
    $this->load->view('ppk/v_footer');
  }
  /*
   ** @author elfarqy
   ** digunakan untuk melakukan pengubahan pada form hps
   ** Rewrite by @author hapidznur
   **
   **
   */
  function submithps()
  {
      $id_user = $this->session->userdata('id_user');
//     echo $id_user;
      $data['count_data']=$this->m_hps->get_hps($id_user);
      var_dump($data);
      // for ($i=0; $i < count($data['count_data']) ; $i++) {
      //   $id_pengadaan = array_values($data['count_data'])[$i]['id_pengadaan'];
      //   echo "-- ".$id_pengadaan;
      //   $data['detail_header'] = $this->m_hps->detail_header_tabel_hps($id_pengadaan);
      //   $penawar_satu = array_values($data['detail_header'])[$i]['nama_penyedia'];
      //   var_dump($penawar_satu);
      // }
//    $id_pengadaan = array_values($data['count_data'])[0]['id_pengadaan'];
//    $data['detail_header'] = $this->m_hps->detail_header_tabel_hps($id_pengadaan);
//    $penawar_satu = array_values($data['detail_header'])[0]['nama_penyedia'];
//    $penawar_dua = array_values($data['detail_header'])[1]['nama_penyedia'];
//    $penawar_tiga = array_values($data['detail_header'])[2]['nama_penyedia'];
//    $data['penawar_terendah_satu'] = $this->m_hps->barang_dari_penawar_terendah_satu($penawar_satu);
//    $data['penawar_terendah_dua'] = $this->m_hps->barang_dari_penawar_terendah_satu($penawar_dua);
//    $data['penawar_terendah_tiga'] = $this->m_hps->barang_dari_penawar_terendah_satu($penawar_tiga);
//    $jumlah_total_tawar = array(array_column($data['penawar_terendah_satu'],'harga_tawar'),array_column($data['penawar_terendah_dua'],'harga_tawar'),array_column($data['penawar_terendah_tiga'],'harga_tawar'));
   //this detail lesson on stackoverflow : http://stackoverflow.com/questions/1496682/how-to-sum-values-of-the-array-of-the-same-key
//    $final = array();
//    array_walk_recursive($jumlah_total_tawar, function($item, $key) use (&$final){
//      $final[$key] = isset($final[$key]) ?  $item + $final[$key] : $item;
//    });
    // print_r();
//    $data['total_tawar'] = $final;
    // print_r($data);
    // $this->load->view('ppk/v_header_ppk');
    $this->load->view('ppk/v_submithps',$data);
    $this->load->view('ppk/v_footer');
  }
  /*
  @author elfarqy
  digunakan untuk melakukan akses pada list pengadaan detail sesuai dengan 3 penawar ter-rendah
   */
  function detail_hps()
  {
    $id_pengadaan = $this->input->post('id_pengadaan');
    $data =  $this->m_penawaran->get_penawaran($id_pengadaan);

     //  $data['user_detail_hps'] = $this->m_hps->get_user_detail_hps($id_pengadaan);

     //  $data['detail_barang_on_detail_hps'] = $this->m_hps->detail_barang_on_detail_hps($id_pengadaan);
     //  // var_dump($data['detail_barang_on_detail_hps']);
     //  $data['detail_header'] = $this->m_hps->detail_header_tabel_hps($id_pengadaan);
     //  // echo count($data['detail_header']);
     //  $penawar_satu = array_values($data['detail_header'])[0]['nama_penyedia'];
     //  $penawar_dua = array_values($data['detail_header'])[1]['nama_penyedia'];
     //  $penawar_tiga = array_values($data['detail_header'])[2]['nama_penyedia'];
     //  $data['penawar_terendah_satu'] = $this->m_hps->barang_dari_penawar_terendah_satu($penawar_satu);
     //  $data['penawar_terendah_dua'] = $this->m_hps->barang_dari_penawar_terendah_satu($penawar_dua);
     //  $data['penawar_terendah_tiga'] = $this->m_hps->barang_dari_penawar_terendah_satu($penawar_tiga);
     //  $jumlah_total_tawar = array(array_column($data['penawar_terendah_satu'],'harga_tawar'),array_column($data['penawar_terendah_dua'],'harga_tawar'),array_column($data['penawar_terendah_tiga'],'harga_tawar'));
     // //this detail lesson on stackoverflow : http://stackoverflow.com/questions/1496682/how-to-sum-values-of-the-array-of-the-same-key
     //  $final = array();
     //  array_walk_recursive($jumlah_total_tawar, function($item, $key) use (&$final){
     //    $final[$key] = isset($final[$key]) ?  $item + $final[$key] : $item;
     //  });
     //  $data['total_tawar'] = $final;

     //  if (count($data['detail_header']) < 1) {
     //    $this->load->view('ppk/v_detail_hps_404');
     //    $this->load->view('ppk/v_footer');
     //    // echo "hello";

     //  } else{
      $this->load->view('ppk/v_detail_hps');
      $this->load->view('ppk/v_footer');
    // }

    //currently still waiting an offer
    // $this->load->view('ppk/v_header_ppk');

  }
  /*
  @author elfarqy
  digunakan untuk melakukan akses pada form submit informasi harga barang
   */
  function submitharga()
  {
    //views
    $data['count_data']=$this->m_pengadaan->get_pengadaan_table();
    // $this->load->view('ppk/v_header_ppk');
    $this->load->view('ppk/v_submitharga');
    $this->load->view('ppk/v_footer');
  }

  /*
  @author elfarqy
  digunakan untuk melakukan aksi submit pada hps, setelah hps diketahui
   */
  function insert_hps()
  {
    $id_pengadaan = $this->input->post('id_pengadaan');
    $biaya_pajak = $this->input->post('biaya_pajak');
    $total_tawar = $this->input->post('total_tawar');
    $total_hps = $this->input->post('total_hps');

    echo $total_hps;


  }
  /*
  @author elfarqy
  digunakan untuk melakukan akses pada detail list pengadaan
   */
  function profil()
  {
    $id_user = $this->session->userdata('id_user');
    // print_r($id_user);
    $detail_profil ['detail_profil']= $this->m_profil->get_profile($id_user);
    // print_r($detail_profil);
    // $this->load->view('ppk/v_header_ppk');
    $this->load->view('ppk/v_profil', $detail_profil);
    $this->load->view('ppk/v_footer');
  }
  /*
  @author elfarqy
  digunakan untuk melakukan penulisan file berbentuk word.
   */
  function save_word()
  {
    $id_pengadaan = $this->input->post('id_pengadaan');
    $id_user = $this->session->userdata('id_user');
    $data['detail_pengadaan']=$this->m_pengadaan->get_pengadaan_params($id_pengadaan);
    $data['ppk_profil']=$this->m_profil->get_profile($id_user);
    $paket_name = $data['detail_pengadaan'][0]['paket_name'];
    $filename = "Surat_Informasi_Harga_".$paket_name.".doc";
    $path = "docs/".$paket_name;
    mkdir($path);
    // $this->load->view('ppk/v_header_ppk');
    $this->load->view('ppk/v_footer');
    #templates didapatkan dari xml word 2003, save as kemudian import jadi .php
    #source: https://klewos.wordpress.com/2010/04/16/using-php-to-fill-a-word-document-quick-tip/
    $print_docs = $this->load->view('ppk/templates_surat_informasi_harga', $data,TRUE);
    write_file("$path/$filename",$print_docs);
    // force_download($filename,file_get_contents("$path/$filename"));

  }


  /**
   * @author       :[elfarqy]
   * @function     :[ digunakan untuk melakukan import pada file berekstensi .doc]
   */

  function list_barang()
  {
    $id_pengadaan = $this->input->post('id_pengadaan');
    // echo $id_pengadaan;
    $id_user = $this->session->userdata('id_user');
    $data['detail_pengadaan']=$this->m_pengadaan->get_pengadaan_params($id_pengadaan);
    $data['ppk_profil']=$this->m_profil->get_profile($id_user);
    $data['detail_barang'] = $this->m_barang->get_barang_params($id_pengadaan);
    var_dump($data['detail_barang']);
    $paket_name = $data['detail_pengadaan'][0]['paket_name'];
    $filename = "Surat_Informasi_List_Barang_".$paket_name.".doc";
    $path = "docs/".$paket_name;
    mkdir($path);
    // $this->load->view('ppk/v_header_ppk');
    $this->load->view('ppk/template_list_barang',$data);
    $this->load->view('ppk/v_footer');
    $print_docs = $this->load->view('ppk/template_list_barang', $data,TRUE);
    write_file("$path/$filename",$print_docs);
    // redirect('ppk/save_word');
  }

  /**
   * @author       :[elfarqy]
   * @function     :[digunakan untuk melakukan pengisian nilai pada detail barang di menu hps]
   */


  function detail_barang_hps()
  {
   $id_pengadaan = $this->input->post('id_pengadaan');
   $id_user = $this->input->post('id_user');
   $data['detail_header'] = $this->m_hps->detail_header_tabel_hps($id_pengadaan);
   $penawar_satu = array_values($data['detail_header'])[0]['nama_penyedia'];
   $penawar_dua = array_values($data['detail_header'])[1]['nama_penyedia'];
   $penawar_tiga = array_values($data['detail_header'])[2]['nama_penyedia'];
   $data['penawar_terendah_satu'] = $this->m_hps->barang_dari_penawar_terendah_satu($penawar_satu);
   $data['penawar_terendah_dua'] = $this->m_hps->barang_dari_penawar_terendah_satu($penawar_dua);
   $data['penawar_terendah_tiga'] = $this->m_hps->barang_dari_penawar_terendah_satu($penawar_tiga);
   $jumlah_total_tawar = array(array_column($data['penawar_terendah_satu'],'harga_tawar'),array_column($data['penawar_terendah_dua'],'harga_tawar'),array_column($data['penawar_terendah_tiga'],'harga_tawar'));
   //this detail lesson on stackoverflow : http://stackoverflow.com/questions/1496682/how-to-sum-values-of-the-array-of-the-same-key
   $final = array();
   array_walk_recursive($jumlah_total_tawar, function($item, $key) use (&$final){
    $final[$key] = isset($final[$key]) ?  $item + $final[$key] : $item;
  });
   $data['total_tawar'] = $final;
   $data['user_detail_barang_hps'] = $this->m_hps->get_user_detail_barang_hps($id_pengadaan);
   $data['detail_barang_hps'] = $this->m_hps->detail_barang_hps($id_pengadaan,$id_user);
   // $this->load->view('ppk/v_header_ppk');
   $this->load->view('ppk/v_detail_barang_hps',$data);
   $this->load->view('ppk/v_footer');
 }
 /**
  * @author       :[elfarqy]
  * @function     :[digunakan untuk melakukan load header pada tiap laman setelah login]
  */

 function header_ppk()
 {
  if ($this->session->userdata('is_ppk') == TRUE) {
    $username = $this->session->userdata('username');
    $ppk_name = $this->m_ppk->get_id($username);
    $data['photo']= base_url().'tmp/profil.png';
    $data['name'] = array_values($this->m_ppk->get_id($username))[0]['ppk_name'];
    $data['badges'] = 100;
    $this->load->view('ppk/v_header',$data);
  }
}

/**
 * @author       :[elfarqy]
 * @function     :[digunakan untuk menghandle value dari pencarian di pengadaaan]
 */
function search()
{
  $kata_pencarian = $this->input->post('kata_pencarian');
  $hasil_cari['kata_cari'] = $kata_pencarian;
  $hasil_cari['data_cari'] = $this->m_pengadaan->search_pengadaan($kata_pencarian);
  // print_r($hasil_cari['data_cari']);

  $this->load->view('ppk/v_search', $hasil_cari);
  $this->load->view('ppk/v_footer');

}

function kalender_pengadaan()
{
  $id_pengadaan = $this->input->post('id_pengadaan');
  $data['timeline_pengadaan'] = $this->m_kalender->kalender_pengadaan_sendiri_ppk($id_pengadaan);
  $data['tahap_pengadaan'] = array(
      'Index 0',
      'Informasi Harga',
      'Harga Barang',
      'HPS dan Spesifikasi',
      'Undangan Pengadaan Langsung',
      'Surat Penawaran',
      'Pakta Integritas',
      'Formulir Isian Kualifikasi',
      'Evaluasi Penawaran dan Kualifikasi',
      'B.A Klarifikasi dan Negosiasi',
      'Surat Penetapan Penyedia',
      'Pengumuman Penyedia',
      'B.A Hasil Pengadaan Langsung',
      'Surat Perintah Kerja',
      'Surat Pesanan',
      'Surat Jalan',
      'B.A Penyelesaian Pekerjaan',
      'B.A Pemeriksaan Pekerjaan',
      'B.A Serah Terima Pekerjaan',
      'B.A Pembayaran',
      'Ringkasan Kontrak'
      );
  $this->load->view('ppk/v_kalender',$data);
  $this->load->view('ppk/v_footer');
}
/**
 * @author       :[elfarqy]
 * @function     :[digunakan untuk menampilkan dalam laman verifikasi informasi harga]
 * @Filename     :[ppk.php]
 */

function verifikasi_informasi_harga()
{
  $id_user = $this->session->userdata('id_user');
  $data['pengadaan_sendiri'] = $this->m_pengadaan->lihat_pengadaan_sendiri($id_user);
  $this->load->view('ppk/v_verifikasi_harga',$data);
  $this->load->view('ppk/v_footer');
}


/**
 * @author       :[elfarqy]
 * @function     :[digunakan untuk melakukan verifikasi penyedia yang sudah melakukan penawaran pada pengadaan]
 * @Filename     :[ppk.php]
 */
function verifikasi_penyedia_yang_menawar()
{
  $id_pengadaan = $this->input->post('id_pengadaan');
  $data['seluruh_penawaran'] = $this->m_pengadaan->data_verifikasi_penawaran_pengadaan_sendiri($id_pengadaan);
  // print_r($data['seluruh_penawaran']);
  // echo count($data['seluruh_penawaran']);
  if (count($data['seluruh_penawaran']) < 2){
    $this->load->view('ppk/v_verifikasi_404');
    $this->load->view('ppk/v_footer');
  } else {
    $this->load->view('ppk/v_verifikasi_penyedia',$data);
    $this->load->view('ppk/v_footer');
  }
  // print_r($data['seluruh_penawaran']);
  // echo array_sum($data['seluruh_penawaran']);

}

/**
 * @author       :[elfarqy]
 * @function     :[digunakan untuk melihat detail dari penawarran penyedia yang melakukan penawaran]
 * @Filename     :[ppk.php]
 */
function detail_tawaran_penyedia()
{
  $id_penawaran = $this->input->post('id_penawaran');
  $id_pengadaan = $this->input->post('id_pengadaan');
  $data['detail_penawaran_penyedia'] = $this->m_penawaran->detail_tawaran_penyedia($id_penawaran, $id_pengadaan);
  // print_r($data['detail_penawaran_penyedia']);

  $this->load->view('ppk/v_detail_tawaran_penyedia',$data);
  $this->load->view('ppk/v_footer');
}

function penawaran_terverifikasi()
{
  $id_penawaran = $this->input->post('id_penawaran');
  $this->m_verifikasi->update_verifikasi_informasi_harga($id_penawaran);
  $this->load->view('ppk/v_footer');
}
// function test()
// {
//   // var_dump($this->session->userdata('is_ppk'));
//  $config['base_url'] = base_url().'ppk/pengadaan';
//  $config['total_rows'] = count($this->m_pengadaan->jumlah_pengadaan());
//  $config['per_page'] = 5;
//  $data['artikel'] = $this->m_pengadaan->lihat_pengadaan($config['per_page']);
//  $this->pagination->initialize($config);
//  $data['halaman'] = $this->pagination->create_links();

//  // $this->load->view('ppk/v_header');
//  $this->load->view('ppk/v_pengadaan', $data);
//  $this->load->view('ppk/v_footer');
// }

}
