<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ulp extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->is_login();
    $this->load->database();
    $this->load->helper('url');
    $this->load->helper('array');
    $this->load->helper('download');
    $this->load->library('pagination');
    $this->load->model('m_barang');
    $this->load->model('m_dtl_penawaran');
    $this->load->model('m_kalender');
    $this->load->model('m_penjadwalan');
    $this->load->model('m_pengadaan');
    $this->load->model('m_penawaran');
    $this->load->model('m_proses');
    $this->load->model('m_status_proses');
    $this->load->model('m_ulp');
    $this->load->model('m_undangan');
    $this->load->model('m_verifikasi');
  }

  function index(){
    $title = 'ULP';
    $this->header_ulp($title);
    $this->load->view('ulp/v_ulp');
    $this->load->view('ulp/v_footer');
  }

  function pengadaan(){
    $title = 'ULP';
    $this->header_ulp($title);
    // $this->load->view('ulp/v_ulp');

    // $jumlah_pengadaan = $this->m_ulp->jumlah_pengadaan();

    // $config['base_url'] = base_url().'ulp/pengadaan';
    // $config['total_rows'] = $jumlah_pengadaan;
    // $config['per_page'] = 7;
    // $dari = $this->uri->segment('3');

    $data['artikel'] = $this->m_pengadaan->get_pengadaan_table();
    $data['proses'] = $this->m_proses->get_proses()->result_array();


    $this->load->view('pengadaan/v_list_pengadaan',$data);
    $this->load->view('ulp/v_footer');
  }

  private function get_notification(){
    $this->m_notification->notification();
    $count_data = $this->m_notification->notification('ULP');
        // $commet::_push_file();
  }

  private function is_login(){
    if ($this->session->userdata('is_ulp') == FALSE) {
        // echo "session not set";
        // var_dump($this->session->all_userdata());
      echo "<script>
      alert('Ndak berhak ngakses :P');history.go(-1);</script>";
      redirect('login/index');
    }
    else
    {
      // $session_penyedia = $this->session->userdata('is_penyedia');
      // return($session_penyedia);
    }
  }

  function header_ulp(){
    if ($this->session->userdata('is_ulp') == TRUE) {

      $user_id = $this->session->userdata('id_user');
      $data['photo']= base_url().'tmp/profil.png';
      $data['name'] = 'ulp';
            // $data['badges'] = $count_undangan;
      $this->load->view('ulp/v_header',$data);
    }
  }

    /**
    * List Pengadaan
    * @author  hapidznur
    */


    /**
    * List Pengadaan
    * @author  hapidznur
    */

    function getpengadaannonhps(){
      $time='20m15-06-11';
      $data['allpaket']= $this->m_ulp->getpengadaan();
    //      $id_p = $allpaket['id_pengadaan'];
      $data['hps']= $this->m_ulp->gethps('1');

      $this->load->view('ulp/v_lihatpengadaan',$data);
      $this->load->view('ulp/v_footer');
    }

    /**
    * Penjadwalan
    * @author  hapidznur
    */
    function penjadwalan(){
      $this->header_ulp();
      $data['kalender']=$this->m_ulp->get_ondate();
        // $data['kalender']=$this->m_status_proses->get_ondate();

      $this->load->view('ulp/v_penjadwalan',$data);
      $this->load->view('ulp/v_footer');
    }


    /**
    * Edit Penjadwalan Sementara
    * @author  hapidznur
    */
    function editpenjadwalan(){
      $this->header_ulp();
      $this->load->view('ulp/v_detailedjadwal');
      $this->load->view('ulp/v_footer');


      // $data['proses']=$proses;
      // $today=date("Y-m-d");


    }


    /**
    * Mengganti tanggal Pengaadaan
    * @author  hapidznur
    */
    public function updatekalender(){

      $proses = $this->input->post('proses');
      $tanggal = $this->input->post('tanggal');
      $id_jadwal = $this->input->post('userjadwal');

      $data = array($proses => $tanggal );

      $this->m_ulp->update_date($data,$id_jadwal);
        // $this->m_kalender->update_date($data,$id_jadwal);

    }

    /**
    * Verisikasi Penawaran
    * @author  hapidznur
    */
    function penawaran(){
    // $data['Verifikasi']=$this->m_verifikasi->get_verifikasi();Pengadaan
      $header = array('Satker','Penyedia','Surat Informasi Harga','Total Harga','List Rekanan');
      $uri = $this->uri->segment(2);
      if ($uri === "pengadaan") {
        $this->load->view('ulp/v_ulp');
        $this->load->view('ulp/v_footer');
      }
    }

    /**
    * Penawaran Penyedia
    * @author  hapidznur
    */
    function penyedia(){
      $id_pengadaan = $this->input->get('pengadaan');

      $data['penawaran'] = $this->m_ulp->get_penawaran($id_pengadaan);
        // $data['penawaran'] = $this->m_penawaran->get_penawaran($id_pengadaan);

      $this->load->view('ulp/v_penyedia',$data);
      $this->load->view('ulp/v_footer');
    }


    /**
    // Rule
    1. Verifikasi Harga dan Spesifikasi yang cocok oleh PPK
    2. Jika Penyedia Masih Kurang dari tiga kolom pengadaan berwarna berwarna Merah
    3. Jika Penyedia kurang dari 3 dan tanggal sudah H-1 Proses selanjutnya maka Kolom Pengadaan Berwarna merah dan button menuju penjadwalan ulang pengadaan1
    */

    function verifikasi(){
      $this->header_ulp();
      $data['header'] = array('Nama Paket','Rekanan', 'HPS dan Spesifikasi','HPS sementara');
      $rekanan = 3;
      if ($rekanan <= 3) {
        $data['td'] = 'danger';
        $data['rekanan'] = array(2,3,4,5,6);
        $this->load->view('ulp/v_verifikasi_pengadaan',$data);
      }
      $this->load->view('ulp/v_footer');
    }

    /**
    * [download_surat :  download surat penyedia]
    * @param  [int] $id_penyedia
    * @param  [int] $id_pengadaan
    * @return download
    * @author hapidznur <[<email address>]>
    */
    public function download_surat()
    {
      $fileName = $this->input->post('value');
      $file_path = $fileName;
      $this->_push_file($file_path, $fileName);
    }

    function _push_file($path, $name)
    {
        // $this->load->helper('download');
        // make sure it's a file before doing anything!
      if (is_file($path)) {
        // required for IE
        if (ini_get('zlib.output_compression')) {
          ini_set('zlib.output_compression', 'Off');
        }
            // get the file mime type using the file extension
        $this->load->helper('file');
        $mime = get_mime_by_extension($path);
            // Build the headers to push out the file properly.
            header('Pragma: public');     // required
            header('Expires: 0');         // no cache
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($path)) . ' GMT');
            header('Cache-Control: private', false);
            header('Content-Type: ' . $mime);  // Add the mime type from Code igniter.
            header('Content-Disposition: attachment; filename="' . basename($name) . '"');  // Add the file name
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: ' . filesize($path)); // provide file size
            header('Connection: close');
            readfile($path); // push it out
            exit();
            forcedownload($name, $path);
          }
        }

    /**
    * [pengumuman pengumuman penyedia]
    * @author hapidznur <[<email address>]>
    */
    function pengumuman(){

      $pengumuman = $this->m_ulp->get_pengadaan('Surat Penetapan Penyedia');
        // $pengumuman = $this->m_penawaran->get_pengadaan('Surat Penetapan Penyedia');

      if ($pengumuman->num_rows() > 0) {
        foreach ($pengumuman->result() as $key) {
          $id_pengadaan = $key->id_pengadaan;
        }
        $data['pengumuman'] = $pengumuman->result();

        $data['penyedia'] = $this->m_ulp->get_penyedia($id_pengadaan);
            // $data['penyedia'] = $this->m_penawaran->get_penyedia($id_pengadaan);

        $this->load->view('ulp/v_pengumuman',$data);

      }
      else
      {
        $data['notfound'] = "Tidak Ada Pengumuman Penyedia";
        $this->load->view('v_not_found',$data);
      }
      $this->load->view('ulp/v_footer');
    }

    function updatepengumuman(){
      $id_pengadaan = $_POST['pengadaan'];
      $id_penyedia = $_POST['list_penyedia'];
      $harga = $_POST['tawaran'];

        // $this->load->model('m_pengumuman');
      $data  = array('id_pengadaan' => $id_pengadaan,'id_penyedia' => $id_penyedia,'kesepakatan_harga' => $harga);

      $this->m_ulp->update_status($id_pengadaan);
      $this->m_ulp->ins_pengumuman($data);
        // $this->m_pengadaan->update_status($id_pengadaan);
        // $this->m_pengumuman->ins_pengumuman($data);

      redirect(base_url('ulp/pengumuman'));
    }

    /**
    * Kirim Undangan Penyedia
    * @author  hapidznur
    */
    function undangan()
    {
      $this->header_ulp();
      if ($this->input->get('pengadaan') == '') {
        $data['pengadaan'] = $this->m_ulp->get_undangan_from_penawaran(1)->result();
        $this->load->view('ulp/v_undangan',$data);
      }
      else
      {
        $id_pengadaan = $this->input->get('pengadaan');
        $get_detail_penawaran = $this->m_ulp->get_detail_penawaran($id_pengadaan);
        $get_detail_hps = $this->m_ulp->get_detail_penawaran($id_pengadaan);
        $this->load->view('ulp/v_detail_penawaran');
      }
      $this->load->view('ulp/v_footer');
    }

    function do_send(){

      $email = $this->input->post('email');
      $nama_penyedia = $this->input->post('nama_penyedia');
      $pemilik = $this->input->post('pemilik');
      $isi_mail = $this->input->post('isi_mail');

      $config = array(
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.googlemail.com',
        'smtp_port' => 465,
        'smtp_user' => 'alvianthelfarqy@gmail.com', // change it to yours
        'smtp_pass' => 'kmzway87aa:)', // change it to yours
      'mailtype' => 'html',
      'charset' => 'iso-8859-1',
      'wordwrap' => TRUE
      );

      $message = '';
      $this->load->library('email', $config);
      $this->email->set_newline("\r\n");
      $this->email->from('alvianthelfarqy@gmail.com'); // change it to yours
      $this->email->to($email);// change it to yours
      $this->email->subject('Resume from JobsBuddy for your Job posting');
      $this->email->message($isi_mail);
      if($this->email->send())
      {
        echo 'Email sent.';
      }
      else
      {
       show_error($this->email->print_debugger());
     }



      // $config = Array(
      //   'protocol' => 'smtp',
      //   'smtp_host' => 'ssl://smtp.googlemail.com',
      //   'smtp_port' => 465,
      //   'smtp_user' => 'alvianthelfarqy@gmail.com', // change it to yours
      //   'smtp_pass' => 'kmzway87aa:)', // change it to yours
      //   'mailtype' => 'html',
      //   'charset' => 'iso-8859-1',
      //   'wordwrap' => TRUE
      //   );
      // $this->load->library('email', $config);
      // $message = $isi_mail;
      //   $this->email->set_newline("\r\n");
      //   $this->email->from('alvianthelfarqy@gmail.com'); // change it to yours
      //   $this->email->to($email);
      //   $this->email->subject('Koreksi Pendaftaran Akses Ke Sistem Informasi Pengadaan');
      //   $this->email->message($message);
      //   if($this->email->send())
      //   {
      //     echo 'Email sent.';
      //   }
      //   else
      //   {
      //    show_error($this->email->print_debugger());
      //  }
   }
   function profil()
   {
     $this->load->view('ulp/v_header');
     $this->load->view('ulp/v_profil');
     $this->load->view('ulp/v_footer');
   }
   /**
    *@author elfarqy
    * method ini digunakan untuk melakukan percobaan pada transisi dari ulp alpha - ulp beta
    */
   function test(){
     $this->load->view('ulp/v_test');
   }

/**
 * @author       :[elfarqy]
 * @function     :[digunakan untuk melakukan list seluruh pengadaan dan menampilkan form download file ]
 * @Filename     :[ulp.php]
 */
public function download_pengadaan()
{
  $jumlah_pengadaan = $this->m_pengadaan->jumlah_pengadaan();
  $config['base_url'] = base_url().'ulp/pengadaan';
  $config['total_rows'] = $jumlah_pengadaan;
  $config['per_page'] = 7;
  $dari = $this->uri->segment('3');
  $data['list_pengadaan_download'] = $this->m_pengadaan->lihat_pengadaan($config['per_page'],$dari);
    // print_r($data['list_pengadaan_download']);
  $this->pagination->initialize($config);

  //$this->load->view('ulp/v_header');
  $this->load->view('ulp/v_download_all', $data);
  $this->load->view('ulp/v_footer');


}

   /**
    * @author       :[elfarqy]
    * @function     :[Digunakan untuk melakukan download file dalam bentuk terkompres]
    * @Filename     :[ulp.php]
    */
   function download_file()
   {
     $id_pengadaan = $this->input->post('id_pengadaan');
     $paket_name = $this->m_pengadaan->get_nama_paket($id_pengadaan);
     $path = 'docs/'.array_values($paket_name)[0]['paket_name'].'/*.doc';
     $this->zip->read_file($path);
     $this->zip->download("bak.zip");
   }
   /**
    * @author       :[elfarqy]
    * @function     :[digunakan untuk melakukan verifikasi terhadap penyedia yang baru melakukan pendaftaran dan nilai verifikasinya tidak]
    * @Filename     :[ulp.php]
    */
   function verifikasi_penyedia()
   {
    $data['penyedia_baru'] = $this->m_verifikasi->data_penyedia_baru();
    // print_r($data['penyedia_baru']);

    //$this->load->view('ulp/v_header');
    $this->load->view('ulp/v_verifikasi_penyedia',$data);
    $this->load->view('ulp/v_footer');
  }
  function penyedia_terverifikasi()
  {
    $id_penyedia = $this->input->post('id_penyedia');
    $this->m_verifikasi->update_penyedia_verifikasi($id_penyedia);
    // $check_data= $this->m_verifikasi->data_penyedia_baru();
    // print_r($check_data);
    //$this->load->view('ulp/v_header');
    $this->load->view('ulp/v_footer');

  }
  function detail_penyedia()
  {
    $id_penyedia = $this->input->post('id_penyedia');
    $data['detail_penyedia_baru'] = $this->m_verifikasi->data_detail_penyedia_baru($id_penyedia);
    // var_dump($data['detail_penyedia_baru']);
    // $this->m_verifikasi->update_penyedia_verifikasi($id_penyedia);
    // $check_data= $this->m_verifikasi->data_penyedia_baru();
    // print_r($check_data);
    //$this->load->view('ulp/v_header');
    $this->load->view('ulp/v_detail_penyedia',$data);
    $this->load->view('ulp/v_footer');

  }
  function tolak_penyedia()
  {
    $email = $this->input->post('email');
    $nama_penyedia = $this->input->post('nama_penyedia');
    $pemilik = $this->input->post('pemilik');
    $data['tertolak'] = array($email, $nama_penyedia, $pemilik);


    //$this->load->view('ulp/v_header');
    $this->load->view('ulp/v_tolak_penyedia',$data);
    $this->load->view('ulp/v_footer');

  }

  function add_ppk()
  {
    $uri = $this->uri->segment(2);
    switch ($uri) {
      case 'add_user':
      echo $uri
      ;                redirect(base_url('ulp'));
      break;

      default:
      echo $uri;
      $this->load->view('ppk/v_daftar');
      $this->load->view('v_footer');
      break;
    }
  }
   function edit_jadwal($id, $no){
      $tanggal = $this->m_penjadwalan->get_edit_penjadwalan($id, $no);
      // var_dump($list_kalender);
      foreach ($tanggal as $tanggal) {
        $data = array('mulai'=> $tanggal['p_'.$no.'_mulai'],
          'akhir'=> $tanggal['p_'.$no.'_akhir'],
          'proses'=> $no,
          'id' => $id);
      }

      echo json_encode($data);

    }

    function simpan_jadwal(){
      $id_proses = $this->input->post('proses');

      $data = array(
              'p_'.$id_proses.'_mulai' => $this->input->post('mulai'),

              'p_'.$id_proses.'_akhir' => $this->input->post('akhir'),
          );
      $this->m_penjadwalan->update_jadwal(array('id_jadwal' => $this->input->post('id')), $data);
      echo json_encode(array("status" => TRUE));
    }

    function list_detail_jadwal(){
      $id=$this->input->get('pengadaan');
      $list_kalender = $this->m_ulp->kalender($id);
      $proses  = $this->m_proses->get_proses()->result();
      $i = 1;
      $data = array();
      foreach ($proses as $proses) {
        foreach ($list_kalender as $kalender) {
          $row = array();
          $row[] =  $proses->id_proses;
          $row[] =  $proses->status;
          $row[] =  $kalender["p_".$i."_mulai"];
          $row[] =  $kalender["p_".$i."_akhir"];
          // add html for action
          $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" onclick="edit_jadwal('."'".$kalender['id_jadwal']."',"."'".$i."'".')" title="Edit"><i class="glyphicon glyphicon-pencil"></i> Edit</a>';
        }
        $data[] = $row;
        $i++;
      }
      // var_dump($data);
      $output = array(
        // "draw" => 1,
        "data" => $data,
      );
      echo json_encode($output);
    }

    function get_status(){

    }
}
/* End of file ulp.php */
/* Location: ./application/controllers/ulp.php */