<?php

/*
* This is generated file by aksamedia
*/

class Email extends CI_Controller {

	function __construct() {
		parent::__construct();
		# this is where your code is
	}

	public function tes()
	{
		$this->load->library('aksamailer');
		# kirim email
		$this->aksamailer->addAddress('hudasanca@gmail.com', 'Nurul Huda');     // Add a recipient
		$this->aksamailer->Body = 'tes';
		$this->aksamailer->Subject = "Teko local Opo jare " . \Carbon\Carbon::now();
		$this->aksamailer->send();
	}

	public function invoice()
	{
		$booking 		= BookingORM::desc()->first();
		$identitasWeb	= IdentitasWebORM::first();
		$invoice 		= $booking->invoice;	
		return blade()->nggambar('emails.invoice', compact('invoice', 'booking','identitasWeb'));
	}

	public function invoice_pending()
	{
		$booking 		= BookingORM::desc()->first();
		$identitasWeb	= IdentitasWebORM::first();
		return blade()->nggambar('emails.invoice-pending', compact('booking','identitasWeb'));
	}

	public function invoice_confirmed()
	{
		$invoiceConfirmation 	= InvoiceConfirmationORM::first();
		$identitasWeb			= IdentitasWebORM::first();
		return blade()->nggambar('emails.payment-confirmed', compact('invoiceConfirmation','identitasWeb'));
	}

	public function next_invoice_details()
	{
		$booking 				= BookingORM::desc()->first();
		$invoice 				= InvoiceORM::orderby('id', 'desc')->first();
		$identitasWeb			= IdentitasWebORM::first();
		$dp						= true;
		return blade()->nggambar('emails.next_invoice_details', compact('invoice','identitasWeb','booking','dp'));
	}

	public function auto()
	{
		$this->load->model('identitasWebModel');
 		blade()->sebarno('identitasWeb', $this->identitasWebModel->get());
 		
		$this->middleware->tutupOpenTripOtomatis();
		$this->middleware->hapusInvoiceYangTidakKonfirmasiOtomatis();

		$this->invoiceModel->kirimEmailReminderPembayaranHaMinSatuPelunasan();

		$date = \Carbon\Carbon::now();
		exec("echo {$date} email.auto >> ../email.auto");
	}
}
