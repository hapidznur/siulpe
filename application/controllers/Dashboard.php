<?php

/*
* This is generated file by aksamedia
*/

class Dashboard extends CI_Controller {

	function __construct() {
		parent::__construct();
		# this is where your code is
		$this->middleware->auth();
		$this->middleware->emailConfirmed();
	}

	function index() {
		blade()->nggambar('dashboard.index');
	}

}
