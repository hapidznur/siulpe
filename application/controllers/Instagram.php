<?php

/*
* This is generated file by aksamedia
*/

class Instagram extends CI_Controller {

	function __construct() {
		parent::__construct();
		# this is where your code is

		$this->client_id = getenv('INSTAGRAM_CLIENT_ID');
		$this->client_secret = getenv('INSTAGRAM_CLIENT_SECRET');
		$this->redirect_uri = getenv('INSTAGRAM_REDIRECT');
		$this->load->model('identitasWebModel');
	}

	function index() {
		echo 'welcome to Instagram';
	}

	public function login()
	{
		redirect("https://api.instagram.com/oauth/authorize/?client_id={$this->client_id}&redirect_uri={$this->redirect_uri}&response_type=code");
	}

	public function logout()
	{
		$this->identitasWebModel->logoutInstagram();
		return redirect("admin/gallery");
	}

	public function redirect()
	{
		if (!$this->input->get('code')) {
			redirect('instagram/login');
		}
		$curl = new \Curl\Curl();
		$curl->post("https://api.instagram.com/oauth/access_token", [
			'client_id' => $this->client_id,
			'client_secret' => $this->client_secret,
			'code' => $this->input->get('code'),
			'grant_type' => 'authorization_code',
			'redirect_uri' => $this->redirect_uri
		]);
		if (isset($curl->response->access_token)) {
			$this->load->model('identitasWebModel');
			$this->identitasWebModel->gantiInstagramAccessToken($curl->response->access_token);

			return redirect('admin/gallery');
		}

		return response_json($curl->response);
		// echo "Error. <a href='" . url('instagram/login') . "'>Coba login lagi</a>";
	}

	public function get_data_recent()
	{
		$access_token = "1436803793.f035258.4e2fe15b402844498bd6eacbec9ecfcb";
		$url = "https://api.instagram.com/v1/users/self/media/recent/?access_token={$this->access_token}";

	}

}
