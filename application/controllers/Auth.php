<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
* This is generated file by aksamedia
*/

class Auth extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('aksamailer');

		# this is where your code is
		blade()->sebarno('controller', $this);
	}

	function index() {
		redirect('auth/login');
	}

	public function login()
	{
		$this->middleware->guest(); # yang bisa akses hanya yang guest (bukan authenticated user)

		echo blade()->nggambar('auth.login');
	}

	public function end_point()
	{
		Hybrid_Endpoint::process();
	}

	public function signup()
	{
		// $this->middleware->guest();

		blade()->nggambar('signup');
	}

	public function register(){

		if ($this->input->is_ajax_request()){
			if (PenyediaORM::where('email', $this->input->post('email'))->count() > 0) {
				$count = PenyediaORM::where('email', $this->input->post('email'))->count();
		        echo 'false';
				// return response_json([
				// 	"email" => [
				// 		"email already taken ".$count 
				// 	]
				// ]);
			}
			else {
				echo "true";
			}
		}
		else
		{				
			$this->load->model('PenyediaModel');
			$this->PenyediaModel->create($this->input->post());
			redirect('auth/confirmation_email');

		}
	}

	public function validation()
	{
		$this->middleware->guest();
		$name = $this->input->post('name');
		$password = $this->input->post('password');
		$role = $this->input->post('role');

		if(auth()->attempt($name, $password, $role)){
			if ($role == 'penyedia') 
			{
				redirect('penyedia');
			}
			elseif ($role == 'ppk') 
			{
				redirect('ppk');
			}
			elseif ($role == 'ulp')
			{
				redirect('ulp');
			}
			else{

				$message = "email dan username tidak tersedia";
			 	$this->session->set_flashdata('error_message', $message);
				redirect('login');
			}
		}
		else {
			$message = "email dan username tidak tersedia";
		    $this->session->set_flashdata('error_message', $message);

			redirect('login');
		}
	}

	public function confirmation()
	{
		$email_token = $this->input->get('email_token');
		$penyedia = PenyediaORM::unconfirmed()->where('email_token', $email_token)->first();

		if (!$penyedia) {
			show_error(404);
		}

		# update the email_confirmed field to 1
		$penyedia->verifikasi = 1;
		$penyedia->save();

		# login by user model
		if (auth()->auth_user($penyedia)) {
			var_dump($this->session->all_userdata_());
			// redirect('dashboard');

		}

		response_json($user);
	}

	public function confirmation_email()
	{
		echo " fsdafjk";
		$this->middleware->email_unconfirmed();
		blade()->nggambar('auth.email-confirmation');
	}

	function ppk_register(){
		$this->load->model('UsersModel');
		$this->UsersModel->create($this->input->post());
		redirect('login');
	}

	public function logout()
	{
		$this->middleware->auth();

		auth()->logout();

		redirect('auth/');
	}

	public function forgot_password(){
		blade()->nggambar('auth.forgot-password');
	}

	public function send_email_forgot(){
		$this->middleware->guest();

		$email = $this->input->post('email');

		# check that the email should have been registered
		$user = PenyediaORM::where('email', $email)->first();

		if (!$user) {
			return response_json([
				'email' => [
					'email is not registered'
				]
			]);
		}

		$token = str_rand(100);
		# delete if there is another reset-password token on the database
		ResetPasswordORM::fromUser($user->id_penyedia)->delete();

		# create new reset password
		$resetPassword = new ResetPasswordORM;
		$resetPassword->id_penyedia = $user->id_penyedia;
		$resetPassword->password_token = $token;
		$resetPassword->save();

		# send the email confirmation to the user
		$this->aksamailer->addAddress($email);     // Add a recipient
		$this->aksamailer->Body = blade()->njupukHtml('emails.forgot-password', compact('token'));
		$this->aksamailer->Subject = "Reset Password";
		$this->aksamailer->send();

		blade()->nggambar('auth.forgot-password-sent');
	}

	public function reset_password()
	{
		$token = $this->input->get('token');
		# find the reset password token
		$resetPassword =  ResetPasswordORM::token($token)->first();
		if (!$resetPassword) {
			show_error(404);
		}

		blade()->nggambar('auth.reset-password');
	}

	public function attempt_password(){
		$validator = new \Valitron\Validator($this->input->post());
		$validator->rule('required', [
			'password', 'password_confirmation'
		]);
		$validator->rule('email', 'email');
		$validator->rule('lengthMin', 'password', 6);
		$validator->rule('equals', 'password', 'password_confirmation');

		if (!$validator->validate()) {
			return response_json($validator->errors());
		}

		$token = $this->input->post('password_token');

		# find the reset password token
		$resetPassword =  ResetPasswordORM::token($token)->first();
		if (!$resetPassword) {
			show_error(404);
		}

		# get the user of this token
		$user = $resetPassword->user;
		# change the password
		$user->password = password_hash($this->input->post('password'), PASSWORD_BCRYPT); # dont forget to encrypt it twice
		$user->save();
		# dont forget to remove the token
		$resetPassword->delete();

		blade()->nggambar('auth.reset-password-success');	}
}
