# Aksamedia Pondasi Sistem

Pondasi sistem ini berbasis codeigniter, dibangun dengan tujuan untuk memangkas waktu development dengan menulis reusable code untuk fitur-fitur umum. Agar tidak perlu menulis ulang ketika mengerjakan project yang baru.

Salah satu fitur-fitur umum yang ada di pondasi sistem ini adalah
  - Blade templating
  - Dotenv configuration
  - Generator
  - Eloquent ORM
  - Authentikasi
  - A ready to use user registration (sampai ke konfirmasi email dan forgot password)

> Karena dengan hanya satu kali tulis, dua tiga project terlampaui :)

### Version
0.0.1 Development

### Installation
NB: Tutorial ini hanya untuk linux user

Pastikan terinstall git.

```sh
$ git clone [git-repo-url] aksa-system
$ cd aksa-system
$ composer update
$ cp .env.example .env
```

Lalu buka file .env, dan isi konfigurasinya sesuai dengan environment anda.



### AksaGenerator

AksaGenerator is used to generate some common files such as controller, model, and migration (migration is not supported yet). For most of linux users, its so much fun rather than create those file manually.

Open the documentation with the following command
```sh
$ php aksa
```

To generate new controller, type the following command
```sh
$ php aksa gae:controller your-controller-name
```


To generate new model, type the following command
```sh
$ php aksa gae:model Your_Model
```

License
----

MIT


**Free Software, Hell Yeah!**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [@thomasfuchs]: <http://twitter.com/thomasfuchs>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [marked]: <https://github.com/chjj/marked>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [keymaster.js]: <https://github.com/madrobby/keymaster>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]:  <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
